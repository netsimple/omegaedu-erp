<?php

class RequestController extends Controller {

    public function actionIndex() {
		if(isset($_SERVER['HTTP_ORIGIN'])){
			$http_origin = $_SERVER['HTTP_ORIGIN'];
			$model = UserPartner::model()->countByAttributes(array('web_id' => str_replace('http://', '', $http_origin)));
			if ($model)
			{
				header("Access-Control-Allow-Origin: $http_origin");
			}
		}

        if (!$_POST) $this->redirect(array('client/index'));
        $webId = Yii::app()->request->getPost('referrer','');
        if ($webId) {
            $model = UserPartner::model()->findByAttributes(array('web_id' => $webId));
            $projectId = $model->user ? $model->user->projectId : 0;
        } else {
            $projectId = 0;
        }
        $fields = array(
            'email' => array(
                'innerName' => 'email',
                'required' => false,
            ),
            'name' => array(
                'innerName' => 'fullname',
                'required' => true,
            ),
            'phone' => array(
                'innerName' => 'phone_num',
                'required' => false,
            ),
            'school' => array(
                'innerName' => 'school',
                'required' => false,
            ),
            'page' => array(
                'innerName' => 'social_vk',
                'required' => false,
            ),
            'colors' => array(
                'innerName' => 'other',
                'required' => false,
            ),
        );
        $model = new ClientRequest;
        foreach ($_POST as $currKey => $currElem) {
            if (!isset($fields[$currKey])) continue;
            if (!$currElem && $fields[$currKey]['required']) throw new CHttpException(400);
            if ($currKey == 'colors') {
                array_shift($currElem);
                $currElem = implode(', ',$currElem);
            }
            $newKey = $fields[$currKey]['innerName'];
            $model->$newKey = $currElem;
        }
        $model->project_id = $projectId;
        $model->save();
        Yii::app()->end();
    }

    public function actionTest() {
        $this->render('/client/_form_request');
    }

}

?>