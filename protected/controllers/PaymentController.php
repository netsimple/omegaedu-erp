<?php

class PaymentController extends Controller {

	public $layout='//layouts/column2';
	public $modelName = 'PaymentTarif';

	public function actions() {
		return array(
			'ajaxUpdate' => array(
				'class' => 'application.controllers.actions.AjaxUpdateAction',
				'accessKey' => 'payment/update',
			),
		);
	}

	public function actionDelete($id) {
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('trcenter/delete')) throw new CHttpException(403);
		$model = $this->loadModel($id);
		$model->delete();
		if (Yii::app()->request->isAjaxRequest) {
			$this->logAction();
		} else {
			$this->redirect(array('index'));
		}
		Yii::app()->end();
	}

	public function actionUpdate($id) {
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('trcenter/update')) throw new CHttpException(403);
		$this->layout = '//layouts/column1';
		$model = TrCenter::model()->findByPk($id);
		if(!$model) throw new CHttpException(404);

		$payments = PaymentTarif::model()->findAll(array('condition'=>'tr_center_id='.$model->id));
		$payment = new PaymentTarif();

		if (isset($_POST['PaymentTarif'])) {
			$payment->attributes = $_POST['PaymentTarif'];
			if ($payment->save()) {
				$this->redirect(array('update','id'=>$model->id));
			}
		}

		$payment_search = new PaymentTarif('search');
		$payment_search->unsetAttributes();
		if (isset($_GET['PaymentTarif'])) $payment_search->attributes = $_GET['PaymentTarif'];
		$payment_search->tr_center_id=$model->id;

		$this->render('update', array('model' => $model, 'payments' => $payments, 'payment' => $payment, 'payment_search'=>$payment_search));
	}

}

?>