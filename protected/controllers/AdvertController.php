<?php

class AdvertController extends Controller {
    
	public $modelName = 'Advert';
	public $layout='//layouts/column1';
    
    public function filters() {
        return array(
            'ajaxOnly + deleteDocument',  
        );
    }
	
	public function actions() {
		return array(
			'ajaxUpdate' => array(
				'class' => 'application.controllers.actions.AjaxUpdateAction',
				'accessKey' => 'advert/update',
			),
		);
	}
    
    public function actionIndex() {
        if (!Yii::app()->user->checkAccess('advert/index')) throw new CHttpException(403);
        $model = new Advert('search');
        $model->unsetAttributes();
        if (isset($_GET['Advert'])) {
            $model->attributes = $_GET['Advert'];
        }
        $this->render('index', array('model' => $model));
    }
    
    public function actionUpdate($id) {
		$id = (int)$id;
        if (!Yii::app()->user->checkAccess('advert/update')) throw new CHttpException(403);
        $model = $this->loadModel($id);
        if (isset($_POST['Advert'])) {
            $model->attributes = $_POST['Advert'];
            if ($model->save()) {
                for ($index = 0; $currentDoc = CUploadedFile::getInstance($model,"[$index]documents"); $index++) {
                    $document = new AdvertDocument;
                    $document->upload($currentDoc,$model->id);
                }
                $this->redirect(array('view','id' => $model->id));
            }
        }
        $model->formatDates();
        $this->render('update', array('model' => $model));
    }
    
    public function actionView($id) {
		$id = (int)$id;
        if (!Yii::app()->user->checkAccess('advert/view')) throw new CHttpException(403);
        $model = $this->loadModel($id);
        $this->render('view', array('model' => $model));
    }
    
    public function actionCreate() {
        if (!Yii::app()->user->checkAccess('advert/create')) throw new CHttpException(403);
        $model = new Advert;
        if (isset($_POST['Advert'])) {
            $model->attributes = $_POST['Advert'];
            if ($model->save()) {
                for ($index = 0; $currentDoc = CUploadedFile::getInstance($model,"[$index]documents"); $index++) {
                    $document = new AdvertDocument;
                    $document->upload($currentDoc,$model->id);
                }
				$this->storeModel($model);
                $this->redirect(array('view','id' => $model->id));
            }
        }
        $this->render('create', array('model' => $model));
    }
    
    public function actionDeleteDocument($id) {
        if (!Yii::app()->user->checkAccess('advert/update')) Yii::app()->end(403);
        if (!($model = AdvertDocument::model()->findByPk($id))) Yii::app()->end(404);
        if ($model->unlink()) {
            Yii::app()->end(200);
        } else {
            Yii::app()->end(204);
        }
    }
    
    public function actionDelete($id) {
		$id = (int)$id;
        $model = $this->loadModel($id);
        $model->delete();
    }

}

?>