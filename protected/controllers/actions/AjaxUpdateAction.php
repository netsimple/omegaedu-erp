<?php

class AjaxUpdateAction extends CAction {
    
    public $modelName;
    public $accessKey;
    public $scenario;

    public function run() {
        if (empty($this->modelName))
            $this->modelName = $this->controller->modelName;

        $request = Yii::app()->request;
		if (!$request->isAjaxRequest || !($paramName = $request->getPost('name')) || !($paramId = $request->getPost('pk')))
            throw new CHttpException(400);

        $paramValue = $request->getPost('value');
        if (!Yii::app()->user->checkAccess($this->accessKey))
            throw new CHttpException(403);

        if (!($model = CActiveRecord::model($this->modelName)->findByPk($paramId)))
            throw new CHttpException(404);

        $model->scenario = !empty($this->scenario) ? $this->scenario : 'ajaxUpdate';

        if ($model->hasAttribute($paramName)) {
            $model->$paramName = $paramValue;
        } else {
            if (method_exists($model,$methodName = 'set'.ucfirst($paramName)))
                call_user_func_array(array($model,$methodName),array($paramValue));
        }

        if ($model->validate(array($paramName)) && $model->save(false,$model->hasAttribute($paramName) ? array($paramName) : null)) {
            ActivityLog::add($model,'update',array('attributeName' => $paramName));
			echo CJSON::encode(array('errors' => null));
		} else {
			echo CJSON::encode(array('errors' => array_map(function($v){return implode(',',$v);},$model->errors)));
		}
		Yii::app()->end(200);
    }

}

?>