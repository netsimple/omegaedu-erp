<?php

class ScheduleController extends Controller {

	public $layout='//layouts/column2';
	public $modelName = 'Schedule';
	
	public function actions() {
		return array(
			'ajaxUpdate' => array(
				'class' => 'application.controllers.actions.AjaxUpdateAction',
				'accessKey' => 'schedule/update',
			),
			'ajaxUpdateClients' => array(
				'class' => 'application.controllers.actions.AjaxUpdateAction',
				'modelName' => 'TrClientLog',
				'accessKey' => 'schedule/update#clientlist',
			),
		);
	}
	
	protected function getSchedule($id=false) {
		$schedule = new Schedule();
		$schedule->project_id = Yii::app()->user->projectId;
		$schedule->tr_center_id = $id;
		return $schedule;
	}
	
	public function actionIndex() {

        if (isset($_GET['frame'])) {
            $this->layout = 'blank';
            
        } else {
            if (!Yii::app()->user->checkAccess('schedule/index')) throw new CHttpException(403);
            $this->layout = '//layouts/column1';
        }

        $model = new Schedule('search');
        $model->unsetAttributes();
        if (isset($_GET['Schedule']) || isset($_GET[0]['Schedule']))
            $model->attributes = isset($_GET['Schedule']) ? $_GET['Schedule'] : $_GET[0]['Schedule'];
        else
            $model->attributes = array(
                'datefrom'=>str_replace('.','',Yii::app()->dateFormatter->format('d MMM yyyy', strtotime('last Monday'))),
                'dateto'=>str_replace('.','',Yii::app()->dateFormatter->format('d MMM yyyy', strtotime('next Sunday'))),
            );

        $datefrom = CDateTimeParser::parse($model->datefrom,'d MMM yyyy');
        $dateto = CDateTimeParser::parse($model->dateto,'d MMM yyyy') + 60*60*24;

        $numberDays = ceil(($dateto - $datefrom) / (60*60*24));
        if ($numberDays > 32) {
            $dateto = $datefrom + 31*60*60*24;
            $model->attributes = array(
                'dateto'=>str_replace('.','',Yii::app()->dateFormatter->format('d MMM yyyy', $dateto)),
            );

            Yii::app()->user->setFlash('schedule', 'Слишком большой период. Показано расписание за месяц.');
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }
	
	public function actionAjaxTable() {
		$request = Yii::app()->request;
		if (!$request->isAjaxRequest || !$request->isPostRequest || ($id = $request->getPost('id',0)) < 0) throw new CHttpException(400);
		if (!Yii::app()->user->checkAccess('schedule/index')) throw new CHttpException(403);
		$this->renderPartial('table', array('model' => $this->getSchedule($id), 'mode' => 'admin'), false, true);
		Yii::app()->end();
	}
	
	public function actionUpdate($id) {
        $this->layout = '//layouts/column1';
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('schedule/update')) throw new CHttpException(403);

        $model = $this->loadModel($id);
		$model->scenario = 'update';
        $allModels = Schedule::model()->findAll(array(
            'condition' => "t.user_id = {$model->user_id} AND t.create_date = {$model->create_date}",
            'order'=>'datetime_start ASC'
        ));

        $day = array();
        foreach ($allModels as $item) {
            $day[$item->day] = $item->day;
            if ($item->datetime_start <= time()) continue;
            $datefrom = isset($datefrom) ? $datefrom : str_replace('.','',Yii::app()->dateFormatter->format('d MMM yyyy',$item->datetime_start));
        }
        $dateto = str_replace('.','',Yii::app()->dateFormatter->format('d MMM yyyy',$item->datetime_start));

        $model->datefrom = $datefrom;
        $model->dateto = $dateto;
        $model->day = array_keys($day);

		if (isset($_POST['Schedule'])) {
			$model->attributes = $_POST['Schedule'];

            if($model->datefrom && $model->dateto && is_array($_POST['Schedule']['day'])) {

                foreach ($allModels as $item)
                    if ($item->datetime_start >= time())
                        $item->delete();

                if ($model->addFromCalendar($_POST['Schedule'])) {
                    Yii::app()->user->setFlash('success','Расписание обновлено');
                    $this->redirect(array('schedule/index', $_POST));
                } else {
                    foreach ($allModels as $item) {
                        $item->is_deleted = 0;
                        $item->deleted_by = 0;
                        $item->save();
                    }
                }
            }



		}
		
		$this->render('update', array('model' => $model));
	}
	
	public function actionCreate() {
        $this->layout = '//layouts/column1';
		if (!Yii::app()->user->checkAccess('schedule/create') || !($userModel = Yii::app()->user->model)) throw new CHttpException(403);
		$model = $this->getSchedule();
		$model->scenario = 'create';
		if (isset($_POST['Schedule'])) {
			$model->attributes = $_POST['Schedule'];

            if($model->datefrom && $model->dateto && is_array($_POST['Schedule']['day'])) {
                if ($model->addFromCalendar($_POST['Schedule'])) {
                    Yii::app()->user->setFlash('success','Расписание добавлено');
                    $this->redirect(array('schedule/index', $_POST));
                }
            } else {
                if ($model->save()) {
                    if ($model->hasOverlap($model)) {
                        $model->delete();
                    } else {
                        Yii::app()->user->setFlash('success','Расписание добавлено');
                        $this->redirect(array('schedule/index', $_POST));
                    }

                }
            }

		} else {
            $model->day = array_keys($model->dayList);
        }
		$this->render('create', array('model' => $model));
		
	}
	
	public function actionDelete($id) {
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('schedule/delete')) throw new CHttpException(403);
		$model = $this->loadModel($id);
		//if ($model->datetime_start <= time()) $this->redirect(array('index'));

        if (Yii::app()->request->getParam('andRelated') !== null) {
            $all = $model->findAllByAttributes(array('user_id' => $model->user_id, 'create_date' => $model->create_date));
            foreach ($all as $item) {
                if ($item->datetime_start >= time())
                    $item->delete();
            }
        } else {
            $model->delete();
        }

		$this->redirect(array('index'));
	}

    public function actionGetGroups() {
        $center_id = $_GET['Schedule']['tr_center_id'];
        $multi = is_array($center_id);

        $data = TrGroup::getByCenterId($center_id);

        $suggest="";
        foreach ($data as $key=>$model)
            $suggest .= $multi ?
                "<input id='Schedule_tr_group_id_{$key}' value='{$model->id}' type='checkbox' name='Schedule[tr_group_id][]'>
                 <label for='Schedule_tr_group_id_{$key}'>{$model->name}</label>
                 <br>" :
                "<option value='{$model->id}'>{$model->name}</option>";

        echo $suggest;
    }

    public function actionGetCourse() {

        $data = TrCourse::findAllByCenter($_GET['Schedule']['tr_center_id']);

        $suggest="";
        foreach ($data as $key=>$model)
            $suggest .= is_array($_GET['Schedule']['tr_center_id']) ?
                "<input id='Schedule_tr_course_id_{$key}' value='{$model->id}' type='checkbox' name='Schedule[tr_course_id][]'>
                 <label for='Schedule_tr_course_id_{$key}'>{$model->name}</label>
                 <br>" :
                "<option value='{$model->id}'>{$model->name}</option>";

        
        echo $suggest;
    }
	
	public function actionView($id) {
		$this->layout = '//layouts/column1';
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('schedule/view')) throw new CHttpException(403);
		$model = $this->loadModel($id);
		$model->setUpdateScenario();

        if (($client_id = Yii::app()->request->getQuery('client_id'))) {
            if (!TrClientLog::model()->exists("client_id = {$client_id} AND tr_lesson_id = {$model->id}")) {
                $client = new TrClientLog();
                $client->tr_group_id = $model->tr_group_id;
                $client->tr_lesson_id = $model->id;
                $client->client_id = $client_id;
                $client->save();

                Client::addToGroup($client_id, $model->tr_group_id);
            }
            $this->redirect(array('view', 'id'=>$id));
        }

		$clientLog = new TrClientLog('search');
		$clientLog->tr_lesson_id = $model->id;
		$this->render('view_lesson', array('model' => $model, 'clientLog' => $clientLog));
	}

    public function actionGetClientName($term, $project_id) {
        if (!Yii::app()->request->isAjaxRequest || (!Yii::app()->user->checkAccess('schedule/view'))) Yii::app()->end();
        echo Client::model()->getJSONNames($term, $project_id);
        Yii::app()->end();
    }
	
	public function actionList($mode='') {
		$this->layout = '//layouts/column1';
		if (!Yii::app()->user->checkAccess('schedule/list')) throw new CHttpException(403);
		if ($mode == 'future' || $mode == 'past') {
			$model = new Schedule('list');
			$model->unsetAttributes();
			if (isset($_GET['Schedule'])) $model->attributes = $_GET['Schedule'];
			$renderView = '_list_'.$mode;
			if (Yii::app()->request->isAjaxRequest) {
				$this->renderPartial($renderView, array('model' => $model),false,true);
				Yii::app()->end();
			} else {
				$this->layout = '//layouts/column1';
				$this->render($renderView, array('model' => $model));
			}
		} else {
			$this->render('list');
		}
	}
	
	public function actionTest() {
		header('Content-Type: text/html; charset = utf-8');
		if ($_POST) {
			$timestamp = CDateTimeParser::parse($_POST['date'],'yyyy-MM-dd HH:mm',array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0));
			echo date('d m Y, H:i',$timestamp);
		} else {
			echo '<form method="post"><input type="text" name="date"><input type="submit"></form>';
		}
	}
	
}

?>
