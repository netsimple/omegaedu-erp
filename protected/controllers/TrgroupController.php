<?php

class TrgroupController extends Controller {
	
	public $modelName = 'TrGroup';
	public $layout='//layouts/column2';
	
	public function actions() {
		return array(
			'ajaxUpdate' => array(
				'class' => 'application.controllers.actions.AjaxUpdateAction',
				'accessKey' => 'trgroup/update',
			),
            'ajaxUpdateRate' => array(
                'class' => 'application.controllers.actions.AjaxUpdateAction',
                'modelName' => 'CourseRates',
                'accessKey' => 'user/rate',
            ),
		);
	}
	
	private function addClient($clientId) {
		$source = Yii::app()->session->itemAt('clientStore');
		if (!$source) $source = array();
		$source[] = $clientId;
		Yii::app()->session['clientStore'] = $source;
	}
	
	public function actionAddclient($clientId) {
		$this->addClient((int)$clientId);
		Yii::app()->end();
	}
	
	public function actionRemoveclient($clientId) {
		$clientId = (int)$clientId;
		if ($source = Yii::app()->session->itemAt('clientStore')) {
			if ($innerKey = array_search($clientId,$source)) {
				unset($source[$innerKey]);
				Yii::app()->session['clientStore'] = $source;
			}
		}
		Yii::app()->end();
	}
	
	public function actionGetclients() {
		$criteria = new CDbCriteria;
		$criteria->select = 'id, fullname';
		$result = array();
		if ($arr = Client::model()->findAll($criteria)) {
			foreach ($arr as $currObj) {
				$result[] = array(
					'id' => $currObj->id,
					'fullname' => $currObj->fullname
				);
			}
		}
	}
	
	public function actionGetcourses($id=null) {
		if (!Yii::app()->user->checkAccess('trcourse/index')) throw new CHttpException(403);
		$criteria = new CDbCriteria;
		$criteria->select = 'id, name';
		$criteria->compare('project_id',Yii::app()->user->projectIdCompare);
		$arr = TrCourse::model()->findAll($criteria);
		$result = array();
		if ($arr) {
			foreach ($arr as $currObj) {
				$result[] = array(
					'id' => $currObj->id,
					'name' => $currObj->name
				);
			}
		}
		echo CJSON::encode($result);
		Yii::app()->end();
	}
	
	public function actionIndex() {
		if (!Yii::app()->user->checkAccess('trgroup/index')) throw new CHttpException(403);
		$this->layout = '//layouts/column1';
		$model = new TrGroup('search');
		$model->unsetAttributes();
		if (isset($_GET['TrGroup'])) $model->attributes = $_GET['TrGroup'];
		$this->render('index', array('model' => $model));
	}
	
	public function loadClient() {
		$result = new Client('search');
		$result->unsetAttributes();
		if (isset($_GET['Client'])) {
			$result->attributes = $_GET['Client'];
		}
		$result->project_id = Yii::app()->user->projectId;
		return $result;
	}
	
	public function actionView($id,$tab='') {
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('trgroup/view')) throw new CHttpException(403);
		$this->layout = '//layouts/column1';
		$model = $this->loadModel($id);
		
		$schedule = new Schedule('search');
		$schedule->unsetAttributes();
		$schedule->attributes = array(
			'tr_group_id'	=> $model->id,
		);
		
		$modelCosts = new TrCosts('group');
		$modelCosts->center_id = $model->tr_center_id;
		$modelCosts->group_id = $model->id;
		$this->render('view', array('tab' => $tab, 'model' => $model, 'schedule' => $schedule, 'modelCosts' => $modelCosts));
	}
	
	public function actionUpdate($id) {
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('trgroup/update')) throw new CHttpException(403);
		
		$model = $this->loadModel($id);
		
		if (isset($_POST['TrGroup'])) {
			$model->attributes = $_POST['TrGroup'];
			if ($model->save()) {
				$this->storeClients($model);
				$this->storeSchedule($model);
				$this->redirect(array('trgroup/view','id'=>$model->id));
			}
		} else {
			$this->clearClients();
			foreach ($model->members as $currentClient) {
				$this->addClient($currentClient->id);
			}
		}




        $schedule = new Schedule('search');
        $schedule->unsetAttributes();
        $schedule->attributes = array(
            'datefrom'=>str_replace('.','',Yii::app()->dateFormatter->format('d MMM yyyy', strtotime('last Monday'))),
            'dateto'=>str_replace('.','',Yii::app()->dateFormatter->format('d MMM yyyy', strtotime('next Sunday'))),
            'tr_center_id' => $model->tr_center_id
        );

		
		$this->render('update', array(
			'model' => $model,
			'client' => $this->loadClient(),
			'schedules' => $schedule->search(),
		));
	}
	
	public function getClients() {
		return Yii::app()->session->itemAt('clientStore') && is_array(Yii::app()->session->itemAt('clientStore')) ? Yii::app()->session->itemAt('clientStore') : array();
	}
	
	private function clearClients() {
		Yii::app()->session['clientStore'] = null;
	}
	
	private function storeClients($model) {
		
		if (!isset($_POST['Client']) || !isset($_POST['Client']['id']) || !is_array($_POST['Client']['id'])) return false;
		$clients = array_unique($_POST['Client']['id']);
		
		$model->storeRelations('member',$clients);
		
		$this->clearClients();
		
	}
	
	private function storeSchedule($model) {
		
		if (!isset($_POST['Schedule']) || !isset($_POST['Schedule']['id']) || !is_array($_POST['Schedule']['id'])) return false;
		$schedule = array_unique($_POST['Schedule']['id']);
		
		$model->storeRelations('schedule',$schedule);
		
	}
	
	protected function storeModel($model) {
		$this->storeClients($model);
		$this->storeSchedule($model);
		parent::storeModel($model);
	}
	
	public function actionCreate() {
		
		if (!Yii::app()->user->checkAccess('trgroup/create')) throw new CHttpException(403);
		
		// установить права доступа #pdovlatov
	
		$model = new TrGroup;
		
		if (isset($_POST['TrGroup'])) {
			$model->attributes = $_POST['TrGroup'];
			$model->project_id = Yii::app()->user->projectId;
			
			if ($model->save()) {
				$this->storeModel($model);
				$this->redirect(array('view','id'=>$model->id));
			}
		} else {
			$this->clearClients();
		}
		
		$this->render('create', array(
			'model' => $model,
			'client' => $this->loadClient(),
			'schedule' => null,
		));
		
	}
	
	public function actionDelete($id) {
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('trgroup/delete')) throw new CHttpException(403);
		$model = $this->loadModel($id);
		$model->delete();
		$this->redirect(array('index'));
	}
	
	public function actionAjaxFinances($id) {
		$id = (int)$id;
		if (!Yii::app()->request->isAjaxRequest) throw new CHttpException(400);
		if (!isset($_POST['datefrom']) || !isset($_POST['dateto'])) throw new CHttpException(400);
		if (!Yii::app()->user->checkAccess('trgroup/update')) throw new CHttpException(403);
		$model = $this->loadModel($id);
		
		$dateFilter = new DateFilterForm;
		$dateFilter->datefrom = $_POST['datefrom'];
		$dateFilter->dateto = $_POST['dateto'];
		if (!$dateFilter->validate()) Yii::app()->end(); // здесь - вернуть ошибку с её текстом
		
		if (isset($_POST['ids'])) {
			$idSelector = array();
			foreach ($_POST['ids'] as $currentId) array_push($idSelector,(int)$currentId);
		} else {
			$idSelector = null;
		}
		
		$model->setDatetimeLimit($dateFilter,null,true);
		$income = $model->getFinanceIncomeData($idSelector)->toArray();
		$costs = array('costs_table' => '');
		$modelCosts = new TrCosts('group');
		$modelCosts->center_id = $model->tr_center_id;
		$modelCosts->group_id = $model->id;
		$modelCosts->setDatetimeLimit($dateFilter,null,true);
		$costs['costs_table'] = $this->renderPartial('_view_finance_summarycosts',array('modelCosts' => $modelCosts),true);
		
		echo CJSON::encode(array_merge($income,$costs,array('selector' => $this->renderPartial('_view_finance_selector',array('model' => $model),true))));
		Yii::app()->end();

	}
	
	protected function checkAjaxPostRequest() {
		if (!Yii::app()->request->isAjaxRequest) throw new CHttpException(400);
		if (!$_POST || !isset($_POST['pk']) || !isset($_POST['name']) || !isset($_POST['value'])) throw new CHttpException(400);
	}
	
	public function actionAjaxGetCourses($centerId) {
		if (!Yii::app()->user->checkAccess('trgroup/update')) throw new CHttpException(403);
		if (!Yii::app()->request->isAjaxRequest) throw new CHttpException(400);
		if (($centerId = (int)$centerId) <= 0) throw new CHttpException(400);
		$center = TrCenter::model()->findByPk($centerId);
		if (!$center) throw new CHttpException(404);
		echo CJSON::encode($center->getTrCourseList());
		Yii::app()->end();
	}
	
	public function actionAjaxCostsAdd($id) {
		$id = (int)$id;
		if (!Yii::app()->request->isAjaxRequest || !Yii::app()->request->isPostRequest) throw new CHttpException(400);
		if (!Yii::app()->user->checkAccess('trgroup/finance')) throw new CHttpException(403);
		$group = $this->loadModel($id);

		$model = new TrCosts('group');
		$model->attributes = $_POST;
		$model->center_id = $group->tr_center_id;
		$model->group_id = $id;
		if($model->save()) {
			$model->scenario = 'group';
			$this->logAction($model,'addCosts');
			echo CJSON::encode(array('costs' => $model->getSummaryCostsValue(), 'id'=>$model->id, 'type'=>$model->type));
		} else {
			$errors = array_map(function($v){ return join(', ', $v); }, $model->getErrors());
			echo CJSON::encode(array('errors' => $errors));
		}
		Yii::app()->end();
	}

    public function actionAjaxCostsDel($id) {
        $id = (int)$id;
        if (!Yii::app()->request->isAjaxRequest || !Yii::app()->request->isPostRequest) throw new CHttpException(400);
        if (!Yii::app()->user->checkAccess('trgroup/finance')) throw new CHttpException(403);
        if (!($model = TrCosts::model()->findByPk($id))) throw new CHttpException(404);
        if ($model->user_id != Yii::app()->user->id) throw new CHttpException(403);

        if ($model) {
            if ($model->delete()) {
				ActivityLog::add($model,'delete');
                echo CJSON::encode(array('success' => '1'));
            } else {
                echo CJSON::encode(array('error' => 'Ошибка удаления'));
            }
        } else {
            echo CJSON::encode(array('error' => 'Данной позиции нет'));
        }
        Yii::app()->end();
    }

    public function actionGroupsList($center_id=0) {

        if (($center = TrCenter::model()->findByPk($center_id)) && $center->groups) {
            echo CHtml::dropDownList('','',CHtml::listData($center->groups, 'id', 'nameWithCourse'),array('empty'=>'Выберите группу'));
        } else {
            echo CHtml::dropDownList('', '', array());
        }

    }
	
}

?>