<?php

class CouponController extends CController {
        
    public function filters() {
        return array(
            'ajaxOnly'
        );
    }

    public function actionGenerate($userId, $couponName) {
        if (!Yii::app()->user->checkAccess('coupon/generate')) throw new CHttpException(403);
        $userId = (int)$userId;

        if ($userId <= 0 || !$user = UserMain::model()->findByPk($userId)) throw new CHttpException(400);

        $coupon = new Coupon;
        $coupon->dealer_id = $userId;
        $coupon->coupon = $couponName;

        if ($coupon->save()) {
            ActivityLog::add($user,'generate',array('count' => 1));
            Yii::app()->user->setFlash('coupons', "Купон был успешно сгенерирован!");
            JsonResponse::success();
        } else {
            JsonResponse::error(CHtml::errorSummary($coupon));
        }

    }

}

?>