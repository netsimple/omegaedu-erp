<?php

class StatController extends Controller {
    
    protected function innerView($action) {
        if ($action != 'finance' && $action != 'lessons') throw new CHttpException(400);
        $stat = new Stat;
        $statFilter = new StatFilterForm($action);
        
        // $statFilter->type = array_keys($statFilter->getTypes());
        
        if (isset($_POST['StatFilterForm'])) {
            $statFilter->attributes = $_POST['StatFilterForm'];
            if ($statFilter->validate()) {
                $stat->filter = $statFilter;
                $stat->datetimeLimit = $statFilter;
            }
        }
        
        $type = (array)$statFilter->type;
        if (in_array(StatFilterForm::TYPE_EDUCATION,$type)) $stat->addLessonsStat();
        if (in_array(StatFilterForm::TYPE_INCOME,$type)) $stat->addIncomeStat();
        if (in_array(StatFilterForm::TYPE_COSTS,$type)) $stat->addCostsStat();
        if (in_array(StatFilterForm::TYPE_GAIN,$type)) $stat->addProfitStat();
        if (in_array(StatFilterForm::TYPE_PROFIT,$type)) $stat->addProfitabilityStat();
        
        return $stat;
    }
    
    public function actionView($action) {
        $stat = $this->innerView($action);
        $stat->filter->scenario = $action;
        $stat->filter->formatDates();
        $this->render('view', array('model' => $stat, 'statFilter' => $stat->filter, 'action' => $action));
    }
    
    public function actionCsv($action) {
        $stat = $this->innerView($action);
        $data = $stat->getStat();
        
        $output = array();
        $monthNames = array_values(Yii::app()->locale->getMonthNames('wide',true));
        foreach ($data as $currKey => $currentChart) {
            if (($object = $stat->getChartObject($currKey)) != 'ChLine' && $object != 'ChBars') continue;
            $label = $stat->getChartLabel($currKey);
            foreach ($currentChart['datasets'] as $currentDataset) {
                $name = isset($currentDataset['label']) ? strip_tags($currentDataset['label']) : '';
                foreach ($currentDataset['data'] as $currDatakey => $currValue) {
                    $output[] = implode(';',array($label,$name,$monthNames[$currDatakey],$currValue));
                }
            }
        }
        
        if (!$output) Yii::app()->end();
        $filename = 'edustat-'.time().'.csv';

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$filename);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        echo "\xEF\xBB\xBF";
        echo $result = implode("\r\n",$output);
        die();
        
    }

    public function actionFindPartnerByName($term) {
        $criteria = new CDbCriteria;
        $criteria->compare('is_partner',1);
        $criteria->compare('is_owner',1,false,'OR');
        $criteria->compare('fullname',$term,true);
        $criteria->order = 'fullname ASC';
        $models = UserMain::model()->findAll($criteria);

        $suggest=array();

        foreach($models as $model) {
            $suggest[] = array(
                'label'=>$model->fullname, // label for dropdown list
                'value'=>$model->fullname, // value for input field
                'id'=>$model->id, // return values from autocomplete
            );
        }

        echo CJSON::encode($suggest);
    }

    public function actionFindTeacherByName($term) {
        $criteria = new CDbCriteria;
        $criteria->compare('is_teacher',1);
        $criteria->compare('fullname',$term,true);
        $criteria->order = 'fullname ASC';
        $models = UserMain::model()->findAll($criteria);

        $suggest=array();

        foreach($models as $model) {
            $suggest[] = array(
                'label'=>$model->fullname, // label for dropdown list
                'value'=>$model->fullname, // value for input field
                'id'=>$model->id, // return values from autocomplete
            );
        }

        echo CJSON::encode($suggest);
    }

    public function actionFindClientByName($term) {
        $criteria = new CDbCriteria;
        $criteria->compare('fullname',$term,true);
        $criteria->order = 'fullname ASC';
        $models = Client::model()->findAll($criteria);

        $suggest=array();

        foreach($models as $model) {
            $suggest[] = array(
                'label'=>$model->fullname, // label for dropdown list
                'value'=>$model->fullname, // value for input field
                'id'=>$model->id, // return values from autocomplete
            );
        }

        echo CJSON::encode($suggest);
    }

    public function actionGetClientGroups($client_id) {
        $client = Client::model()->findByPk($client_id);
        $suggest='';
        foreach($client->groups as $model) $suggest .= "<option value='{$model->id}' >{$model->name}</option>";
        echo $suggest;
    }

    public function actionGetCenters() {
        $partner_id = Yii::app()->request->getPost('partner_id');

        if (!Yii::app()->user->checkAccess('owner'))
            $partner_id = Yii::app()->user->projectIdCompare;

        $criteria = new CDbCriteria;
        $criteria->select = 'id, name';
        $criteria->order = 'name ASC';
        if ($partner_id) {
            if (($user = UserMain::model()->findByPk($partner_id)) && !$user->is_owner)
                $criteria->compare('project_id',$user->project_id);
        }

        $data = TrCenter::model()->findAll($criteria);

        $suggest='<option value="0">Все центры</option>';
        foreach($data as $model){
            $suggest .= "<option value='{$model->id}' >{$model->name}</option>";
        }

        echo $suggest;
    }

    public function actionGetGroups($center_id=0) {

        if ($center_id && !Yii::app()->user->checkAccess('owner')) {
            $center = TrCenter::model()->findByAttributes(array(
                'id'=>$center_id,
                'project_id'=>Yii::app()->user->projectIdCompare,
            ));

            if (!$center)
                throw new CHttpException(403);
        }
        $data = TrGroup::getByCenterId($center_id);

        $suggest="<span class='groupItem'><input type='checkbox' id='StatFilterForm_group_id_0' name='StatFilterForm[group_id][]' value='0' /> <label for='StatFilterForm_group_id_0'>Все группы</label></span>";
        foreach ($data as $model)
            $suggest .= "<span class='groupItem'><input type='checkbox' id='StatFilterForm_group_id_{$model->id}' name='StatFilterForm[group_id][]' value='{$model->id}' /> <label for='StatFilterForm_group_id_{$model->id}'>{$model->name}</label></span>";

        echo $suggest;
    }
    
}

?>