<?php

class AccessController extends Controller {

	public $layout='//layouts/column1';
	public $defaultAction = 'groups';
	
	protected $archiveClasses = array('UserMain', 'UserDocuments', 'TrCenter', 'TrCourse', 'TrGroup', 'Schedule', 'Client', 'Contract', 'Advert', 'ClientRequest', 'TrCosts', 'TrIncome');
	
	public function actionGroups() {
		if (!Yii::app()->user->checkAccess('owner')) throw new CHttpException(403);
		
		if ($_POST) {
			foreach ($_POST as $userKey => $userData) {
				if (!isset($userData['id'])) continue;
				$userId = (int)$userData['id'];
				$roles = array(
					'is_partner' => (int)(isset($userData['is_partner']) && $userData['is_partner']),
					'is_manager' => (int)(isset($userData['is_manager']) && $userData['is_manager']),
					'is_adman' => (int)(isset($userData['is_adman']) && $userData['is_adman']),
					'is_teacher' => (int)(isset($userData['is_teacher']) && $userData['is_teacher'])
				);
				UserMain::model()->updateByPk($userId,$roles);
			}
			Yii::app()->user->setFlash('success', 'Роли пользователей были успешно сохранены.');
			$this->refresh();
		} else {
			$criteria = new CDbCriteria();
			$criteria->select = 'id, email, fullname, is_owner, is_partner, is_manager, is_adman, is_teacher';
			$criteria->order = 'id';
			$dataProvider = UserMain::model()->findAll($criteria);
		}

		
		$this->render('groups', array(
			'dataProvider' => $dataProvider,	
		));
	}
	
	public function actionActions() {
		if (!Yii::app()->user->checkAccess('owner')) throw new CHttpException(403);
		
		if ($_POST) {
			foreach ($_POST as $accessKey => $accessData) {
				if (!isset($accessData['id'])) continue;
				$accessId = (int)$accessData['id'];
				$model = AccessTable::model()->findByPk($accessId);
				if (!$model) continue; // throw new CHttpException(404);
				$model->allowed_partner = (int)isset($accessData['allowed_partner']);
				$model->allowed_manager = (int)isset($accessData['allowed_manager']);
				$model->allowed_adman = (int)isset($accessData['allowed_adman']);
				$model->allowed_teacher = (int)isset($accessData['allowed_teacher']);
				$model->save();
				unset($model);
			}
			Yii::app()->user->setFlash('success', 'Права доступа были успешно сохранены.');
			$this->refresh();
		} else {
			$criteria = new CDbCriteria();
			$criteria->select = 'id, label, allowed_partner, allowed_manager, allowed_adman, allowed_teacher';
			$criteria->order = 'id ASC';
			$data = AccessTable::model()->findAll($criteria);
		}
		
		$this->render('actions', array(
			'dataProvider' => $data,	
		));
	}
	
	public function actionArchive() {
		if (!Yii::app()->user->checkAccess('access/archive')) throw new CHttpException(403);
			
		$result = array();
		foreach ($this->archiveClasses as $currClass) {
			$data = CActiveRecord::model($currClass)->getAllArchiveObjects();
			foreach ($data as $currObject) {
				$datetime = $currObject->is_deleted;
				while (isset($result[$datetime])) $datetime++;
				$result[$datetime] = $currObject;
			}
		}
		krsort($result);
		$result = new CArrayDataProvider($result,array('pagination' => array('pageSize' => 40)));
		
		$this->render('archive',array('dataProvider' => $result));

	}
	
	protected function loadModel($primaryKey,$className=null) {
		if (!$this->_model = CActiveRecord::model($className)->getArchiveObject($primaryKey)) throw new CHttpException(404);
		return $this->_model;
	}
	
	public function actionRestore() {
		if (!Yii::app()->user->checkAccess('access/restore')) throw new CHttpException(403);
		if (!isset($_POST['ids'])) throw new CHttpException(400);
		$lastKey = null;
		foreach ($_POST['ids'] as $currKey => $currData) {
			if ($lastKey !== null) $this->logAction($model);
			if (!isset($currData['class']) || !isset($currData['id']) || !in_array($currData['class'],$this->archiveClasses)) throw new CHttpException(400);
			$model = $this->loadModel($currData['id'],$currData['class']);
			$model->restore();
			$lastKey = $currKey;
		}
		$this->redirect(array('archive'));
	}
	
	public function actionLog() {
		if (!Yii::app()->user->checkAccess('access/log')) throw new CHttpException(403);
		$accessModel = new AccessLog('search');
		$accessModel->unsetAttributes();
		if (isset($_GET['AccessLog'])) $accessModel->attributes = $_GET['AccessLog'];
		$activityModel = new ActivityLog('search');
		$activityModel->unsetAttributes();
		if (isset($_GET['ActivityLog'])) $activityModel->attributes = $_GET['ActivityLog'];
		$this->render('log',array('accessModel' => $accessModel, 'activityModel' => $activityModel));
	}
	
}

?>