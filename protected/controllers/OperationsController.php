<?php

class OperationsController extends Controller {

    public $layout = '//layouts/column1';
    
    public function actions() {
        return array(
            'ajaxUpdateCosts' => array(
                'class' => 'application.controllers.actions.AjaxUpdateAction',
                'accessKey' => 'trcosts/update',
                'modelName' => 'TrCosts',
            ),
            'ajaxUpdateIncome' => array(
                'class' => 'application.controllers.actions.AjaxUpdateAction',
                'accessKey' => 'trincome/update',
                'modelName' => 'TrIncome',
            ),
        );
    }
    
    public function actionIndex($export=false) {
        if (!Yii::app()->user->checkAccess('operations/index')) throw new CHttpException(403);
        
        $incomeModel = new TrIncome('search');
        $incomeModel->unsetAttributes();
        if ($data = Yii::app()->request->getParam('TrIncome')) $incomeModel->attributes = $data;
        
        $costsModel = new TrCosts('search');
        $costsModel->unsetAttributes();
        if ($data = Yii::app()->request->getParam('TrCosts')) {
            $costsModel->attributes = $data;
            if (isset($data['object_param'])) $costsModel->setObject_param($data['object_param']);
        }
        
        $this->render(in_array($export, array('income', 'costs')) ? "export_csv_{$export}" : 'index', array(
            'incomeModel' => $incomeModel, 'costsModel' => $costsModel
        ));
    }
	
	public function actionAjaxBalance($city_id = 0) {
        if (!Yii::app()->user->checkAccess('operations/index')) throw new CHttpException(403);
        
        $incomeModel = new TrIncome('search');
        $incomeModel->unsetAttributes();
        $incomeModel->city_id = $city_id;
        
        $costsModel = new TrCosts('search');
        $costsModel->unsetAttributes();
        $costsModel->city_id = $city_id;
        
        $this->renderPartial('balance',array('income' => $incomeModel, 'costs' => $costsModel),false,true);
		Yii::app()->end();
	}
    
    public function actionAjaxAddIncome() {
        if (!Yii::app()->user->checkAccess('trincome/add')) throw new CHttpException(403);
        if (!Yii::app()->request->isAjaxRequest || !Yii::app()->request->isPostRequest) throw new CHttpException(400);
        $model = new TrIncome;
        $data = Yii::app()->request->getPost('TrIncome');
        $model->attributes = $data;
		if($model->save()){
			$this->logAction($model,'addIncome');
			echo CJSON::encode(array('id'=>$model->id));
		} else {
			$errors = array_map(function($v){ return join(', ', $v); }, $model->getErrors());
			echo CJSON::encode(array('errors' => $errors));
		}
        Yii::app()->end();
    }
    
    public function actionAjaxAddCosts() {
        if (!Yii::app()->user->checkAccess('trcosts/add')) throw new CHttpException(403);
        if (!Yii::app()->request->isAjaxRequest || !Yii::app()->request->isPostRequest) throw new CHttpException(400);
        $model = new TrCosts;
        $data = Yii::app()->request->getPost('TrCosts');
        if (isset($data['object_param'])) $model->setObject_param($data['object_param']);
        $model->attributes = $data;
		if($model->save()){
			$this->logAction($model,'addCosts');
			echo CJSON::encode(array('costs' => $model->getSummaryCostsValue(), 'id'=>$model->id));
		} else {
			$errors = array_map(function($v){ return join(', ', $v); }, $model->getErrors());
			echo CJSON::encode(array('errors' => $errors));
		}
        Yii::app()->end();
    }

    public function actionAjaxUpdateSectionList() {
        if (!Yii::app()->user->checkAccess('trcosts/add'))
            throw new CHttpException(403);
        if (!Yii::app()->request->isAjaxRequest || !Yii::app()->request->isPostRequest || !($object = Yii::app()->request->getPost('object_id')))
            throw new CHttpException(400);

        $model = new TrCosts;
        $model->setObject_param($object);
        echo CJSON::encode($model->getSectionList(true));
        Yii::app()->end();
    }
    
    public function actionDeleteCosts($id) {
        if (!Yii::app()->user->checkAccess('trcosts/delete')) throw new CHttpException(403);
        $model = $this->loadModel($id,'TrCosts');
        $model->delete();
        $this->logAction($model,'delete');
    }
    
    public function actionDeleteIncome($id) {
        if (!Yii::app()->user->checkAccess('trincome/delete')) throw new CHttpException(403);
        $model = $this->loadModel($id,'TrIncome');
        $model->delete();
        $this->logAction($model,'delete');
    }
	
	public function actionAjaxUpdateBalance() {
		if (!Yii::app()->request->isAjaxRequest || !Yii::app()->request->isPostRequest || !($type = Yii::app()->request->getPost('type')) || ($type != 'cash' && $type != 'cashless')) throw new CHttpException(400);
		if (!TrBalance::checkUpdateAccess()) throw new CHttpException(403);
		TrBalance::update($type,Yii::app()->request->getPost('value'));
		Yii::app()->end();
	}

	public function actionExport() {

    }

	public function actionExportExcell() {


        $phpExcelPath = Yii::getPathOfAlias('ext.PHPExcel.Classes');

        // Turn off our amazing library autoload
        spl_autoload_unregister(array('YiiBase','autoload'));

        //
        // making use of our reference, include the main class
        // when we do this, phpExcel has its own autoload registration
        // procedure (PHPExcel_Autoloader::Register();)
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("PDF Test Document")
            ->setSubject("PDF Test Document")
            ->setDescription("Test document for PDF, generated using PHP classes.")
            ->setKeywords("pdf php")
            ->setCategory("Test result file");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Hello')
            ->setCellValue('B2', 'world!')
            ->setCellValue('C1', 'Hello')
            ->setCellValue('D2', 'world!');

        // Miscellaneous glyphs, UTF-8
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'Miscellaneous glyphs')
            ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Simple');

        // Set active sheet index to the first sheet,
        // so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);




// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="01simple.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');



	}
    
}

?>