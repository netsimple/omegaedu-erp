<?php if ($flashMsg = Yii::app()->user->getFlash('success')): ?>
<div class="alert alert-success">
	<?php echo CHtml::encode($flashMsg); ?>
</div>
<?php endif; ?>

<?php echo CHtml::beginForm(); ?>
    <table width="100%">
      <tr>
        <td><b>Наименование</b></td>
        <td><b>Партнер</b></td>
        <td><b>Руководитель</b></td>
        <td><b>Администратор</b></td>
        <td><b>Учитель</b></td>
      </tr>
    <?php
        foreach ($dataProvider as $currentAccess):
    ?>
      <tr>
        <td><?php echo $currentAccess->label; echo CHtml::hiddenField('id'.$currentAccess->id.'[id]',$currentAccess->id); ?></td>
        <td><?php echo CHtml::checkBox('id'.$currentAccess->id.'[allowed_partner]', $currentAccess->allowed_partner); ?></td>
        <td><?php echo CHtml::checkBox('id'.$currentAccess->id.'[allowed_manager]', $currentAccess->allowed_manager); ?></td>
        <td><?php echo CHtml::checkBox('id'.$currentAccess->id.'[allowed_adman]', $currentAccess->allowed_adman); ?></td>
        <td><?php echo CHtml::checkBox('id'.$currentAccess->id.'[allowed_teacher]', $currentAccess->allowed_teacher); ?></td>
      </tr>
    <?php
        endforeach;
    ?>
    </table>
    <?php echo CHtml::submitButton('Сохранить',array('class' => 'btn')); ?>
<?php echo CHtml::endForm(); ?>