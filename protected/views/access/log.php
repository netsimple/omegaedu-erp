<?php

$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=> array(
		'Лог активности' => $this->renderPartial('_log_activity',array('model' => $activityModel),true),
		'Лог доступа' 	 => $this->renderPartial('_log_access',array('model' => $accessModel),true),
	),
    'options'=>array(
        'collapsible'=>false,
        'selected'=>0,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));

?>