    <li>
        
        <div class="row">
            <span style="font-weight: bold; font-size: 1.2em;"><?php echo $model->course ? $model->course->name : ''; ?></span>
            <?php echo CHtml::activeHiddenField($model,"[$index]id"); ?>
            <?php echo CHtml::activeHiddenField($model,"[$index]course_id"); ?>
        </div>
        
        <div style="clear: left;"></div>
        
        <div class="row" style="float: left; margin-right: 15px; vertical-align: middle;">
            <?php echo CHtml::activeLabel($model,"[$index]total_time"); ?>
            <?php echo CHtml::activeTextField($model, "[$index]total_time", array('size'=>25)) ?>
        </div>
        
        <div class="row" style="float: left; margin-right: 15px; vertical-align: middle;">
            <?php echo CHtml::activeLabel($model,"[$index]period_time"); ?>
            <?php echo CHtml::activeTextField($model, "[$index]period_time", array('size'=>25)) ?>
        </div>
        
        <div class="row" style="float: left; margin-right: 15px; vertical-align: middle;">
            <?php echo CHtml::activeLabel($model,"[$index]period_price"); ?>
            <?php echo CHtml::activeTextField($model, "[$index]period_price", array('size'=>40)) ?>
        </div>
        
        <div class="row" style="float: left; vertical-align: middle;">
            <br/>
            [&nbsp;<?php echo CHtml::link('удалить', '#', array('onclick' => '$(this).parent().parent().remove(); return false;')) ?>&nbsp;]
        </div>
        
        <div style="clear: left;"></div>

    </li>
    
