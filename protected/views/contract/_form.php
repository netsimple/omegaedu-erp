<?php

$urlCourses = Yii::app()->createAbsoluteUrl('trgroup/getcourses');

$js = <<<JSCRIPT

	var mainSelect = $('#TrCourse_tr_center_id');
	var childSelect = $('#TrCourse_id');
	var addButton = $('.btn_add_course');
	
	var names = ['name','pass_num','pass_where','pass_when','addr_index','addr','phone_home','phone_mobile'];
	
	function switchParentFields() {
		for (var i = 0; i < names.length; i++) {
			$('#Contract_parent_'+names[i]).prop('disabled',$('#dublicate').prop('checked'));
			if ($('#dublicate').prop('checked')) $('#Contract_parent_'+names[i]).val($('#Contract_client_'+names[i]).val());
		}
	}
	
	$(document).ready(function(){
		
		switchParentFields();
		
		$('#dublicate').click(switchParentFields);
		
		$('#Contract_client_pass_num').change(function(){ if ($('#Contract_parent_pass_num').prop('disabled')) $('#Contract_parent_pass_num').val($(this).val()); });
		$('#Contract_client_pass_where').change(function(){ if ($('#Contract_parent_pass_where').prop('disabled')) $('#Contract_parent_pass_where').val($(this).val()); });
		$('#Contract_client_pass_when').change(function(){ if ($('#Contract_parent_pass_when').prop('disabled')) $('#Contract_parent_pass_when').val($(this).val()); });
		$('#Contract_client_addr_index').change(function(){ if ($('#Contract_parent_addr_index').prop('disabled')) $('#Contract_parent_addr_index').val($(this).val()); });
		$('#Contract_client_addr').change(function(){ if ($('#Contract_parent_addr').prop('disabled')) $('#Contract_parent_addr').val($(this).val()); });
		$('#Contract_client_phone_home').change(function(){ if ($('#Contract_parent_phone_home').prop('disabled')) $('#Contract_parent_phone_home').val($(this).val()); });
		$('#Contract_client_phone_mobile').change(function(){ if ($('#Contract_parent_phone_mobile').prop('disabled')) $('#Contract_parent_phone_mobile').val($(this).val()); });
		
	});
		
JSCRIPT;

Yii::app()->clientScript->registerScript('selectCoursesJS', $js, CClientScript::POS_READY);

?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contract-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>
 
    <?php echo $form->errorSummary($model); ?>
	
	<fieldset style="border: 1px solid black;">
	
    <div class="row">
        <?php echo $form->label($model,'date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'Contract_date',
                'attribute' => 'date',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ));
		?>
    </div>
	
    <div class="row">
		<?php echo $form->checkBox($model,'dublicate',array('id' => 'dublicate')); ?>
        <?php echo $form->label($model,'dublicate', array('style' => 'display: inline;')); ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'parent_name'); ?>
        <?php echo $form->textField($model, 'parent_name', array('size'=>75)) ?>
    </div>
 
    <div class="row">
        <?php echo $form->label($model,'client_name'); ?>
		<?php echo CHtml::textField('client_name', $model->client_name, array('size'=>75, 'disabled' => 'disabled', 'id' => 'Contract_client_name')); ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'status'); ?>
        <?php echo $form->dropDownList($model, 'status', $model->statusList) ?>
    </div>
	
	</fieldset>
	
	<fieldset style="border: 1px solid black;">
		
    <div class="row" style="float: left; margin-right: 15px;">
        <?php echo $form->label($model,'start_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'Contract_start_date',
                'attribute' => 'start_date',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ));
		?>
    </div>
	
    <div class="row" style="float: left;">
        <?php echo $form->label($model,'end_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'Contract_end_date',
                'attribute' => 'end_date',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ));
		?>
    </div>
	
	<div style="clear: left;"></div>
	
    <div class="row">
		<div style="float: left;">
			<?php echo $form->label($model,'has_discount', array('style' => 'display: inline')); ?>
			<?php echo $form->checkBox($model, 'has_discount') ?>
			&nbsp;&nbsp;&nbsp;
			<?php echo $form->label($model,'discount_reason', array('style' => 'display: inline')); ?>:
			<?php echo $form->textField($model, 'discount_reason', array('size'=>25)) ?>
		</div>
		<div style="clear: left;"></div>
    </div>
	
	<div class="row">
        <?php echo $form->label($model,'discount_coupon'); ?>
		<?php if ($model->isNewRecord || !$model->discount_coupon || $form->error($model, 'discount_coupon')): ?>
			<?php echo $form->textField($model, 'discount_coupon', array('size'=>20)) ?>
		<?php else: ?>
			<b><?php echo $model->discount_coupon; ?></b>
		<?php endif; ?>
	</div>
	
    <div class="row" style="float: left; margin-right: 15px;">
        <?php echo CHtml::label('Центр', ''); ?>
        <?php
        echo CHtml::dropDownList('center_id','', TrCenter::model()->centerNames, array('empty'=>'Выберите центр', 'ajax' => array(
            'url'=>$this->createUrl('trgroup/groupsList'),
            'data'=>array('center_id'=>'js:this.value'),
            'update'=>'#TrIncome_group_id'
        )));
        ?>
    </div>
    <div class="row" style="float: left; margin-right: 15px;">
        <?php echo CHtml::activeLabel(TrIncome::model(),'group_id'); ?>
        <?php echo CHtml::activeDropDownList(TrIncome::model(),'group_id',array()); ?>
    </div>
	
    <div class="row" style="float: left;">
        <br/><?php echo CHtml::button('Добавить', array('class' => 'btn_add_course', 'contract/courseForm', 'courseId' => 0)); ?>
    </div>
	
	<div style="clear: left;"></div>
	
	<ul class="courses" style="list-style-type: none; padding-left: 0px; margin: 0">
		<?php foreach ($model->getAllCourses() as $index => $courseModel): ?>
			<?php $this->renderPartial('_form_courses', array('model' => $courseModel, 'index' => $index)); ?>
		<?php endforeach; ?>
	</ul>
	
	<script>
		$(".btn_add_course").click(function(){
			$.ajax({
				success: function(html){
					$(".courses").append(html);
				},
				type: 'get',
				url: '<?php echo $this->createUrl('courseForm')?>',
				data: {
					groupId: $("#TrIncome_group_id").val(),
					index: $(".courses li").size()
				},
				cache: false,
				dataType: 'html'
			});
		});
	</script>
	
	</fieldset>
	
	<!-- place for courses list -->
	
	
	<fieldset style="border: 1px solid black; width: 45%; float: left; margin-right: 15px;">
	
    <div class="row">
        <?php echo $form->label($model,'client_pass_num'); ?>
		<?php echo $form->textField($model,'client_pass_num',array('size' => 35)); ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'client_pass_where'); ?>
        <?php echo $form->textField($model, 'client_pass_where', array('size'=>70)) ?>
    </div>
	
    <div class="row" style="float: left; margin-right: 7px;">
        <?php echo $form->label($model,'client_pass_when'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'Contract_client_pass_when',
                'attribute' => 'client_pass_when',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true,
					'yearRange' => '-100:+0',
                ),
				'htmlOptions' => array('size' => 35),
            ));
		?>
    </div>
	
    <div class="row" style="float: left; margin-left: 20px;">
        <?php echo $form->label($model,'client_addr_index'); ?>
        <?php
			$this->widget('CMaskedTextField', array(
				'model' => $model,
				'attribute' => 'client_addr_index',
				'mask' => '999999',
				'placeholder' => '*',
				'htmlOptions' => array('size' => 25),
			));
        ?>
	</div>
	
	<div style="clear: left;"></div>
	
    <div class="row">
        <?php echo $form->label($model,'client_addr'); ?>
        <?php echo $form->textField($model, 'client_addr', array('size'=>70)) ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'client_phone_home'); ?>
        <?php
			$this->widget('CMaskedTextField', array(
				'model' => $model,
				'attribute' => 'client_phone_home',
				'mask' => '+7-999-999-9999',
				'placeholder' => '*',
				'htmlOptions' => array('size' => 70),
			));
        ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'client_phone_mobile'); ?>
        <?php
			$this->widget('CMaskedTextField', array(
				'model' => $model,
				'attribute' => 'client_phone_mobile',
				'mask' => '+7-999-999-9999',
				'placeholder' => '*',
				'htmlOptions' => array('size' => 70),
			));
        ?>
    </div>
	
	</fieldset>
	
	<fieldset style="border: 1px solid black; width: 45%; float: left;">
	
    <div class="row">
        <?php echo $form->label($model,'parent_pass_num'); ?>
		<?php echo $form->textField($model,'parent_pass_num',array('size' => 35)); ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'parent_pass_where'); ?>
        <?php echo $form->textField($model, 'parent_pass_where', array('size'=>70)) ?>
    </div>
	
    <div class="row" style="float: left; margin-right: 7px;">
        <?php echo $form->label($model,'parent_pass_when'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'Contract_parent_pass_when',
                'attribute' => 'parent_pass_when',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true,
					'yearRange' => '-100:+0',
                ),
				'htmlOptions' => array('size' => 35),
            ));
		?>
    </div>
	
    <div class="row" style="float: left; margin-left: 20px;">
        <?php echo $form->label($model,'parent_addr_index'); ?>
        <?php
			$this->widget('CMaskedTextField', array(
				'model' => $model,
				'attribute' => 'parent_addr_index',
				'mask' => '999999',
				'placeholder' => '*',
				'htmlOptions' => array('size' => 25),
			));
        ?>
	</div>
	
	<div style="clear: left;"></div>
	
    <div class="row">
        <?php echo $form->label($model,'parent_addr'); ?>
        <?php echo $form->textField($model, 'parent_addr', array('size'=>70)) ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'parent_phone_home'); ?>
        <?php
			$this->widget('CMaskedTextField', array(
				'model' => $model,
				'attribute' => 'parent_phone_home',
				'mask' => '+7-999-999-9999',
				'placeholder' => '*',
				'htmlOptions' => array('size' => 70),
			));
        ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'parent_phone_mobile'); ?>
        <?php
			$this->widget('CMaskedTextField', array(
				'model' => $model,
				'attribute' => 'parent_phone_mobile',
				'mask' => '+7-999-999-9999',
				'placeholder' => '*',
				'htmlOptions' => array('size' => 70),
			));
        ?>
    </div>
	
	</fieldset>
	
	<div style="clear: left;"></div>
 
    <div class="row submit">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
    </div>
 
<?php $this->endWidget(); ?>
</div><!-- form -->