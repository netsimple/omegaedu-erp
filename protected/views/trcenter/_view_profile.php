<?php
    $this->widget('application.components.widgets.TabLinksWidget', array(
		'links' => array(
			array('label' => 'Редактировать', 'url' => array('update','id' => $model->id), 'visible' => Yii::app()->user->checkAccess('trcenter/update')),
			array('label' => 'Редактировать ЗП преподавателям', 'url' => array('payment/update','id' => $model->id), 'visible' => Yii::app()->user->checkAccess('trcenter/update')),
		),
	));
?>

<?php

$attributes = array(
    'name' => array(
        'name' => 'name',
        'editable' => array(
            'type' => 'text',
            'validate' => 'function(value) {
                if (!value) return "Название обязательно для заполнения."
            }'
        ),
    ),
	array(
		'name' => 'city_id',
		'value' => $model->city ? $model->city->name : 'Не выбрано',
        'editable' => array(
            'type' 	=> 'select',
            'source'=> CHtml::listData(City::model()->findAll(), 'id', 'name'),
        ),
	),
	'address',
    'contacts' => array(
        'name' => 'contacts',
        'editable' => array(
            'type' => 'textarea',
        ),
	),
	'manager' => array(
		'name' => 'managerIDs',
		'type' => 'raw',
		//'value' => $model->managerLinks,
		'editable' => array(
			'type'      => 'checklist',
			'model'     => $model,
			'attribute' => 'managerIDs',
			'placement' => 'right',
			//'url'       => $this->createUrl('ajaxUpdate'),
			'source'    => UserMain::model()->getManagerList(),
		),
	),
    array(
	    'name' => 'late_fee',
	    'value' => $model->currency ? $model->late_fee.' '.$model->currency->name  : 'Не выбрано',
    ),
    array(
	    'name' => 'fine_for_truancy',
	    'value' => $model->currency ? $model->fine_for_truancy.' '.$model->currency->name  : 'Не выбрано',
    ),
);

?>

<?php
    $this->widget('editable.EditableDetailView', array(
		'data'       => $model,
		'url'        => $this->createUrl('ajaxUpdate'),
		'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
		'emptytext'  => 'Редактировать',
		'attributes' => $attributes,
    ));
?>