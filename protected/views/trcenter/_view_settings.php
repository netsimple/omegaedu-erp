<?php

	$css = <<<CSS
		.is-debt a {
			color: red;
			font-weight: bold;
		}
CSS;

	//Yii::app()->clientScript->registerCss('debt',$css);

?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'trgroup-grid',
	'dataProvider'=>$model->search(),
	'template' => '{items}{pager}',
	'columns'=>array(
		'id' => array(
			'name' => 'id',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->name),array("trgroup/view", "id" => $data->id))',
		),
		'teacher_id' => array(
			'name' => 'teacher_id',
			'type' => 'raw',
			'value' => '$data->teacher ? $data->teacher->profileLink : ""',
		),
		'member_count' => array(
			'name' => 'member_count',
			'type' => 'raw',
			'value' => '$data->member_count',
		),
		'debt' => array(
            'header'=> 'Списано',
			'name' => 'debt',
			'type' => 'raw',
			'value' => '$data->debtForGridView',
			'cssClassExpression' => '$data->countDebt ? "is-debt" : ""',
		),
	),
)); ?>