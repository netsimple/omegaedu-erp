<div>
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'summary-grid',
        'dataProvider'=>$modelCosts->summaryData,
        'template' => '{items}{pager}',
        'columns'=>array(
            'section' => array(
                'name' => CHtml::activeLabel($modelCosts,'section'),
                'type' => 'raw',
                'value' => '$data->sectionList[$data->section]',
            ),
            'value' => array(
                'name' => CHtml::activeLabel($modelCosts,'value'),
                'type' => 'raw',
                'value' => '$data->value',
            ),
        ),
    )); ?>
</div>