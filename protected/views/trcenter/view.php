<h1><?php echo $model->name; ?></h1>

<?php

$tabs = array();
$tabs['Профиль'] = $this->renderPartial('_view_profile',array('model' => $model),true);
$tabs['Группы'] = $this->renderPartial('_view_settings',array('model' => $groupModel),true);
$tabs['Финансы'] = $this->renderPartial('_view_finance',array('model' => $model, 'modelCosts' => $modelCosts),true,true);

$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=>$tabs,
    'options'=>array(
        'collapsible'=>false,
        'selected'=>0,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));

?>