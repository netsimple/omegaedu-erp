<?php
    
    $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'groups-grid',
        'dataProvider'=>$model->groupFinanceDP,
        'template' => '{items}{pager}',
        'columns'=>array(
            array(
                'class' => 'CCheckBoxColumn',
                'selectableRows' => 2,
                'checked' => 'true',
            ),
            'id' => array(
                'name' => 'id',
                'type' => 'raw',
                'value' => 'CHtml::link(CHtml::encode($data->name),array("trgroup/view", "id" => $data->id))',
            ),
            'lessons_success' => array(
                'name' => 'lessons_success',
                'type' => 'raw',
                'value' => '$data->lessons_success',
            ),
        ),
    ));
    
?>