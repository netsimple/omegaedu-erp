<?php
	$this->menu = array(
	    array('label'=>'Список учебных центров', 'url'=>array('trcenter/index')),
	    array('label'=>'Профиль учебного центра', 'url'=>array('trcenter/view', 'id' => $model->id)),
	);
?>

<h1>Редактирование учебного центра</h1>

<?php
    $this->renderPartial('_form',array('model'=>$model, 'managers' => $managers));
?>