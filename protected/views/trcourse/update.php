<?php
	$this->menu = array(
	    array('label'=>'Список предметов', 'url'=>array('trcourse/index')),
	    array('label'=>'Профиль предмета', 'url'=>array('trcourse/view', 'id' => $model->id)),
	);
?>

<h1>Редактирование предмета</h1>

<?php
    $this->renderPartial('_form',array('model'=>$model));
?>