<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'trgroup-grid',
	'dataProvider'=>$model->search(),
	'template' => '{items}{pager}',
	'columns'=>array(
		'id' => array(
			'name' => 'id',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->name),array("trgroup/view", "id" => $data->id))',
		),
		'teacher_id' => array(
			'name' => 'teacher_id',
			'type' => 'raw',
			'value' => '$data->teacher ? $data->teacher->profileLink : ""',
		),
		'member_count' => array(
			'name' => 'member_count',
			'type' => 'raw',
			'value' => '$data->member_count',
		),
	),
)); ?>