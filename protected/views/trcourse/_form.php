<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'trcourse-form',
	'enableAjaxValidation'=>false,
)); ?>
 
    <?php echo $form->errorSummary($model); ?>
 
    <div class="row">
        <?php echo $form->label($model,'name'); ?>
        <?php echo $form->textField($model, 'name', array('size'=>75)); ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'description'); ?>
        <?php echo $form->textArea($model, 'description', array('cols' => 55, 'rows' => 3)) ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'client_lvl'); ?>
        <?php echo $form->dropDownList($model, 'client_lvl', $model->levelList) ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'hours'); ?>
        <?php echo $form->textField($model, 'hours'); ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'start_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'start_date',
                'attribute' => 'start_date',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ));
		?>
    </div>
 
    <div class="row submit">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-primary')); ?>
    </div>
 
<?php $this->endWidget(); ?>
</div><!-- form -->