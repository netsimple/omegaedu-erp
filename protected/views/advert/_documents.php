<?php

    $accessDelete = !empty($documents) && Yii::app()->user->checkAccess('advert/delete');
	if ($accessDelete) {
		$targetUrl = Yii::app()->createAbsoluteUrl('advert/deleteDocument');
		$js = <<<JSCRIPT
		$(document).ready(function(){
			$('.delete-document').click(function(){
				var elemId = $(this).data('id');
				$.ajax({
					url: '$targetUrl',
					type: 'get',
					data: {'id': elemId}, // this is PARENT ID
					success: function(){
						$('#document-'+elemId).remove();
						if (!$('.document-elem').size()) {
							$('#document-container').remove();
						}
					}
				});
			});
		});
JSCRIPT;
		Yii::app()->clientScript->registerScript('deleteDocumentJS', $js, CClientScript::POS_READY);
	}
	
?>

<?php if ($documents): ?>
	<div id="document-container" style="margin-top: 10px;">
		<h5 style="margin-bottom: 5px">Прикрепленные фотографии:</h5>
		<ul id="document-list">
			<?php foreach ($documents as $currDoc): ?>
				<li class="document-elem" id="document-<?php echo $currDoc->id; ?>">
					<?php echo CHtml::link($currDoc->filename,$currDoc->viewpath); ?>
					<?php if ($accessDelete): ?>
						&nbsp;[<?php echo CHtml::link('x','#',array('class' => 'delete-document', 'data-id' => $currDoc->id)); ?>]
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>