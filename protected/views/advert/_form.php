<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'advert-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="row">
		<?php echo $form->label($model,'user_id',array('style' => 'display:inline')); ?>: <b><?php echo $model->user->fullname; ?></b>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('maxlength'=>255, 'style' => 'width: 350px')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows' => 7, 'style' => 'width: 350px')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'date',
                'attribute' => 'date',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ));
		?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row" style="float: left; margin-right: 15px;">
		<?php echo $form->labelEx($model,'member_count'); ?>
		<?php echo $form->textField($model,'member_count',array('size'=>20,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'member_count'); ?>
	</div>

	<div class="row" style="float: left;">
		<?php echo $form->labelEx($model,'activity_type'); ?>
		<?php echo $form->dropDownList($model,'activity_type',$model->activityType); ?>
		<?php echo $form->error($model,'activity_type'); ?>
	</div>
	
	<div style="clear: left;"></div>
	
	<?php if ($model->documents): ?>
		<?php $this->renderPartial('_documents',array('documents' => $model->documents)); ?>
	<?php endif; ?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'documents'); ?>
		<?php echo $form->fileField($model,'[]documents',array('multiple' => true)); ?>
		<?php echo $form->error($model,'documents'); ?>
	</div>

	<div class="row buttons" style="margin-top:15px;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->