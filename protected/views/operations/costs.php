<script type="text/javascript">
    jQuery(function($){
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    });
</script>

<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->getClientScript()->getCoreScriptUrl() . '/jui/js/jquery-ui-i18n.min.js');
?>

<?php

    $js = <<<JSCRIPT
    
        $(document).ready(function(){
            var errorMsg = 'Неизвестная ошибка';
            var successMsg = 'Позиция была успешно добавлена.';
           $('#btn-add-costs').click(function(){
                var formObj = $(this).parent().parent();
                $.ajax({
                   url: '{$this->createUrl('ajaxAddCosts')}',
                   type: 'POST',
                   data: formObj.serialize(),
                   dataType: 'json',
                   success: function(response, config){
                        if (response && typeof response.errors == 'undefined') {
                            $.fn.yiiGridView.update('costs-grid');
                            formObj.trigger('reset');
                            $("#msg-costs").removeClass("alert-error").addClass("alert-success").html(successMsg).show().fadeToggle(1000, "swing");
                        } else {
                            this.error.call(this, response && response.errors ? response.errors : errorMsg);
                        }
                   },
                   error: function(errors) {
                        var msg = "";
                        if(errors && errors.responseText) {
                            msg = errors.responseText;
                        } else {
                            $.each(errors, function(k, v) { msg += v+"<br>"; });
                        }
                        $("#msg-costs").removeClass("alert-success").addClass("alert-error").html(msg).show();
                   },
                });
                return false;
           });
           $('#new-costs-object-list').change(function(){
                var firstElem = $('#new-costs-section-list :first').clone();
                $('#new-costs-section-list').empty();
                $('#new-costs-section-list').append(firstElem);
                $('#new-costs-section-list').prop('disabled',true);
                $('#new-costs-comment').prop('disabled',true);
                $('#new-costs-value').prop('disabled',true);
				$('#btn-add-costs').prop('disabled',true);
                if ($('#new-costs-object-list').val()) {
					if ($('#new-costs-object-list').val() == 'custom') {
						$('#new-costs-section-block').hide();
						$('#new-costs-comment').prop('disabled',false);
						$('#new-costs-value').prop('disabled',false);
						$('#btn-add-costs').prop('disabled',false);
					} else {
						$('#new-costs-section-block').show();
						$('#new-costs-comment').prop('disabled',$('#new-costs-section-list').val() ? false : true);
						$('#new-costs-value').prop('disabled',$('#new-costs-section-list').val() ? false : true);
						$('#btn-add-costs').prop('disabled',$('#new-costs-section-list').val() ? false : true);
						$.ajax({
							url: '{$this->createUrl('ajaxUpdateSectionList')}',
							type: 'POST',
							data: {object_id: $('#new-costs-object-list').val()},
							dataType: 'json',
							success: function(response){
								$.each(response,function(key,value){
								   $('#new-costs-section-list').append('<option value="'+key+'">'+value+'</option>');
								});
								$('#new-costs-section-list').prop('disabled',false);
							},
						});
					}
                }
           });
           $('#new-costs-section-list').change(function(){
                $('#new-costs-comment').prop('disabled',$(this).val() ? false : true);
                $('#new-costs-value').prop('disabled',$(this).val() ? false : true);
				$('#btn-add-costs').prop('disabled',$(this).val() ? false : true);
           });
        });
    
JSCRIPT;

    Yii::app()->clientScript->registerScript('add-costs',$js,CClientScript::POS_READY);

?>

<?php $newModel = new TrCosts; ?>

<div id="msg-costs" class="alert hide"></div>

<div class="form">
    
<?php echo CHtml::beginForm(); ?>

<div class="row" style="float: left; margin-right: 15px">
<?php
    echo CHtml::activeLabel($newModel,'period_start');
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model' => $newModel,
        'id' => 'new_costs-period-start',
        'attribute' => 'period_start',
        'options' => array(
            'dateFormat' => 'd M yy',
            'changeYear' => true,
        ),
        'htmlOptions' => array('size' => 15),
    ));
?>
</div>

<div class="row" style="float: left; margin-right: 15px">
<?php
    echo CHtml::activeLabel($newModel,'period_end');
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model' => $newModel,
        'id' => 'new_costs-period-end',
        'attribute' => 'period_end',
        'options' => array(
            'dateFormat' => 'd M yy',
            'changeYear' => true,
        ),
        'htmlOptions' => array('size' => 15),
    ));
?>
</div>

<div class="row" style="float: left; margin-right: 15px">
<?php
    echo CHtml::activeLabel($newModel,'type');
    echo CHtml::activeDropDownList($newModel,'type',$newModel->typeList);
?>
</div>

<div style="clear: left;"></div>

<div class="row" style="float: left; margin-right: 15px">
<?php
    echo CHtml::activeLabel($newModel,'object_param');
    echo CHtml::activeDropDownList($newModel,'object_param',$newModel->objectParamList,array('empty' => 'Выберите элемент', 'id' => 'new-costs-object-list'));
?>
</div>

<div class="row" id="new-costs-section-block" style="float: left; margin-right: 15px">
<?php
    echo CHtml::activeLabel($newModel,'section');
    echo CHtml::activeDropDownList($newModel,'section',$newModel->getSectionList(true),array('empty' => 'Выберите категорию', 'id' => 'new-costs-section-list', 'disabled' => true));
?>
</div>

<div class="row" style="float: left; margin-right: 15px">
<?php
    echo CHtml::activeLabel($newModel,'comment');
    echo CHtml::activeTextField($newModel,'comment', array('id' => 'new-costs-comment', 'disabled' => true));
?>
</div>

<div class="row" style="float: left; margin-right: 15px">
<?php
    echo CHtml::activeLabel($newModel,'value');
    echo CHtml::activeTextField($newModel,'value', array('id' => 'new-costs-value', 'disabled' => true, 'size' => 10));
?>
</div>

<div class="row" style="float: left; margin-top: 10px">
<?php
    echo CHtml::submitButton('+',array('class' => 'btn btn-primary', 'id' => 'btn-add-costs', 'disabled' => true));
?>
</div>

<div style="clear: left;"></div>

<?php echo CHtml::endForm(); ?>

</div>

<button class="btn " onclick="location.href='/operations?export=costs&' + $('#costs-grid :input').serialize(); return false;">Экспорт расходов</button>

<?php

$summary = "<div style=\"text-align: right; margin-bottom: 5px;\">";
if ($model->searchCash !== null) 	 $summary .= "<b>Наличный расчёт: {$model->searchCash}</b><br/>";
if ($model->searchCashless !== null) $summary .= "<b>Безналичный расчёт: {$model->searchCashless}</b><br/>";
$summary .= "<b>Итого: {$model->searchSummary}</b>";
$summary .= "</div>";

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'costs-grid',
	'template'=>$summary.'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	'afterAjaxUpdate' => 'function(){
		jQuery("#costs-period-start, #costs-period-end, #costs-datetime").datepicker({
			dateFormat: "d M yy",
			monthNames: '.CJSON::encode(array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь')).',
			monthNamesShort: '.CJSON::encode(array('янв','февр','марта','апр','мая','июня','июля','авг','сент','окт','нояб','дек')).',
			dayNames: '.CJSON::encode(array('Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота')).',
            dayNamesShort: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            dayNamesMin: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            firstDay: 1,  
			changeYear: true
		});
	}',
    'columns' => array(
        'city' => array(
            'name' => 'city_id',
            'type' => 'raw',
            'value'=> 'City::nameByProjectId($data->project_id)',
            'filter'=> CHtml::activeDropDownList($model,'city_id',City::listArray(),array('empty' => 'Все города')),
            'visible'=> Yii::app()->user->model->checkRole('owner'),
        ),
        'username' => array(
            'name' => 'username',
            'type' => 'raw',
            'value'=> 'CHtml::link(($data->user) ? $data->user->fullname : "Пусто",array("user/view","id" => $data->user_id))',
        ),
        'datetime' => array(
            'name' => 'datetime',
            'type' => 'raw',
            'value'=> 'Yii::app()->dateFormatter->format("d MMM yyyy",$data->datetime)',
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'id' => 'costs-datetime',
                'attribute' => 'datetime',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
            'htmlOptions' => array('width' => 100),
        ),
        'costs-period-start' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'period_start',
            'type' => 'raw',
            'value'=> '$data->period_start ? Yii::app()->dateFormatter->format("d MMM yyyy",$data->period_start) : ""',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'costs-period-start',
                'attribute' => 'period_start',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
			'editable' => array(
				'type'       => 'date',
				'url' => $this->createUrl('ajaxUpdateCosts'),
				'viewformat' => 'dd M yyyy',
				'params' => array(
					'weekStart' => 1,
				),
			),
            'htmlOptions' => array('width' => 100),
        ),
        'period_end' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'period_end',
            'type' => 'raw',
            'value'=> '$data->period_end ? Yii::app()->dateFormatter->format("d MMM yyyy",$data->period_end) : ""',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'costs-period-end',
                'attribute' => 'period_end',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
			'editable' => array(
				'type' => 'date',
				'url' => $this->createUrl('ajaxUpdateCosts'),
				'viewformat' => 'dd M yyyy',
				'params' => array(
					'weekStart' => 1,
				),
			),
            'htmlOptions' => array('width' => 100),
        ),
        'object_param' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'object_param',
            'type' => 'raw',
            'value'=> '$data->objectName',
            'filter' => CHtml::activeDropDownList($model,'object_param',$model->objectParamList,array('empty' => 'Выберите элемент')),
            'editable' => array(
                'url' => $this->createUrl('ajaxUpdateCosts'),
                'type' => 'select',
                'source' => $model->getObjectParamList(true),
            ),
        ),
        'type' => array(
            'class'  => 'editable.EditableColumn',
            'name' 	 => 'type',
            'type' 	 => 'raw',
            'value'	 => '$data->typeList[$data->type]',
            'filter' =>$model->typeList,
            'editable' => array(
                'url' 	=> $this->createUrl('ajaxUpdateCosts'),
                'type' 	=> 'select',
                'source'=> $model->typeList,
            ),
        ),
        'section' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'section',
            'type' => 'raw',
            'value'=> '$data->sectionList[$data->section]',
            'filter'=>$model->sectionList,
            'editable' => array(
                'url' => $this->createUrl('ajaxUpdateCosts'),
                'type' => 'select',
                'source' => $model->sectionList,
            ),
        ),
        'comment' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'comment',
            'editable' => array(
                'url' => $this->createUrl('ajaxUpdateCosts'),  
            ),
        ),
        'value' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'value',
            'editable' => array(
                'url' => $this->createUrl('ajaxUpdateCosts'),
            ),
            'htmlOptions' => array('width' => 50),
        ),
        array(
            'class' => 'ButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array('url' => 'array("deleteCosts", "id" => $data->id)'),  
            ),
        ),
    ),
)); ?>