<?php
	$this->menu = array(
	    array('label'=>'Список учебных центров', 'url'=>array('trcenter/index')),
	    array('label'=>'Профиль учебного центра', 'url'=>array('trcenter/view', 'id' => $model->id)),
	);

	$this->breadcrumbs=array(
		'Список учебных центров'=>array('trcenter/index'),
	);
?>

<h2>Заработная плата преподавателям учебного центра <?=$model->name;?></h2>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'client-grid',
	'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$payment_search->search(),
	'filter' => $payment_search,
	'columns'=>array(
		'count_begin' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'count_begin',
		   'editable' => array(
				'type' => 'text',
				'url' => $this->createUrl('ajaxUpdate'),
		   ),
        ),
		'if' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'if',
		   'value' => 'PaymentTarif::$if_value[$data->if]',
		   'editable' => array(
				'type' => 'select',
				'url' => $this->createUrl('ajaxUpdate'),
			    'source' => PaymentTarif::$if_value
		   ),
        ),
		'summa' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'summa',
		   'editable' => array(
				'type' => 'text',
				'url' => $this->createUrl('ajaxUpdate'),
		   ),
        ),
		array(
			'class'=>'ButtonColumn',
			'template' => '{delete}',
		),
	),
)); ?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'trcenter-payment-form',
	'enableAjaxValidation'=>false,
)); ?>

    <?php echo $form->errorSummary($model); ?>

	<fieldset style="margin-right: 10px">
		<legend>Добавить тариф</legend>

		<div class="row">
			<div class="span-4">
				<?php echo $form->label($payment,'if'); ?>
				<?php echo $form->dropDownList($payment, 'if', PaymentTarif::$if_value, array('style'=>'width: 95%')); ?>
			</div>
			<div class="span-5">
				<?php echo $form->label($payment,'count_begin'); ?>
				<?php echo $form->textField($payment, 'count_begin', array('style'=>'width: 95%')); ?>
			</div>
			<div class="span-3">
				<?php echo $form->label($payment,'summa'); ?>
				<?php echo $form->textField($payment, 'summa', array('style'=>'width: 95%')); ?>
			</div>

			<?php echo $form->hiddenField($payment, 'tr_center_id', array('value'=>$model->id)); ?>
		</div>
	</fieldset>

	<div style="clear: left;"></div>

    <div class="row submit">
        <?php echo CHtml::submitButton($payment->isNewRecord ? 'Создать' : 'Сохранить',array('class' => 'btn btn-primary')); ?>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->