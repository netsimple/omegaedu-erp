<?php
/**
 * Global defination for CLinkPager
 */
return array(
	'default'=>array(
		'header'=>'',
		'firstPageLabel'=>'&lt;&lt;',
		'prevPageLabel'=>'<span class="prevBtn"></span>',
		'nextPageLabel'=>'<span class="nextBtn"></span>',
		'lastPageLabel'=>'&gt;&gt;',
		'cssFile'=>Yii::app()->baseUrl.'/css/skins/pager/pager.css',
		'maxButtonCount'=>0,
	)
);