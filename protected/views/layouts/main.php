<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />
	
	<link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl; ?>/logo.png">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-alert.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-btn.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom.js"></script>

</head>

<body>
<div id="CContent">

        <div id="menuContainer">
            <div class="menuShadow"></div>
            <?php $this->widget('YDropDownMenu',array(
                'lastItemCssClass'=>'last',
                'firstItemCssClass'=>'first',
                'encodeLabel'=>false,
                'items'=>array(
                    array('label'=>CHtml::image(Yii::app()->request->baseUrl.'/images/logo_3.png','ROBBOClubCRM', array('class'=>'logoTop'))),
                    array('label'=>'Каталоги', 'url'=>'', 'items' => array (
	                    array('label'=>'Города', 'url'=>array('city/index'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('user/view','partner')),
	                    array('label'=>'Валюты', 'url'=>array('currency/index'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('user/view', 'manager')),
                    ), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('user/view', '*')
                    ),
                    array('label'=>'Пользователи', 'url'=>'', 'items' => array (
                        array('label'=>'Партнеры', 'url'=>array('user/view', 'target' => 'partner'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('user/view','partner')),
                        array('label'=>'Руководители', 'url'=>array('user/view', 'target' => 'manager'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('user/view', 'manager')),
                        array('label'=>'Администраторы', 'url'=>array('user/view', 'target' => 'admin'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('user/view', 'adman')),
                        array('label'=>'Преподаватели', 'url'=>array('user/view', 'target' => 'teacher'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('user/view', 'teacher')),
                    ), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('user/view', '*')
                    ),
                    array('label'=>'Клиенты', 'url'=>'', 'items' => array (
                        array('label'=>'Список', 'url'=>array('client/index'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('client/index')),
                        array('label'=>'Договоры', 'url'=>array('contract/index'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('contract/index')),
                    ), 'visible'=>!Yii::app()->user->isGuest && (Yii::app()->user->checkAccess('client/index') || Yii::app()->user->checkAccess('contract/index'))
                    ),
                    array('label'=>'Обучение', 'url'=>'', 'items' => array (
                        array('label'=>'Центры', 'url'=>array('trcenter/index'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('trcenter/index')),
                        array('label'=>'Группы', 'url'=>array('trgroup/index'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('trgroup/index')),
                        array('label'=>'Предметы', 'url'=>array('trcourse/index'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('trcourse/index')),
                        array('label'=>'Занятия', 'url'=>array('schedule/list'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('schedule/list')),
                        array('label'=>'Расписание', 'url'=>array('schedule/index'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('schedule/index')),
                    ), 'visible'=>!Yii::app()->user->isGuest && (Yii::app()->user->checkAccess('trcenter/index') || Yii::app()->user->checkAccess('trcourse/index') || Yii::app()->user->checkAccess('trgroup/index') || Yii::app()->user->checkAccess('schedule/list') || Yii::app()->user->checkAccess('schedule/index'))
                    ),
                    array('label'=>'Отчёты', 'url'=>'', 'items' => array (
                        array('label'=>'Занятия', 'url'=>array('stat/lessons'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('stat/lessons')),
                        array('label'=>'Финансы', 'url'=>array('stat/finance'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('stat/finance')),
                        array('label'=>'Реклама', 'url'=>array('advert/index'), 'visible'=>!Yii::app()->user->isGuest && (Yii::app()->user->checkAccess('advert/index'))),
                        array('label'=>'Операции', 'url'=>array('operations/index'), 'visible'=>!Yii::app()->user->isGuest && (Yii::app()->user->checkAccess('operations/index'))),
                    ), 'visible'=>!Yii::app()->user->isGuest && (Yii::app()->user->checkAccess('stat/lessons') || Yii::app()->user->checkAccess('stat/finance') || Yii::app()->user->checkAccess('advert/index') || Yii::app()->user->checkAccess('operations/index'))
					),
                    array('label'=>'Доступ', 'url'=>'', 'items' => array (
                        array('label'=>'Роли', 'url'=>array('access/groups'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('access/groups')),
                        array('label'=>'Права', 'url'=>array('access/actions'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('access/actions')),
                        array('label'=>'Архив', 'url'=>array('access/archive'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('access/archive')),
                        array('label'=>'Журнал', 'url'=>array('access/log'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('access/log')),
                    ), 'visible'=> !Yii::app()->user->isGuest && (Yii::app()->user->checkAccess('access/groups') || Yii::app()->user->checkAccess('access/actions') || Yii::app()->user->checkAccess('access/archive') || Yii::app()->user->checkAccess('access/log'))
                    ),
                    array('label'=>Yii::app()->user->name, 'url'=>'', 'items' => array (
                        array('label'=>'Редактировать', 'url'=>array('user/view', 'target' => 'profile')),
                        array('label'=>'Выйти', 'url'=>array('user/logout')),
                    ), 'visible'=> !Yii::app()->user->isGuest
                    ),

                ),
            )); ?>
        </div><!-- mainmenu -->

        <div class="container" id="page">

            <?php if ($flashMsg = Yii::app()->user->getFlash('system')): ?>
                <div class="alert alert-success" style="margin-top: 12px;">
                    <?php echo CHtml::encode($flashMsg); ?>
                </div>
            <?php endif; ?>

            <?php echo $content; ?>

            <div class="clear"></div>

        </div><!-- page -->

</div>
<div id="CFooter">
    <div id="footer">
        <div id="footerCenter">
            Copyright &copy; <?php echo date('Y'); ?> by <?php echo CHtml::link('Формула Образования','http://omegaedu.ru/'); ?>.<br/>
            Все права защищены.<br/>
            Поддержка <?php echo CHtml::link('Тимохин Игорь','http://igor-timohin.ru/'); ?><br/>
            <div id="footerFos"></div>
        </div>
    </div><!-- footer -->
</div>

</body>
</html>
