<?php
    $datePickerOptions = array(
        'dateFormat' => 'd M yy',
        'changeYear' => true,
    );

?>
<div class="row">
    <div class="span-5"><label for="datefrom">Дата</label>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'date',
            'options' => $datePickerOptions,
            'htmlOptions' => array('size' => 15),
        ));
        ?>
    </div>
</div>

<div class="clear"></div>