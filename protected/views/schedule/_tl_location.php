<div class="tl_location">
    <div class="tl_the_location" data-location="<?php echo $locationKey; ?>">
        <h3>
            <?php if (!$grouped || ($grouped && $firstInGroup)) echo Schedule::model()->dayList[Yii::app()->dateFormatter->format('c',$day) - 1]; ?>
            <?php if ($grouped): ?>
                <span style="float: right; margin-right: 6px;"><?php echo $groupName; ?></span>
            <?php endif; ?>
        </h3>
        <span class="clear"></span>
        <p class="tl_the_location_subtitle">
            <?php if (!$grouped || ($grouped && $firstInGroup)) echo str_replace('.','',Yii::app()->dateFormatter->format('d MMM yyyy',$day)); ?>
        </p>
    </div>
    <div class="tl_the_timeline">
        <div class="tl_the_timeline_content tl_slidable">
            <?php echo $events; ?>
        </div>
        <?php echo $eventsDetails; ?>
    </div>
    <div class="tl_clear">&nbsp;</div>
</div>
