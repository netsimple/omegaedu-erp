<?php
    $datePickerOptions = array(
        'dateFormat' => 'd M yy',
        'changeYear' => true,
    );

?>
<div class="row">
    <div class="span-4 first">
        <div class="row">
            <div class="span-4"><label for="datefrom">Период, с</label>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'datefrom',
                    'options' => $datePickerOptions,
                    'htmlOptions' => array('size' => 15),
                ));
                ?>
            </div>
        </div>

        <div class="clear"></div>

        <div class="row">
            <div class="span-4"><label for="dateto">по</label>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'dateto',
                    'options' => $datePickerOptions,
                    'htmlOptions' => array('size' => 15),
                ));?>
            </div>
        </div>

        <div class="clear"></div>
    </div>
    <?php if ($this->action->id != 'index') { ?>
        <div class="span-4 last">
            <div class="row labelinline span-4">

                <?php echo $form->label($model,'day'); ?><br>
                <?php echo $form->checkboxList($model, 'day', $model->dayList, array('separator'=>' <br>', 'labelOptions'=>array('class'=>'noBold'))); ?>
            </div>

            <div class="clear"></div>
        </div>
    <?php } ?>
</div>

<div class="clear"></div>