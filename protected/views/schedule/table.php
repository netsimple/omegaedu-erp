<?php
/* @var $this ScheduleController */

$table = '';
$dayStart = 13;
$endScheduleKey = count($schedules)-1;
$lastSchedule = $schedules[$endScheduleKey];

$locationKey=0;
$oneDay = 60*60*24;

$timeTableStart = CDateTimeParser::parse($schedules[0]->date,'d MMM yyyy');
$timeTableEnd = CDateTimeParser::parse($lastSchedule->date,'d MMM yyyy');

while ($timeTableStart <= $timeTableEnd) {
    $events = '';
    $eventsDetails = '';
    $currentDayGroups = array();
    $currentDaySchedules = array();

    foreach ($schedules as $scheduleKey => $schedule) {
        $dayStart = min($dayStart, $schedule->hours_start);
        if ($schedule->datetime_start >= $timeTableStart && $schedule->datetime_start < ($timeTableStart + $oneDay)) {
            $currentDaySchedules[] = $schedule;
            $currentDayGroups[$schedule->tr_group_id] = $schedule->group ? $schedule->group->name : $schedule->tr_group_id;
        }
    }

    if (count($currentDayGroups) < 2) {

        foreach ($currentDaySchedules as $currentDaySchedule) {
            $events .= $this->renderPartial('/schedule/_tl_event', array('schedule' => $currentDaySchedule, 'locationKey'=>$locationKey, 'dayStart'=>$dayStart), true);
            if (Yii::app()->user->checkAccess('schedule/update'))
                $eventsDetails .= $this->renderPartial('/schedule/_tl_event_details', array('schedule' => $currentDaySchedule), true);
        }

        $table .=  $this->renderPartial('/schedule/_tl_location', array(
            'locationKey'=>$locationKey,
            'events'=>$events,
            'eventsDetails'=>$eventsDetails,
            'day' => $timeTableStart,
            'grouped' => false,
            'firstInGroup' =>  false,
            'groupName' =>  false,
        ), true);

        $locationKey++;

    } else {

        $firstInGroup = true;
        foreach ($currentDayGroups as $groupId => $groupName) {
            foreach ($currentDaySchedules as $currentDaySchedule) {
                if ($currentDaySchedule->tr_group_id == $groupId) {
                    $events .= $this->renderPartial('/schedule/_tl_event', array('schedule' => $currentDaySchedule, 'locationKey'=>$locationKey, 'dayStart'=>$dayStart), true);
                    if (Yii::app()->user->checkAccess('schedule/update'))
                        $eventsDetails .= $this->renderPartial('/schedule/_tl_event_details', array('schedule' => $currentDaySchedule), true);
                }
            }

            $table .=  $this->renderPartial('/schedule/_tl_location', array(
                'locationKey'=>$locationKey,
                'events'=>$events,
                'eventsDetails'=>$eventsDetails,
                'day' => $timeTableStart,
                'grouped' => true,
                'firstInGroup' => $firstInGroup,
                'groupName' => $groupName,
            ), true);

            $events = '';
            $eventsDetails = '';
            $firstInGroup = false;
            $locationKey++;
        }

    }

    $timeTableStart += $oneDay;
}

?>

<!--<link rel="stylesheet" type="text/css" href="http://rikdevos.com/demos/timetable/css/style.css" media="screen" />-->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/js/timetable/css/timetable.css" media="screen" />
<link href='http://fonts.googleapis.com/css?family=Shanti' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/js/timetable/css/timetable.css"/>



<!-- Colorbox http://colorpowered.com/colorbox/ -->
<link media="screen" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/js/timetable/css/colorbox.min.css" />
<script src="<?php echo Yii::app()->baseUrl; ?>/js/timetable/js/jquery.colorbox-min.js"></script>

<!-- timetable script -->
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/timetable/js/timetable-script.js"></script>


<script type="text/javascript">
    // Prevent "event.layerX and event.layerY are broken and deprecated in WebKit. They will be removed from the engine in the near future."
    // in latest Chrome builds.
    (function () {
        // remove layerX and layerY
        var all = $.event.props,
            len = all.length,
            res = [];
        while (len--) {
            var el = all[len];
            if (el != 'layerX' && el != 'layerY') res.push(el);
        }
        $.event.props = res;
    } ());

    $(function(){
        $('.tl_program_button').click(function(){
            if ($(this).attr('href') != '#')
                location.href = $(this).attr('href');
        });
    })

    function eventDelDialog(scheduleId, who) {
        $('#scheduleDetails'+scheduleId).find(who).toggleClass('hidden');
        $('#scheduleDetails'+scheduleId).find('[data-val]').attr('data-val', 'false');
        $.cookie('program', null);
    }
</script>


<div class="tl_container" style="width: <?php echo isset($_GET['frame']) ? '690' : '1200' ?>px;">
    <div data-autoscroll="false" class="tl_timeline" data-increment="2" data-hours="<?php echo (23 - $dayStart); ?>" data-indicatorbar="false">
        <div class="tl_time_indicator">
            <div class="tl_slidable">
                <ul>
                    <?php while ($dayStart <= 22) {
                        echo "<li>{$dayStart}:00</li>";
                        $dayStart++;
                    } ?>
                </ul>
                <div class="tl_clear">&nbsp;</div>
            </div>
        </div>
        <a class="tl_next" href="#"></a><a class="tl_previous" href="#"></a>

        <?php echo $table;?>

    </div>
</div>
