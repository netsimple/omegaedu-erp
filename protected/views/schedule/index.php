<div>
    <?php
         ?>
    <?php

        if (!isset($_GET['frame'])) {
            $this->widget('application.components.widgets.AddLinkWidget', array('accessKey' => 'schedule/create', 'actionUrl' => array('create')));
         ?>
            <p>Чтобы посмотреть расписание нажмите "фильтр".</p>

            <a style="text-decoration: none; color: #000000;" href="#" onclick="$('.formScheduleShowHide').toggle(); $(this).find('.icon-toggle-right').toggleClass('icon-toggle-down');">
            <span class="icon-toggle-right"></span> Фильтр
        </a>
        <?php } ?>



    <?php

    if (!isset($_GET['frame']) && Yii::app()->user->model->is_partner || !isset($_GET['frame']) && Yii::app()->user->model->is_manager) {

        echo CHtml::link('Интеграция', '#', array('onclick'=>'$("#embedDialog").dialog("open"); return false;', 'style'=>'float: right; margin-right: 11px;'));

        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id'=>'embedDialog',
            'htmlOptions'=>array('style'=>'display: none;'),
            'options'=>array(
                'title'=>'Код для интеграции расписания на сайт',
                'width'=>'470',
                'height'=>'auto',
                'autoOpen'=>false,
                'dialogClass'=> 'alert',
            ),
        ));
        $url = Yii::app()->createUrl('schedule/index', array_merge($_GET,['frame'=>true]));
        echo CHtml::encode('<iframe src="http://'.$_SERVER['HTTP_HOST'].$url.'" width="750" height="500" align="left" frameborder="0"></iframe>');

        $this->endWidget('zii.widgets.jui.CJuiDialog');

    }
    ?>

</div>

<div class="row hidden formScheduleShowHide">
    <?php
        $this->renderPartial('_form',array('model'=>$model));
    ?>
</div>

<?php if ($flashMsg = Yii::app()->user->getFlash('schedule')): ?>
    <div class="alert alert-success" style="margin-top: 12px;">
        <?php echo CHtml::encode($flashMsg); ?>
    </div>
<?php endif; ?>

<?php if ($flashMsg = Yii::app()->user->getFlash('success')): ?>
    <div style="border: 1px solid green; background: lightgreen; padding-left: 15px; padding-top: 5px; padding-bottom: 5px; margin-bottom: 15px; margin-top: 10px;">
        <?php echo CHtml::encode($flashMsg); ?>
    </div>
<?php endif; ?>

<div id="table">
    <?php
        $schedules = $model->search();
        if (count($schedules))
            $this->renderPartial('table', array('schedules' => $schedules));
        else
            if (isset($_GET['Schedule']))
                echo '<p>По данным параметрам занятий не найдено</p>';
    ?>
</div>