<div id="scheduleDetails<?php echo $schedule->id ?>" class="tl_event_details" data-start="20:00" data-event="<?php echo $schedule->id ?>">
    <div class="tl_description_text">
        <?php // echo any text ?>

        <?php if (Yii::app()->user->checkAccess('schedule/delete')) { ?>
        <div class="delScheduleAndRelated hidden">
            <p>
                Вы уверены, что хотите удалить расписание?
            </p>
            <a href="<?php echo Yii::app()->baseUrl; ?>/schedule/delete/<?php echo $schedule->id ?>?andRelated" data-eventid="<?php echo $schedule->id ?>" data-event="<?php echo $schedule->id ?>" class="tl_program_button">
                Удалить расписание
            </a>

            <a onclick="eventDelDialog(<?php echo $schedule->id ?>, '.tl_description_bottom, .delScheduleAndRelated')" href="#" data-eventid="<?php echo $schedule->id ?>" data-event="<?php echo $schedule->id ?>" class="tl_program_button">
                Отмена
            </a>

        </div>
        <div class="delSchedule hidden">
            <p>
                Вы уверены, что хотите удалить урок?
            </p>
            <a href="<?php echo Yii::app()->baseUrl; ?>/schedule/delete/<?php echo $schedule->id ?>" data-eventid="<?php echo $schedule->id ?>" data-event="<?php echo $schedule->id ?>" class="tl_program_button">
                Удалить урок
            </a>

            <a onclick="eventDelDialog(<?php echo $schedule->id ?>, '.tl_description_bottom, .delSchedule')" href="#" data-eventid="<?php echo $schedule->id ?>" data-event="<?php echo $schedule->id ?>" class="tl_program_button">
                Отмена
            </a>
            </del>
            <?php } ?>
        </div>
    </div>

    <div class="tl_description_bottom">

        <?php if (Yii::app()->user->checkAccess('schedule/delete')) { ?>
            <?php if ($schedule->datetime_start > time()): ?>
            <a onclick="eventDelDialog(<?php echo $schedule->id ?>, '.tl_description_bottom, .delScheduleAndRelated')" href="#" data-eventid="<?php echo $schedule->id ?>" data-event="<?php echo $schedule->id ?>" class="tl_program_button">
                Удалить расписание
            </a>
            <?php endif; ?>
            <a onclick="eventDelDialog(<?php echo $schedule->id ?>, '.tl_description_bottom, .delSchedule')" href="#" data-eventid="<?php echo $schedule->id ?>" data-event="<?php echo $schedule->id ?>" class="tl_program_button">
                Удалить урок
            </a>
        <?php } ?>
        <?php if ($schedule->datetime_start > time()): ?>
        <a href="<?php echo Yii::app()->baseUrl; ?>/schedule/update/<?php echo $schedule->id ?>" data-eventid="<?php echo $schedule->id ?>" data-event="<?php echo $schedule->id ?>" class="tl_program_button">
            Редактировать расписание
        </a>
        <?php endif; ?>
        <a href="<?php echo Yii::app()->baseUrl; ?>/schedule/view/<?php echo $schedule->id ?>" data-eventid="<?php echo $schedule->id ?>" data-event="<?php echo $schedule->id ?>" class="tl_program_button">
            Редактировать урок
        </a>

    </div>
    <div class="tl_clear">&nbsp;</div>
</div>