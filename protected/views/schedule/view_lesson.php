<?php

	$css = <<<CSS
		.is-debt {
			color: red;
			font-weight: bold;
		}
CSS;

	Yii::app()->clientScript->registerCss('debt',$css);

?>

<h1>Занятие</h1>

<?php

	$startTimestamp = $model->datetime_start;

	$datetime_start = Yii::app()->dateFormatter->format('dd MMM yyyy, HH:mm',$model->datetime_start);
	$datetime_end = Yii::app()->dateFormatter->format('dd MMM yyyy, HH:mm',$model->datetime_end);

	$model->datetime_start = Yii::app()->dateFormatter->format('yyyy-MM-dd HH:mm',$model->datetime_start);
	$model->datetime_end = Yii::app()->dateFormatter->format('yyyy-MM-dd HH:mm',$model->datetime_end);

	$attributes = array(
		'tr_group_id' => array(
			'name' => 'tr_group_id',
			'type' => 'raw',
			'value'=> CHtml::link(CHtml::encode($model->group->name), array('trgroup/view','id'=>$model->tr_group_id), array('target' => '_blank')),
		),
		'tr_course_id' => array(
			'name' => 'tr_course_id',
			'type' => 'raw',
			'value'=> CHtml::link(CHtml::encode($model->course->name), array('trcourse/view','id'=>$model->tr_course_id), array('target' => '_blank')),
		),
		'datetime_start' => array(
			'name' => 'datetime_start',
			'value'=> $datetime_start,
			'editable' => array(
				'type'   	 => 'combodate',
				'format' 	 => 'YYYY-MM-DD HH:mm',
				'viewformat' => 'DD MMM YYYY, HH:mm',
				'template' 	 => 'DD / MMM / YYYY, HH : mm',
			),
		),
		'datetime_end' => array(
			'name' => 'datetime_end',
			'value'=> $datetime_end,
			'editable' => array(
				'type'   	 => 'combodate',
				'format' 	 => 'YYYY-MM-DD HH:mm',
				'viewformat' => 'DD MMM YYYY, HH:mm',
				'template' 	 => 'DD / MMM / YYYY, HH : mm',
			),
		),
		'description' => array(
			'name' => 'description',
			'type' => 'text',
			'editable' => array(
				'type' => 'textarea',
			),
		),
		'did_not_come' => array(
			'name' => 'did_not_come',
			'type' => 'raw',
			'value'=> $model->did_not_come ? 'Да' : 'Нет',
			'editable' => array(
				'type' => 'select',
				'source' => array('0'=>'Нет', '1'=>'Да'),
			),
		),
		'late' => array(
			'name' => 'late',
			'type' => 'raw',
			'value'=> $model->late ? 'Да' : 'Нет',
			'editable' => array(
				'type' => 'select',
				'source' => array('0'=>'Нет', '1'=>'Да'),
			),
		),
		'is_conducted' => array(
			'name' => 'is_conducted',
			'type' => 'raw',
			'value'=> $startTimestamp <= time() ? $model->conductedList[$model->is_conducted] : 'Планируемое занятие',
			'editable' => array(
				'type' => 'select',
				'source' => $model->getConductedList(),
			),
		),
	);


	if (Yii::app()->user->checkAccess('owner')){
		$attributes['user_id']=array(
			'name' => 'user_id',
			'value'=> $model->group->teacher->name,
			'type' => 'raw',
		);
		$attributes['held_user_id']=array(
			'name' => 'held_user_id',
			'value'=> $model->held_user->name,
			'type' => 'raw',
			'editable' => array(
				'type' => 'select',
				'source' => array(''=>'')+CHtml::listData(UserMain::model()->findAll(array(
					'condition'=>'t.is_deleted=0 and (t.city_id='.$model->held_user->city_id.' OR t.city_id=0)',
					'join'=>'inner join {{user_teacher}} ut ON ut.user_id=t.id',
					'order'=>'t.fullname',
					'group'=>'t.id'
				)), 'id', 'fullname'),
			),
		);
		/*$attributes['rate_two']=array(
			'name' => 'rate_two',
			'editable' => array(
				'type' => 'textarea',
			),
		);*/
	}
?>

<?php
    $this->widget('editable.EditableDetailView', array(
		'data'       => $model,
		'url'        => $this->createUrl('ajaxUpdate'),
		'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
		'emptytext'  => 'Редактировать',
		'attributes' => $attributes,
    ));
?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/moment-lang-ru.js'); ?>

<br/><br/>

<form class="row">
    <?php echo CHtml::label('Клиент:',''); ?>
    <?php $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
        'name'=>'client_name',
        'sourceUrl' => array('getClientName', 'project_id'=>$model->project_id),
        'options'=>array(
            'minLength'=>'2',
            'select' =>'js: function(event, ui) {
					this.value = ui.item.fullname;
					$("#client-id").val(ui.item.id);
					return false;
				}',
        ),
        'htmlOptions'=>array(
            'size'=>'75',
        ),
    )); ?>
    <?php echo CHtml::hiddenField('client_id','0',array('id' => 'client-id')); ?>
    <?php echo CHtml::submitButton('Добавить', array('id' => 'continue-btn')); ?>
</form>



<?php
if (($dataProvider = $clientLog->search()) && $dataProvider->itemCount && $startTimestamp <= time()) {

	$is_skipped = array(
		'name' => 'is_skipped',
		'type' => 'raw',
		'value' => '$data->is_skipped == TrClientLog::SKIPPED_UNDEFINED ? CHtml::tag("span", array("style"=>"color: red"), $data->isSkippedList[$data->is_skipped]) : $data->isSkippedList[$data->is_skipped]',
	);
	if ($model->getUpdateAccess('clients')) {
		$is_skipped['class'] = 'editable.EditableColumn';
		$is_skipped['editable'] = array(
			'type'     => 'select',
			'url'      => $this->createUrl('ajaxUpdateClients'),
			'source'   => $clientLog->getIsSkippedList(),
			'success'  => 'function(response, newValue) {
								if(!response) { return "Неизвестная ошибка."; }
								if(response.errors) { return response.errors; }
								$.fn.yiiGridView.update("client-grid");
							}',
		);
	}
	$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'client-grid',
		'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
		'dataProvider'=>$dataProvider,
		'columns'=>array(
			array(
				'name' => 'client_id',
				'type' => 'raw',
				'value'=> 'CHtml::link($data->client->fullname,array("client/view", "id" => $data->client_id))',
			),
			$is_skipped,
			array(
				'name' => 'debt',
				'type' => 'raw',
				'value' => '$data->debt ? $data->debt : ""',
			),
		),
	));

}
?>