<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'trlesson-grid',
    'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->past()->dataProvider,
	'filter' => $model,
	'afterAjaxUpdate' => 'function(){
		jQuery("#datetime_past").datepicker({
			dateFormat: "d M yy",
			monthNames: '.CJSON::encode(array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь')).',
			monthNamesShort: '.CJSON::encode(array('янв','февр','марта','апр','мая','июня','июля','авг','сент','окт','нояб','дек')).',
			dayNames: '.CJSON::encode(array('Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота')).',
            dayNamesShort: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            dayNamesMin: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            firstDay: 1,
			changeYear: true
		});
	}',
	'columns'=>array(
		array(
			'name' => 'datetime_start',
			'header'=>'Занятие',
			'type' => 'raw',
			'value' => 'CHtml::link(Yii::app()->dateFormatter->format("dd MMM yyyy, HH:mm",$data->datetime_start),array("schedule/view", "id" => $data->id))',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'datetime_past',
                'attribute' => 'datetime_start',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
		),
		array(
			'name' => 'group_name',
			'type' => 'raw',
			'value'=> 'CHtml::link(CHtml::encode($data->group->name),array(\'trgroup/view\', \'id\' => $data->tr_group_id),array("target" => "_blank"))',
		),
		array(
			'name' => 'tr_center_id',
			'type' => 'raw',
			'value'=> 'CHtml::link($data->center->name,array("trcenter/view", "id" => $data->tr_center_id))',
			'filter' => CHtml::activeDropDownList($model,'tr_center_id',$model->centerList,array('empty' => 'Все')),
		),
		array(
			'name' => 'course_name',
			'type' => 'raw',
			'value'=> 'CHtml::link($data->course->name,array("trcourse/view", "id" => $data->tr_course_id))',
		),
		array(
			'name' => 'teacher_name',
			'type' => 'raw',
			'value'=> '$data->group->teacher ? $data->group->teacher->profileLink : ""',
		),
		'is_conducted' => array(
			'name' => 'is_conducted',
			'type' => 'raw',
			'value' => '$data->conductedList[$data->is_conducted]',
			'filter' => CHtml::activeDropDownList($model,'is_conducted',$model->conductedList,array('empty' => 'Все')),
		),
		array(
			'class' => 'ButtonColumn',
			'template' => '{delete}',
		),
	),
)); ?>