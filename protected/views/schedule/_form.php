<?php
/* @var $form CActiveForm */
?>

<script type="text/javascript">

    $(function(){

        var ScheduleForm = {
            action: '<?php echo $this->action->id?>',
            controller: '<?php echo Yii::app()->baseUrl; ?>/schedule/',
            filtered: <?php echo (int)isset($_GET['Schedule']); ?>,
            form: $("#schedule-form"),
            center: $("#Schedule_tr_center_id"),
            group: $('#Schedule_tr_group_id'),
            course: $('#Schedule_tr_course_id'),

            groupListUpdate: function() {
                var ids = [];
                var $this = this;

                this.group.find(':selected, :checked').each(function(){
                    ids.push($(this).attr('value'));
                });

                this.group.load(this.controller + 'getGroups?' + this.form.serialize(), function(){
//                    $this.group.find('[value]').each(function(){
//                        //console.log(ids);
//                        //console.log($(this).attr('value'));
//                        if ($.inArray($(this).attr('value'), ids))
//                            $(this).attr('checked','checked').attr('selected','selected');
//                    });
                });

            },
            courseListUpdate: function() {
                var ids = [];
                var $this = this;

                this.group.find(':selected, :checked').each(function(){
                    ids.push($(this).attr('value'));
                });

                this.course.load(this.controller + 'getCourse?' + this.form.serialize(), function(){
//                    $this.group.find('[value]').each(function(){
//                        //console.log(ids);
//                        //console.log($(this).attr('value'));
//                        if ($.inArray($(this).attr('value'), ids))
//                            $(this).attr('checked','checked').attr('selected','selected');
//                    });
                });
            },
            reStyle: function() {
                this.form.find('.span-5').addClass('labelinline').find('label:first').hide().after('<br>');
                this.form.find('label[for=Schedule_tr_center_id]').show();
                this.form.find('.span-5 > span').addClass('noBold');
                if (this.filtered)
                    this.form.find('label').show();
            },
            init: function() {

                if (this.action == 'index') {
                    this.reStyle();
                }

                var $this = this;
                this.center.change(function(){
                    $this.form.find('label').show();
                    $this.groupListUpdate();
                    $this.courseListUpdate();
                });

            }
        };

        ScheduleForm.init();
    });

</script>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'schedule-form',
	'action'=>Yii::app()->baseUrl.'/schedule/'.(isset($_GET['frame']) ? 'frame-'.$_GET['frame'] : $this->action->id . '/' . $model->id),
    'method'=> $this->action->id == 'index' ? 'get' : 'post',
	'enableAjaxValidation'=>false,
)); ?>
 
    <?php echo $form->errorSummary($model); ?>

    <div class="row">

        <?php if (Yii::app()->user->checkAccess('owner')) { ?>
            <div class="cityList">
                <?php echo $form->dropDownList($model, 'city_id', City::listArray(),array('empty' => 'Все города'));?>
            </div>
        <?php } ?>

        <div class="span-<?php echo isset($_GET['frame'])?'8':'11'?>">
            <?php if ($this->action->id != 'index') { ?>
                <div class="row">
                    <div class="span-5 first  labelinline">
                        <?php echo $form->label($model,'time_start'); ?><br>
                        <?php echo $form->dropDownList($model, 'hours_start', $model->hourList); ?>
                        <?php echo $form->dropDownList($model, 'minutes_start', $model->minuteList); ?>
                    </div>
                    <div class="span-6 last labelinline">
                        <?php echo $form->label($model,'time_end'); ?><br>
                        <?php echo $form->dropDownList($model, 'hours_end', $model->hourList); ?>
                        <?php echo $form->dropDownList($model, 'minutes_end', $model->minuteList); ?>
                    </div>
                </div>
            <?php } ?>
            <div class="clear"></div>

            <div class="row">
                <div class="span-5">
                    <?php echo $form->label($model,'tr_center_id'); ?>
                    <?php
                        if ($this->action->id == 'index') {
                            echo $form->checkBoxList($model, 'tr_center_id', TrCenter::model()->getCenterNames(Yii::app()->user->projectIdCompare));
                        } else {
                            if (($groupId = Yii::app()->request->getParam('id', false)) && ($trGroup = TrGroup::model()->findByPk($groupId)))
                                $listCenter = array($trGroup->tr_center_id => $trGroup->center->name);
                            else
                                $listCenter = false;
                            echo $form->dropDownList($model, 'tr_center_id', $listCenter ? $listCenter : $model->list, array('empty'=>$listCenter?null:'Выберите центр'));
                        }
                    ?>
                </div>
                <div class="span-5">
                    <?php echo $form->label($model,'tr_group_id'); ?>
                    <?php
                        if ($this->action->id == 'index') {
                            $listGroup = isset($_GET['Schedule']['tr_center_id']) ? CHtml::listData(TrGroup::getByCenterId($_GET['Schedule']['tr_center_id']), 'id', 'name') : array();
                            echo $form->checkBoxList($model, 'tr_group_id', $listGroup);
                        } else {
                            $listGroup = $model->tr_center_id ? CHtml::listData(TrGroup::getByCenterId($model->tr_center_id), 'id', 'name') : array();
                            
                            if (($groupId = Yii::app()->request->getParam('id', false)) && ($trGroup = TrGroup::model()->findByPk($groupId)))
                                $listGroup = array($groupId => $trGroup->name);
                            
                            echo $form->dropDownList($model, 'tr_group_id', $listGroup, array('empty' => $groupId ? null : '--'));
                        }
                    ?>
                </div>
            </div>

            <div class="clear"></div>

            <div class="row">
                <div class="span-5">
                    <?php echo $form->label($model,'tr_course_id'); ?>
                    <?php
                    if ($this->action->id == 'index') {
                        $listData = isset($_GET['Schedule']['tr_center_id']) ? CHtml::listData(TrCourse::findAllByCenter($_GET['Schedule']['tr_center_id']), 'id', 'name') : array();
                        echo $form->checkBoxList($model, 'tr_course_id', $listData);
                    } else {
                        echo $form->dropDownList($model, 'tr_course_id', TrCourse::listArray(), array('empty'=>'--'));
                    }
                    ?>
                </div>
            </div>
            <div class="clear"></div>

        </div>
        <div class="span-<?php echo isset($_GET['frame'])?'8':'11'?>">
            <?php
                $btnText = '';
                switch ($this->action->id) {
                    case 'create':
                        $btnText = 'Добавить';
                        $tabs = array();
                        $tabs['Период'] = $this->renderPartial('_form_period',array('model' => $model,'form' => $form),true);
                        $tabs['Дата'] = $this->renderPartial('_form_date',array('model' => $model,'form' => $form),true);
                        $this->widget('zii.widgets.jui.CJuiTabs', array(
                            'tabs'=>$tabs,
                            'options'=>array(
                                'collapsible'=>false,
                                'selected'=>0,
                            ),
                            'htmlOptions'=>array(
                                'style'=>'width:100%;'
                            ),
                        ));
                        break;
                    case 'update':
                        $btnText = 'Сохранить';
                        $this->renderPartial('_form_period',array('model' => $model,'form' => $form));
                        break;
                    case 'index':
                        $btnText = 'Показать';
                        $this->renderPartial('_form_period',array('model' => $model,'form' => $form));
                        break;
                }

            ?>
        </div>
    </div>
    <div class="clear"></div>
    <div class="row submit" style="margin-top: 40px">
        <?php echo CHtml::submitButton($btnText, array('class'=>'btn btn-primary btn-small')); ?>
    </div>

    <div class="clear"></div>
 
<?php $this->endWidget(); ?>
</div><!-- form -->