<?php $groupObj = TrGroup::model(); $dataProvider = new CArrayDataProvider($model->groups); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'groups-grid',
    'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=> $dataProvider,
	'columns'=>array(
		
		'name' => array(
			'name' => CHtml::activeLabel($groupObj,'name'),
			'header' => 'Группа',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->name),array("trgroup/view", "id" => $data->id))',
		),
		
		'center.name' => array(
			'name' => CHtml::activeLabel($groupObj,'tr_center_id'),
			'type' => 'raw',
			'value' => 'CHtml::link($data->center->name,array("trcenter/view", "id" => $data->tr_center_id))',
		),
		
		'course.name' => array(
			'name' => CHtml::activeLabel($groupObj,'tr_course_id'),
			'type' => 'raw',
			'value' => 'CHtml::link($data->course->name,array("trcourse/view", "id" => $data->tr_course_id))',
		),
		
		'teacher_id' => array(
			'name' => CHtml::activeLabel($groupObj,'teacher_id'),
			'type' => 'raw',
			'value' => '$data->teacher ? $data->teacher->profileLink : ""',
		),
		
		'member_count' => array(
			'name' => CHtml::activeLabel($groupObj,'member_count'),
			'type' => 'raw',
			'value' => '$data->member_count',
		),

        array(
            'class'=>'ButtonColumn',
            'template' => '{delete}',
            'buttons'=>array(
                'delete' => array(
                    'url' => 'array("client/deleteFromGroup", "groupId" => $data->id, "clientId" => Yii::app()->request->getQuery("id"))',
                ),
            ),
        ),
        
	),
)); ?>

<div>
    Добавить в группу: <?= CHtml::dropDownList('groupId', '', CHtml::listData(TrGroup::model()->findAll('project_id = '.$model->project_id), 'id', 'name'));?>
    <input type="button" value="Добавить" class="btn btn-small btn-primary" id="addToGroupButton"/>

    <script>
        $(function(){
            $('#addToGroupButton').click(function(){
                $.ajax({
                    type: 'get',
                    url: '/client/addToGroup',
                    data: {groupId: $('#groupId').val(), clientId: <?= $model->id; ?>},
                    success: function(data) {
                        jQuery('#groups-grid').yiiGridView('update');
                    }
                });
            })
        })
    </script>
</div>