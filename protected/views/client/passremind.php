<?php

$this->pageTitle=Yii::app()->name . ' - Восстановить пароль';
$this->breadcrumbs=array(
	'Профиль',
);
?>

<h1>Восстановление пароля</h1>

<p>Пожалуйста, заполните все нижеприведенные поля:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'remind-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Поля, отмеченные звёздочкой (<span class="required">*</span>), обязательны для заполнения.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Сбросить пароль'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
