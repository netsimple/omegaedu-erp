<style type="text/css">
    td a {
        word-wrap: break-word;
    }
</style>
<?php
    $user = Yii::app()->user->getId();
    $userModel = UserMain::model()->findByPk($user);
    $centers = [];

    if($userModel->relatedCenters){
        foreach ($userModel->relatedCenters as $center){
            $centers[$center->tr_center_id] = $center->tr_center_id;
        }
    }
    ; ?>
<div>
	<?php $this->widget('application.components.widgets.AddLinkWidget', array('htmlOptions' => array('onclick' => '$(".client-add-form").toggle(); return false;'))); ?>
</div>


<div class="hidden client-add-form">
    <?php
    if (Yii::app()->user->checkAccess('client/create')) {
        $userModel = Yii::app()->user->model;
        $managerList = UserMain::getManagerList();
        $clientModel = new Client(($managerList && ($userModel->is_partner || $userModel->is_owner)) ? 'createWithManagers' : 'createDefault');

        $this->renderPartial('_form', array('model'=>$clientModel, 'managers' => $managerList));
    }

    ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'client-grid',
	'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	/* не удалять
	'afterAjaxUpdate' => 'function(){
		jQuery("#reg_date").datepicker({
			dateFormat: "d M yy",
			monthNames: '.CJSON::encode(array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь')).',
			monthNamesShort: '.CJSON::encode(array('янв','февр','марта','апр','мая','июня','июля','авг','сент','окт','нояб','дек')).',
			dayNames: '.CJSON::encode(array('Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота')).',
            dayNamesShort: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            dayNamesMin: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            firstDay: 1,
			changeYear: true
		});
		jQuery("#phone_num").mask(
			"+7-999-999-9999",
			{ placeholder:"*" }
		);
	}',
	*/
	'columns'=>array(
        'city_id' => array(
            'name' => 'city_id',
            'type' => 'raw',
            'value'=> 'City::nameByProjectId($data->project_id)',
            'filter'=> CHtml::activeDropDownList($model,'city_id',City::listArray(),array('empty' => 'Все города')),
            'visible'=> Yii::app()->user->model->checkRole('owner'),
        ),
        array(
            'class' => 'editable.EditableColumn',
            'name' => 'status',
            'type' => 'raw',
            'filter'=> $model->getStatusList(false),
            'headerHtmlOptions' => array('style' => 'width: 150px'),
            'editable' => array(
                'type'     => 'select',
                'url'      => $this->createUrl('ajaxUpdate'),
                'source'   => $model->getStatusList(),
				'success'  => 'function(response, newValue) {
									if(!response) {
										return "Неизвестная ошибка.";
									}

									if(response.errors) {
										 return response.errors;
									}
								}',
                'options'  => array(    //custom display
                    'display' => 'js: function(value, sourceData) {
                          var selected = $.grep(sourceData, function(o){ return value == o.value; }),
                              icons = ' . json_encode(Client::getStatusIcons()) . ';
                          $(this).html("<img style=\'height: 16px;\' src=\""+icons[value]+"\"> " + selected[0].text);
                      }'
                ),
            ),
			'value' => 'CHtml::image($data->statusIcons[$data->status], "", array("style"=>"height: 16px;")) . " " . $data->getStatusText()',
        ),
        array(
            'class' => 'editable.EditableColumn',
            'name' => 'source',
            'type' => 'raw',
            'filter' => $model->sourceList,
            'headerHtmlOptions' => array('style' => 'width: 100px'),
            'value'=> '$data->sourceText',
            'editable' => array(
                'type'     => 'select',
                'url'      => $this->createUrl('ajaxUpdate'),
                'source'   => $model->sourceList,
                'success'  => 'function(response, newValue) {
									if(!response) {
										return "Неизвестная ошибка.";
									}

									if(response.errors) {
										 return response.errors;
									}
								}'
            ),
        ),
		array(
			'name' => 'fullname',
			'type' => 'html',
			'value'=> 'CHtml::link($data->fullname,array(\'client/view\', \'id\' => $data->id))',
		),
		/* не удалять
		'reg_date' => array(
			'name' => 'reg_date',
			'type' => 'text',
			'value' => 'Yii::app()->dateFormatter->format(\'dd MMM yyyy\', $data->reg_date)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'reg_date',
                'attribute' => 'reg_date',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
		),
		*/
        'phone_num' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'phone_num',
           'headerHtmlOptions' => array('style' => 'width: 110px'),
           'editable' => array(    //editable section
               'url'      => $this->createUrl('ajaxUpdate'),
               'placement'  => 'right',
               'options'     => array(
                   'placeholder'=> '+7-922-333-4444',
               ),
               'validate'   => 'function(value) {
                   if (!validatePhone(value)) return "Телефона должен быть в формате +7-922-333-4444";
               }'
            ),
        ),
        'school' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'school',
           'headerHtmlOptions' => array('style' => 'width: 110px'),
           'editable' => array(    //editable section
                'url'      => $this->createUrl('ajaxUpdate'),
                'placement'  => 'right',
            ),
        ),
		'course_list' => array(
			'name' => 'course_list',
			'type' => 'raw',
			'value'=> '$data->contractCourseLinks',
			'filter' => CHtml::activeDropDownList($model,'course_list',CHtml::listData(TrCourse::model()->findAll(),'id','name'),array('empty' => 'Все')),
		),
		'group_name' => array(
			'name' => 'group_name',
			'type' => 'raw',
			'value' => '$data->groupLinks',
		),
		'center__id' => array(
			'name' => 'center__id',
			'type' => 'raw',
			'value' => '$data->center->name',
			'filter' => CHtml::activeDropDownList($model,'center__id',TrCenter::model()->getCenterNamesIn(false,$centers),array('empty' => 'Все')),
		),
		'rate_one' => array(
			'name' => 'rate_one',
			'type' => 'raw',
			'value' => '$data->rate_one',
			'htmlOptions' => array('style'=>'font-size: 11px;'),
		),
		'comments_text' => array(
			'name' => 'comments_text',
			'type' => 'raw',
			'value' => '$data->commentsText',
			'htmlOptions' => array('style'=>'font-size: 11px;'),
            'filter' =>''
		),
		array(
			'class'=>'ButtonColumn',
			'template' => '{comment} {create} {delete}',
			'buttons'=>array(
				'comment' => array(
					'imageUrl' => Yii::app()->baseUrl.'/images/icon-import.png',
					'url' => 'array("client/comment", "id" => $data->id)',
			    ),
				'create' => array(
					'url' => 'array("contract/create", "id" => $data->id)',
					'label' => 'Добавить договор',
					'imageUrl' => Yii::app()->baseUrl.'/images/icon-add-contract.png',
					'visible' => '!$data->contracts',
				),
			    'update' => array(
					'url' => 'array("client/update", "id" => $data->id)',
			    ),
			    'delete' => array(
					'url' => 'array("client/delete", "id" => $data->id)',
			    ),
			),
		),
	),
));




$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
	'id'=>'commentDialog',
	'htmlOptions'=>array('style'=>'display: none;'),
	'options'=>array(
		'title'=>'Добавление комментария',
		'width'=>'470',
		'height'=>'auto',
		'autoOpen'=>false,
		'dialogClass'=> 'alert',
	),
)); ?>

	<form method="post" action="">
		<div class="row">
			<label for="ClientComments_content">Комментарий</label>
			<textarea id="ClientComments_content" name="ClientComments[content]" rows="3" cols="60"></textarea>
		</div>
		<div class="row">
			<input class="btn" type="submit" value="Добавить" name="yt0">
		</div>
	</form>
<?php
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>


