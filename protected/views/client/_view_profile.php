<?php

$openContract = (count($model->contracts)==1);
$openContractId = 0;
if ($openContract)
    $openContractId = $model->contracts[0]->id;

	$this->widget('application.components.widgets.TabLinksWidget', array(
		'links' => array(
			array('label' => 'Список договоров', 'url' => array('contract/index', 'Contract[client_name]' => $model->fullname), 'visible' => (count($model->contracts)>1)),
			array('label' => 'Открыть договор', 'url' => array('contract/pdf','id' => $openContractId),'visible' => $openContract),
			array('label' => 'Добавить договор', 'url' => array('contract/create', 'id' => $model->id), 'visible' => Yii::app()->user->checkAccess('contract/create')),
		),
	));
?>

<?php

$attributes = array(
	'status' => array(
		'name' => 'status',
		'editable' => array(
			'type' => 'select',
			'source' => $model->statusList,
		),
	),
	'status_date' => array(
		'name' => 'status_date',
		'type' => 'raw',
		'value' => Yii::app()->dateFormatter->format('d MMM yyyy, HH:mm', $model->status_date),
	),
	'fullname',
	'birthday' => array(
		'name' => 'birthday',
		'editable' => array(
			'type' => 'date',
		),
	),
	'phone_num',
    'email' => array(
        'name' => 'email',
    ),
	'source' => array(
		'name' => 'source',
		'editable' => array(
			'type' => 'select',
			'source' => $model->sourceList,
		),
	),
	'school',
	'manager_id' => array(
		'name' => 'manager_id',
		'type' => 'raw',
		'editable' => array(
			'type' => 'select',
			'source' => $model->managerList,
		),
	),
	'parent_name',
	'parent_phone',
	'region',

	'other' => array(
		'name' => 'other',
		'editable' => array(
			'type' => 'textarea',
		),
	),
	'skype',
    'social_yandex',
    'social_twitter',
    'social_vk',
    'social_facebook',
    'social_mailru',
	'rate_one' => array(
		'name' => 'rate_one',
		'editable' => array(
			'type' => 'textarea',
		),
	),
	/*'rate_two' => array(
		'name' => 'rate_two',
		'editable' => array(
			'type' => 'textarea',
		),
	),*/
);
?>

<?php
    $this->widget('editable.EditableDetailView', array(
    'data'       => $model,
    'url'        => $this->createUrl('ajaxUpdate'),
    'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
    'emptytext'  => 'Редактировать',
    'attributes' => $attributes
    ));
?>

<?php $this->renderPartial('_profile_comments', array('clientId' => $model->id, 'model' => $comments)); ?>