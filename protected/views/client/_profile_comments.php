<?php if ($model): ?>

<?php if (Yii::app()->user->checkAccess('client/view','comment')): ?>

<br/><br/>
<b><?php echo CHtml::encode('История взаимодействия'); ?></b><br/><br/>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'comments-grid',
    'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	'columns'=>array(
        'datetime' => array(
            'name' => 'datetime',
            'type' => 'raw',
            'value' => '$data->datetime ? Yii::app()->dateFormatter->format("dd MMM yyyy",$data->datetime) : ""',
        ),
        'username' => array(
            'name' => 'username',
            'type' => 'raw',
            'value' => '$data->user ? CHtml::link(($data->user) ? $data->user->fullname : "Пусто",array("user/view", "id" => $data->user_id)) : ""'
        ),
        'content',
	),
)); ?>

<?php endif; ?>
<?php if (Yii::app()->user->checkAccess('client/update','addcomment')): ?>
    <div class="form">
        <?php echo CHtml::beginForm(array('client/comment', 'id' => $clientId)); ?>
        <div class="row">
            <?php echo CHtml::activeLabel($model, 'content'); ?>
            <?php echo CHtml::activeTextArea($model, 'content', array('cols' => 60, 'rows' => 3)); ?>
        </div>
        <div class="row">
            <?php echo CHtml::submitButton('Добавить', array('class' => 'btn')); ?>
        </div>
        <?php echo CHtml::endForm(); ?>
    </div>
<?php endif; ?>

<?php endif; ?>