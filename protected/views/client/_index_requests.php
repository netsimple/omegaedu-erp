<h2>Заявки из других источников (олимпиады, сайт, др.)</h2>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'request-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
        'city_id' => array(
            'type' => 'raw',
            'header' => 'Город',
            'value'=> 'City::nameByProjectId($data->project_id)',
            'visible'=> Yii::app()->user->model->checkRole('owner'),
        ),
		array(
            'class'=> 'editable.EditableColumn',
			'name' => 'fullname',
			'type' => 'raw',
			'value'=> '$data->fullname',
			'editable' => array(
				'type' => 'text',
				'url'  => $this->createUrl('ajaxUpdateRequest'),
				'validate'   => 'function(value) {
					if (!value) return "ФИО обязательны для заполнения."
				}',
                'display' => 'js: function(value, sourceData) {
					$(this).css("color",value ? "#222" : "#f00");
					$(this).html(value);
                }',
			),
			'htmlOptions' => array('class' => 'field-fullname'),
		),
		array(
            'class'=> 'editable.EditableColumn',
			'name' => 'email',
			'type' => 'raw',
			'value'=> '$data->email',
			'editable' => array(
				'type' => 'text',
				'url'  => $this->createUrl('ajaxUpdateRequest'),
				'options'     => array(
					'placeholder'=> 'mail@mail.ru',
				),
			),
			'htmlOptions' => array('class' => 'field-email'),
		),
		array(
            'class'=> 'editable.EditableColumn',
			'name' => 'source',
			'type' => 'raw',
            'filter' => Client::model()->sourceList,
			'value'=> 'Client::model()->getSourceText($data->source)',
			'editable' => array(
				'type' => 'select',
                'source'   => Client::model()->sourceList,
				'url'  => $this->createUrl('ajaxUpdateRequest'),
			),
			'htmlOptions' => array('class' => 'field-source'),
		),
		array(
            'class'=> 'editable.EditableColumn',
			'name' => 'status',
			'type' => 'raw',
            'filter' => Client::getStatusList(),
            'value'=> 'Client::model()->getStatusText($data->status)',
			'editable' => array(
				'type' => 'select',
                'source'   => Client::getStatusList(),
				'url'  => $this->createUrl('ajaxUpdateRequest'),
			),
			'htmlOptions' => array('class' => 'field-status'),
		),
		array(
            'class'=> 'editable.EditableColumn',
			'name' => 'phone_num',
			'type' => 'raw',
			'value'=> '$data->phone_num',
			'editable' => array(
				'type' => 'text',
				'url'  => $this->createUrl('ajaxUpdateRequest'),
                'options'     => array(
                   'placeholder'=> '+7-922-333-4444',
                ),
				/*
                'validate'   => 'function(value) {
                   if (!validatePhone(value)) return "Телефона должен быть в формате +7-922-333-4444";
                }',
                */
                'display' => 'js: function(value, sourceData) {
					$(this).css("color","#222");
					$(this).html(value);
                }',
			),
			'htmlOptions' => array('class' => 'field-phone'),
		),
		array(
			'name' => 'datetime',
			'type' => 'raw',
			'value' => '$data->datetime ? Yii::app()->dateFormatter->format("d MMM, HH:mm",$data->datetime) : ""',
			'htmlOptions' => array('nowrap' => true),
		),
		array(
            'class'=> 'editable.EditableColumn',
			'name' => 'school',
			'type' => 'raw',
			'value'=> '$data->school',
			'editable' => array(
				'type' => 'text',
				'url'  => $this->createUrl('ajaxUpdateRequest'),
			),
		),
		array(
            'class'=> 'editable.EditableColumn',
			'name' => 'social_vk',
			'type' => 'raw',
			'value'=> '$data->social_vk',
			'editable' => array(
				'type' => 'text',
				'url'  => $this->createUrl('ajaxUpdateRequest'),
			),
		),
		array(
            'class'=> 'editable.EditableColumn',
			'name' => 'other',
			'type' => 'raw',
			'value'=> '$data->other',
			'editable' => array(
				'type' => 'textarea',
				'url'  => $this->createUrl('ajaxUpdateRequest'),
			),
		),
		array(
			'class'=>'ButtonColumn',
			'template' => '{import} {delete}',
			'buttons'=>array(
			    'import' => array(
                    'label' => 'Импортировать в список клиентов',
                    'imageUrl' => Yii::app()->baseUrl . '/images/icon-import.png',
					'url' => 'array("client/complete", "id" => $data->id)',
					'click'=>'js: function(){
						var fullname = $(this).parent().parent().find(".field-fullname a").first().text();
						if (fullname) return true;
						alert("Необходимо заполнить ФИО.");
						return false;
					}',
			    ),
			    'delete' => array(
					'url' => 'array("client/reject", "id" => $data->id)',
			    ),
			),
		),
	),
)); ?>