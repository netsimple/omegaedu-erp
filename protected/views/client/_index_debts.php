<?php

	// Yii::app()->clientScript->scriptMap['jquery.js'] = false;

	$css = <<<CSS
		.is-debt {
			color: red;
			font-weight: bold;
		}
CSS;

	Yii::app()->clientScript->registerCss('debt',$css);
	// Yii::app()->clientScript->registerCoreScript('jquery.ui');
	
	$js = <<<JSCRIPT

		
			function refreshTable(scenario) {
				$('#scenario').val(scenario);
				$.ajax({
					url: '{$this->createUrl("debtList")}',
					data: $('#filter').serialize(),
					dataType: 'json',
					type: 'POST',
					success: function(data) {
						if (data.centers) {
							if (data.refreshcenters) {
								$('#filter-centers').html(data.centers);
								$('#filter-centers').fadeIn(300,'swing');
							}
						} else {
							$('#filter-groups').fadeOut(300,'swing');
							$('#filter-centers').fadeOut(300,'swing');
							$('#filter-groups').empty();
							$('#filter-centers').empty();
						}
						if (data.groups) {
							if (data.refreshgroups) {
								$('#filter-groups').html(data.groups);
								$('#filter-groups').fadeIn(300,'swing');
							}
						} else {
							$('#filter-groups').fadeOut(300,'swing');
							$('#filter-groups').empty();
						}
						$('#container').html(data.grid);
					}
				});
			}

			jQuery('body').on('change','.select-partners',function(){ refreshTable('partner'); });
			

JSCRIPT;

	Yii::app()->clientScript->registerScript('updateFilter',$js,CClientScript::POS_END);

?>

<?php echo CHtml::beginForm('','post',array('id' => 'filter')); ?>
<?php echo CHtml::activeHiddenField($selector,'update',array('id' => 'scenario')); ?>
<div>
	<div id="filter-partners">
		<div style="float:left; width: 250px; padding-right: 5px;">
			<h4>Партнёры</h4>
			<div style="padding-bottom: 5px; padding-top: 5px;">
			<?php
				$this->widget('ext.EchMultiSelect.EchMultiSelect', array(
					'model' => $selector,
					'dropDownAttribute'  => 'partners',
					'data'  => $selector->basePartners,
					'dropDownHtmlOptions'=> array(
						'class' => 'select-partners',
						'style'=>'width:250px;',
					),
					'options' => array(
						'noneSelectedText' => 'Выберите партнёра',
						// 'ajaxRefresh' => true,
					),
				));
			?>
			</div>
		</div>
	</div>
	
	<div id="filter-centers"></div>
	<div id="filter-groups"></div>
	
	<div style="clear: left;"></div>
</div>
	
<?php echo CHtml::endForm(); ?>
	
<div id="container"><?php $this->renderPartial('_index_debts_grid',array('dataProvider' => $dataProvider)); ?></div>