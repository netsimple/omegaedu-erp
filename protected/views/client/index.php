<?php

$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=> array(
		'Список' => $this->renderPartial('_index_clients',array('model' => $model),true),
		'Необработанные заявки' => array('ajax'=>array('requestList')),
		'Должники' => array('ajax'=>array('debtList')),
	),
    'options'=>array(
        'collapsible'=>false,
        'selected'=>$item,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));

?>