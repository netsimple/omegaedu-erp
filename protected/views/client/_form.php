<?php
    $user = Yii::app()->user->getId();
    $userModel = UserMain::model()->findByPk($user);
    $centers = [];

    if($userModel->relatedCenters){
        foreach ($userModel->relatedCenters as $center){
            $centers[$center->tr_center_id] = $center->tr_center_id;
        }
    }


	$clientStatusOther = Client::STATUS_OTHER;
	$clientSourceOther = Client::SOURCE_OTHER;
	$js = <<<JSCRIPT
		$(document).ready(function(){
			if ($('#Client_status').val() == $clientStatusOther) {
				$('#Client_status_text').show();
			}
			if ($('#Client_source').val() == $clientSourceOther) {
				$('#Client_source_text').show();
			}


			$(document).on('change', '#Client_status', function(){
				if ($(this).val() == $clientStatusOther) {
					$('#Client_status_text').show();
				} else {
					$('#Client_status_text').hide();
				}
			});
			$(document).on('change', '#Client_source', function(){
				if ($(this).val() == $clientSourceOther) {
					$('#Client_source_text').show();
				} else {
					$('#Client_source_text').hide();
				}
			});
		});
JSCRIPT;
if (!Yii::app()->request->isAjaxRequest)
    Yii::app()->clientScript->registerScript('clientStatusUpdate',$js,CClientScript::POS_READY);

?>

<div class="form form-client-create">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'client-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<div style="float: left; margin-right: 15px;">
			<?php echo $form->dropDownList($model, 'status', $model->statusList); ?>
		</div>
		<div style="float: left;">
			<?php echo $form->textField($model, 'status_text', array('style'=>($model->status == $clientStatusOther ? '' : 'display: none;'))); ?>
		</div>
		<div style="clear: left;"></div>
	</div>

    <div class="row">
        <?php echo $form->label($model,'fullname'); ?>
        <?php echo $form->textField($model, 'fullname', array('size'=>75)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'birthday'); ?>
        <?php echo $form->textField($model, 'birthday'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'phone_num'); ?>
        <?php
			$this->widget('CMaskedTextField', array(
				'model' => $model,
				'attribute' => 'phone_num',
				'mask' => '+7-999-999-9999',
				'placeholder' => '*',
				'htmlOptions' => array('size' => 75, 'placeholder'=>'+7-922-333-4444'),
			));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'email'); ?>
        <?php echo $form->textField($model, 'email', array('size'=>75)) ?>
    </div>

	<div class="row">
		<?php echo $form->label($model,'source'); ?>
		<div style="float: left; margin-right: 15px;">
			<?php echo $form->dropDownList($model, 'source', $model->sourceList); ?>
		</div>
		<div style="float: left;">
			<?php echo $form->textField($model, 'source_text', array('style'=>($model->source == $clientSourceOther ? '' : 'display: none;'))); ?>
		</div>
		<div style="clear: left;"></div>
	</div>

    <div class="row">
        <?php echo $form->label($model,'center__id'); ?>
        <div style="float: left; margin-right: 15px;">
            <?php echo $form->dropDownList($model, 'center__id', TrCenter::model()->getCenterNamesIn(false,$centers)); ?>
        </div>
        <div style="clear: left;"></div>
    </div>

    <div class="row submit">

        <?php echo CHtml::ajaxButton('Добавить', Yii::app()->baseUrl.'/client/create', array(
            'type' => 'POST',
            'update' => '#client-form',
        ), array('class' => 'btn btn-primary')); ?>

    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->