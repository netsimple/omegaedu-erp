<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs=array(
	'Города',
);

$this->menu=array(
	array('label'=>'Добавить', 'url'=>array('create')),
);
?>

<h1>Города</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'city-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'name',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
		),
	),
)); ?>
