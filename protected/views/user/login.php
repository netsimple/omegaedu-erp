<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Вход в систему';
?>


<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<div class="row">
        <div class="span-2 upper"><?php echo $form->labelEx($model,'username'); ?></div>
        <div class="span-5">
            <?php echo $form->textField($model,'username', array('placeholder'=>'mail@gmail.com')); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
	</div>
    <div class="clear"></div>

	<div class="row">
		<div class="span-2 upper"><?php echo $form->labelEx($model,'password'); ?></div>
		<div class="span-5">
            <?php echo $form->passwordField($model,'password', array('placeholder'=>'******')); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>
	</div>
    <div class="clear"></div>
	<div class="row rememberMe">
        <div class="span-5 push-2">
            <?php echo $form->checkBox($model,'rememberMe'); ?>
            <?php echo $form->label($model,'rememberMe'); ?>
            <?php echo $form->error($model,'rememberMe'); ?>
        </div>
	</div>
    <div class="clear"></div>

	<div class="row buttons">
		<div class="span-5 push-2"><?php echo CHtml::submitButton('Вход', array('class'=>'btn upper foColor')); ?></div>
	</div>
    <div class="clear"></div>
<?php $this->endWidget(); ?>
</div><!-- form -->
<div class="span-5 push-2 passremind">
    <p><?php echo CHtml::link('забыли пароль?',array('passremind')); ?></p>
</div>
<div class="clear"></div>
