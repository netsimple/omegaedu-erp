    <div class="row">
        <?php echo $form->label($model,'education'); ?>
        <?php echo $form->textField($model, 'education', array('size'=>35)) ?>
    </div>
 
    <div class="row">
        <?php echo $form->label($model,'birthdate'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'birthdate',
                'attribute' => 'birthdate',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ));
		?>
    </div>