    <div class="row">
        <?php echo $form->label($model,'organization'); ?>
        <?php echo $form->textField($model, 'organization', array('size'=>75)) ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'position'); ?>
        <?php echo $form->textField($model, 'position', array('size'=>75)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'legal_addr'); ?>
        <?php echo $form->textField($model, 'legal_addr', array('size'=>75)) ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'real_addr'); ?>
        <?php echo $form->textField($model, 'real_addr', array('size'=>75)) ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'details'); ?>
        <?php echo $form->textArea($model,'details', array('cols' => 45, 'rows' => 7)) ?>
    </div>