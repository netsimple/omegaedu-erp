<?php
    $user = Yii::app()->user->getID();
    $userModel = UserMain::model()->findByPk($user);

    $centersArr = array();
    $centersIDs = array();
    foreach ($model->relatedCenters as $center){
        $findCenter = TrCenter::model()->findByPk($center->tr_center_id);
        if($findCenter){
            $centersArr[$findCenter->id] = $findCenter->name;
            $centersIDs[] = $findCenter->id;
        }

    }


    $jsManager = CJSON::encode($centersArr);

    $js = <<<JSCRIPT
		$(document).ready(function(){
			var selectedManagerIds = [];
			var selectedManagerNames = [];
			var jsManager = $jsManager;
			for (var i in jsManager) {
				var appendText = '<div id="manager-' + i + '" style="border: 1px solid black; background: #E5F1F4; padding: 5px; margin: 10px;">' +
				'<a href="#" class="manager-delete-btn" data-id="' + i + '" style="float: right; color: black; text-decoration: none;">[x]</a>' +
				'<input name="TrCenter[centers][]" type="hidden" value="' + i + '" />' +
				jsManager[i] +
				'</div>';
				$('#manager-add-container').append(appendText);
				selectedManagerIds.push(i);
				selectedManagerNames.push(jsManager[i]);
			}
			$('#manager-add-btn').click(function(){
				var appendText = '<div id="manager-' + $('#manager-add-src').val() + '" style="border: 1px solid black; background: #E5F1F4; padding: 5px; margin: 10px;">' +
				'<a href="#" class="manager-delete-btn" data-id="' + i + '" style="float: right; color: black; text-decoration: none;">[x]</a>' +
				'<input name="TrCenter[centers][]" type="hidden" value="' + $('#manager-add-src').val() + '" />' +
				$('#manager-add-src :selected').text() +
				'</div>';
				$('#manager-add-container').append(appendText);
				selectedManagerIds.push($('#manager-add-src').val());
				selectedManagerNames.push($('#manager-add-src :selected').text());
				$('#manager-add-src :selected').remove();
				if ($('select[id=manager-add-src] option').size()) {
					$('#manager-add-src :first').select();
				} else {
					$('#manager-add-src').parent().parent().hide();
				}
			});
			
			$('.manager-delete-btn').click(function(){
				var id = $(this).attr('data-id');
				$('#manager-'+ id).remove();
			});
		});
JSCRIPT;

    Yii::app()->clientScript->registerScript('addManagerJS',$js,CClientScript::POS_READY);
?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'trcenter-form',
    'enableAjaxValidation'=>false,
)); ?>

<fieldset style="width: 40%; float: left">
    <legend>Центры</legend>
    <div class="row">
        <?php if ($userModel->is_owner == 1): ?>
            <?php if(!empty(TrCenter::model()->getCenterNamesCriteria(false,$centersIDs))){ ?>
            <div style="float: left; margin-right: 15px;">
                <?php echo CHtml::dropDownList('centers','', TrCenter::model()->getCenterNamesCriteria(false,$centersIDs), array('id' => 'manager-add-src')) ?>
            </div>
            <div style="float: left;">
                <?php echo CHtml::button('Добавить', array('id' => 'manager-add-btn')); ?>
            </div>
            <div style="clear: left;"></div>
        <?php }; ?>
        <?php elseif (!$userModel->manager): ?>
            Нет доступных для выбора центров.
        <?php endif; ?>
    </div>
    <div class="row" id="manager-add-container">

    </div>
</fieldset>

<div class="row submit">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array('class' => 'btn btn-primary')); ?>
</div>

<?php $this->endWidget(); ?>

