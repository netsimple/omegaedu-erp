<div style="margin-top: 15px;">
<?php
if ($dataProvider = $model->payment) {
    $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'lessons1-grid',
        'dataProvider'=>$dataProvider,
        'template' => '{items}{pager}',
        'columns'=>array(
            array(
				'header'=>'Дата проведения занятия',
                'name' => 'begin',
				'type' => 'raw',
				'value' => 'CHtml::link($data["begin"], array("schedule/view", "id" => $data["id"]))'
            ),
            array(
				'header'=>'Кол-во учеников',
                'name' => 'count',
            ),
			array(
				'header'=>'Оплата',
				'name'=>'pay',
            ),
			array(
				'header'=>'Примечание',
				'name'=>'info',
            ),
        ),
    ));
}

?>
</div>