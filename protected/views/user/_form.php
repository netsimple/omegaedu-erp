<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profile-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>
 
    <?php echo $form->errorSummary($model); ?>
 
    <div class="row">
        <?php echo $form->label($model,'email'); ?>
        <?php echo $form->textField($model, 'email', array('size'=>75)) ?>
    </div>
 
    <div class="row">
        <?php echo $form->label($model,'fullname'); ?>
        <?php echo $form->textField($model, 'fullname', array('size'=>75)); ?>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'avatar_name'); ?>
	<?php
	    if ($model->avatar_name) {
			echo CHtml::image(Yii::app()->getBaseUrl().'/'.Yii::app()->params['uploadDirectory'].'/avatars/'.$model->avatar_name);
			echo '<br/>';
	    }
	?>
        <?php echo $form->fileField($model, 'avatar_name') ?>
    </div>
    
    <div class="row">
        <?php echo $form->label($model,'phone_num'); ?>
        <?php echo $form->textField($model, 'phone_num', array('size'=>75)) ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'hire_date'); ?>
        <?php // echo $form->dateField($model, 'hire_date', array('size'=>10)) ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'hire_date',
                'attribute' => 'hire_date',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ));
		?>
    </div>
	
	<?php
		$additKey = Yii::app()->user->id == $model->id ? 'selfcomment' : 'comment';
		if (Yii::app()->user->checkAccess('user/update', $additKey)):
	?>
    <div class="row">
        <?php echo $form->label($model,'comments'); ?>
        <?php echo $form->textArea($model, 'comments', array('cols' => 60, 'rows' => 3)) ?>
    </div>
	<?php endif; ?>
	
    <div class="row">
        <?php echo $form->label($model,'social_google'); ?>
        <?php echo $form->textField($model, 'social_google', array('size'=>75)); ?>
    </div>
	
	<div class="row">
        <?php echo $form->label($model,'social_yandex'); ?>
        <?php echo $form->textField($model, 'social_yandex', array('size'=>75)); ?>
    </div>
	
	<div class="row">
        <?php echo $form->label($model,'social_twitter'); ?>
        <?php echo $form->textField($model, 'social_twitter', array('size'=>75)); ?>
    </div>
	
	<div class="row">
        <?php echo $form->label($model,'social_vk'); ?>
        <?php echo $form->textField($model, 'social_vk', array('size'=>75)); ?>
    </div>
	
	<div class="row">
        <?php echo $form->label($model,'social_facebook'); ?>
        <?php echo $form->textField($model, 'social_facebook', array('size'=>75)); ?>
    </div>
	
	<div class="row">
        <?php echo $form->label($model,'social_mailru'); ?>
        <?php echo $form->textField($model, 'social_mailru', array('size'=>75)); ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'skype'); ?>
        <?php echo $form->textField($model, 'skype', array('size'=>75)) ?>
    </div>
	
	<?php foreach ($model->mainRoles as $currentRole) { $this->renderPartial('_form_'.$currentRole, array('model' => $model[$currentRole], 'form' => $form, 'target' => $target, 'parent' => $model)); } ?>
	
    <div class="row">
        <?php echo $form->label($model,'documents'); ?>
        <?php $this->renderPartial('_profile_documents', array('documents'=>$model->documents, 'allowDelete'=>false)); ?>
        <?php echo $form->fileField($model, 'documents[]', array('multiple'=>1)) ?>
    </div>
 
    <div class="row submit">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
    </div>
 
<?php $this->endWidget(); ?>
</div><!-- form -->