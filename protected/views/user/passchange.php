<?php

$this->pageTitle=Yii::app()->name . ' - Смена пароля';

?>

<h1>Сменить пароль</h1>

<p>Пожалуйста, заполните все нижеприведенные поля:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'restore-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Поля, отмеченные звёздочкой (<span class="required">*</span>), обязательны для заполнения.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'password_old'); ?>
		<?php echo $form->passwordField($model,'password_old'); ?>
		<?php echo $form->error($model,'password_old'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password_new'); ?>
		<?php echo $form->passwordField($model,'password_new'); ?>
		<?php echo $form->error($model,'password_new'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'password_retype'); ?>
		<?php echo $form->passwordField($model,'password_retype'); ?>
		<?php echo $form->error($model,'password_retype'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Изменить пароль'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
