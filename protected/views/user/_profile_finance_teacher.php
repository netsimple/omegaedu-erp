<?php

$js = <<<JSCRIPT
    $(document).ready(function(){
       $('.checkbox-column input[type=checkbox]').click(function(){
            var arr = [];
            if ($(this).attr('id') == 'lessons-grid_c0_all') {
                if ($('#lessons-grid_c0_all').prop('checked')) {
                    $('.checkbox-column input[type=checkbox]').each(function(){
                        if ($(this).attr('id') != 'lessons-grid_c0_all') arr.push($(this).val());
                    });
                }
            } else {
                $('.checkbox-column input[type=checkbox]').each(function(){
                    if ($(this).attr('id') != 'lessons-grid_c0_all' && $(this).prop('checked')) arr.push($(this).val());
                });
            }
            if (arr) {
                $.ajax({
                    url: '{$this->createUrl('ajaxCourseFinances', array('id' => $model->id))}',
                    type: 'post',
                    data: {ids: arr},
                    success: function(result){
                        result = JSON.parse(result);
                        $('#lessons-price').html(result.income);
                        $('#lessons-cost').html(result.costs);
                        $('#lessons-total').html(result.costs - result.income);
                    }
                });
            } else {
                $('#lessons-price').html(0);
                $('#lessons-cost').html(0);
                $('#lessons-total').html(0);
            }
       });
    });
JSCRIPT;

Yii::app()->clientScript->registerScript('lessons-selector',$js,CClientScript::POS_READY);

?>

<div style="margin-top: 15px;">
<?php

if ($dataProvider = $model->lessonsData) {
    $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'lessons-grid',
        'dataProvider'=>$dataProvider,
        'template' => '{items}{pager}',
        'columns'=>array(
            array(
                'class' => 'CCheckBoxColumn',
                'selectableRows' => 2,
                'checked' => 'true',
            ),
            array(
                'name' => 'id',
                'type' => 'raw',
                'value'=> 'CHtml::link(CHtml::encode($data->name),array("trgroup/view", "id" => $data->id))',
            ),
            array(
                'name' => 'conducted_success',
                'type' => 'raw',
                'value' => '$data->lessons_success',
            ),
            array(
                'name' => 'conducted_fail',
                'type' => 'raw',
                'value' => '$data->lessons_fail',
            ),
        ),
    ));
}

?>
</div>

<div style="margin-top: 5px; margin-bottom: 5px;">Начислено:&nbsp;<span id="lessons-price"><?php echo $_lessonsPrice = $model->getLessonsPrice(); ?></span></div>
<div style="margin-top: 5px; margin-bottom: 5px;">Оплачено:&nbsp;<span id="lessons-cost"><?php echo $_lessonsCost = $model->getLessonsCost(); ?></span></div>
    
<div style="margin-top: 15px;">Итого:&nbsp;<span id="lessons-total"><?php echo $_lessonsCost - $_lessonsPrice; ?></span></div>