<script type="text/javascript">
    $(window).load(function() {
        $('#imageToCropBlock').show();
    });
</script>
<span id="imageToCropBlock">
<?php $this->widget('ext.EFineUploader.EFineUploader',
    array(
        'id'=>'avatarUploader',
        'config'=>array(
            'autoUpload'=>true,
            'request'=>array(
                'endpoint'=>$this->createUrl('user/avatarupload'),
                'params'=>array(
                    'touid'=>$model->id,
                ),
            ),
            'text'=>array(
                'uploadButton'=>'<span class="downImg"></span>',
            ),
            'retry'=>array(
                'enableAuto'=>false,
                'maxAutoAttempts'=> 1,
                'preventRetryResponseProperty'=>true,
                'autoRetryNote'=> "Загружаем {retryNum}/{maxAuto}..."
            ),
            'chunking'=>array('enable'=>true,'partSize'=>100),//bytes
            'callbacks'=>array(
                'onComplete'=>"js:function(id, name, response){
                    if (response.error)
                        $('.qq-upload-status-text').html(response.error);
                    else
                        location.reload();
                }",
            ),
            'validation'=>array(
                'allowedExtensions'=>array('jpg','jpeg','png','gif'),
                'sizeLimit'=>10 * 1024 * 1024,//maximum file size in bytes
                //'minSizeLimit'=>2*1024*1024,// minimum file size in bytes
            ),
            'messages'=>array(
                'tooManyItemsError'=>'Ошибка загрузки',
                'typeError'=>"Файл {file} имеет неверное расширение. Разрешены файлы только с расширениями: {extensions}.",
                'sizeError'=>"Размер файла {file} велик, максимальный размер {sizeLimit}.",
                'minSizeError'=>"Размер файла {file} мал, минимальный размер {minSizeLimit}.",
                'emptyError'=>"{file} пуст",
                'onLeave'=>"Отменить"
            ),
        )
    ));
?>

<?php
if ($model->avatar_name) {
    echo CHtml::link('', '#', array('onclick'=>'$("#resizeDialog").dialog("open"); return false;', 'class'=>'editImg'));

    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id'=>'resizeDialog',
        'options'=>array(
            'title'=>'Dialog',
            'width'=>'auto',
            'height'=>'auto',
            'autoOpen'=>false,
            'dialogClass'=> 'alert',
            'open'=>'js:function(){$("#start_imageToCrop").click()}',
            'afterClose'=>'js:function(){ejcrop_reinitThumb("imageToCrop")}',
        ),
    ));

    ?>

    <script type="text/javascript">

        $(function(){
            $(".ui-dialog-titlebar").hide();
        })

        function ejcrop_reinitThumb(id) {
            $('#mirror_' + id).hide();
            $('#thumb_' + id).show();
            $("#resizeDialog").dialog("close");
            $(".userAvatar").attr("src", $(".userAvatar").attr("src").split('?')[0] + "?timestamp=" + new Date().getTime());
            $('.downImg').show();
            $('.editImg').show();
        }
        function ejcrop_showThumb(coords, id) {
            var rx = 100 / coords.w;
            var ry = 100 / coords.h;

            var height = $('#'+id).prop('height') / 2;
            var width = $('#'+id).prop('width') / 2;

            $('#thumbEvent_'+id).css({
                width: Math.round(rx * width) + 'px',
                height: Math.round(ry * height) + 'px',
                marginLeft: '-' + Math.round(rx * coords.x) + 'px',
                marginTop: '-' + Math.round(ry * coords.y) + 'px'
            });
            if ($('#mirror_'+id).css('display') == 'none') {
                $('#mirror_'+id).css('display', '');
                $('#thumb_'+id).css('display', 'none');
                $('.downImg').hide();
                $('.editImg').hide();
            }
        }
    </script>

    <?php
    $this->widget('ext.jcrop.EJcrop', array(
        // Image URL
        'url' => Yii::app()->getBaseUrl().'/'.Yii::app()->params['uploadDirectory'].'/avatars/orig/'.$model->avatarMain,
        // ALT text for the image
        'alt' => 'Обрезать это изображение',
        // options for the IMG element
        'htmlOptions' => array('id' => 'imageToCrop'),
        // Jcrop options (see Jcrop documentation)
        'options' => array(
            'minSize' => array(100, 100),
            'aspectRatio' => 1,
            'bgColor' => 'white',
            'onRelease' => "js:function() {ejcrop_cancelCrop(this);}",
        ),
        // if this array is empty, buttons will not be added
        'buttons' => array(
            'start' => array(
                'label' => 'crop start',
                'htmlOptions' => array(
                    'class' => 'myClass',
                    'style' => 'color:red;' // make sure style ends with « ; »
                )
            ),
            'cancel' => array(
                'label' => 'Отменить',
                'htmlOptions' => array(
                    'class' => 'btn',
                    'style' => 'float: right;',
                )
            ),
            'crop' => array(
                'label' => 'Применить',
                'htmlOptions' => array(
                    'class' => 'btn',
                )
            ),
        ),
        // URL to send request to (unused if no buttons)
        'ajaxUrl' => Yii::app()->getBaseUrl().'/user/avatarcrop',
        // Additional parameters to send to the AJAX call (unused if no buttons)
        'ajaxParams' => array('uid' => $model->id),
    ));



    $this->endWidget('zii.widgets.jui.CJuiDialog');
}
?>
</span>