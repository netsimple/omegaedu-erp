<div class="form form-partner-mini">

<?php
    if ($model->hasErrors())
        echo CHtml::errorSummary($model);
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profile-form',
	'action'=>Yii::app()->baseUrl.'/user/'.$target.'/create',
	'enableAjaxValidation'=>false,
)); ?>
 
    <div class="row">
        <?php echo $form->label($model,'email'); ?>
        <?php echo $form->textField($model, 'email', array('size'=>75)) ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
 
    <div class="row">
        <?php echo $form->label($model,'fullname'); ?>
        <?php echo $form->textField($model, 'fullname', array('size'=>75)); ?>
        <?php echo $form->error($model,'fullname'); ?>
    </div>

    <div style="display: none;">
        <?php foreach ($model->mainRoles as $currentRole) {
            $this->renderPartial('_form_'.$currentRole, array('model' => $model[$currentRole], 'form' => $form, 'target' => $target, 'parent' => $model));
        } ?>
    </div>

    <div class="row submit">

        <?php echo CHtml::ajaxButton('Добавить', Yii::app()->baseUrl.'/user/'.$target.'/create', array(
            'type' => 'POST',
            'update' => '.form-partner-mini',
        ), array('class' => 'btn btn-primary')); ?>

    </div>
 
<?php $this->endWidget(); ?>
</div><!-- form -->