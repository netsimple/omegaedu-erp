<?php

	$css = <<<CSS
	    a.active { font-weight: bold; text-decoration: none; }
CSS;

	if (!Yii::app()->clientScript->isCssRegistered('links-custom-style')) Yii::app()->clientScript->registerCss('links-custom-style',$css);

	if (Yii::app()->user->checkAccess('coupon/generate')) {
		$url = Yii::app()->createAbsoluteUrl('coupon/generate');
		$js = <<<JSCRIPT
		
			$(document).ready(function(){
		
				$('#coupon-item').click(function(){
					$.ajax({
						url: '$url',
						type: 'get',
						dataType: 'json',
						data: {userId: {$model->id}, couponName: $('#coupon-name').val()},
						success: function(data){

						    if (data.success) {
						        location.reload();
						    } else {
                                alert($(data.message).text());
                            }


						}
					});
				});
				
				$('.selectable').click(function () {
				   var w = window.getSelection();
				  // w.removeAllRanges();  // вроде бы старые промежутки удаляются автоматически при каждом клике
				   var range = document.createRange();
				   range.selectNode(this);
				   w.addRange(range);
				 });
			
			});
JSCRIPT;
		
		Yii::app()->clientScript->registerScript('couponGenerate', $js, CClientScript::POS_READY);
	}

?>

<?php if (Yii::app()->user->checkAccess('coupon/generate') && !$model->coupon): ?>
<div style="margin-bottom: 10px; text-align: right;">
    <input id="coupon-name" type="text" placeholder="код купона" /> <?php echo CHtml::link('Добавить','#',array('id' => 'coupon-item', 'class'=>'')); ?>
</div>
<?php endif; ?>

<div style="margin-top: 5px;">
	
	<?php if ($model->coupon): ?>
	<h5>Купон:
		<?php if ($model->isCurrentUser || Yii::app()->user->checkAccess('coupon/view')): ?>
			<span class="selectable"><?php echo $model->coupon->coupon; ?></span>
		<?php else: ?>
			<i>скрыт</i>
		<?php endif; ?>
	</h5>
	<div>Количество использований: <b><?php echo $model->coupon->activate_count; ?></b></div>

	<?php else: ?>
	<?php echo 'Купон не сгенерирован.'; ?>
	<?php endif; ?>
	
</div>