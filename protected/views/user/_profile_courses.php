<?php

$successJS = <<<JSCRIPT
	function (data,config) {
		data = JSON.parse(data);
		if(data.errors) {
			var msg = "";
            $.each(data.errors, function(k, v) { msg += v+"<br>"; });
			alert($('.control-group').html());
			$('.editable-error-block').addClass('ui-state-error').html(msg).show();
			return false;
        }
	}
JSCRIPT;

?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'courserates-grid',
	'dataProvider'=>$courses->search(),
	'template' => '{items}{pager}',
	'columns'=>array(
		'course_name',
		'group' => array(
			'name' => 'group',
			'type' => 'raw',
			'value' => '$data->group ? $data->group->name : "не определено"',
		),
		'centers_list' => array(
			'name' => 'centers_list',
			'type' => 'raw',
			'value' => '$data->centers_list',
		),
        'rate' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'rate',
           'editable' => array(
                'url'      => $this->createUrl('ajaxUpdateRate'),
                'placement'  => 'right',
				'success' => $successJS,
            ),
        ),
	),
)); ?>