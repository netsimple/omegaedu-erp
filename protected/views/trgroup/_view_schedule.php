<?php
    $this->widget('application.components.widgets.TabLinksWidget', array(
		'links' => array(
			array('label' => 'Добавление расписания', 'url' => array('schedule/create','id' => $modelId), 'visible' => Yii::app()->user->checkAccess('schedule/create')),
		),
	));
?>

<div style="margin-left: -20px;">
<?php

if (count($schedules = $schedule->search()))
    $this->renderPartial('/schedule/table', array('schedules' => $schedules));

?>
</div>
