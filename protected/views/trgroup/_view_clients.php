<?php

	$css = <<<CSS
		.is-debt {
			color: red;
			font-weight: bold;
		}
CSS;

	//Yii::app()->clientScript->registerCss('debt',$css);

    $this->widget('application.components.widgets.TabLinksWidget', array(
		'links' => array(
			array('label' => 'Редактировать клиентов', 'url' => array('update','id' => $model->id), 'visible' => Yii::app()->user->checkAccess('trgroup/update')),
		),
	));
?>

<?php
if ($members && ($dataProvider = new CArrayDataProvider($members, array('pagination' => false, 'sort' => array('defaultOrder' => 'fullname ASC'))))) {

	$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'client-grid',
		'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
		'dataProvider'=>$dataProvider,
		'columns'=>array(
			array(
                'name' => 'client_id',
				'header'=> 'Клиент',
				'type' => 'raw',
				'value'=> 'CHtml::link($data->fullname,array("client/view", "id" => $data->id))',
			),
			array(
				'name' => 'debt',
				'header'=> 'Списано',
				'type' => 'raw',
				'value' => '$data->getDebtByGroupId('.$model->id.') ? $data->getDebtByGroupId('.$model->id.') : ""',
				'htmlOptions' => array('class' => 'is-debt'),
			),
		),
	));

}
?>