<h1><?php echo $model->name; ?></h1>

<?php
$tabs = array();
$tabs['Профиль'] = $this->renderPartial('_view_profile',array('model' => $model),true);
$tabs['Расписание'] = $this->renderPartial('_view_schedule',array('modelId' => $model->id, 'schedule' => $schedule),true);
$tabs['Клиенты'] = $this->renderPartial('_view_clients',array('model' => $model, 'members' => $model->members),true);
$tabs['Финансы'] = $this->renderPartial('_view_finance',array('model' => $model, 'modelCosts' => $modelCosts),true,true);

switch ($tab) {
	case 'profile':
		$tab = 0;
		break;
	case 'schedule':
		$tab = 1;
		break;
	case 'clients':
		$tab = 2;
		break;
	case 'finance':
		$tab = 3;
		break;
	default: $tab = 0;
}

$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=>$tabs,
    'options'=>array(
        'collapsible'=>false,
        'selected'=>$tab,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));

?>