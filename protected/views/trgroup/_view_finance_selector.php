<?php

if ($dataProvider = $model->clientFinance) {
    $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'clients-grid',
        'dataProvider'=>$dataProvider,
        'template' => '{items}{pager}',
        'columns'=>array(
            array(
                'class' => 'CCheckBoxColumn',
                'selectableRows' => 2,
                'checked' => 'true',
            ),
            array(
                'name' => 'fullname',
                'type' => 'raw',
                'value'=> 'CHtml::link(CHtml::encode($data->fullname),array("client/view", "id" => $data->id))',
            ),
            array(
                'name' => 'client_appeared_stat',
                'type' => 'raw',
                'value' => '$data->getAppearedCount('.$model->id.')',
            ),
            array(
                'name' => 'client_paid_stat',
                'type' => 'raw',
                'value' => '$data->getPaidCount('.$model->id.')',
            ),
        ),
    ));
}

?>