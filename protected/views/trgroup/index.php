<div>
	<?php $this->widget('application.components.widgets.AddLinkWidget'); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'trgroup-grid',
    'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search(),
	// 'filter' => $model,
	'columns'=>array(

		'name' => array(
			'name' => 'name',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->name),array("trgroup/view", "id" => $data->id))',
		),

		'center.name' => array(
			'name' => 'tr_center_id',
			'type' => 'raw',
			'value' => 'CHtml::link(($data->center) ? $data->center->name : "Пусто",array("trcenter/view", "id" => $data->tr_center_id))',
		),

		'course.name' => array(
			'name' => 'tr_course_id',
			'type' => 'raw',
			'value' => 'CHtml::link($data->course ? $data->course->name : "предмет не выбран" ,array("trcourse/view", "id" => $data->tr_course_id))',
		),

		'first_lesson_date' => array(
			'name' => 'first_lesson_date',
			'type' => 'raw',
			'value' => '$data->firstLessonDate',
		),

		'teacher_id' => array(
			'name' => 'teacher_id',
			'type' => 'raw',
			'value' => '$data->teacher ? $data->teacher->profileLink : ""',
		),

		'member_count' => array(
			'name' => 'member_count',
			'type' => 'raw',
			'value' => '$data->member_count',
		),


		array(
			'class'=>'ButtonColumn',
			'template' => '{update} {delete}',
		),
	),
)); ?>