-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июн 22 2013 г., 21:55
-- Версия сервера: 5.5.27
-- Версия PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `edu`
--

-- --------------------------------------------------------

--
-- Структура таблицы `edu_access_table`
--

CREATE TABLE IF NOT EXISTS `edu_access_table` (
  `id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `skey` varchar(30) NOT NULL,
  `label` varchar(255) NOT NULL DEFAULT '',
  `allowed_partner` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allowed_manager` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `allowed_dealer` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `allowed_adman` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `allowed_teacher` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `skey` (`skey`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_advert`
--

CREATE TABLE IF NOT EXISTS `edu_advert` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date` varchar(10) NOT NULL,
  `member_count` int(10) unsigned NOT NULL,
  `activity_type` tinyint(1) NOT NULL,
  `create_date` int(10) unsigned NOT NULL,
  `update_date` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_advert_documents`
--

CREATE TABLE IF NOT EXISTS `edu_advert_documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `advert_id` int(10) unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filename` (`filename`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_client`
--

CREATE TABLE IF NOT EXISTS `edu_client` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `fullname` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `reg_date` int(10) unsigned NOT NULL DEFAULT '0',
  `phone_num` varchar(255) NOT NULL DEFAULT '',
  `skype` varchar(255) NOT NULL DEFAULT '',
  `other` tinytext NOT NULL,
  `status_date` int(10) unsigned NOT NULL DEFAULT '0',
  `school` varchar(255) NOT NULL DEFAULT '',
  `source` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `manager_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_name` varchar(255) NOT NULL DEFAULT '',
  `parent_phone` varchar(255) NOT NULL DEFAULT '',
  `region` varchar(255) NOT NULL DEFAULT '',
  `office` varchar(255) NOT NULL DEFAULT '',
  `social_google` varchar(255) NOT NULL DEFAULT '',
  `social_yandex` varchar(255) NOT NULL DEFAULT '',
  `social_twitter` varchar(255) NOT NULL DEFAULT '',
  `social_vk` varchar(255) NOT NULL DEFAULT '',
  `social_facebook` varchar(255) NOT NULL DEFAULT '',
  `social_mailru` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_client_comment`
--

CREATE TABLE IF NOT EXISTS `edu_client_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `datetime` int(10) unsigned NOT NULL,
  `content` tinytext NOT NULL,
  `is_system` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_client_request`
--

CREATE TABLE IF NOT EXISTS `edu_client_request` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `fullname` varchar(255) NOT NULL DEFAULT '',
  `phone_num` varchar(255) NOT NULL DEFAULT '',
  `datetime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_contract`
--

CREATE TABLE IF NOT EXISTS `edu_contract` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `parent_name` varchar(255) NOT NULL,
  `date` varchar(10) NOT NULL,
  `start_date` varchar(10) NOT NULL,
  `end_date` varchar(10) NOT NULL,
  `has_discount` tinyint(1) NOT NULL,
  `discount_reason` varchar(255) NOT NULL,
  `discount_coupon` varchar(25) NOT NULL DEFAULT '',
  `client_phone_home` varchar(255) NOT NULL,
  `client_phone_mobile` varchar(255) NOT NULL,
  `parent_phone_home` varchar(255) NOT NULL,
  `parent_phone_mobile` varchar(255) NOT NULL,
  `client_pass_num` varchar(255) NOT NULL,
  `client_pass_where` varchar(255) NOT NULL,
  `client_pass_when` varchar(255) NOT NULL,
  `client_addr_index` int(10) unsigned NOT NULL,
  `client_addr` varchar(255) NOT NULL,
  `parent_pass_num` varchar(255) NOT NULL,
  `parent_pass_where` varchar(255) NOT NULL,
  `parent_pass_when` varchar(255) NOT NULL,
  `parent_addr_index` int(10) unsigned NOT NULL,
  `parent_addr` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_contract_course`
--

CREATE TABLE IF NOT EXISTS `edu_contract_course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contract_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `total_time` float unsigned NOT NULL,
  `period_time` float unsigned NOT NULL,
  `period_price` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_coupon`
--

CREATE TABLE IF NOT EXISTS `edu_coupon` (
  `coupon` varchar(25) NOT NULL,
  `dealer_id` int(10) unsigned NOT NULL,
  `is_activated` tinyint(1) NOT NULL,
  `target_client_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`coupon`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_eauth`
--

CREATE TABLE IF NOT EXISTS `edu_eauth` (
  `id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_login_keys`
--

CREATE TABLE IF NOT EXISTS `edu_login_keys` (
  `user_id` int(10) unsigned NOT NULL,
  `key` varchar(40) NOT NULL,
  `expired` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_rel_group_member`
--

CREATE TABLE IF NOT EXISTS `edu_rel_group_member` (
  `tr_group_id` int(10) unsigned NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `tr_group_id` (`tr_group_id`,`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_rel_group_schedule`
--

CREATE TABLE IF NOT EXISTS `edu_rel_group_schedule` (
  `tr_group_id` int(10) unsigned NOT NULL,
  `schedule_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `tr_group_id` (`tr_group_id`,`schedule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_schedule`
--

CREATE TABLE IF NOT EXISTS `edu_schedule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `tr_center_id` int(10) unsigned NOT NULL,
  `day` tinyint(3) unsigned NOT NULL,
  `time` mediumint(8) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `create_date` int(10) unsigned NOT NULL,
  `update_date` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_tr_center`
--

CREATE TABLE IF NOT EXISTS `edu_tr_center` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `manager_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contacts` text NOT NULL,
  `create_date` int(10) unsigned NOT NULL DEFAULT '0',
  `update_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_tr_client_log`
--

CREATE TABLE IF NOT EXISTS `edu_tr_client_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tr_lesson_id` int(10) unsigned NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `is_skipped` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_tr_course`
--

CREATE TABLE IF NOT EXISTS `edu_tr_course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tr_center_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  `client_lvl` int(10) unsigned NOT NULL,
  `create_date` int(10) unsigned NOT NULL DEFAULT '0',
  `update_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_tr_group`
--

CREATE TABLE IF NOT EXISTS `edu_tr_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tr_course_id` int(10) unsigned NOT NULL,
  `teacher_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `create_date` int(10) unsigned NOT NULL DEFAULT '0',
  `update_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_tr_lesson`
--

CREATE TABLE IF NOT EXISTS `edu_tr_lesson` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tr_group_id` int(10) unsigned NOT NULL,
  `datetime` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `is_conducted` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_user_documents`
--

CREATE TABLE IF NOT EXISTS `edu_user_documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `filetitle` varchar(255) NOT NULL DEFAULT '',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `filesize` int(11) NOT NULL DEFAULT '0',
  `uploaded` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_user_main`
--

CREATE TABLE IF NOT EXISTS `edu_user_main` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `is_owner` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_partner` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_teacher` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_manager` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_dealer` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_adman` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `fullname` varchar(255) NOT NULL,
  `reg_date` int(10) unsigned NOT NULL,
  `lastvisit` int(10) unsigned NOT NULL,
  `ip_addr` varchar(15) NOT NULL,
  `avatar_name` varchar(255) NOT NULL DEFAULT '',
  `phone_num` varchar(255) NOT NULL DEFAULT '',
  `hire_date` varchar(10) NOT NULL DEFAULT '0000-00-00',
  `comments` tinytext NOT NULL,
  `social_google` varchar(255) NOT NULL DEFAULT '',
  `social_yandex` varchar(255) NOT NULL DEFAULT '',
  `social_twitter` varchar(255) NOT NULL DEFAULT '',
  `social_vk` varchar(255) NOT NULL DEFAULT '',
  `social_facebook` varchar(255) NOT NULL DEFAULT '',
  `social_mailru` varchar(255) NOT NULL DEFAULT '',
  `skype` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_user_manager`
--

CREATE TABLE IF NOT EXISTS `edu_user_manager` (
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_user_partner`
--

CREATE TABLE IF NOT EXISTS `edu_user_partner` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organization` varchar(255) NOT NULL DEFAULT '',
  `position` varchar(255) NOT NULL DEFAULT '',
  `legal_addr` varchar(255) NOT NULL DEFAULT '',
  `real_addr` varchar(255) NOT NULL DEFAULT '',
  `details` text NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `edu_user_teacher`
--

CREATE TABLE IF NOT EXISTS `edu_user_teacher` (
  `user_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `education` varchar(255) NOT NULL,
  `birthdate` varchar(10) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
