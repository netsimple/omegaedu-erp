<?php

class UserManager extends CActiveRecord {

    public $centers;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{user_manager}}';
    }

    public function getAjaxUpdateFields() {
        return array(

        );
    }

    public function rules() {
        return array(
        );
    }

    public function relations() {
        return array(
            // 'manager' => array(self::HAS_ONE, 'UserMain', 'id'),
            'documents' => array(self::HAS_MANY, 'UserDocuments', 'user_id'),
            'relatedCenters' => array(self::HAS_MANY, 'TrCenterManager', 'manager_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'user_id' => 'ID',
            'is_manager' => 'Руководитель',
            'is_adman' => 'Администратор',
            'documents' => 'Документы',
        );
    }

}


?>