<?php

class ContractCourse extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{contract_course}}';
    }
    
    public function relations() {
        return array(
            'contract' => array(self::BELONGS_TO, 'Contract', 'contract_id'),
            'course' => array(self::BELONGS_TO, 'TrCourse', 'course_id'),
        );
    }
    
    public function rules() {
        return array(
              array('course_id, total_time, period_time, period_price', 'required'),
              array('course_id', 'numerical', 'integerOnly' => true, 'min' => 1),
              array('course_id', 'exist', 'attributeName' => 'id', 'className' => 'TrCourse'),
              array('total_time', 'numerical', 'min' => 1),
              array('period_time', 'numerical', 'min' => 0),
              array('period_price', 'numerical', 'integerOnly' => true, 'min' => 0),
        );
    }
    
    public function attributeLabels() {
        return array(
            'total_time' => 'Количество часов',
            'period_time' => 'Длительность периода',
            'period_price' => 'Стоимость периода (с учетом скидки)',
        );
    }
    
}