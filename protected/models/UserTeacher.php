<?php

class UserTeacher extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{user_teacher}}';
    }

    public function getAjaxUpdateFields() {
        return array(
            'education', 'birthdate', 'rate_one', 'rate_two'
        );
    }

    public function rules() {
        return array(
            array('education', 'length', 'max' => 255),
            array('birthdate', 'date', 'format' => 'yyyy-MM-dd', 'on' => 'ajaxUpdate'),
            array('birthdate', 'DateValidator', 'format' => 'd MMM yyyy', 'outputFormat' => 'yyyy-MM-dd', 'except' => 'ajaxUpdate'),
			array('rate_one, rate_two', 'safe'),
        );
    }

    public function relations() {
        return array(
            'teacher' => array(self::HAS_ONE, 'UserMain', 'id'),
            'documents'=>array(self::HAS_MANY, 'UserDocuments', 'user_id'),
        );
    }

    public function behaviors() {
        return array(
            'formatDates' => array('class' => 'application.components.FormatDatesBehavior', 'attributes' => 'birthdate')
        );
    }

    public function attributeLabels() {
        return array(
            'user_id' => 'ID',
            'education' => 'Образование',
            'birthdate' => 'Дата рождения',
            'rate_one' => 'Рейтинг',
            'rate_two' => 'rate_two',
        );
    }

}


?>