<?php

class CourseRates extends CActiveRecord {
    // по факту теперь это GroupRates
    public $course_name;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{course_rates}}';
    }
    
    public function getClassName() {
        return 'Стоимость урока';
    }
    
    public function defaultScope() {
        return array(
            'condition' => 'in_archive = 0'
        );
    }
    
    public function rules() {
        return array(
            array('course_id, rate', 'required'),
            array('teacher_id', 'required', 'on' => 'create'),
            array('course_id, group_id', 'numerical', 'integerOnly' => true, 'min' => 1),
            array('course_id', 'exist', 'attributeName' => 'id', 'className' => 'TrCourse'),
            array('teacher_id', 'numerical', 'integerOnly' => true, 'min' => 1, 'on' => 'create'),
            array('teacher_id', 'exist', 'attributeName' => 'id', 'className' => 'UserMain', 'criteria' => array('condition' => 'is_teacher = 1'), 'on' => 'create'),
            array('rate', 'numerical', 'integerOnly' => true, 'min' => 1),
        );
    }
    
    public function relations() {
        return array(
            'course' => array(self::BELONGS_TO, 'TrCourse', 'course_id'),
            'teacher'=> array(self::BELONGS_TO, 'UserMain', 'teacher_id'),
            'group'=> array(self::BELONGS_TO, 'TrGroup', 'group_id'),
        );
    }
    
    public function search() {
        $criteria = new CDbCriteria;
        $criteria->select = 'id, course_id, group_id, course.name AS course_name, teacher_id, rate';
        $criteria->compare('t.teacher_id',$this->teacher_id);
        $criteria->order = 'course_name ASC';
        $criteria->with = array('course', 'group');
        $criteria->addCondition('group.is_deleted = 0');
        return new CActiveDataProvider(__CLASS__,array('criteria' => $criteria, 'pagination' => false));
        return $this->findAll($criteria);
    }
    
    public function attributeNames() {
        return array(
            'course_id' => 'Название',
            'course_name' => 'Название',
            'teacher_id' => 'Преподаватель',
            'rate' => 'Стоимость за ак/ч',
            'in_archive' => 'Архив',
            
            'group' => 'Группа',
            'centers_list' => 'Центры',
        );
    }
    
    protected function afterSave() {
        if ($this->rate) {
            $criteria = new CDbCriteria;
            $criteria->with = 'group';
            $criteria->compare('teacher_rate',0);
            $criteria->compare('group.teacher_id',$this->teacher_id);
            $data = Schedule::model()->findAll($criteria);
            foreach ($data as $currObj) {
                $currObj->teacher_rate = $this->rate;
                $currObj->save();
            }
        }
        parent::afterSave();
    }
    
    public function getCenters_list() {
        if (!$this->teacher_id || !$this->course_id) return null;
        $criteria = new CDbCriteria;
        $criteria->select = 'id, name';
        $criteria->with = array('groups' => array('joinType' => 'INNER JOIN', 'select' => false, 'condition' => 'groups.teacher_id = '.$this->teacher_id.' AND groups.tr_course_id = '.$this->course_id));
        return ProjectHelper::formatList(TrCenter::model()->findAll($criteria),'trcenter/view');
    }
    
    public function getName() {
        if ($this->teacher && $this->course) return $this->teacher->fullname.': '.$this->course->name;
        if ($this->teacher) return $this->teacher->fullname;
        return 'ID #'.$this->id;
    }

}

?>