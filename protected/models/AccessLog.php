<?php

class AccessLog extends CActiveRecord {
    
    public $username;
    public $timestamp;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{access_log}}';
    }
    
    public function rules() {
        return array(
            array('user_ip_addr, username, datetime, action, is_success', 'safe', 'on' => 'search'),  
        );
    }
    
    public function relations() {
        return array(
            'user' => array(self::BELONGS_TO,'UserMain','user_id'),
        );
    }
    
	public function defaultScope() {
		return array(
            'with' => array('user' => array('joinType' => 'INNER JOIN', 'condition' => Yii::app()->user->getProjectIdCondition('project_id','user'))),
		);
	}
    
    public function getObject() {
        if (!empty($this->_object)) return $this->_object;
        if ($this->class_name && $this->object_id) {
            if (!($this->_object = CActiveRecord::model($this->class_name)->findByPk($this->object_id))) {
                $this->_object = CActiveRecord::model($this->class_name)->getArchiveObject($this->object_id);
            }
        } else {
            $this->_object = null;
        }
        return $this->_object;
    }
    
    public function attributeLabels() {
        return array(
            'user_ip_addr' => 'IP-адрес',
            'user_id'      => 'Пользователь',
            'username'     => 'Пользователь',
            'datetime'     => 'Дата',
            'action'       => 'Действите',
            'is_success'   =>'Результат',
        );
    }
    
    public function getActionNames() {
        return array(
            'login'      => 'Вход в систему',
            'login_google'  =>'Вход в систему (Google)',
            'login_yandex'  =>'Вход в систему (Яндекс)',
            'login_twitter' =>'Вход в систему (Twitter)',
            'login_facebook'=>'Вход в систему (Facebook)',
            'login_vkontakte'=>'Вход в систему (VK)',
            'login_mailru'  =>'Вход в систему (Mail.Ru)',
            'logout'     => 'Выход из системы',
            'openid'     => 'Прикрепление OpenID',
            'openid_google'  =>'Прикрепление OpenID (Google)',
            'openid_yandex'  =>'Прикрепление OpenID (Яндекс)',
            'openid_twitter' =>'Прикрепление OpenID (Twitter)',
            'openid_facebook'=>'Прикрепление OpenID (Facebook)',
            'openid_vkontakte'=>'Прикрепление OpenID (VK)',
            'openid_mailru'  =>'Прикрепление OpenID (Mail.Ru)',
            'passremind' => 'Восстановление пароля',
            'passchange' => 'Смена пароля',
        );
    }
    
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->user_ip_addr = Yii::app()->request->userHostAddress;
            if (!$this->user_id) $this->user_id = Yii::app()->user->id;
            $this->datetime= time();
            return true;
        }
    }
    
    public static function add($actionId,$actionParams=array(),$success = true) {
        $model = new AccessLog;
        $additInfo = null;
        switch ($actionId) {
            case 'login':
                if (isset($actionParams['username'])) {
                    $user = UserMain::model()->findByAttributes(array('email'=>$actionParams['username']));
                    if ($user) $model->user_id = $user->id;
                }
                if (isset($actionParams['serviceName'])) $actionId .= '_'.$actionParams['serviceName'];
                break;
            case 'logout':
                break;
            case 'passremind':
                break;
            case 'passchange':
                break;
            case 'openid':
                if (isset($actionParams['serviceName'])) $actionId .= '_'.$actionParams['serviceName'];
                break;
            default:
                return null;
        }
        $model->action = $actionId;
        $model->is_success = (int)$success;
        $model->save();
        return $model;
    }
    
    public function getSuccessList() {
        return array(
            0 => 'Ошибка',
            1 => 'ОК',
        );
    }
    
    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('user_ip_addr',$this->user_ip_addr,true);
        if ($this->username) {
            $criteria->compare('user.fullname',$this->username,true);
            $criteria->with = array('user');
        }
		if ($this->datetime && $this->timestamp = CDateTimeParser::parse($this->datetime,'d MMM yyyy',array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0))) {
            $criteria->addBetweenCondition('datetime',$this->timestamp,$this->timestamp+86400);
        }
        $criteria->compare('action',$this->action);
        $criteria->compare('is_success',$this->is_success);
        return new CActiveDataProvider(__CLASS__,array('criteria' => $criteria, 'sort' => array('defaultOrder' => 'datetime DESC'), 'pagination' => array('pageSize' => 40)));
    }
    
    public function getActionName(){
        $actionNames = $this->actionNames;
        return isset($actionNames[$this->action]) ? $actionNames[$this->action] : $this->action;
    }
    
}

?>