<?php

class UserPartner extends CActiveRecord {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{user_partner}}';
    }
    
    public function getAjaxUpdateFields() {
        return array(
            'organization', 'position', 'legal_addr', 'real_addr', 'contacts', 'details', 'web_id'
        );
    }
    
    public function rules() {
        return array(
            array('organization, position, legal_addr, real_addr, contacts, web_id', 'length', 'max' => 255),
            array('details', 'length', 'max' => 2000),
            array('web_id', 'unique'),
        );
    }
    
    public function relations() {
        return array(
            'partner' => array(self::HAS_ONE, 'UserMain', 'id'), // DEPRECATED
            'user'    => array(self::HAS_ONE, 'UserMain', 'id'),
        );
    }
        
    public function attributeLabels() {
        return array(
            'user_id' => 'ID',
            'organization' => 'Организация',
            'position' => 'Должность',
            'legal_addr' => 'Юридический адрес',
            'real_addr' => 'Фактический адрес',
            'contacts' => 'Контакты',
            'details' => 'Банковские реквизиты',
            'web_id' => 'Web ID',
        );
    }
    
    public function getName() {
        if (!$this->user) return null;
        return $this->user->fullname;
    }
    
}


?>