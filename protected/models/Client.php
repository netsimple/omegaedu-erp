<?php

	/*
	 * Scenario list:
	 *  > createWithManagers
	 *  > createDefault
	 *  > updateWithManagers
	 *  > updateDefault
	 */

	class Client extends CActiveRecord {

		const STATUS_DEFAULT = 0;
		const STATUS_WAITING = 1;
		const STATUS_REJECTED = 2;
		const STATUS_CONCLUDE = 3;
		const STATUS_STUDY = 4;
		const STATUS_PAID = 5;
		const STATUS_TRIAL = 6;
		const STATUS_DEBT = 7;
		const STATUS_OTHER = -1;

		const SOURCE_DEFAULT = 0;
		const SOURCE_INTERNET = 1;
		const SOURCE_RECOMMENDATION = 2;
		const SOURCE_PRESENTATION = 3;
		const SOURCE_OLYMPIAD = 4;
		const SOURCE_ADVERT = 5;
		const SOURCE_OTHER = -1;

		public $group_name;
		public $center_id;
		public $course_list;
		public $comments_text;
		public $city_id;

		protected $_balanceByCourseId = array();

		protected $_currentGroup;

		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		public function tableName() {
			return '{{client}}';
		}

		public function getClassName() {
			return 'Клиент';
		}

		public function rules() {
			return array(
				array('fullname, phone_num', 'required'),
				array('manager_id', 'required', 'on' => 'createWithManagers, updateWithManagers'),
				array('manager_id', 'safe', 'on' => 'createWithManagers, updateWithManagers'),
				array('manager_id, birthday, rate_one, rate_two', 'safe'),
				array('email', 'email'),
                array('center__id','safe'),
				array('email', 'unique', 'except' => 'request'),
				array('email, fullname', 'length', 'max' => 255),
				array('phone_num, parent_phone', 'match', 'pattern' => '/^((\+?7)(-?\d{3})-?)?(\d{3})(-?\d{4})$/', 'message' => 'Некорректный формат номера телефона в поле {attribute}', 'except' => 'request'),
				array('other', 'length', 'max' => 255),
				array('school, parent_name, region, office', 'length', 'max' => 255),
				array('skype, social_google, social_yandex, social_twitter, social_vk, social_facebook, social_mailru', 'length', 'max' => 255),
				array('social_google, social_yandex, social_twitter, social_vk, social_facebook, social_mailru', 'url', 'except' => 'request'),
				array('status', 'in', 'range' => array(self::STATUS_WAITING, self::STATUS_REJECTED, self::STATUS_CONCLUDE, self::STATUS_STUDY, self::STATUS_OTHER), 'except' => 'request', 'message' => 'Не выбран {attribute}'),
				array('status_text', 'length', 'max' => 255),
				array('source', 'in', 'range' => array(self::SOURCE_INTERNET, self::SOURCE_RECOMMENDATION, self::SOURCE_PRESENTATION, self::SOURCE_OLYMPIAD, self::SOURCE_ADVERT, self::SOURCE_OTHER), 'except' => 'request', 'message' => 'Не выбран {attribute}'),
				array('source_text', 'length', 'max' => 255),
				array('reg_date', 'date', 'format' => 'd MMM yyyy', 'timestampAttribute' => 'reg_date', 'on' => 'search'),
				array('email, fullname, phone_num, status_text, group_name, center_id, course_list, city_id, rate_one', 'safe', 'on' => 'search'),
			);
		}

		public function relations() {
			return array(
				'comments' => array(self::HAS_MANY, 'ClientComments', 'client_id'),
				'contracts' => array(self::HAS_MANY, 'Contract', 'client_id'),
				'manager' => array(self::HAS_ONE, 'UserMain', array('id' => 'manager_id')),
				'groups' => array(self::MANY_MANY, 'TrGroup', Yii::app()->db->tablePrefix.'rel_group_member(client_id,tr_group_id)'),
				'balanceRel' => array(self::STAT, 'TrIncome', 'client_id',
				                      'select' => 'SUM(value * SIGN(hours))',
				                      'condition' => 'section = '.TrIncome::INCOME_CLIENT,
				),
				'partner' => array(self::BELONGS_TO, 'UserMain', 'project_id'),
				'center' => array(self::BELONGS_TO, 'TrCenter', 'center__id'),
			);
		}

		public function behaviors() {
			return array(
				'datetimeLimit' => array('class' => 'application.components.DatetimeLimitBehavior'),
				'softDelete' => array('class' => 'application.components.SoftDeleteBehavior', 'children' => array('contracts')),
			);
		}

		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'status' => 'Статус',
				'status_text' => 'Статус',
				'email' => 'E-Mail',
				'passwd' => 'Пароль',
				'project_id' => 'ID проекта',
				'fullname' => 'ФИО',
				'reg_date' => 'Регистрация',
				'phone_num' => 'Телефон',
				// 'comments' => 'Комментарии',
				'skype' => 'Skype',
				'other' => 'Иная информация',
				'status_date' => 'Дата обновления статуса',
				'school'=> 'Школа',
				'source'=> 'Источник',
				'source_text' => 'Источник',
				'manager_id' => 'Менеджер',
				'parent_name' => 'ФИО родителей',
				'parent_phone' => 'Телефон родителей',
				'region' => 'Район',
				'office' => 'Офис',
				'social_google' => 'Аккаунт Google',
				'social_yandex' => 'Аккаунт Yandex',
				'social_twitter' => 'Аккаунт Twitter',
				'social_vk' => 'Аккаунт VK',
				'social_facebook' => 'Аккаунт Facebook',
				'social_mailru' => 'Аккаунт Mail.Ru',

				'client_appeared_stat' => 'Кол-во посещений',
				'client_paid_stat' => 'Оплачено уроков',

				'group_name' => 'Группы',
				'center_id' => 'Центры',
				'course_list' => 'Предметы',
				'comments_text' => 'Комментарии',
				'city_id' => 'Город',
				'birthday' => 'День рождения',
				'rate_one' => 'Рейтинг',
				'rate_two' => 'Набор пройденых курсов',
				'center__id' => 'Центр',
			);
		}

		public function defaultScope() {
			return array(
				'condition' => Yii::app()->user->getProjectIdCondition('project_id',$this->getTableAlias(false,false)),
			);
		}

		protected function beforeSave() {
			parent::beforeSave();

			if ($this->status != self::STATUS_OTHER) $this->status_text = '';

			if ($this->isNewRecord) {
				$this->reg_date = time();
				$this->status_date = $this->reg_date;
				if (!$this->manager_id) $this->manager_id = Yii::app()->user->id;
			} else {
				$oldModel = Client::model()->findByPk($this->id);
				if ($oldModel->statusText != $this->statusText) {
					$this->status_date = time();
					/* Создание служебного комментария */
					$comment = new ClientComments;
					$comment->user_id = Yii::app()->user->id;
					$comment->client_id = $this->id;
					$comment->system_status = $this->status != Client::STATUS_STUDY ? ClientComments::SYSTEM_STATUS_CHANGE : ClientComments::SYSTEM_STATUS_TOSTUDY;
					$comment->content = $this->changeStatusMsg($oldModel->statusText,$this->statusText);
					$comment->save();
				}
			}

			return true;
		}

		protected function beforeDelete() {
			if (parent::beforeDelete()) {
				if ($this->contracts) foreach ($this->contracts as $currentObject) $currentObject->delete();
			}
		}

		public function search() {

            $user = Yii::app()->user->getId();
            $userModel = UserMain::model()->findByPk($user);
            $centers = [];

            $criteria = new CDbCriteria;
            if($userModel->relatedCenters){
                foreach ($userModel->relatedCenters as $center){
                    $centers[$center->tr_center_id] = $center->tr_center_id;
                }
            }
            $criteria->addInCondition('center__id',$centers);
			$criteria->compare('fullname', $this->fullname, true);
			$criteria->compare('phone_num', $this->phone_num, true);
			$criteria->compare('school', $this->school, true);
			$criteria->compare('rate_one', $this->rate_one, true);
			$criteria->compare('status', $this->status);
			$criteria->compare('source', $this->source);

			if ($this->city_id) {
				$projects = UserMain::projectsByCityId($this->city_id);
				$criteria->addInCondition('project_id', $projects);
			}

			if ($this->status_text) {
				$this->status_text = strtolower($this->status_text);
				$matched = array();
				foreach ($this->statusList as $currentStatusCode => $currentStatusText) {
					if (strpos(strtolower($currentStatusText),$this->status_text) !== false) {
						$matched[] = $currentStatusCode;
					}
				}
				if (count($matched) > 1) {
					$criteria->addInCondition('status',$matched);
				} elseif (count($matched) == 1) {
					$criteria->compare('status',$matched[0]);
				} else {
					$criteria->compare('status_text',$this->status_text,true);
				}
			}

			if ($this->reg_date) {
				$criteria->addCondition('reg_date >= '.$this->reg_date.' AND reg_date <= '.($this->reg_date+86400));
				$this->reg_date = Yii::app()->dateFormatter->format('d MMM yyyy',$this->reg_date);
			}

			$arr = array();
			if ($this->group_name) {
				array_push($arr,$this->getIDsByGroupName($this->group_name));
			}
			if ($this->center_id && $centerId = (int)$this->center_id) {
				array_push($arr,$this->getIDsByCenterId($centerId));
			}
			if ($this->course_list) {
				array_push($arr,$this->getIDsByCourseIDs($this->course_list));
			}
			if ($arr) {
				$ids = array_pop($arr);
				foreach ($arr as $currentSet) $ids = array_intersect($ids,$currentSet);
				$criteria->addInCondition('id',array_unique($ids));
			}

			return new CActiveDataProvider(__CLASS__, array('criteria' => $criteria, 'pagination' => array('pageSize' => 40)));
		}

		public function getName() {
			return $this->fullname;
		}

		public function getStatusText($status = null) {
			if ($status === null) $status = $this->status;
			if ($status != self::STATUS_OTHER) {
				return isset($this->statusList[$status]) ? $this->statusList[$status] : 'неизвестен';
			} else {
				return $this->status_text ? $this->status_text : $this->statusList[self::STATUS_OTHER];
			}
		}

		public function getSourceText($source = null) {
			if ($source === null) $source = $this->source;
			if ($source != self::SOURCE_OTHER) {
				return isset($this->sourceList[$source]) ? $this->sourceList[$source] : 'неизвестен';
			} else {
				return $this->source_text ? $this->source_text : $this->sourceList[self::STATUS_OTHER];
			}
		}

		public static function getStatusList($withStatusDefault=true) {

			$answer = array(
				self::STATUS_DEFAULT 	=> 'выберите статус',
				self::STATUS_WAITING 	=> 'ожидание',
				self::STATUS_REJECTED 	=> 'отказ',
				self::STATUS_CONCLUDE 	=> 'заключение договора',
				self::STATUS_STUDY 		=> 'приступил к учёбе',
				self::STATUS_OTHER		=> 'иное',
				self::STATUS_PAID		=> 'оплачено',
				self::STATUS_TRIAL		=> 'пробное',
				self::STATUS_DEBT		=> 'долг',
			);

			if (!$withStatusDefault)
				unset($answer[self::STATUS_DEFAULT]);

			return $answer;
		}

		public static function getSourceList() {
			return array(
				self::SOURCE_DEFAULT 		=> 'выберите источник',
				self::SOURCE_INTERNET 		=> 'интернет',
				self::SOURCE_RECOMMENDATION	=> 'рекомендация',
				self::SOURCE_PRESENTATION 	=> 'презентация в школе',
				self::SOURCE_OLYMPIAD 		=> 'олимпиада',
				self::SOURCE_ADVERT 		=> 'наружная реклама',
				self::SOURCE_OTHER			=> 'другое'
			);
		}

		public static function getStatusIcons() {
			return array(
				self::STATUS_DEFAULT 	=> '',
				self::STATUS_WAITING 	=> Yii::app()->baseUrl.'/images/icon-mb-clock.png',
				self::STATUS_REJECTED 	=> Yii::app()->baseUrl.'/images/icon-cancel-file.png',
				self::STATUS_CONCLUDE 	=> Yii::app()->baseUrl.'/images/icon-guarantee.png',
				self::STATUS_STUDY 		=> Yii::app()->baseUrl.'/images/icon-blackboard.png',
				self::STATUS_OTHER		=> Yii::app()->baseUrl.'/images/icon-info.png',
			);
		}

		private function changeStatusMsg($oldStatus, $newStatus) {
			return 'Статус был изменен с "'.$oldStatus.'" на "'.$newStatus.'".';
		}

		public function getIsManagerRequired() {
			return $this->scenario == 'createWithManagers' || $this->scenario == 'updateWithManagers';
		}

		public function getJSONNames($attributeVal, $project_id = null) {
			$criteria = new CDbCriteria;
			$criteria->select = "id, fullname";
			$criteria->compare('fullname',$attributeVal,true);
			$criteria->compare('project_id',$project_id);
			$data = Client::model()->findAll($criteria);
			$result = array();
			foreach ($data as $currentObj) {
				$result[] = array('id' => $currentObj->id, 'fullname' => $currentObj->fullname, 'value' => $currentObj->fullname);
			}
			return CJSON::encode($result);
		}

		public function getAppearedCount($groupId) {
			$lessonsIDs = Yii::app()->db->createCommand()
				->select('id')
				->from(Yii::app()->db->tablePrefix.'schedule')
				->where($this->addDatetimeCondition('tr_group_id = '.$groupId,'datetime_start'))
				->queryColumn();
			$appearedCount = Yii::app()->db->createCommand()
				->select('COUNT(is_skipped = '.TrClientLog::SKIPPED_NO.' OR is_skipped = '.TrClientLog::SKIPPED_YES.')')
				->from(Yii::app()->db->tablePrefix.'tr_client_log')
				->where(array('and', array('in','tr_lesson_id',$lessonsIDs), 'client_id = '.$this->id))
				->queryScalar();
			return (int)$appearedCount;
		}

		public function getPaidCount($groupId) {
			$result = Yii::app()->db->createCommand()
				->select('COUNT(debt)')
				->from('{{tr_client_log}} t')
				->join('{{schedule}} s','s.id = t.tr_lesson_id')
				->where($this->addDatetimeCondition(array('and','debt = 0',array('and','s.tr_group_id = '.$groupId,'t.client_id = '.$this->id)),'s.datetime_start'))
				->queryScalar();
			return (int)$result;
		}

		public function getGroupLinks() {
			if ($this->isNewRecord) return null;
			return ProjectHelper::formatList($this->groups,'trgroup/view');
		}

		protected function getIDsByGroupName($searchValue) {
			$result = array();
			$result = Yii::app()->db->createCommand()
				->select('t.client_id')
				->from(Yii::app()->db->tablePrefix.'rel_group_member t')
				->join(Yii::app()->db->tablePrefix.'tr_group group','group.id = t.tr_group_id')
				->where(array('like','group.name','%'.mysql_real_escape_string($searchValue).'%'))
				->queryColumn();
			return $result;
		}

		public function getCenterLinks() {
			if ($this->isNewRecord) return null;
			$centerIDs = Yii::app()->db->createCommand()
				->select('t.tr_center_id')
				->from(Yii::app()->db->tablePrefix.'tr_group t')
				->join(Yii::app()->db->tablePrefix.'rel_group_member rel','t.id = rel.tr_group_id')
				->where('rel.client_id = '.$this->id)
				->queryColumn();
			if (!$centerIDs) return null;
			$criteria = new CDbCriteria;
			$criteria->select = 'id, name';
			$criteria->addInCondition('id',$centerIDs);
			return ProjectHelper::formatList(TrCenter::model()->findAll($criteria),'trcenter/view');
		}

		public function getContractCourseLinks() {
			if ($this->isNewRecord) return null;
			$courses = array();
			foreach ($this->contracts as $currContract) {
				foreach ($currContract->courses as $currCourse) if ($currCourse->course && !isset($courses[$currCourse->course->id])) $courses[$currCourse->course->id] = $currCourse->course;
			}
			return ProjectHelper::formatList(array_values($courses),'trcourse/view');
		}

		protected function getIDsByCenterId($centerId) {
			$result = array();
			if (!($centerId = (int)$centerId)) return $result;
			$result = Yii::app()->db->createCommand()
				->select('t.client_id')
				->from(Yii::app()->db->tablePrefix.'rel_group_member t')
				->join(Yii::app()->db->tablePrefix.'tr_group group','t.tr_group_id = group.id')
				->where('group.tr_center_id = '.$centerId)
				->queryColumn();
			return $result;
		}

		protected function getIDsByCourseIDs($courseIDs) {
			if (!$courseIDs) return array();
			$courseIDs = (array)$courseIDs;
			$result = Yii::app()->db->createCommand()
				->select('c.client_id')
				->from('{{contract_course}} t')
				->join('{{contract}} c','c.id = t.contract_id')
				->where(array('in','t.course_id',$courseIDs))
				->queryColumn();
			return (array)$result;
		}

		public function getManagerList() {
			$criteria = new CDbCriteria;
			$criteria->select = 'id, fullname';
			$criteria->compare('is_manager',1);
			$data = UserMain::model()->findAll($criteria);
			$result = array('Выберите менеджера');
			foreach ($data as $currentObject) {
				$result[$currentObject->id] = $currentObject->fullname;
			}
			return $result;
		}

		public function getBalanceByCourseId($courseId) {
			if (!$courseId) return null;
			if (isset($this->_balanceByCourseId[$courseId])) return $this->_balanceByCourseId[$courseId];
			return $this->_balanceByCourseId[$courseId] = $this->getRelated('balanceRel',true,array('condition' => 'course_id = '.$courseId));
		}

		public function getDebtByGroupId($groupId) {
			if (!$groupId || !$group = TrGroup::model()->findByPk($groupId)) return null;
			// $this->attachGroup($group);
			return $group->getDebtByClientId($this->id);
		}

		public function attachGroup(TrGroup $group) {
			$this->_currentGroup = $group;
			return $this;
		}

		public function getAttachedGroup() {
			return $this->_currentGroup;
		}

		public function getDebt() {
			if (empty($this->_currentGroup)) return null;
			return $this->_currentGroup->getDebtByClientId($this->id);
		}

		public function getCommentsText() {
			if (!$this->comments) return '';

			$out = array();
			foreach ($this->comments as $comment) {
				if (strpos($comment->content, 'Статус') === 0)
					continue;

				$out[] = $comment->content;
			}

			return implode("<br>", $out);
		}

		public static function addToGroup($clientId, $groupId) {

			$connection = Yii::app()->db;
			$tableName = $connection->tablePrefix.'rel_group_member';

			$command = $connection->createCommand("SELECT * FROM $tableName WHERE tr_group_id = :groupId AND client_id = :clientId");
			$res = $command->queryScalar(array(
				":groupId" => $groupId,
				":clientId" => $clientId
			));

			if ($res !== false)
				return true;

			$command = $connection->createCommand("INSERT INTO $tableName (tr_group_id, client_id) VALUES (:groupId, :clientId)");
			$command->execute(array(
				":groupId" => $groupId,
				":clientId" => $clientId
			));

			return true;

		}

		public static function deleteFromGroup($clientId, $groupId) {

			$connection = Yii::app()->db;
			$tableName = $connection->tablePrefix.'rel_group_member';

			$sql = "DELETE FROM $tableName WHERE tr_group_id = :groupId AND client_id = :clientId";

			$command = $connection->createCommand($sql);
			$command = $command->bindParam(":groupId", $groupId, PDO::PARAM_STR);
			$command = $command->bindParam(":clientId", $clientId, PDO::PARAM_STR);
			$command->execute();

			return true;

		}

		public function getCourseList() {
			$result = array();
			if ($this->id) {
				foreach ($this->groups as $group)
					$result[$group->course->id] = $group->course->name . " [{$group->name}]";
			}
			return $result;
		}

		public function getGroupList() {
			$result = array();
			if ($this->id) {
				foreach ($this->groups as $group)
					$result[$group->id] = $group->course->name . " [{$group->name}]";
			}
			return $result;
		}

		public function getAge()
		{
			if(!$this->birthday || $this->birthday=='1970-01-01' || $this->birthday=='0000-00-00'){
				return '';
			}
			$dob=explode("-", $this->birthday);
			$curMonth = date("m");
			$curDay = date("j");
			$curYear = date("Y");
			$age = $curYear - $dob[0];
			if($curMonth<$dob[1] || ($curMonth==$dob[1] && $curDay<$dob[2]))
				$age--;
			return Yii::t('app', '{n} год|{n} года|{n} лет', $age);
		}
	}



?>