<?php

class UserDocuments extends CActiveRecord {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{user_documents}}';
    }
    
    public function getClassName() {
        return 'Документ';
    }
    
	public function behaviors() {
		return array(
			'softDelete' => array('class' => 'application.components.SoftDeleteBehavior', 'parents' => array('user')),
		);
	}
    
    public function relations() {
        return array(
            'user' => array(self::BELONGS_TO, 'UserMain', 'user_id'),
        );
    }
    
    public function attributeLabels() {
        return array(
            'user_id' => 'Пользователь',
            'filename' => 'Имя файла',
            'filesize' => 'Размер',
            'filetitle' => 'Имя файла',
            'uploaded' => 'Загружен',
        );
    }
    
    public function getName() {
        return $this->filename;
    }
    
    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            if (file_exists($tmp = Yii::app()->getBasePath().'/../'.Yii::app()->params['uploadDirectory'].'/documents/'.$model->filename)) @unlink($tmp);
            return true;
        }
    }
    
}

?>