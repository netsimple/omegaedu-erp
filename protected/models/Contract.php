<?php

class Contract extends CActiveRecord {
    
    const STATUS_CONCLUDED = 0;
    const STATUS_PAID = 1;
    const STATUS_TERMINATED = 2;
    
    private $_allCourses;
	
	public $dublicate;
	public $date_from;
	public $date_to;
	public $city_id;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{contract}}';
    }
	
	public function getClassName() {
		return 'Договор';
	}
    
    public function relations() {
        return array(
            'client' => array(self::BELONGS_TO,'Client','client_id'),
            'partner' => array(self::BELONGS_TO,'UserPartner','project_id'),
            'courses' => array(self::HAS_MANY, 'ContractCourse', 'contract_id'),
			'coupon' => array(self::HAS_ONE, 'Coupon', array('coupon' => 'discount_coupon')),
        );
    }
    
    public function rules() {
        return array(
            array('date, parent_name, start_date, end_date', 'required'),
            array('parent_pass_num, parent_pass_where, parent_pass_when, parent_addr, parent_phone_mobile', 'required'),
            array('client_pass_num, client_pass_where, client_pass_when, client_addr, client_phone_mobile', 'required'),
            array('status', 'required'),
            array('status', 'in', 'range' => array(self::STATUS_CONCLUDED,self::STATUS_PAID), 'on' => 'create'),
            array('status', 'in', 'range' => array(self::STATUS_CONCLUDED,self::STATUS_PAID,self::STATUS_TERMINATED), 'on' => 'update, search, ajaxUpdate'),
            array('dublicate', 'safe'),
            array('date, start_date, end_date, parent_pass_when, client_pass_when','DateValidator', 'format' => 'd MMM yyyy', 'outputFormat' => 'yyyy-MM-dd'),
            array('parent_name, discount_reason', 'length', 'max' => 255),
            array('discount_coupon', 'length', 'max' => 25),
            array('discount_coupon', 'exist', 'attributeName' => 'coupon', 'className' => 'Coupon', 'message' => 'Указанный купон неактивен'),
            array('has_discount', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 1),
            array('parent_addr_index, client_addr_index', 'numerical', 'integerOnly' => true, 'min' => 100000, 'max' => 999999),
            array('parent_pass_num, parent_pass_where, parent_addr', 'length', 'max' => 255),
            array('client_pass_num, client_pass_where, client_addr', 'length', 'max' => 255),
            array('parent_phone_home, parent_phone_mobile, client_phone_home, client_phone_mobile', 'match', 'pattern' => '/^((\+?7)(-?\d{3})-?)?(\d{3})(-?\d{4})$/', 'message' => 'Некорректный формат номера телефона в поле {attribute}'),
            array('id, parent_name, client_name, status, date_from, date_to, city_id', 'safe', 'on' => 'search'),
        );
    }
	
    public function behaviors() {
        return array(
            'formatDates' => array('class' => 'application.components.FormatDatesBehavior', 'attributes' => array('date','start_date','end_date','parent_pass_when','client_pass_when')),
			'softDelete' => array('class' => 'application.components.SoftDeleteBehavior', 'parents' => array('client')),
        );
    }
    
    public function attributeLabels() {
        return array(
            'id' => 'Договор',
            'date' => 'Дата заключения',
            'parent_name' => 'ФИО заказчика',
            'client_name' => 'ФИО потребителя',
            'start_date' => 'Начало периода',
            'end_date' => 'Конец периода',
            'has_discount' => 'Имеет скидку?',
            'discount_reason' => 'Причина наличия скидки',
            'discount_coupon' => 'Купон, предоставляющий скидку',
            'parent_pass_num' => 'Серия и номер паспорта заказчика',
            'parent_pass_where' => 'Где выдан',
            'parent_pass_when' => 'Когда выдан',
            'parent_addr_index' => 'Индекс регистрации',
            'parent_addr' => 'Адрес регистрации',
            'parent_phone_home' => 'Номер телефона (дом.)',
            'parent_phone_mobile' => 'Номер телефона (раб./сот.)',
            'client_pass_num' => 'Серия и номер паспорта потребителя',
            'client_pass_where' => 'Где выдан',
            'client_pass_when' => 'Когда выдан',
            'client_addr_index' => 'Индекс регистрации',
            'client_addr' => 'Адрес регистрации',
            'client_phone_home' => 'Номер телефона (дом.)',
            'client_phone_mobile' => 'Номер телефона (раб./сот.)',
            'status' => 'Статус',
			'dublicate' => 'Потребитель является заказчиком',
            'city_id' => 'Город',
        );
    }
	
	public function defaultScope() {
		return array(
			'condition' => Yii::app()->user->getProjectIdCondition('project_id',$this->getTableAlias(false,false)),	
		);
	}
    
	public function search() {
			
		$criteria = new CDbCriteria;
		$criteria->select = 'id, client_id, client_name, client_phone_mobile, status, date, project_id';
		$criteria->compare('client_name', $this->client_name, true);
		$criteria->compare('client_phone_mobile', $this->client_phone_mobile, true);
		$criteria->compare('id', $this->id);
        $criteria->compare('status',$this->status);

        if ($this->city_id) {
            $projects = UserMain::projectsByCityId($this->city_id);
            $criteria->addInCondition('project_id', $projects);
        }

        if ($this->date_from && $date_from = CDateTimeParser::parse($this->date_from,'d MMM yyyy',array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0)))
            $criteria->compare('date', '>='.date('Y-m-d', $date_from));

        if ($this->date_to && $date_to = CDateTimeParser::parse($this->date_to,'d MMM yyyy',array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0)))
            $criteria->compare('date', '<='.date('Y-m-d', $date_to));

        $sort = ($this->date_to || $this->date_from) ? array('defaultOrder'=>'date ASC') : array();

		return new CActiveDataProvider(__CLASS__, array('criteria' => $criteria, 'pagination' => array('pageSize' => 40), 'sort'=>$sort));
	}
    
    protected function afterConstruct() {
        $this->project_id = Yii::app()->user->model->projectId;
        return parent::afterConstruct();
    }
	
	protected function beforeValidate() {
		if (parent::beforeValidate()) {
			$this->dublicate = !empty($this->dublicate) ? (bool)$this->dublicate : false;
			if ($this->dublicate) {
				$this->parent_name = $this->client_name;
				$this->parent_pass_num = $this->client_pass_num;
				$this->parent_pass_where = $this->client_pass_where;
				$this->parent_pass_when = $this->client_pass_when;
				$this->parent_addr_index = $this->client_addr_index;
				$this->parent_addr = $this->client_addr;
				$this->parent_phone_home = $this->client_phone_home;
				$this->parent_phone_mobile = $this->client_phone_mobile;
			}
			return true;
		}
	}
    
    protected function afterSave() {
        parent::afterSave();

        $this->getRelated('coupon', true);

        if ($this->coupon) {
			$this->coupon->activate_count = Contract::model()->countByAttributes(array('discount_coupon' => $this->discount_coupon));
			$this->coupon->update(array('activate_count'));
		}

    }
    
    public function addCourse(ContractCourse $course, $replace = true) {
        if (empty($this->_allCourses)) $this->_allCourses = array();
		if (!$this->isNewRecord && !$course->isNewRecord) {
			foreach ($this->_allCourses as $currKey => $currCourse)
				if ($currCourse->id == $course->id) {
					if ($replace) $this->_allCourses[$currKey] = $course;
					return;
				}
		}
		array_push($this->_allCourses,$course);
    }
    
    public function getAllCourses() {
		if ($this->getRelated('courses',true)) foreach($this->courses as $currCourse) $this->addCourse($currCourse,false);
		return (array)$this->_allCourses;
    }
	
    public function getClientData($client) {
        $this->client_name = $client->fullname;
        $this->parent_name = $client->parent_name;
        $this->client_phone_mobile = $client->phone_num;
        $this->parent_phone_mobile = $client->parent_phone;
        return;
    }
    
    public function getStatusList() {
        if ($this->scenario == 'create')
            return array(
                self::STATUS_CONCLUDED => 'Заключён',
                self::STATUS_PAID => 'Оплачен'
            );
        else
            return array(
                self::STATUS_CONCLUDED => 'Заключён',
                self::STATUS_PAID => 'Оплачен',
                self::STATUS_TERMINATED => 'Расторгнут'
            );
    }
	
	public function dublicateCheck() {
		if ($this->dublicate === null)
			$this->dublicate = $this->client_name == $this->parent_name && $this->client_pass_num == $this->parent_pass_num && $this->client_pass_where == $this->parent_pass_where && $this->client_pass_when == $this->parent_pass_when &&
				$this->client_addr_index == $this->parent_addr_index && $this->client_addr == $this->parent_addr && $this->client_phone_home == $this->parent_phone_home && $this->client_phone_mobile == $this->parent_phone_mobile;
	}
	
	public function getName() {
		$name = 'Договор #'.$this->id;
		if ($this->client) $name .= ' ('.$this->client->name.')';
		return $name;
	}

    public function getSum() {

        $c = Yii::app()->db->createCommand();
        $where = 'c.is_deleted = 0';

        if (!Yii::app()->user->model->is_owner)
            $where = array('and', $where, Yii::app()->user->model->projectIdCondition);
        else if ($city_id = Yii::app()->request->getParam('city_id'))
            $where = array('and', $where, array('in', 'project_id', UserMain::projectsByCityId($city_id)));

        return (int) $c->select('SUM(cc.period_price)')
        ->from('{{contract_course}} cc')
        ->join('{{contract}} c', 'c.id = cc.contract_id')
        ->where($where)
        ->group('c.is_deleted')
        ->queryScalar();

    }

    public function getCityName() {

        if ($this->city_id)
            return City::name($this->city_id);

        if ($this->partner && $this->partner->city)
            return $this->partner->city->name;

        return 'Не определен';

    }
}

?>