<?php

class PassRemindForm extends CFormModel {
    
    private $_identity;
    
    public $email;
    
    public function rules() {
        return array(
            array('email', 'required'),
	    array('email', 'email'),
	    array('email', 'length', 'max'=>255),
        );
    }
    
    public function attributeLabels() {
        return array(
            'email' => 'E-Mail',
        );
    }
    
    public function passremind() {
	if(!$this->hasErrors()) {
		if (!($model = UserMain::model()->findByAttributes(array('email' => $this->email)))) {
		    $this->addError('email','Указанный E-Mail в системе не зарегистрирован.');
		    return false;
		} else {
		    $model->setPassword('restore');
		    return true;
		}
	}
        return false;
    }
    
}

?>