<?php

	class UserMain extends CActiveRecord {

		public $group_name;
		public $center_id;

		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		public function tableName() {
			return '{{user_main}}';
		}

		public function getClassName() {
			return 'Пользователь';
		}

		public function getAjaxUpdateFields() {
			return array(
				'email', 'fullname', 'phone_num', 'hire_date', 'comments', 'social_google', 'social_yandex', 'social_twitter', 'social_vk', 'social_facebook', 'social_mailru', 'skype', 'city_id', 'currency_id'
			);
		}

		public function rules() {
			return array(
				array('email, fullname', 'required'),
				array('email', 'email'),
				array('email', 'unique'),
				array('email, fullname, phone_num', 'length', 'max' => 255),
				array('hire_date', 'date', 'format' => 'yyyy-MM-dd', 'on' => 'ajaxUpdate'),
				array('hire_date', 'DateValidator', 'format' => 'd MMM yyyy', 'outputFormat' => 'yyyy-MM-dd', 'except' => 'ajaxUpdate'),
				array('comments, social_google, social_yandex, social_twitter, social_vk, social_facebook, social_mailru, skype', 'length', 'max' => 255),
				array('social_google, social_yandex, social_twitter, social_vk, social_facebook, social_mailru', 'url'),
				array('avatar_name', 'length', 'max' => 255, 'on' => 'crop'),
				array('city_id', 'in', 'range' => City::range(), 'except' => 'create'),
				array('fullname, email, group_name, center_id, city_id, currency_id', 'safe', 'on' => 'search'),
				array('currency_id', 'numerical', 'integerOnly'=>true),
			);
		}

		public function relations() {
			return array(
				'partner' => array(self::HAS_ONE, 'UserPartner', 'user_id'),
				'manager' => array(self::HAS_ONE, 'UserManager', 'user_id'),
				'teacher' => array(self::HAS_ONE, 'UserTeacher', 'user_id'),
				'eauth'   => array(self::HAS_MANY, 'Extauth', 'user_id'),
				'documents'=>array(self::HAS_MANY, 'UserDocuments', 'user_id'),
				'coupon'  => array(self::HAS_ONE, 'Coupon', 'dealer_id'),
				'city'  => array(self::BELONGS_TO, 'City', 'city_id'),
				'parent'  => array(self::BELONGS_TO, 'UserMain', 'project_id'),
				'currency' => array(self::BELONGS_TO,'Currency','currency_id'),
                'relatedCenters' => array(self::HAS_MANY, 'TrCenterManager', 'manager_id'),
			);
		}

		public function behaviors() {
			return array(
				'datetimeLimit' => array('class' => 'application.components.DatetimeLimitBehavior'),
				'financeData' => array('class' => 'application.components.FinanceDataBehavior'),
				'formatDates' => array('class' => 'application.components.FormatDatesBehavior', 'attributes' => 'hire_date'),
				'softDelete' => array('class' => 'application.components.SoftDeleteBehavior', 'children' => array('documents')),
			);
		}

		public function defaultScope(){
			$result = array();
			// if (!Yii::app()->user->isGuest) $result['condition'] = Yii::app()->user->getProjectIdCondition('project_id',$this->getTableAlias(false,false));
			return $result;
		}

		public function getCenters($criteria = null, $isProvider = false) {
			$c = new CDbCriteria;
			$c->compare('project_id',$this->projectIdCompare);
			if ($criteria !== null) $c->mergeWith($criteria);
			return $isProvider ? new CActiveDataProvider('TrCenter',array('criteria' => $c, 'pagination' => false)) : TrCenter::model()->findAll($c);
		}

		public function resetRoles() {
			$this->is_owner = 0;
			$this->is_partner = 0;
			$this->is_manager = 0;
			$this->is_adman = 0;
			$this->is_teacher = 0;
			return;
		}

		public function eraseRole($role) {
			if (isset($this['is_'.$role])) $this['is_'.$role] = 0;
		}

		public function getRole() {
			if ($this->is_owner || $this->is_partner) return 'partner';
			if ($this->is_manager || $this->is_adman) return 'manager';
			if ($this->is_teacher) return 'teacher';
			if ($this->is_owner) return 'owner';
			return null;
		}

		public function getMainRoles() {
			$result = array();
			if ($this->is_owner || $this->is_partner) $result[] = 'partner';
			if ($this->is_manager || $this->is_adman) $result[] = 'manager';
			if ($this->is_teacher) $result[] = 'teacher';
			return $result;
		}

		public function getRoles() {

			$result = array();
			if ($this->is_owner) $result[] = 'owner';
			if ($this->is_partner) $result[] = 'partner';
			if ($this->is_manager) $result[] = 'manager';
			if ($this->is_adman) $result[] = 'adman';
			if ($this->is_teacher) $result[] = 'teacher';

			return $result;

		}


		public static function getManagerList() {
			$criteria = new CDbCriteria;
			$criteria->select = 'id, fullname';
			$criteria->addCondition('is_manager = 1');
			$criteria->compare('project_id',Yii::app()->user->projectIdCompare);
			$criteria->order = 'fullname ASC';
			$result = array();
			$tmpResult = UserMain::model()->findAll($criteria);
			if (!$tmpResult) return $result;
			foreach ($tmpResult as $val) {
				$result[$val['id']] = $val['fullname'];
			}
			return $result;
		}

		public function setRole($role) {
			if (isset($this['is_'.$role]))
				$this['is_'.$role] = 1;
		}

		public function getExt() {
			$ext = $this[$this->role];
			if (!$ext) {
				switch ($this->role) {
					case 'partner':
						return new UserPartner;
					case 'manager':
						return new UserManager;
					case 'teacher':
						return new UserTeacher;
				}
			}
			else { /* Вопрос в том, нужна ли эта строчка... */
				// $ext->user_id = $this->id;
			}
			return $ext;
		}

		public function getInPartnerGroup() {
			return $this->is_owner || $this->is_partner;
		}

		public function getInManagerGroup() {
			return $this->is_manager || $this->is_adman;
		}

		public function getInTeacherGroup() {
			return $this->is_teacher;
		}

		public function checkRole($role) {
			switch ($role) {
				case 'owner':   return $this->is_owner;
				case 'partner': return $this->is_partner;
				case 'manager': return $this->is_manager;
				case 'adman':   return $this->is_adman;
				case 'teacher': return $this->is_teacher;
				default:        return false;
			}
		}

		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'email' => 'E-Mail',
				'passwd' => 'Пароль',
				'project_id' => 'ID проекта',
				'role' => 'Тип пользователя',
				'fullname' => 'ФИО',
				'reg_date' => 'Дата регистрации',
				'lastvisit' => 'Последнее посещение',
				'ip_addr' => 'IP-адрес',
				'avatar_name' => 'Аватар',
				'phone_num' => 'Номер телефона',
				'owner' => 'Владелец',
				'partner' => 'Партнер',
				'manager' => 'Руководитель',
				'adman' => 'Администратор',
				'teacher' => 'Преподаватель',
				'is_owner' => 'Владелец',
				'is_partner' => 'Партнер',
				'is_manager' => 'Руководитель',
				'is_adman' => 'Администратор',
				'is_teacher' => 'Преподаватель',
				'hire_date' => 'Дата регистрации',
				'comments' => 'Комментарии',
				'social_google' => 'Аккаунт Google',
				'social_yandex' => 'Аккаунт Yandex',
				'social_twitter' => 'Аккаунт Twitter',
				'social_vk' => 'Аккаунт VK',
				'social_facebook' => 'Аккаунт Facebook',
				'social_mailru' => 'Аккаунт Mail.Ru',
				'skype' => 'Skype',
				'documents' => 'Документы',

				'group_name' => 'Группы',
				'center_id' => 'Центры',
				'city_id' => 'Город',
				'currency_id' => 'Валюта',
			);
		}

		public function getName() {
			return $this->fullname;
		}

		protected function afterFind() {
			if ($this->isCurrentUser) {
				$this->lastvisit = time();
				$this->update(array('lastvisit'));
			}
			return parent::afterFind();
		}

		protected function beforeSave() {
			parent::beforeSave();

			if ($this->isNewRecord) {
				$this->reg_date = time();
				return true;
			}

			if (($this->is_partner || $this->is_owner) && !$this->partner) {
				$this->partner = new UserPartner;
				$this->partner->user_id = $this->id;
				$this->partner->save();
			}

			if (($this->is_manager || $this->is_adman) && !$this->manager) {
				$this->manager = new UserManager;
				$this->manager->user_id = $this->id;
				$this->manager->save();
			}

			if ($this->is_teacher && !$this->teacher) {
				$this->teacher = new UserTeacher;
				$this->teacher->user_id = $this->id;
				$this->teacher->save();
			}

			return true;
		}

		protected function afterSave() {
			if ($this->isNewRecord) {
				if ($this->inPartnerGroup) {
					$this->project_id = $this->id;
				} else {
					$this->project_id = Yii::app()->user->projectId;
				}
				$this->currency_id=Yii::app()->db->createCommand('SELECT id FROM {{currency}} LIMIT 1')->queryScalar();
				$this->isNewRecord = false;
				$this->save(false,array('project_id', 'currency_id'));
				$this->setPassword('register');
			}
			return parent::afterSave();
		}

		protected function beforeDelete() {
			if (parent::beforeDelete()) {
				Extauth::model()->deleteAllByAttributes(array('user_id' => $this->id));
				if ($this->partner) $this->partner->delete();
				if ($this->manager) $this->manager->delete();
				if ($this->teacher) $this->teacher->delete();
				return true;
			}
		}

		public static function generateRandomSequence($length=10) {
			$seq = '';
			$rand_table = array('a','b','c','d','e','f','g','h','i','g','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9');
			for ($i = 0; $i < $length; $i++) {
				$seq .= $rand_table[rand(0,35)];
			}
			return $seq;
		}

		public function generateHash($regtime,$passwd) {
			return sha1($regtime.md5($passwd));
		}

		public function generateOldHash($email,$passwd) {
			return sha1($email.md5($passwd));
		}

		public function generateCookie($email,$passwd)  {
			return crypt($email.md5($passwd));
		}

		public function setPassword($view, $password_core = '') {
			if (!$password_core) $password_core = $this->generateRandomSequence(rand(10,15));
			$password_hash = $this->generateHash($this->reg_date,$password_core);
			$this->passwd = $password_hash;
			if ($this->save(false,array('passwd'))) {
				$this->passwd = $password_core;
				$subject = $view == 'register' ? 'Регистрация в системе "ROBBOClubCRM"' : 'Пароль в системе "ROBBOClubCRM" был изменен';
                
                $mail = Yii::app()->Smtpmail;
                	        $mail->SetFrom(Yii::app()->params['adminEmail']);
                	        $mail->Subject    = $subject;
                	        $mail->MsgHTML('<p>Здравствуйте, <b>'.$this->fullname.'</b>!</p>
                                <p>Вы были зарегистрированы в системе управления сайтом "ROBBOClubCRM".</p>
                                <p>Войти в систему вы сможете с помощью следующих параметров:<br/>
                                <b>E-Mail</b>: '.$this->email.'<br/>
                                <b>Пароль</b>: '.$this->passwd.'</p>');
                	        $mail->AddAddress($this->email);
                	        $mail->Send();
				$this->passwd = $password_hash;
			}
		}

		public function getIsCurrentUser() {
			return $this->id == Yii::app()->user->id;
		}

		public function getProfileLink() {
			return CHtml::link(CHtml::encode($this->fullname),array("user/profile","id" => $this->id));
		}

		public function getLessonsData($idSelector = array()) {
			if (!$this->inTeacherGroup) return null;
			$criteria = new CDbCriteria;
			$criteria->with = array('lessons_fail', 'lessons_success');
			$criteria->select = 't.id AS id, t.name AS name';
			if ($idSelector) $criteria->addInCondition('t.id', $idSelector);
			$criteria->compare('t.teacher_id',$this->id);
			$criteria->order = 't.id ASC';
			return new CActiveDataProvider('TrGroup',array('criteria' => $criteria, 'pagination' => false));
		}

		public function getPayment() {
			if (!$this->inTeacherGroup) return null;

			$data = Yii::app()->db->createCommand('SELECT
edu_schedule.id,
edu_schedule.is_conducted,
edu_schedule.did_not_come,
edu_schedule.late,
edu_schedule.held_user_id,
Count(edu_rel_group_member.client_id) AS count,
FROM_UNIXTIME(edu_schedule.datetime_start) as `begin`,
edu_schedule.tr_group_id,
edu_tr_group.teacher_id,
edu_tr_group.tr_center_id,
edu_tr_center.late_fee,
edu_tr_center.fine_for_truancy
FROM
edu_schedule
INNER JOIN edu_tr_group ON edu_tr_group.id = edu_schedule.tr_group_id
INNER JOIN edu_rel_group_member ON edu_rel_group_member.tr_group_id = edu_schedule.tr_group_id
INNER JOIN edu_tr_center ON edu_tr_center.id = edu_tr_group.tr_center_id
WHERE
(edu_tr_group.teacher_id = '.$this->id.' OR edu_schedule.held_user_id = '.$this->id.')
AND
edu_schedule.is_deleted = 0 AND
edu_schedule.datetime_start <= UNIX_TIMESTAMP()
GROUP BY
edu_schedule.id
ORDER BY
edu_schedule.datetime_start DESC
')
				->queryAll();

			if($data){
				foreach ($data as &$value) {
					$value['pay']=$this->getPayByPeople($value['count'], $value);
					$value['info']=$this->getPayInfo($value);
				}
			}

			return new CArrayDataProvider($data, array('pagination' => false));
		}

		private function getPayInfo($data) {
			if($data['did_not_come'])
				$info[]='Не пришел';
			if($data['late'])
				$info[]='Опоздал';
			if($data['held_user_id']<>$this->id)
				$info[]='Занятие провел другой';
			if($data['teacher_id']<>$this->id)
				$info[]='Подменил';
			if(isset($info)){
				return implode(', ', $info);
			}
		}

		private function getPayByPeople($count, $data) {
			$tariff=PaymentTarif::model()->findAll(array(
				'condition'=>'tr_center_id='.$data['tr_center_id'],
				'order'=>'count_begin asc'
			));

			$sum=0;
			if($tariff){
				foreach ($tariff as $value) {
					$tmp_sum=$value->getByIf($count);
					if($tmp_sum>=$sum)
						$sum=$tmp_sum;
				}
			}

			if($data['late'])
				$sum-=(int)$data['late_fee'];
			if($data['did_not_come'])
				$sum-=(int)$data['fine_for_truancy'];
			if($data['held_user_id']<>$this->id)
				$sum=0;
			return $sum.'р.';
		}


		public function getLessonsPrice($idSelector = array()) {
			if (!$this->inTeacherGroup) return null;
			if (!$idSelector) {
				$idSelector = Yii::app()->db->createCommand()
					->select('id')
					->from('{{tr_group}}')
					->where('is_deleted = 0 AND teacher_id = '.$this->id)
					->queryColumn();
			}
			$price = Yii::app()->db->createCommand()
				->select('SUM(rate)')
				->from('{{schedule}} s')
				->join('{{course_rates}} r', 'r.group_id = s.tr_group_id')
				->where(array(
					'and',
					array('in', 'tr_group_id', $idSelector),
					array('and', 'is_conducted = '.Schedule::CONDUCT_OK, 'datetime_start <= '.time())
				))
				->queryScalar();
			return (int)$price;
		}

		public function setType($target) {
			if ($target == 'partner') {
				$this->is_owner = 1;
				$this->is_partner = 1;
			} else {
				$attributeName = 'is_'.$target;
				$this->$attributeName = 1;
			}
		}

		public function search($target) {
			$criteria = new CDbCriteria();
			$criteria->select = 'id, fullname, email, avatar_name, phone_num';
			$customCondition = array();
			foreach (array('is_owner','is_partner','is_manager','is_adman','is_teacher') as $attributeName) {
				if ($this->$attributeName) array_push($customCondition,$attributeName.'=1');
			}
			$criteria->addCondition(implode(' OR ',$customCondition));
			$criteria->compare('fullname',$this->fullname,true);
			$criteria->compare('email',$this->email,true);
			$criteria->compare('phone_num',$this->phone_num,true);
			if (Yii::app()->user->model->project_id) $criteria->compare('project_id',Yii::app()->user->model->project_id);

			$arr = array();
			if ($this->group_name)
				array_push($arr,$this->getIDsByGroupName($this->group_name));
			if ($this->center_id && $centerId = (int)$this->center_id)
				array_push($arr,$this->getIDsByCenterId($centerId));
			if ($arr) {
				$ids = array_pop($arr);
				foreach ($arr as $currentSet) $ids = array_intersect($ids,$currentSet);
				$criteria->addInCondition('id',array_unique($ids));
			}

			return new CActiveDataProvider(__CLASS__,
				array('criteria' => $criteria, 'sort' => array('defaultOrder' => 'id ASC', 'attributes' => array('fullname', 'email')), 'pagination' => array('pageSize' => 40))
			);
		}

		public function getGroupLinks() {
			if ($this->isNewRecord) return null;
			$criteria = new CDbCriteria;
			$criteria->select = 'id, name';
			$criteria->compare('teacher_id',$this->id);
			return ProjectHelper::formatList(TrGroup::model()->findAll($criteria),'trgroup/view');
		}

		protected function getIDsByGroupName($searchValue) {
			$result = array();
			$result = Yii::app()->db->createCommand()
				->select('teacher_id')
				->from(Yii::app()->db->tablePrefix.'tr_group')
				->where(array('like','name','%'.mysql_real_escape_string($searchValue).'%'))
				->queryColumn();
			return $result;
		}

		public function getCenterLinks() {
			if ($this->isNewRecord) return null;
			$centerIDs = Yii::app()->db->createCommand()
				->select('tr_center_id')
				->from(Yii::app()->db->tablePrefix.'tr_group group')
				->where('group.teacher_id = '.$this->id)
				->queryColumn();
			if (!$centerIDs) return null;
			$criteria = new CDbCriteria;
			$criteria->select = 'id, name';
			$criteria->addInCondition('id',$centerIDs);
			return ProjectHelper::formatList(TrCenter::model()->findAll($criteria),'trcenter/view');
		}

		public function getCenterList() {
			$list = TrCenter::model()->getCenterNames();
			return array_merge(array('Все'),$list);
		}

		protected function getIDsByCenterId($centerId) {
			$result = array();
			if (!($centerId = (int)$centerId)) return $result;
			$result = Yii::app()->db->createCommand()
				->select('teacher_id')
				->from('{{tr_group}}')
				->where(array('and','tr_center_id = '.$centerId,'is_deleted = 0'))
				->queryColumn();
			return $result;
		}

		private $_centerFinanceDP;

		public function getCenterFinanceDP($idSelector = array()) {
			if (!$this->inPartnerGroup) return null;
			if ($this->_centerFinanceDP === null) {
				$criteria = new CDbCriteria;
				$criteria->select = 'id, name';
				if ($idSelector) {
					$criteria->addInCondition('id',$idSelector);
				}
				$criteria->compare('project_id',$this->getProjectIdCompare());
				$this->_centerFinanceDP = new CActiveDataProvider('TrCenter',array('criteria' => $criteria, 'pagination' => false));
			}
			return $this->_centerFinanceDP;
		}

		public function getFinanceIncomeData($idSelector = array()) {
			$result = new CAttributeCollection(array('income_debet' => 0, 'income_credit' => 0, 'income_total' => 0));
			if ($idSelector !== null && $dataProvider = $this->getCenterFinanceDP($idSelector)) {
				foreach ($dataProvider->data as $currentObject) {
					$currentObject->setDatetimeLimit($this->datetimeFilter,null,true);
					$financeData = $currentObject->getFinanceIncomeData();
					$result->income_debet += $financeData->income_debet;
					$result->income_credit+= $financeData->income_credit;
					$result->income_total += $financeData->income_total;
				}
			}
			return $result;
		}

		public function getFinanceCostsData($idSelector = array()) {
			$result = new CAttributeCollection(array('costs' => 0));
			if ($idSelector !== null && $dataProvider = $this->getCenterFinanceDP($idSelector)) {
				foreach ($dataProvider->data as $currentObject) {
					$currentObject->setDatetimeLimit($this->datetimeFilter,null,true);
					$financeData = $currentObject->getFinanceCostsData();
					$result->costs += $financeData->costs;
				}
			}
			return $result;
		}

		public function getAvatarMain() {
			if (!$this->avatar_name) return '';
			$file = 'u'.$this->id.'.'.pathinfo($this->avatar_name,PATHINFO_EXTENSION);
			if (file_exists($file))
				return $file;
			else
				return '';
		}

		public function getCostsObject($idSelector = array()) {
			if (!$this->inPartnerGroup) return null;
			if ($idSelector === null) return null;
			$modelCosts = new TrCosts('center');
			if (!$idSelector) {
				$idSelector = Yii::app()->db->createCommand()
					->select('id')
					->from('{{tr_center}}')
					->where(array('and', $this->projectIdCondition, 'is_deleted = 0'))
					->queryColumn();
			}
			$modelCosts->idSelector = $idSelector;
			$modelCosts->setDatetimeLimit($this->datetimeFilter,null,true);
			return $modelCosts;
		}

		public function getLessonsCost($idSelector = array()) {
			if (!$this->inTeacherGroup) return null;
			if (!$idSelector) {
				$idSelector = Yii::app()->db->createCommand()
					->select('id')
					->from('{{tr_group}}')
					->where(array('and', 'teacher_id = '.$this->id, 'is_deleted = 0'))
					->queryColumn();
			}
			$result = Yii::app()->db->createCommand()
				->select('SUM(value)')
				->from('{{tr_costs}}')
				->where(array(
					'and',
					array('and',
					      array('in', 'group_id', $idSelector),
					      'section = '.TrCosts::COSTS_TEACHER_WAGE,
					),
					'is_deleted = 0',
				))
				->queryScalar();
			return (int)$result;
		}

		public function getProjectId() {
			return $this->project_id ? $this->project_id : $this->id;
		}

		public function getProjectIdCompare() {
			return !$this->is_owner ? $this->project_id : null;
		}

		public function getProjectIdCondition($attributeName = 'project_id', $tableAlias = '') {
			return !$this->is_owner ? ($tableAlias ? $tableAlias.'.'.$attributeName : $attributeName).'='.$this->getProjectId() : 1;
		}

		public function getCityName() {

			if ($this->city_id)
				return City::name($this->city_id);

			if ($this->parent && $this->parent->city)
				return $this->parent->city->name;

			return 'Не определен';

		}

		public static function listPartnersArray() {
			$partners = UserMain::model()->findAll('is_deleted = 0 AND (is_partner OR is_owner)');
			return CHtml::listData($partners, 'id', 'fullname');
		}

		public static function projectsList() {
			return array_unique(array_keys(self::listPartnersArray()));
		}

		public static function projectsByCityId($cityId) {

			$projectArray = array();
			$users = self::model()->findAll('city_id = :city_id AND (is_partner = 1 OR is_owner = 1)', array(':city_id'=>$cityId));
			foreach ($users as $user)
				array_push($projectArray, $user->projectId);

			return array_unique($projectArray);
		}

		public function getAvatarPath() {
			$file = Yii::app()->getBaseUrl().'/'.Yii::app()->params['uploadDirectory'].'/avatars/'.$this->avatar_name;
			if (file_exists(Yii::getPathOfAlias('webroot').$file))
				return $file;
			else
				return '';
		}

		public function getAvatarOrigPath() {
			$file = Yii::app()->getBaseUrl().'/'.Yii::app()->params['uploadDirectory'].'/avatars/orig/'.$this->avatar_name;
			if (file_exists(Yii::getPathOfAlias('webroot').$file))
				return $file;
			else
				return '';
		}

	}



?>