<?php

class DebtFilterForm extends CFormModel {
    
    const MAX_LENGTH = 33;

    public $partners = array();
    public $centers = array();
    public $groups = array();
    public $update;
    
    protected $_basePartners = array();
    protected $_baseCenters = array();
    protected $_baseGroups = array();
    
    protected $_hasBase = false;
    
    public function rules() {
        return array(
            array('update', 'in', 'range' => array('partner','center','group')),
            array('centers, groups', 'mergeArr'),
            array('partners, centers, groups', 'checkArr'),
        );
    }
    
    protected function beforeValidate() {
        if (parent::beforeValidate()) {
            if (!$this->partners) $this->centers = array();
            if (!$this->centers)  $this->groups = array();
            return true;
        }
    }
    
    public function checkArr($attributeName,$params) {
        if (empty($this->$attributeName)) return true;
        if (!is_array($this->$attributeName)) return false;
        foreach ($this->$attributeName as $currElem) if (!is_numeric($currElem) || $currElem <= 0) return false;
        return true;
    }
    
    public function mergeArr($attributeName,$params) {
        if (empty($this->$attributeName)) return true;
        if (!is_array($this->$attributeName)) return false;
        $result = array();
        foreach ($this->$attributeName as $currElem) {
            if (!is_array($currElem)) return false;
            foreach ($currElem as $v) array_push($result,$v);
        }
        $this->$attributeName = $result;
        return true;
    }
    
    public function getBase() {
        return array('partner' => $this->getBasePartners(), 'center' => $this->getBaseCenters(), 'group' => $this->getBaseGroups());
    }
    
    public function getBasePartners() {
        return $this->_basePartners;
    }
    
    public function getBaseCenters() {
        return $this->_baseCenters;
    }
    
    public function getBaseGroups() {
        return $this->_baseGroups;
    }
    
    public function setBase($data) {
        if (!$data) return true;
        if (!isset($data['partner']) || !isset($data['center']) || !isset($data['group'])) return false;
        $this->setBasePartners($data['partner']);
        $this->setBaseCenters($data['center']);
        $this->setBaseGroups($data['group']);
        $this->_hasBase = true;
        return true;
    }
    
    public function setBasePartners($data) {
        $this->_basePartners = $data;
    }
    
    public function setBaseCenters($data) {
        $this->_baseCenters = $data;
    }
    
    public function setBaseGroups($data) {
        $this->_baseGroups = $data;
    }
    
    public function getHasBase() { return $this->_hasBase; }
    
    public function addBasePartner(UserMain $partner) {
        if (!isset($this->_basePartners[$partner->id])) {
            $this->_basePartners[$partner->id] = $partner->name;
            //$this->_baseCenters[$partner->id] = array();
        }
    }
    
    public function addBaseCenter(TrCenter $center, $partnerId) {
        if (!isset($this->_baseCenters[$partnerId])) $this->_baseCenters[$partnerId] = array();
        if (!isset($this->_baseCenters[$partnerId][$center->id])) {
            $this->_baseCenters[$partnerId][$center->id] = $center->name;
            //$this->_baseGroups[$center->id] = array();
        }
    }
    
    public function addBaseGroup(TrGroup $group, $centerId) {
        if (!isset($this->_baseGroups[$centerId])) $this->_baseGroups[$centerId] = array();
        if (!isset($this->_baseGroups[$centerId][$group->id])) {
            $this->_baseGroups[$centerId][$group->id] = $group->name;
        }
    }
    
    public function getPartnerNameById($partnerId) {
        $partnerName = isset($this->_basePartners[$partnerId]) ? $this->_basePartners[$partnerId] : '';
        // if (mb_strlen($partnerName) > self::MAX_LENGTH) $partnerName = mb_substr($partnerName,0,self::MAX_LENGTH-3).'...';
        return $partnerName;
    }
    
    public function getCenterNameById($centerId) {
        $centerName = '';
        foreach ($this->_baseCenters as $currSet) if (isset($currSet[$centerId])) { $centerName = $currSet[$centerId]; break; }
        // if (mb_strlen($centerName) > self::MAX_LENGTH) $centerName = mb_substr($centerName,0,self::MAX_LENGTH-3).'...';
        return $centerName;
    }

}

?>