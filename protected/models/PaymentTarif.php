<?php

/**
 * This is the model class for table "{{payment_tarif}}".
 *
 * The followings are the available columns in table '{{payment_tarif}}':
 * @property string $id
 * @property string $tr_center_id
 * @property string $count_begin
 * @property integer $count_end
 * @property string $if
 * @property string $summa
 * @property string $created
 * @property string $modified
 */
class PaymentTarif extends CActiveRecord
{
	public static $if_value=array(
		'>'=>'Больше',
		'='=>'Равно',
		'<'=>'Меньше',
		'<='=>'Меньше или равно',
		'>='=>'Больше или равно',
	);

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => $this->hasAttribute('created') ? 'created' : null,
				'updateAttribute' => $this->hasAttribute('modified') ? 'modified' : null,
			)
		);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{payment_tarif}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tr_center_id, if, summa, count_begin', 'required'),
			array('count_end', 'numerical', 'integerOnly'=>true),
			array('tr_center_id, count_begin, summa', 'length', 'max'=>10),
			array('if', 'length', 'max'=>2),
			array('created, modified', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tr_center_id, count_begin, count_end, if, summa, created, modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tr_center_id' => 'Tr Center',
			'count_begin' => 'Кол-во человек',
			'count_end' => 'Заканчивая кол-вом человек',
			'if' => 'Условие',
			'summa' => 'Сумма',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('tr_center_id',$this->tr_center_id,true);
		$criteria->compare('count_begin',$this->count_begin,true);
		$criteria->compare('count_end',$this->count_end);
		$criteria->compare('if',$this->if,true);
		$criteria->compare('summa',$this->summa,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaymentTarif the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getByIf($count){
		$sum=0;
		$this->count_begin=(int)$this->count_begin;
		switch ($this->if) {
			case '>'://Больше
				if($count>$this->count_begin)
					$sum=$this->summa;
				break;
			case '='://Равно
				if($count==$this->count_begin)
					$sum=$this->summa;
				break;
			case '<'://Меньше
				if($count<$this->count_begin)
					$sum=$this->summa;
				break;
			case '<='://Меньше или равно
				if($count<=$this->count_begin)
					$sum=$this->summa;
				break;
			case '>='://Больше или равно
				if($count>=$this->count_begin)
					$sum=$this->summa;
				break;
		}

		return (int)$sum;
	}
}
