<?php

/*
 * Scenario list:
 *  > createWithManagers
 *  > createDefault
 *  > updateWithManagers
 *  > updateDefault
 */

class   TrCenter extends CActiveRecord {

	public $manager;
	protected $managerIDs;

	protected $_debtByClientId = array();

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{tr_center}}';
    }

	public function getClassName() {
		return 'Центр';
	}

    public function rules() {
        return array(
            array('name', 'required'),
			array('late_fee, fine_for_truancy, city_id', 'numerical', 'integerOnly' => true),
            array('name, address', 'length', 'max' => 255),
			array('contacts', 'length', 'max' => 2000),
			array('manager, managerIDs', 'manager'),
			array('name, address, contacts, city_id', 'safe', 'on' => 'search'),
            array('currency_id','safe'),
        );
    }

    public function relations() {
        return array(
            'managers' => array(self::HAS_MANY, 'TrCenterManager', 'tr_center_id'),
			'groups' => array(self::HAS_MANY, 'TrGroup', 'tr_center_id'),
			'costs'	=> array(self::HAS_MANY, 'TrCosts', 'center_id', 'condition' => 'group_id = 0'),
			'partner' => array(self::BELONGS_TO, 'UserMain', 'project_id'),
			'city'  => array(self::BELONGS_TO, 'City', 'city_id'),
			'currency'  => array(self::BELONGS_TO, 'Currency', 'currency_id'),
        );
    }

	public function behaviors() {
		return array(
			'datetimeLimit' => array('class' => 'application.components.DatetimeLimitBehavior'),
			'financeData' => array('class' => 'application.components.FinanceDataBehavior'),
			'softDelete' => array('class' => 'application.components.SoftDeleteBehavior', 'children' => array('groups', 'costs', 'managers')),
		);
	}

    public function attributeLabels() {
        return array(
            'id' => 'Центр',
            'project_id' => 'ID проекта',
			'name' => 'Название',
			'address' => 'Адрес',
			'contacts' => 'Контакты',
			'create_date' => 'Дата создания',
			'update_date' => 'Дата обновления',
			'manager' => 'Руководители',
			'managerIDs' => 'Руководители',
			'late_fee' => 'Штраф за опоздание',
			'fine_for_truancy' => 'Штраф за прогул',
			'city_id' => 'Город',
			'currency_id' => 'Валюта',
        );
    }

	public function defaultScope() {
		return array(
			'condition' => Yii::app()->user->getProjectIdCondition('project_id',$this->getTableAlias(false,false)),
		);
	}

	public function manager($attributeName,$params) {
		if (!isset($this->$attributeName) || !is_array($this->$attributeName)) {
			$this->addError($attributeName,'Передаваемым типом данных обязан быть массив идентификаторов.');
			return false;
		}
		$managerArr = &$this->$attributeName;
		if ($managerArr) {
			$managerArr = array_unique($managerArr);
			foreach ($managerArr as $currKey => $currentManager) {
				$managerArr[$currKey] = (int)$currentManager;
				if (!UserMain::model()->exists('id = :id', array(':id' => $managerArr[$currKey]))) {
					$this->addError($attributeName,'Некоторые из указанных менеджеров в системе не зарегистрированы!');
					return false;
				}
			}
		}
		return true;
	}

	protected function beforeValidate() {
		if (parent::beforeValidate()) {
			if (!$this->managerIDs) $this->managerIDs = (array)$this->managerIDs;
			return true;
		}
	}

    protected function beforeSave() {
        if (!parent::beforeSave()) return false;

        if ($this->isNewRecord) {
            $this->create_date = time();
            $this->update_date = $this->create_date;
        } else {
            $this->update_date = time();
        }

        return true;
    }

	protected function afterConstruct() {
		$this->manager = array();
		return parent::afterConstruct();
	}

	protected function afterFind() {
		$this->manager = array();
		return parent::afterFind();
	}

	protected function afterSave() {
		if (!$this->isNewRecord) {
			TrCenterManager::model()->deleteAllByAttributes(array('tr_center_id' => $this->id));
		}
		if ($this->managerIDs) {
			foreach ($this->managerIDs as $currentManager) {
				$obj = new TrCenterManager;
				$obj->manager_id = (int)$currentManager;
				$obj->tr_center_id = $this->id;
				$obj->save();
			}
		}
		if ($this->manager) {
			foreach ($this->manager as $currentManager) {
				$obj = new TrCenterManager;
				$obj->manager_id = (int)$currentManager;
				$obj->tr_center_id = $this->id;
				$obj->save();
			}
		}
		parent::afterSave();
	}

	protected function beforeDelete() {
		if (parent::beforeDelete()) {
			if ($this->groups) foreach ($this->groups as $currentObject) $currentObject->delete();
			TrCenterManager::model()->deleteAllByAttributes(array('tr_center_id' => $this->id));
			return true;
		}
	}

	public function getCenters(){
		$ids = array();
		$criteria = new CDbCriteria();
		$criteria->addCondition('manager_id='.Yii::app()->user->getID());
		$data = TrCenterManager::model()->findAll($criteria);
		foreach ($data as $center){
			$ids[$center->tr_center_id] = $center->tr_center_id;
		}

		return $ids;
	}

	public function search() {

		$criteria = new CDbCriteria;
		$criteria->select = 'id, name, address, contacts, city_id';
		$criteria->compare('name', $this->name, true);
		$criteria->compare('address', $this->address, true);
		$criteria->compare('contacts', $this->contacts, true);
		$criteria->compare('city_id', $this->city_id);
		$criteria->addInCondition('id',$this->getCenters(),'AND');

		$criteria->order = 'create_date ASC';

		return new CActiveDataProvider(__CLASS__, array('criteria' => $criteria, 'pagination' => array('pageSize' => 40)));

	}

	public function getCenterNames($project_id = false) {
		$criteria = new CDbCriteria;
		$criteria->select = 'id, name';
		$criteria->order = 'id ASC';

        if ($project_id !== false)
            $criteria->compare('project_id', $project_id);

		$result = array();
		if ($data = TrCenter::model()->findAll($criteria)) {
			foreach ($data as $currentModel) {
				$result[$currentModel->id] = $currentModel->name;
			}
		}
		return $result;
	}

    public function getCenterNamesCriteria($project_id = false,$notIn) {
        $criteria = new CDbCriteria;
        $criteria->select = 'id, name';
        $criteria->order = 'id ASC';
        $criteria->addNotInCondition('id',$notIn);

        if ($project_id !== false)
            $criteria->compare('project_id', $project_id);

        $result = array();
        if ($data = TrCenter::model()->findAll($criteria)) {
            foreach ($data as $currentModel) {
                $result[$currentModel->id] = $currentModel->name;
            }
        }
        return $result;
    }

    public function getCenterNamesIn($project_id = false,$in) {
        $criteria = new CDbCriteria;
        $criteria->select = 'id, name';
        $criteria->order = 'id ASC';
        $criteria->addInCondition('id',$in);

        if ($project_id !== false)
            $criteria->compare('project_id', $project_id);

        $result = array();
        if ($data = TrCenter::model()->findAll($criteria)) {
            foreach ($data as $currentModel) {
                $result[$currentModel->id] = $currentModel->name;
            }
        }
        return $result;
    }

    public static function getLevelList() {
        return array(
            0 => 'Базовый уровень',
            1 => 'Продвинутый уровень',
        );
    }

	public function attachManagerNames() {
		$newArr = array();
		if ($this->managers) {
			foreach ($this->managers as $currManager) $newArr[$currManager->manager_id] = $currManager->profile->fullname;
		}
		$this->manager = $newArr;
		return $this->manager;
	}

	public function getManagerIDs() {
		if (!empty($this->managerIDs)) return $this->managerIDs;
		$result = array();
		if ($this->managers) {
			foreach ($this->managers as $currManager) array_push($result,$currManager->manager_id);
		}
		return $this->managerIDs = $result;
	}

	public function getManagerLinks() {
		if (!$this->managers) {
			return '';
		} else {
			$result = array();
			foreach ($this->managers as $currentManager) {
				$result[] = CHtml::link($currentManager->profile->fullname, array('user/view','id' => $currentManager->manager_id));
			}
			return implode('<br/>',$result);
		}
	}

	public function getTrCourseList($centerId = null) {
		if ($centerId === null) $centerId = $this->id;

		$criteria = new CDbCriteria;
		$criteria->select = 'id, name';
		$criteria->compare('tr_center_id',(int)$centerId);

		$result = array();
		if ($arr = TrCourse::model()->findAll($criteria)) {
			foreach ($arr as $currObj) {
				$result[$currObj->id] = $currObj->name;
			}
		}

		return $result;
	}

	private $_groupFinanceDP;

	public function getGroupFinanceDP($idSelector = array()) {
		if ($this->_groupFinanceDP === null) {
			$criteria = new CDbCriteria;
			$criteria->with = array('lessons_success' => array('condition' => $this->getDatetimeCondition('datetime_start')));
			$criteria->select = 'id, name, tr_course_id';
			$criteria->compare('tr_center_id',$this->id);
			if ($idSelector) {
				$criteria->addInCondition('t.id',$idSelector);
			}
			$this->_groupFinanceDP = new CActiveDataProvider('TrGroup',array('criteria' => $criteria, 'pagination' => false));
		}
		return $this->_groupFinanceDP;
	}

	public function getFinanceIncomeData($idSelector = array()) {
		$result = new CAttributeCollection(array('income_debet' => 0, 'income_credit' => 0, 'income_total' => 0));
		if ($idSelector !== null && $dataProvider = $this->getGroupFinanceDP($idSelector)) {
			foreach ($dataProvider->data as $currentObject) {
				$currentObject->setDatetimeLimit($this->datetimeFilter,null,true);
				$financeData = $currentObject->getFinanceIncomeData();
				$result->income_debet += $financeData->income_debet;
				$result->income_credit+= $financeData->income_credit;
				$result->income_total += $financeData->income_total;
			}
		}
		return $result;
	}

	public function getFinanceIncomeDataStat() {
		$monthArr = array(); for ($i = 0; $i < 12; $i++) $monthArr[$i] = 0;
		$debet = $monthArr;
		$credit= $monthArr;
		$total = $monthArr;
		if ($dataProvider = $this->getGroupFinanceDP()) {
			foreach ($dataProvider->data as $currentObject) {
				$currentObject->setDatetimeLimit($this->datetimeFilter,null,true);
				$financeData = $currentObject->getFinanceIncomeDataStat();
				for ($i = 0; $i <= 11; $i++) {
					$debet[$i] += $financeData->income_debet[$i];
					$credit[$i]+= $financeData->income_credit[$i];
					$total[$i] += $financeData->income_total[$i];
				}
			}
		}
		return $result = new CAttributeCollection(array('income_debet' => $debet, 'income_credit' => $credit, 'income_total' => $total));;
	}

	public function getFinanceCostsData($idSelector = array()) {
		$where = $idSelector ? array('and',array('in','costs.group_id',$idSelector),'t.id = '.$this->id) : 't.id = '.$this->id;
        $where = $this->addDatetimeCondition($where,'costs.datetime');
        $data = Yii::app()->db->createCommand()
            ->select('SUM(costs.value) AS sum')
            ->from(Yii::app()->db->tablePrefix.'tr_center t')
            ->join(Yii::app()->db->tablePrefix.'tr_costs costs','costs.center_id = t.id')
			->where($where)
			->queryScalar();
		return new CAttributeCollection(array('costs' => (int)$data));
	}

	public function getFinanceCostsDataStat() {
        $data = Yii::app()->db->createCommand()
            ->select('FROM_UNIXTIME(costs.datetime,\'%m\') AS month, SUM(costs.value) AS sum')
            ->from(Yii::app()->db->tablePrefix.'tr_center t')
            ->join(Yii::app()->db->tablePrefix.'tr_costs costs','costs.center_id = t.id')
            ->where($this->addDatetimeCondition('t.id = '.$this->id,'costs.datetime'))
            ->group('month')
            ->queryAll();

		$result = array(); for ($i = 0; $i <= 11; $i++) $result[$i] = 0;
		foreach ($data as $currentSet) {
			if (!($currentSet['month'] = (int)$currentSet['month'])) continue;
			$result[$currentSet['month']-1] = $currentSet['sum'];
		}
		return new CAttributeCollection(array('costs' => $result));
	}

	public function getIncome() {
		$financeData = $this->getFinanceIncomeData();
		return $financeData->income_debet - $financeData->income_credit;
	}

	public function getCostsObject($idSelector = array()) {
        if ($idSelector === null) return null;
		$modelCosts = new TrCosts('group');
		$modelCosts->center_id = $this->id;
        if (!$idSelector) {
            $idSelector = Yii::app()->db->createCommand()
                ->select('t.id')
                ->from(Yii::app()->db->tablePrefix.'tr_group t')
                ->where('t.tr_center_id = '.$this->id)
                ->queryColumn();
        }
		$modelCosts->idSelector = $idSelector;
		$modelCosts->setDatetimeLimit($this->datetimeFilter,null,true);
		return $modelCosts;
	}

	public function getDebtByClientId($clientId) {
		if (!$clientId) return null;
		if (isset($this->_debtByClientId[$clientId])) return $this->_debtByClientId[$clientId];
		$this->_debtByClientId[$clientId] = 0;
		foreach ($this->groups as $currRecord) $this->_debtByClientId[$clientId] += $currRecord->getDebtByClientId($clientId);
		return $this->_debtByClientId[$clientId];
	}

	public function getCityName() {

        if ($this->city_id)
            return City::name($this->city_id);

        if ($this->parent && $this->parent->city)
            return $this->parent->city->name;

        return 'Не определен';

    }

	public function getCurrencyName()
	{
		if($this->currency){
			return $this->currency->name;
		}else{
			return 'Укажите валюту у центра';
		}
	}

}



?>