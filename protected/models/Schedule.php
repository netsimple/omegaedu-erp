<?php

class Schedule extends CActiveRecord {

	const CONDUCT_UNDEFINED = 0;
	const CONDUCT_OK		= 1;
	const CONDUCT_MOVED	 	= 2;
	const CONDUCT_CANCELED  = 3;

    public $hours_start;
    public $minutes_start;

    public $hours_end;
    public $minutes_end;

	public $date;
	public $datefrom;
	public $dateto;

	public $teacher_name;
	public $course_name;
	public $group_name;
	public $city_id;

	protected $_currentDatetimeStart = 0;
	protected $_academicHours;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{schedule}}';
    }

	public function getClassName() {
		return 'Урок';
	}

    public function rules() {
        return array(
            array('hours_start, minutes_start, hours_end, minutes_end, datetime_start, datetime_end, tr_center_id, tr_group_id, tr_course_id, project_id', 'required', 'on' => 'create, update'),
			array('hours_start, hours_end', 'numerical', 'integerOnly' => false, 'min' => 0, 'max' => 23, 'on' => 'create, update'),
			array('did_not_come, late, held_user_id', 'numerical', 'integerOnly' => true),
			array('minutes_start, minutes_end', 'numerical', 'integerOnly' => false, 'min' => 0, 'max' => 59, 'on' => 'create, update'),
			array('day', 'numerical', 'min' => 0, 'max' => 7),
			array('date, datefrom, dateto', 'date', 'format' => 'd MMM yyyy', 'on' => 'create, update, search'),

            array('datetime_start', 'date', 'format' => 'd MMM yyyy HH:mm:00', 'timestampAttribute' => 'datetime_start', 'allowEmpty' => true, 'on' => 'create, update, search'),
            array('datetime_end', 'date', 'format' => 'd MMM yyyy HH:mm:00', 'timestampAttribute' => 'datetime_end', 'allowEmpty' => true, 'on' => 'create, update, search'),
            array('hours_start, minutes_start, hours_end, minutes_end, datetime_start, datetime_end, tr_center_id, tr_group_id, tr_course_id, date, datefrom, dateto, city_id', 'safe', 'on' => 'search'),

			array('datetime_start', 'date', 'format' => 'yyyy-MM-dd HH:mm', 'timestampAttribute' => 'datetime_start', 'on' => 'description, description_conducted, description_clients, description_conducted_clients'),
			array('datetime_end', 	'date', 'format' => 'yyyy-MM-dd HH:mm', 'timestampAttribute' => 'datetime_end',   'on' => 'description, description_conducted, description_clients, description_conducted_clients'),
			array('description', 'length', 'max'=>2000, 'on'=>'description, description_conducted, description_clients, description_conducted_clients'),
			array('is_conducted', 'in', 'range' => array(self::CONDUCT_UNDEFINED, self::CONDUCT_OK, self::CONDUCT_MOVED, self::CONDUCT_CANCELED), 'on' => 'conducted, description_clients, description_conducted, conducted_clients, description_conducted_clients'),
			array('group_name, teacher_name, tr_center_id, course_name, datetime_start, is_conducted', 'safe', 'on' => 'list'),

            //array('datetime_start', 'compare', 'compareAttribute' => 'datetime_end', 'operator' => '<', 'message'=>'Дата начала должна быть меньше даты завершения'),
            //array('datetime_end', 'compare', 'compareAttribute' => 'datetime_start', 'operator' => '>', 'message'=>'Дата завершения должна быть больше даты начала'),
        );
    }

    public function behaviors() {
        return array(
            'formatDates' => array('class' => 'application.components.FormatDatesBehavior', 'attributes' => array()),
			'softDelete' => array('class' => 'application.components.SoftDeleteBehavior', 'parents' => array('group')),
        );
    }

    public function relations() {
        return array(
            'user' => array(self::BELONGS_TO, 'UserMain', 'user_id'),
            'held_user' => array(self::BELONGS_TO, 'UserMain', 'held_user_id'),
            'group' => array(self::BELONGS_TO, 'TrGroup', 'tr_group_id'),
            'center' => array(self::BELONGS_TO, 'TrCenter', 'tr_center_id'),
            'course' => array(self::BELONGS_TO, 'TrCourse', 'tr_course_id'),
			'clients' => array(self::HAS_MANY, 'TrClientLog', 'tr_lesson_id'),
			'summaryDebt' => array(self::STAT, 'TrClientLog', 'tr_lesson_id', 'select' => 'SUM(debt)'),
			'seenNoted' => array(self::STAT, 'TrClientLog', 'tr_lesson_id', 'select' => 'SUM(is_skipped)'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'tr_center_id' => 'Учебный центр',
            'tr_group_id' => 'Группа',
			'group_name' => 'Группа',
            'tr_course_id' => 'Предмет',
			'course_name' => 'Предмет',
			'day' => 'Дни',
			'time_start' => 'Время начала',
			'time_end' => 'Время завершения',
			'datetime_start' => 'Дата начала',
			'datetime_end' => 'Дата завершения',
			'date' => 'Дата',
			'minutes' => 'Минуты',
			'user_id' => 'Преподаватель',
			'held_user_id' => 'Провел преподаватель',
			'create_date' => 'Дата создания',
			'update_date' => 'Дата обновления',
			'description' => 'Описание',
			'is_conducted' => 'Статус',
			'teacher_name' => 'Преподаватель',
			'did_not_come' => 'Преподаватель не пришел',
			'late' => 'Преподаватель опоздал',
        );
    }

	public function defaultScope() {
		return array(
			'condition' => Yii::app()->user->getProjectIdCondition('project_id',$this->getTableAlias(false,false)),
		);
	}

	public function scopes() {
		$currDatetime = time();
		return array(
			'future' => array('condition' => 'datetime_start > '.$currDatetime, 'order' => 'datetime_start ASC'),
			'past'	 => array('condition' => 'datetime_start <= '.$currDatetime, 'order' => 'datetime_start DESC'),
		);
	}

	protected function afterFind() {
		if ($this->hasAttribute('datetime_start')) $this->_currentDatetimeStart = $this->datetime_start;

        $this->datesFormatFromTo('d MMM yyyy', 'datetime_start', 'date');
        $this->datesFormatFromTo('d MMM yyyy', 'datetime_start', 'datefrom');
        $this->datesFormatFromTo('d MMM yyyy', 'datetime_end', 'dateto');
        $this->datesFormatFromTo('HH', 'datetime_start', 'hours_start');
        $this->datesFormatFromTo('mm', 'datetime_start', 'minutes_start');
        $this->datesFormatFromTo('HH', 'datetime_end', 'hours_end');
        $this->datesFormatFromTo('mm', 'datetime_end', 'minutes_end');

		if ($this->scenario != 'search' && $this->_currentDatetimeStart <= time() && !count($this->clients) && $this->group && $this->group->members) {
			foreach ($this->group->members as $currentClient) {
				$client = new TrClientLog;
				$client->tr_group_id = $this->tr_group_id;
				$client->client_id = $currentClient->id;
				$client->is_skipped = TrClientLog::SKIPPED_UNDEFINED;
				$client->tr_lesson_id = $this->id;
				$client->insert();
			}
		}
		parent::afterFind();
	}

	protected function beforeDelete() {
		if (parent::beforeDelete()) {
			if ($this->clients) {
				foreach ($this->clients as $currentClient) $currentClient->delete();
			}
			return true;
		}
	}

    protected function beforeSave() {
        if (!parent::beforeSave()) return false;
        if ($this->isNewRecord) {
            $this->create_date = time();
            $this->update_date = $this->create_date;
			$this->user_id = Yii::app()->user->id;
			if (!$this->group) {
                $this->addError('badGroup', "Такой группы не существует" );
                return false;
            }
			$this->held_user_id = $this->group->teacher_id;
			
			$rateObj = CourseRates::model()->findByAttributes(array('teacher_id' => $this->group->teacher_id, 'group_id' => $this->group->id));
			if ($rateObj) $this->teacher_rate = $rateObj->rate;
        } else {
            $this->update_date = time();
        }
        return true;
    }

	protected function afterSave() {
		if ($this->is_conducted == self::CONDUCT_MOVED || $this->is_conducted == self::CONDUCT_CANCELED) {
			foreach ($this->clients as $currentClient) {
				$currentClient->is_skipped = TrClientLog::SKIPPED_BY_REASON;
				$currentClient->save();
			}
		}
		parent::afterSave();
	}

	protected function afterConstruct() {
		$this->description = '';
		$this->is_conducted = self::CONDUCT_UNDEFINED;
		parent::afterConstruct();
	}

    protected function beforeValidate() {

		if (parent::beforeValidate()) {
			if ($this->scenario == 'ajaxUpdate') $this->setUpdateScenario();
			if ($this->scenario == 'create' || $this->scenario == 'update' || $this->scenario == 'search') {
				$this->datetime_start = $this->date . " " . $this->hours_start . ":" . $this->minutes_start . ":00";
				$this->datetime_end = $this->date . " " . $this->hours_end . ":" . $this->minutes_end . ":00";
			}
			return true;
		}

    }

	public function search() {

		$criteria = new CDbCriteria;
		$criteria->compare('project_id',Yii::app()->user->projectIdCompare);
		$criteria->compare('tr_center_id',$this->tr_center_id);
		$criteria->compare('tr_group_id',$this->tr_group_id);
		$criteria->compare('tr_course_id',$this->tr_course_id);
		$criteria->compare('day',$this->day);

        if ($this->city_id) {
            $projects = UserMain::projectsByCityId($this->city_id);
            $criteria->addInCondition('project_id', $projects);
        }

        $datefrom = CDateTimeParser::parse($this->datefrom,'d MMM yyyy');
        $dateto = CDateTimeParser::parse($this->dateto,'d MMM yyyy') + 60*60*24;
        if ($datefrom && $dateto)
		    $criteria->addBetweenCondition('datetime_start', $datefrom, $dateto);

		$criteria->order = 'datetime_start ASC, id ASC';

		return Schedule::model()->findAll($criteria);

		//return new CActiveDataProvider('Schedule', array('criteria' => $criteria, 'pagination'=>false));

	}

	public function getDayList() {
		return array(
			'Понедельник',
			'Вторник',
			'Среда',
			'Четверг',
			'Пятница',
			'Суббота',
			'Воскресенье'
		);
	}

	public function getHourList() {
		$result = array();
		for ($i = 8; $i < 23; $i++) {
            $i = $i < 10 ? '0'.$i : $i;
			$result[$i] = $i;
		}
		return $result;
	}

	public function getMinuteList() {
		$result = array();
		for ($i = 0; $i < 60; $i+=5) {
            $i = $i < 10 ? '0'.$i : $i;
			$result[$i] = $i;
		}
		return $result;
	}

	public function getName() {
		$name = Yii::app()->dateFormatter->format('d MMM yyyy, HH:mm',$this->_currentDatetimeStart);
		if ($this->group) $name = $this->group->name . ', '.$name;
		return $name;
	}

    public function addFromCalendar ($data) {
        $from = CDateTimeParser::parse($this->datefrom,'d MMM yyyy');
        $to = CDateTimeParser::parse($this->dateto,'d MMM yyyy');

        if ($from > $to) {
            $this->addError('badPeriod','Неверно задан период');
            return false;
        }

        $ids = array();
        while ($from <= $to) {

            $currentDay = Yii::app()->dateFormatter->format('c', $from);
            $currentDay--;

            if (in_array($currentDay, $data['day'])) {

                $model = new Schedule('create');
                $model->project_id = Yii::app()->user->projectId;
                $model->attributes = $data;

                $model->date = $from;
                $model->datesFormatFromTo('d MMM yyyy', 'date', 'date');
                $model->day = $currentDay;



                if ($model->save()) {
                    $ids[] = $model->id;

                    if ($this->hasOverlap($model)) {
                        $all = self::model()->findAll('id IN(:ids)', array(':ids'=>implode(',', $ids)));
                        foreach ($all as $item)
                            $item->delete();

                        return false;
                    }

                } else {
                    $this->addErrors($model->getErrors());
                    if ($ids) {
                        $all = self::model()->findAll('id IN(:ids)', array(':ids'=>implode(',', $ids)));
                        foreach ($all as $item)
                            $item->delete();
                    }
                    return false;
                }
            }

            $from += 60*60*24;
        }

        return true;
    }

    public function hasOverlap($model) {
        $c = new CDbCriteria;
        $c->addCondition("
            id != $model->id
            AND
            tr_group_id = $model->tr_group_id
            AND (
                (datetime_start BETWEEN $model->datetime_start AND $model->datetime_end)
                OR
                (datetime_end BETWEEN $model->datetime_start AND $model->datetime_end)
                OR (
                    ($model->datetime_start BETWEEN datetime_start AND datetime_end)
                    AND
                    ($model->datetime_end BETWEEN datetime_start AND datetime_end)
                )
            )
        ");

        $overlap = Schedule::model()->find($c);

        if ($overlap) {
            $this->addError('overlap', "У группы {$model->group->name} уже есть занятие по предмету {$overlap->course->name} {$overlap->date} c {$overlap->hours_start}:{$overlap->minutes_end} до {$overlap->hours_end}:{$overlap->minutes_end}" );
            return true;
        } else {
            return false;
        }
    }

	public function getConductedList() {
		return array(
			self::CONDUCT_UNDEFINED => 'Не указано',
			self::CONDUCT_OK 		=> 'Проведено',
			self::CONDUCT_MOVED 	=> 'Перенесено',
			self::CONDUCT_CANCELED  => 'Не проведено',
		);
	}

    public function getCenterList() {
        $list = TrCenter::model()->getCenterNames();
        return $list;
    }

	public function setUpdateScenario() {
		$scenario = array();
		foreach (array('description','conducted','clients') as $currKey)
			if ($this->getUpdateAccess($currKey)) array_push($scenario,$currKey);
		$this->scenario = implode('_',$scenario);
	}

	public function getUpdateAccess($attributeName) {
		switch ($attributeName) {
			case 'description':
				return Yii::app()->user->checkAccess('schedule/update','description');
			case 'is_conducted':
			case 'conducted':
			case 'conductcheck':
				return Yii::app()->user->checkAccess('schedule/update','conductcheck') && $this->_currentDatetimeStart <= time();
			case 'clients':
			case 'clientlist':
				return Yii::app()->user->checkAccess('schedule/update','clientlist') && $this->_currentDatetimeStart <= time() && $this->clients;
		}
		return false;
	}

	public function getDataProvider(){
		$with = array();
		$criteria = new CDbCriteria;
		$criteria->compare('project_id',Yii::app()->user->projectIdCompare);
		$criteria->compare('tr_center_id',$this->tr_center_id);
		if ($this->course_name) {
			$with['course'] = array('select' => 'name', 'joinType' => 'INNER JOIN');
			$criteria->compare('course.name',$this->course_name,true);
		}
		if ($this->group_name) {
			$with['group'] = array('select' => 'name', 'joinType' => 'INNER JOIN');
			$criteria->compare('group.name',$this->group_name,true);
		}
		if ($this->datetime_start && $timestamp = CDateTimeParser::parse($this->datetime_start,'d MMM yyyy',array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0))) {
            $criteria->addBetweenCondition('datetime_start',$timestamp,$timestamp+86400);
        }
		$criteria->compare('is_conducted',$this->is_conducted);
		$criteria->with = $with;
		$criteria->mergeWith($this->dbCriteria);

		return new CActiveDataProvider(__CLASS__, array('criteria' => $criteria, 'pagination' => array('pageSize' => 40)));
	}

	public function getAcademicHours() {
		if (empty($this->_academicHours)) $this->_academicHours = round(($this->datetime_end - $this->datetime_start) / (Yii::app()->params['academic_hour'] * 60), 2);
		return $this->_academicHours;
	}

	public function getLessonCost($clientId) {
		$hours = $this->academicHours;

		if ($clientId === null) {

			$cost = $this->teacher_rate;

		} else {

			/*
			$criteria = new CDbCriteria;
			$criteria->select = 'value, hours';
			$criteria->addCondition('hours > '.$hours);
			$criteria->addCondition('period_start <= '.$this->datetime_start);
			$criteria->addCondition('period_end >= '.$this->datetime_start);
			$object = TrIncome::model()->findByAttributes(array('client_id' => $clientId, 'course_id' => $this->tr_course_id),$criteria);

			if ($object) {
				$cost = $object->value / $object->hours;
			} else {
			*/
				$formattedDate = Yii::app()->dateFormatter->format('yyyy-MM-dd',$this->datetime_start);
				$criteria = new CDbCriteria;
				$criteria->select = 'period_time, period_price';
				$criteria->addCondition('period_time > '.$hours);
				$criteria->with = array(
					'contract' => array('joinType' => 'INNER JOIN', 'select' => false, 'condition' => "client_id = $clientId AND start_date <= '$formattedDate' AND end_date >= '$formattedDate'"),
				);
				$object = ContractCourse::model()->findByAttributes(array('course_id' => $this->tr_course_id),$criteria);

				$cost = $object ? $object->period_price / $object->period_time : $this->teacher_rate;
			/*
			}
			*/

		}

		return round($cost * $hours,2);
	}

	public function getList(){
		$ids = array();
		$criteria = new CDbCriteria();
		$criteria->addCondition('manager_id='.Yii::app()->user->getID());
		$data = TrCenterManager::model()->findAll($criteria);
		foreach ($data as $center){
			$ids[$center->tr_center_id] = $center->tr_center_id;
		}

		$criteria = new CDbCriteria();
		$criteria->addInCondition('id',$ids,'AND');
		$centers = TrCenter::model()->findAll($criteria);

		return CHtml::listData($centers,
			'id', 'name');
	}
}



?>