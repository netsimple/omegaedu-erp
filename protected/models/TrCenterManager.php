<?php

class TrCenterManager extends CActiveRecord {
	
	public $fullname;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{tr_center_managers}}';
    }
    
    public function relations() {
        return array(
            'center' => array(self::BELONGS_TO, 'TrCenter', 'tr_center_id'),
			'profile'=> array(self::BELONGS_TO, 'UserMain', 'manager_id'),
        );
    }
    
    public function attributeLabels() {
        return array(

        );
	}
	
	public function getId() {
		return array('tr_center_id', 'manager_id');
	}
    
}



?>