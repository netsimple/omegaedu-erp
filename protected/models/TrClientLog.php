<?php

class TrClientLog extends CActiveRecord {
	
	const SKIPPED_UNDEFINED = 0;
	const SKIPPED_NO	  = 1;
	const SKIPPED_YES	  = 2;
	const SKIPPED_BY_REASON = 3;
	
	public $datetime;
	public $tr_center_id;
	
	protected $_hours;
	protected $_income;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{tr_client_log}}';
    }
	
	public function getClassName() {
		$name = 'Клиент';
		if ($this->lesson) $name .= ' ('.$this->lesson->name.')';
		return $name;
	}
    
    public function rules() {
        return array(
			array('is_skipped', 'in', 'range' => array(self::SKIPPED_UNDEFINED,self::SKIPPED_NO,self::SKIPPED_YES,self::SKIPPED_BY_REASON)),
        );
    }
    
    public function relations() {
        return array(
			'client' => array(self::BELONGS_TO,'Client','client_id'),
			'lesson' => array(self::BELONGS_TO,'Schedule','tr_lesson_id'),
        );
    }
    
    public function attributeLabels() {
        return array(
            'id' => 'ID',
			'tr_lesson_id' => 'Занятие',
			'client_id' => 'Клиент',
			'is_skipped' => 'Посещение',
			'debt' => 'Списано',
        );
    }
	
	public function search() {
		
		if (!$this->tr_lesson_id) return null;
		
		$criteria = new CDbCriteria;
		$criteria->select = 'id, client.fullname AS fullname, client_id, is_skipped, tr_lesson_id, debt';
		$criteria->with = array('client');
		$criteria->compare('tr_lesson_id',$this->tr_lesson_id);

		return new CActiveDataProvider(__CLASS__, array('criteria' => $criteria, 'pagination' => false, 'sort' => array('defaultOrder' => 'fullname ASC')));
	}
	
	public function getIsSkippedList() {
		return array(
			self::SKIPPED_UNDEFINED => 	'Не указано',
			self::SKIPPED_NO => 		'Явился',
			self::SKIPPED_YES => 		'Не явился',
			self::SKIPPED_BY_REASON => 	'Не явился (ув.причина)',
		);
	}
	
	public function getDatetime() {
		if (!$this->lesson) return null;
		return $this->lesson->datetime;
	}
	
	public function getTr_center_id() {
		if (!$this->lesson) return null;
		return $this->lesson->tr_center_id;
	}
	
	public function getIncome() {
		if (!$this->lesson) return null;
		if ($this->_income !== null) return $this->_income;
        $model = TrIncome::model()->findByAttributes(array(
            'period_start' => $this->lesson->datetime_start,
            'client_id' => $this->client_id,
            'course_id' => $this->lesson->tr_course_id),
            'hours < 0'
        );
		if (!$model) {
			$model = new TrIncome('clientInsert');
			$model->period_start = $model->period_end = $this->lesson->datetime_start;
			$model->client_id = $this->client_id;
			$model->course_id = $this->lesson->tr_course_id;
		}
		$model->hours = -$this->lesson->academicHours;
		$model->value = $this->lesson->getLessonCost($this->client_id);
		return $this->_income = $model;
	}
	
	public function updateIncome() {
		if ($this->is_skipped == self::SKIPPED_YES || $this->is_skipped == self::SKIPPED_NO) {

			if (!$this->lesson) return false;
			
			if ($this->income->isNewRecord) {
				$this->income->insert();
			} else {
				$this->income->update(array('hours','value'));
			}

		} else {

			if (!$this->isNewRecord) {
				Yii::app()->db->createCommand()->delete('{{tr_income}}',array(
					'and',
					array('and', 'course_id = '.$this->lesson->tr_course_id, 'period_start = '.$this->lesson->datetime_start),
					array('and', 'client_id = '.$this->client_id, 'hours < 0'),
				));
			}

		}
	}
	
	protected function beforeSave() {
		if (parent::beforeSave()) {
			if ($this->scenario == 'ajaxUpdate') return true;
			if ($this->is_skipped == self::SKIPPED_YES || $this->is_skipped == self::SKIPPED_NO) {
				$this->debt = $this->income->debt;
			} else {
				$this->debt = 0;
			}
			return true;
		}
	}
	
	protected function afterSave() {
		if ($this->scenario == 'ajaxUpdate') {
			$this->income->recountLessonsPaid();
		}
		return parent::afterSave();
	}
	
	public function getName() {
		return $this->client ? $this->client->fullname : 'ID #'.$this->id;
	}
    
}



?>