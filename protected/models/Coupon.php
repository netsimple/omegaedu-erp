<?php

class Coupon extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{coupon}}';
    }
    
    public function rules() {
        return array(
            array('coupon, dealer_id', 'required'),
            array('dealer_id', 'unique'),
            array('coupon', 'unique'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'coupon'=>'Купон'
        );
    }
    
    public function getId() {
        return $this->coupon;
    }

}

?>