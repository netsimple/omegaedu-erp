<?php

	class City extends CActiveRecord {

		private static $projectToCityRel = array();

		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		public function tableName() {
			return '{{city}}';
		}

		public function getClassName() {
			return 'Город';
		}

		public function rules() {
			return array(
				array('name', 'required'),
				array('name', 'length', 'max' => 255),
				array('id, name', 'safe', 'on'=>'search'),
			);
		}

		public function attributeLabels() {
			return array(
				'id' => 'ID',
				'name' => 'Название',
			);
		}

		public static function range() {
			return Yii::app()->db->createCommand()->select('id')->from('{{city}}')->queryColumn();
		}

		public static function listArray() {
			return CHtml::listData(self::model()->findAll(), 'id', 'name');
		}

		public static function name($id) {
			$city = self::model()->findByPk($id);

			if ($city)
				return $city->name;
			else
				return 'Не определен';
		}

		public static function nameByProjectId($id) {

			if (isset(self::$projectToCityRel[$id]))
				return self::$projectToCityRel[$id];

			$user = UserMain::model()->findByPk($id);

			if ($user && $user->city)
				self::$projectToCityRel[$id] = $user->city->name;
			else
				self::$projectToCityRel[$id] = 'Не определен';

			return self::$projectToCityRel[$id];
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 *
		 * Typical usecase:
		 * - Initialize the model fields with values from filter form.
		 * - Execute this method to get CActiveDataProvider instance which will filter
		 * models according to data in model fields.
		 * - Pass data provider to CGridView, CListView or any similar widget.
		 *
		 * @return CActiveDataProvider the data provider that can return the models
		 * based on the search/filter conditions.
		 */
		public function search()
		{
			// @todo Please modify the following code to remove attributes that should not be searched.

			$criteria=new CDbCriteria;

			$criteria->compare('id',$this->id,true);
			$criteria->compare('name',$this->name,true);

			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
			));
		}

	}