<?php

class ExtProfileWidget extends CWidget {

	public $component = 'eauth';
	public $services = null;
	public $predefinedServices = null;
	public $popup = null;
	public $action = null;
	public $cssFile = true;
	
	public $activeServices = null;

	public function init() {
		parent::init();

		// EAuth component
		$component = Yii::app()->getComponent($this->component);

		// Some default properties from component configuration
		if (!isset($this->services)) {
			$this->services = $component->getServices();
		}

		if (is_array($this->predefinedServices)) {
			$_services = array();
			foreach ($this->predefinedServices as $_serviceName) {
				if (isset($this->services[$_serviceName]))
					$_services[$_serviceName] = $this->services[$_serviceName];
			}
			$this->services = $_services;
		}		
		
		if (!isset($this->popup)) {
			$this->popup = $component->popup;
		}

		// Set the current route, if it is not set.
		if (!isset($this->action)) {
			$this->action = Yii::app()->urlManager->parseUrl(Yii::app()->request);
		}
	}

    public function run() {
		parent::run();
		
		$active = array();
		$disabled=$this->services;
		foreach ($this->activeServices as $objExtAuth) {
			if (isset($this->services[$objExtAuth->service_name])) {
				$active[$objExtAuth->service_name] = $this->services[$objExtAuth->service_name];
				unset($disabled[$objExtAuth->service_name]);
			}
		}

		$this->registerAssets();
		$this->render('profile', array(
			'id' => $this->getId(),
			'activeServices' => $active,
			'disabledServices' => $disabled,
			'action' => $this->action,
		));
    }

	protected function registerAssets() {
		$cs = Yii::app()->clientScript;
		$cs->registerCoreScript('jquery');

		$assets_path = Yii::getPathOfAlias('ext.eauth').DIRECTORY_SEPARATOR.'assets';
		$url = Yii::app()->assetManager->publish($assets_path, false, -1, YII_DEBUG);
		if($this->cssFile) $cs->registerCssFile($url.'/css/auth.css');

		// Open the authorization dilalog in popup window.
		if ($this->popup) {
			$cs->registerScriptFile($url.'/js/auth.js', CClientScript::POS_END);
			$js = '';
			foreach ($this->services as $name => $service) {
				$args = $service->jsArguments;
				$args['id'] = $service->id;
				$js .= '$(".auth-service.'.$service->id.' a").eauth('.json_encode($args).');'."\n";
			}
			$cs->registerScript('eauth-services', $js, CClientScript::POS_READY);
		}
	}
}
