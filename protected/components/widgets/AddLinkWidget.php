<?php

class AddLinkWidget extends CWidget {
    
    public $actionUrl;
    public $accessKey;
    public $htmlOptions;

    public function run() {
        if (empty($this->actionUrl)) $this->actionUrl = array($this->controller->id.'/create');
        if (empty($this->accessKey)) $this->accessKey = is_array($this->actionUrl) ? $this->actionUrl[0] : $this->actionUrl;
        if ($this->accessKey && !Yii::app()->user->checkAccess(is_array($this->accessKey) ? $this->accessKey[0] : $this->accessKey, is_array($this->accessKey) ? $this->accessKey[1] : '')) return;
        if (empty($this->htmlOptions)) $this->htmlOptions = array();
        if (!isset($this->htmlOptions['style'])) $this->htmlOptions['style'] = 'text-decoration: none; color: #000000;';
        echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/images/icon-add-24x24.png','',array('style' => 'vertical-align: middle;')).CHtml::encode(' Добавить'),$this->actionUrl,$this->htmlOptions);
    }

}

?>