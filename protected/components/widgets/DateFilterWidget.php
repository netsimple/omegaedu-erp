<?php

class DateFilterWidget extends CWidget {

    public $model;
    public $showButton;
    
    public function init(){
        
        if (empty($this->model)) $this->model = new DateFilterForm;
        if (empty($this->showButton)) $this->showButton = false;
        
    }
    
    public function run(){
        
        $dateFrom = $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $this->model,
            'id' => 'datefrom',
            'attribute' => 'datefrom',
            'options' => array(
                'dateFormat' => 'd M yy',
                'changeYear' => true
            ),
            'htmlOptions' => array('size' => 15),
        ),true);
        
        $dateTo = $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $this->model,
            'id' => 'dateto',
            'attribute' => 'dateto',
            'options' => array(
                'dateFormat' => 'd M yy',
                'changeYear' => true
            ),
            'htmlOptions' => array('size' => 15),
        ),true);
        
        echo '<div style="margin-top: 5px; margin-bottom: 10px;">';
        echo CHtml::beginForm();
        echo "Фильтр по дате, от&nbsp;&nbsp;&nbsp;$dateFrom&nbsp;&nbsp;&nbsp;до&nbsp;&nbsp;&nbsp;$dateTo.";
        if ($this->showButton) echo '&nbsp;&nbsp;&nbsp;'.CHtml::submitButton('Применить',array('class' => 'btn'));
        echo CHtml::endForm();
        echo '</div>';
        
    }

}

?>