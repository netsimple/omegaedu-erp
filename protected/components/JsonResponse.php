<?php

class JsonResponse {

    public static function error($text='Ошибка на сервере') {

        if (is_array($text))
            $text = implode(" \n", $text);

        $data = array(
            'success'=>false,
            'message'=>$text,
        );

        echo CJSON::encode($data);
        Yii::app()->end();

    }

    public static function success($response = array(), $text='Обновлено') {

        $data = array(
            'success'=>true,
            'message'=>$text,
        );

        echo CJSON::encode(array_merge($data,$response));
        Yii::app()->end();

    }

}