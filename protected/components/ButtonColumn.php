<?php

class ButtonColumn extends CButtonColumn {

    public $updateButtonImageUrl;
    public $deleteButtonImageUrl;
    
    public function init(){
        $this->updateButtonImageUrl = Yii::app()->baseUrl . '/images/icon-update.png';
        $this->deleteButtonImageUrl = Yii::app()->baseUrl . '/images/icon-delete.png';
        return parent::init();
    }

}

?>