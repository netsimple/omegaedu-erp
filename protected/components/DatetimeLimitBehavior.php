<?php

class DatetimeLimitBehavior extends CBehavior {
    
    public $datetimeFilter;
    public $datetimeAttribute;
    
    public function getDatetimeCondition($attribute = null, $plainString = true) {
        if (empty($this->datetimeFilter)) return '';
        if (empty($attribute)) $attribute = empty($this->datetimeAttribute) ? 'datetime' : $this->datetimeAttribute;
        if ($this->datetimeFilter->datefrom && $this->datetimeFilter->dateto) {
            $condition = array('and', $attribute. ' >= '.$this->datetimeFilter->datefrom, $attribute.' <= '.$this->datetimeFilter->dateto);
        } elseif ($this->datetimeFilter->datefrom || $this->datetimeFilter->dateto) {
            $condition = $this->datetimeFilter->datefrom ? $attribute . '>=' . $this->datetimeFilter->datefrom : $attribute . '<=' . $this->datetimeFilter->dateto;
        } else {
            $condition = '';
        }
        if (is_array($condition) && $plainString) $condition = $condition[1] . ' AND ' . $condition[2];
        return $condition;
    }

    public function setDatetimeLimit($filter,$attribute = null,$merge = false) {
        if (!$filter instanceof DateFilterForm) return;
        $this->datetimeFilter = $filter;
        if ($merge && ($condition = $this->getDatetimeCondition($attribute))) {
            $criteria = new CDbCriteria;
            $criteria->addCondition($condition);
            $this->owner->dbCriteria->mergeWith($criteria);
        }
        return;
    }
    
    public function addDatetimeCondition($where = null,$attribute = null) {
        if (empty($this->datetimeFilter)) return $where;
        $condition = $this->getDatetimeCondition($attribute,false);
        return !empty($where) ? array('and',$where,$condition) : $condition;
    }

}

?>