<?php

class OutputFileHelper {

    public function __construct($data,$newFile = true) {
        $f = fopen('output.txt',$newFile ? 'w' : 'a');
        ob_start();
        print_r($data);
        fwrite($f,ob_get_clean());
        fclose($f);
    }

}

?>