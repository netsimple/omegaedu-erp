<?php

class ExtAuthUserIdentity extends EAuthUserIdentity {

	public function authenticate() {
		if ($this->service->isAuthenticated && $model = Extauth::model()->findByPk($this->service->id)) {
		
			$this->id = $model->user_id;
			$this->name = $model->user->name;

			$this->setState('id', $this->id);
			$this->setState('name', $this->name);
			$this->setState('service', $this->service->serviceName);

			$this->errorCode = self::ERROR_NONE;
		} else {
			$this->errorCode = self::ERROR_NOT_AUTHENTICATED;
		}
		return !$this->errorCode;
	}
	
	public function attach() {
		if ($this->service->isAuthenticated) {
			if (Extauth::model()->exists('id = :id', array(':id' => $this->service->id))) return true;
			$model = new Extauth;
			$model->service = $this->service;
			if ($model->save()) {
				$this->setState('service', $this->service->serviceName);
				$this->errorCode = self::ERROR_NONE;
			}
		} else {
			$this->errorCode = self::ERROR_NOT_AUTHENTICATED;
		}
		return !$this->errorCode;
	}
	
	public function getServiceName() { return $this->service->serviceName; }

}
