<?php

class Controller extends CController {

	public $layout='//layouts/column1';
	public $menu=array();
	public $breadcrumbs=array();
    
    public $modelName;
    protected $_model;
    
    protected function loadModel($primaryKey,$className = null) {
        if ($className === null) {
            if (empty($this->modelName)) throw new CException();
            $className = $this->modelName;
        }
        if (!empty($this->_model) && get_class($this->_model) == $className) return $this->_model;
        if (!$this->_model = CActiveRecord::model($className)->findByPk($primaryKey)) throw new CHttpException(404);
        return $this->_model;
    }
    
    protected function storeModel($model) {
        $this->_model = $model;
    }
    
	public function redirect($url,$terminate=true,$statusCode=302) {
        if ($this->beforeRedirect()) {
            parent::redirect($url,$terminate,$statusCode);
        }
	}
    
    public function beforeRedirect($model = null) { return $this->logAction($model); }

    protected function beforeAction($action) {

        if (!Yii::app()->request->isAjaxRequest &&
             Yii::app()->user->model &&
            !Yii::app()->user->model->city_id &&
             Yii::app()->user->model->inPartnerGroup &&
             $_SERVER['REQUEST_URI'] != '/user/view' &&
             Yii::app()->controller->action->id != 'error'
        ) {

            Yii::app()->user->setFlash('system', 'Укажите пожалуйста ваш город в профиле');
            $this->redirect('/user/view');

        }

        return true;
    }

    public function logAction($model = null, $actionId = null) {
        if ($model === null) $model = $this->_model;
        if ($actionId === null) $actionId = $this->action->id;
        if ($actionId == 'passremind' || $actionId == 'passchange') {
            AccessLog::add($actionId);
        } else {
            ActivityLog::add($model,$actionId);
        }
        return true;
    }
}