<?php

	class WebUser extends CWebUser {

		private $_model;
		private $_roleList = array('partner','manager','adman','teacher');

		public function getModel() {
			if (!$this->isGuest && $this->_model === null)
				$this->_model = UserMain::model()->findByPk($this->id);
			return $this->_model;
		}

		public function checkAccess($primaryKey,$additKey='') {

			/*
			if (!$additKey && !AccessTable::model()->exists('skey = :skey',array(':skey' => $primaryKey))) {
				$access = new AccessTable;
				$access->skey = $primaryKey;
				$access->label= $primaryKey;
				$access->save();
			}
			*/

//      for iframe schedule
			if ($this->isGuest && Yii::app()->request->getQuery('iframe') !== null) throw new CHttpException(401);
			if (empty($this->id)) return false;

			if ($this->model->is_owner) return true;
			if ($primaryKey == 'owner') return false;

			$criteria = new CDbCriteria;
			$criteria->select = 'allowed_partner, allowed_manager, allowed_adman, allowed_teacher';

			if ($additKey) {
				$multiple = $additKey == '*';
				if (!$multiple) {
					if (is_array($additKey)) {
						foreach ($additKey as $currentAdditKey)
							$criteria->compare('skey',$primaryKey.'#'.$currentAdditKey,false,'OR');
						$multiple = true;
					} else {
						$criteria->compare('skey',$primaryKey.'#'.$additKey,false);
					}
				} else {
					$criteria->compare('skey',$primaryKey,true);
				}
			} else {
				$multiple = false;
				$criteria->compare('skey',$primaryKey);
			}

			$access = $multiple ? AccessTable::model()->findAll($criteria) : AccessTable::model()->find($criteria);
			if (!$access) return false;

			if ($multiple) {
				foreach ($access as $currentAccess)
					foreach ($this->_roleList as $currentRole) {
						if ($this->model['is_'.$currentRole] && $currentAccess['allowed_'.$currentRole]) return true;
					}
			} else {
				foreach ($this->_roleList as $currentRole) {
					if ($this->model['is_'.$currentRole] && $access['allowed_'.$currentRole]) return true;
				}
			}

			return false;

		}

		public function getProjectId() {
			return $this->model->getProjectId();
		}

		public function getProjectIdCompare() {
			// for iframe
			if ($this->isGuest && ($pid = Yii::app()->request->getQuery('project_id')) !== null)
				return $pid;

			return $this->model->getProjectIdCompare();
		}

		public function getProjectIdCondition($attributeName = 'project_id', $tableAlias = '') {
			// for iframe
			if ($this->isGuest && ($pid = Yii::app()->request->getQuery('project_id')) !== null)
				return 1;

			return $this->model->getProjectIdCondition($attributeName, $tableAlias);
		}

		public function login($identity, $duration = 0) {
			$login = parent::login($identity,$duration);
			if ($identity instanceof ExtAuthUserIdentity) AccessLog::add('login',array('serviceName' => $identity->serviceName),$login);
			return $login;
		}

		public function logout($destroySession = true) {
			AccessLog::add('logout');
			parent::logout($destroySession);
		}

		public function getCurrency() {
			return $this->model->currency;
		}
	}


?>