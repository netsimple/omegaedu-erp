<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

// Yii::setPathOfAlias('bootstrap','../extensions/bootstrap');

return CMap::mergeArray(

require(dirname(__FILE__).'/main.php'),
array(
	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123789',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('*.*.*.*','::1'),
                        /*
                        'generatorPaths'=>array(
                            'bootstrap.gii',
                        ),
                        */

		),
	),

	// application components
	'components'=>array(

		'eauth' => array(
                    'class' => 'ext.eauth.EAuth',
                    'popup' => true, // Use the popup window instead of redirecting.
                    'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache'.
                    'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
                    'services' => array( // You can change the providers and their classes.
                        'google' => array(
                            'class' => 'GoogleOpenIDService',
                        ),
                        'yandex' => array(
                            'class' => 'YandexOpenIDService',
                        ),
                        'twitter' => array(
                            // register your app here: https://dev.twitter.com/apps/new
                            'class' => 'TwitterOAuthService',
                            'key' => 'k9wFc4WVAPwqFXy5WChhMA',
                            'secret' => '8FfBTqeF8vsx2cUHMzTSNiDAuq60NPC3HPB3bGjplI',
                        ),
                        'facebook' => array(
                            // register your app here: https://developers.facebook.com/apps/
                            'class' => 'FacebookOAuthService',
                            'client_id' => '126611647530175',
                            'client_secret' => '398fc5b8f83c4772f24d2fc5676c26ad',
                        ),
                        'vkontakte' => array(
                            // register your app here: https://vk.com/editapp?act=create&site=1
                            // https://vk.com/app3856047
                            'class' => 'VKontakteOAuthService',
                            'client_id' => '3856047',
                            'client_secret' => 'rUf8PRDURnwTFkxvzJET',
                        ),
                        'mailru' => array(
                            // register your app here: http://api.mail.ru/sites/my/add
                            'class' => 'MailruOAuthService',
                            'client_id' => '703430',
                            'client_secret' => 'b2ffc009d87b3b8c4c83835a276baf37',
                        ),
                    ),
		),

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=expo_erpomega',
			'emulatePrepare' => true,
			'username' => 'expo_erpomega',
			'password' => 'yxuSDeWG3G',
			'charset' => 'utf8',
            'tablePrefix' => 'edu_',
            'enableProfiling' => true,
		),

        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages

                array(
                    'class'=>'CEmailLogRoute',
                    'levels'=>'error, warning',
                    'except'=>'exception.CHttpException.403',
                    'emails'=>'a.ryzhenkov@gaikagroup.com',
                ),
            ),
        ),


	),

	'params'=>array(
		'adminEmail'=>'info@robboclub.ru',
        'uploadDirectory' => 'uploads',
	),
));