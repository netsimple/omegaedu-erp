<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
    
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'ROBBOClubCRM',
    
    'sourceLanguage' => 'ru',
    'language' => 'ru',
    
    'defaultController' => 'user',
    
    'aliases' => array(
        'bootstrap' => 'ext.bootstrap',
        'chartjs' => 'ext.yii-chartjs',
        'editable' => 'ext.x-editable',
    ),

	'preload'=>array('log', 'bootstrap', 'chartjs'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		/*
		 * http://www.yiiframework.com/extension/mail
		 */
		'ext.yii-mail.YiiMailMessage',
		/*
		 * https://github.com/Nodge/yii-eauth
		 */
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.*',
        'ext.eauth.services.*',
        /*
         * http://www.yiiframework.com/extension/upload
         */
        'ext.upload.*',
        /*
         * http://x-editable.demopage.ru/
         */
        'editable.*',
        'ext.easyimage.EasyImage',
	),

	// application components
	'components'=>array(
        
        'bootstrap'=>array(
            'class'=>'bootstrap.components.Bootstrap',
        ),
        'easyImage' => array(
            'class' => 'application.extensions.easyimage.EasyImage',
            //'driver' => 'GD',
            //'quality' => 100,
            //'cachePath' => '/assets/easyimage/',
            //'cacheTime' => 2592000,
            //'retinaSupport' => false,
        ),
        'chartjs' => array(
            'class' => 'chartjs.components.ChartJs',
        ),
        
        'editable' => array(
            'class'     => 'editable.EditableConfig',
            'form'      => 'jqueryui',        //form style: 'bootstrap', 'jqueryui', 'plain' 
            'mode'      => 'popup',            //mode: 'popup' or 'inline'  
            'defaults'  => array(              //default settings for all editable elements
               'emptytext' => 'Изменить'
            ),
        ),
        
        'ePdf' => array(
            'class' => 'ext.yii-pdf.EYiiPdf',
            'params' => array(
                'mpdf' => array(
                    'librarySourcePath' => 'application.vendors.mpdf.*',
                    'constants'         => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                    ),
                    'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder.
                    /*'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
                        'mode'              => '', //  This parameter specifies the mode of the new document.
                        'format'            => 'A4', // format A4, A5, ...
                        'default_font_size' => 0, // Sets the default document font size in points (pt)
                        'default_font'      => '', // Sets the default font-family for the new document.
                        'mgl'               => 15, // margin_left. Sets the page margins for the new document.
                        'mgr'               => 15, // margin_right
                        'mgt'               => 16, // margin_top
                        'mgb'               => 16, // margin_bottom
                        'mgh'               => 9, // margin_header
                        'mgf'               => 9, // margin_footer
                        'orientation'       => 'P', // landscape or portrait orientation
                    )*/
                ),
                'HTML2PDF' => array(
                    'librarySourcePath' => 'application.vendors.html2pdf.*',
                    'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap
                    /*'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
                        'orientation' => 'P', // landscape or portrait orientation
                        'format'      => 'A4', // format A4, A5, ...
                        'language'    => 'en', // language: fr, en, it ...
                        'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
                        'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
                        'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
                    )*/
                )
            )
        ),
            
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
            // 'loginUrl' => array('site/login'), // #pdovlatov #09-04-2013
            'class' => 'WebUser', // #pdovlatov #09-04-2013
		),
                
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName' => false, // #pdovlatov #09-04-2013 #link-with: .htaccess
			'rules'=>array(
                'user/<target:(owner|partner|manager|adman|teacher|profile)>/<id:\d+>' => 'user/view',
                'user/<target:(owner|partner|manager|adman|teacher|profile)>' => 'user/view',
				'user/<target:(owner|partner|manager|adman|teacher)>/<action:\w+>/<id:\d+>'=>'user/<action>',
				'user/<target:(owner|partner|manager|adman|teacher)>/<action:\w+>'=>'user/<action>',
				'logout'=>'user/logout',
				'login'=>'user/login',
                'stat/<action:(lessons|finance)>' => 'stat/view',
                'schedule/<frame:frame>-<project_id:\d+>' => 'schedule/index',
                'client/<new:new>' => 'client/index',
                '<controller:trgroup>/<tab:profile|schedule|clients|finance>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>' => '<controller>/index',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
                
		'mail' => array(
			'class' => 'ext.yii-mail.YiiMail',
			'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'smtp',
                'port' => '25',
            ),
			'viewPath' => 'application.views.email',
			'logging' => true,
			'dryRun' => false,
		),

        'Smtpmail'=>array(
            'class'=>'application.extensions.smtpmail.PHPMailer',
            'Host'=>"smtp",
            'Mailer'=>'smtp',
            'Port'=>25,
            'SMTPAuth'=>false, 
        ),
		
		'loid' => array(
			'class' => 'ext.lightopenid.loid',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'user/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages

                /*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
        'widgetFactory' => array(
            'enableSkin'=>true,
            'widgets' => array(
                'CJuiDatePicker' => array(
                    'options' => array(
                        'monthNames' => array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'),
                        'monthNamesShort' => array('янв','февр','марта','апр','мая','июня','июля','авг','сент','окт','нояб','дек'),
                        'dayNames' => array('Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'),
                        'dayNamesShort' => array('Вс','Пн','Вт','Ср','Чт','Пт','Сб'),
                        'dayNamesMin' => array('Вс','Пн','Вт','Ср','Чт','Пт','Сб'),
                        'firstDay' => 1,  
                    ),
                ),
                'EchMultiSelect' => array(
                    'options' => array(
                        'checkAllText' => 'Выбрать всё',
                        'uncheckAllText' => 'Снять выбор',
                        'selectedText' =>Yii::t('general','# выбрано'),
                        'noneSelectedText'=>'Выбрать',
                    ),
                ),
            ),
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
    
	'params'=>array(
        'max_coupon_count' => 1000,
        'academic_hour' => 45, // in minutes
        'adminEmail'=>'yiiframe@yandex.ru'
    ),

);