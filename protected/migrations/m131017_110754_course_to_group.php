<?php

class m131017_110754_course_to_group extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE `edu_course_rates` ADD `group_id` INT(10)  UNSIGNED  NOT NULL  AFTER `course_id`");
        $this->execute("ALTER TABLE `edu_course_rates` DROP INDEX `course_id`");

        $rates = $this->queryAll("SELECT * FROM `edu_course_rates`");

        foreach ($rates as $rate) {

            $clone = $rate; unset($clone['id']);

            $groups = $this->queryAll("SELECT * FROM edu_tr_group WHERE teacher_id = {$rate['teacher_id']} AND tr_course_id = {$rate['course_id']}");
            if (count($groups) > 1) {

                foreach ($groups as $i => $group) {
                    if ($i == 0) {
                        $this->execute("UPDATE `edu_course_rates` SET `group_id` = {$groups[0]['id']} WHERE id = {$rate['id']}");
                    } else {
                        $this->insert('edu_course_rates', array_merge($clone, array('group_id'=>$group['id'])));
                    }
                }

            } else {
                $this->execute("UPDATE `edu_course_rates` SET `group_id` = {$groups[0]['id']} WHERE id = {$rate['id']}");
            }

        }

	}

	public function queryAll($sql)
	{
        $db = $this->getDbConnection();
        return $db->createCommand($sql)->queryAll();
	}

    public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}