<?php

class m160104_145937_create_table_payment_tarif extends CDbMigration
{
	public function up()
	{
		$this->createTable('{{payment_tarif}}', array(
				'id'=>'int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT',
				'tr_center_id'=>'int(10) unsigned NOT NULL',
				'count_begin'=>'int(10) unsigned DEFAULT 1 NOT NULL',
				'count_end'=>'int(10) DEFAULT NULL',
				'if'=>'varchar(2) NOT NULL',
				'summa'=>'int(10) unsigned NOT NULL',
				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			),
			'ENGINE=MyISAM DEFAULT CHARSET=utf8'
		);
	}

	public function down()
	{
		$this->dropTable('{{payment_tarif}}');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}