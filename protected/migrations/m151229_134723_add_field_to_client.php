<?php

class m151229_134723_add_field_to_client extends CDbMigration
{
	public function up()
	{
		$this->addColumn('{{client}}', 'birthday', 'DATETIME');
		$this->addColumn('{{client}}', 'rate_one', 'TEXT');
		$this->addColumn('{{client}}', 'rate_two', 'TEXT');
		$this->addColumn('{{user_teacher}}', 'rate_one', 'TEXT');
		$this->addColumn('{{user_teacher}}', 'rate_two', 'TEXT');
		$this->addColumn('{{schedule}}', 'did_not_come', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
		$this->addColumn('{{schedule}}', 'late', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
		$this->addColumn('{{schedule}}', 'held_user_id', 'INT(10) UNSIGNED NOT NULL');
		$this->execute('UPDATE edu_schedule SET edu_schedule.held_user_id=(SELECT edu_tr_group.teacher_id FROM edu_tr_group WHERE edu_tr_group.id=edu_schedule.tr_group_id)');
		$this->addColumn('{{tr_center}}', 'late_fee', 'INT(10) UNSIGNED NOT NULL');//Опоздание
		$this->addColumn('{{tr_center}}', 'fine_for_truancy', 'INT(10) UNSIGNED NOT NULL');//Прогул
		$this->addColumn('{{tr_center}}', 'city_id', 'INT(3) UNSIGNED NOT NULL');//Прогул
		$this->execute('INSERT INTO `edu_city` (`name`) VALUES ("Ростов-на-Дону")');
	}

	public function down()
	{
		$this->dropColumn('{{client}}', 'birthday');
		$this->dropColumn('{{client}}', 'rate_one');
		$this->dropColumn('{{client}}', 'rate_two');
		$this->dropColumn('{{user_teacher}}', 'rate_one');
		$this->dropColumn('{{user_teacher}}', 'rate_two');
		$this->dropColumn('{{schedule}}', 'did_not_come');
		$this->dropColumn('{{schedule}}', 'late');
		$this->dropColumn('{{schedule}}', 'held_user_id');
		$this->dropColumn('{{tr_center}}', 'late_fee');
		$this->dropColumn('{{tr_center}}', 'fine_for_truancy');
		$this->dropColumn('{{tr_center}}', 'city_id');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}