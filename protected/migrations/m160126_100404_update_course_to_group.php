<?php

class m160126_100404_update_course_to_group extends CDbMigration
{
	public function up()
	{
		$rates = $this->queryAll("SELECT * FROM `edu_course_rates` WHERE group_id=0");

        foreach ($rates as $rate) {

            $clone = $rate; unset($clone['id']);

            $groups = $this->queryAll("SELECT g.id,r.group_id FROM edu_tr_group AS g LEFT JOIN edu_course_rates AS r ON r.group_id = g.id WHERE g.teacher_id = {$rate['teacher_id']} AND g.tr_course_id = {$rate['course_id']} HAVING r.group_id IS NULL");
            if (count($groups) > 1) {

                foreach ($groups as $i => $group) {
                    if ($i == 0) {
                        $this->execute("UPDATE `edu_course_rates` SET `group_id` = {$groups[0]['id']} WHERE id = {$rate['id']}");
                    } else {
                        $this->insert('edu_course_rates', array_merge($clone, array('group_id'=>$group['id'])));
                    }
                }

            } elseif($groups) {
                $this->execute("UPDATE `edu_course_rates` SET `group_id` = {$groups[0]['id']} WHERE id = {$rate['id']}");
            }

        }

		$this->execute("DELETE FROM `edu_course_rates` WHERE `group_id` = '0' AND `rate` = '0'");
	}

	public function queryAll($sql)
	{
        $db = $this->getDbConnection();
        return $db->createCommand($sql)->queryAll();
	}
	
    public function down()
	{
		return true;
	}
}