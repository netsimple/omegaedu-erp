<?php

class m160622_064227_create_table_currency extends CDbMigration
{
	public function up()
	{
		$this->createTable('{{currency}}', array(
				'id'=>'smallint(5) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT',
				'name'=>'varchar(30) NOT NULL COMMENT "Название"',
				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			),
			'ENGINE=MyISAM DEFAULT CHARSET=utf8'
		);

		$this->insert('{{currency}}', array(
			'id'=>1,
			'name'=>'руб.'
		));
	}

	public function down()
	{
		$this->dropTable('{{currency}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}