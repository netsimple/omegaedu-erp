<?php

class m160622_064326_add_columns_to_tr_income extends CDbMigration
{
	public function up()
	{
		$this->addColumn('{{tr_income}}', 'discount', 'INT(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT "Скидка"');
		$this->addColumn('{{user_main}}', 'currency_id', 'SMALLINT(5) UNSIGNED NULL COMMENT "Валюта"');
		$this->update('{{user_main}}', array('currency_id'=>1));
		$this->createIndex('curr_index', '{{user_main}}', 'currency_id');
	}

	public function down()
	{
		$this->dropColumn('{{tr_income}}', 'discount');
		$this->dropColumn('{{user_main}}', 'currency_id');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}