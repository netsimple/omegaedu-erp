function validateEmail(email) {
    // http://stackoverflow.com/a/46181/11236

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhone(phone) {

    var re = /^\+7-[0-9]{3}-[0-9]{3}-[0-9]{4}$/;
    return re.test(phone);
}

$(function(){

    if (window.location.pathname == '/user/profile' || window.location.pathname == '/client/new')
        return;

    var current = $('#menuContainer a[href="'+window.location.pathname+'"]');
    var parentLink = window.location.pathname.substr(1).split('/')[0];

    if (current.length == 0)
        var current = $('#menuContainer a[href="/'+parentLink+'"]');

    if (current.length == 1) {
        var name = current.text();
        var href = current.attr('href');
        var parentName = current.parent().parent().parent().find('a:first').text();
        var crumbs = parentName + " » " + "<a href='"+href+"'>"+name+"</a>";

    } else {
        var parent = $('#menuContainer a[href^="/'+parentLink+'"]:first').parent().parent().parent().find('a:first').text();
        var crumbs = parent + " » ";

        if (parent == "Пользователи") {
            var roles = $('.role-block').text().split(':')[1];
            roles = roles.split(', ');
            for (i in roles) {
                var role = roles[i].replace(/^\s+|\s+$/g, '').substr(0, 4);

                var addHref = $(".sf-menu a[href^='/user']:contains('"+role+"')").attr('href');
                var addText = $(".sf-menu a[href^='/user']:contains('"+role+"')").text();

                if (typeof addHref != 'undefined') {
                    crumbs += "<a href='"+addHref+"'>"+addText+"</a>";
                    break;
                }

            }
        }

    }

    crumbs = crumbs.replace(/» \d+/g, '');

    $('#content').prepend("<div class='breadcrumbs'>"+crumbs+"</div>");

	$('body').on('click', '.button-column a[title=comment]', function(){
		var $this=$(this);
		$(".ui-dialog-title").text($this.closest('tr').find('td:eq(4)').text());
		$("#commentDialog form").attr('action', $this.attr('href'));
		$("#commentDialog").dialog("open");
		return false;
	});

	$('body').on('submit', '#commentDialog form', function(){
		var $this=$(this);

		$.ajax({
			url: $this.attr('action'),
			type: 'post',
			data: $this.serialize(),
			success: function(data) {
				$("#commentDialog").dialog("close");
				$.fn.yiiGridView.update('client-grid');
			}
		});

		return false;
	});
})
