<?php

class Stat extends CFormModel {
    
    protected $_colorPull=array();
    protected $_monthData;
    protected $_charts;
    
    protected $_statFilter;
    
    public function attributeNames() { return array(); }

    public function setFilter(StatFilterForm $object) {
        $this->_statFilter = $object;
    }

    public function getFilter() {
        return empty($this->_statFilter) ? $this->_statFilter = new StatFilterForm : $this->_statFilter;
    }

	public function behaviors() {
		return array(
			'datetimeLimit' => array('class' => 'application.components.DatetimeLimitBehavior'),
		);
	}
    
    protected function chartLabels() {
        return array(
            'income_center_sng' => 'Соотношение доходов по центрам',
            'income_group_sng'  => 'Соотношение доходов по группам',
            'client_increase_mul' => 'Прирост клиентов',
            'income_centers_mul'=> 'Динамика выручки по центрам',
            'income_groups_mul' => 'Динамика выручки по группам',
            'costs_center_sng' => 'Соотношение расходов по центрам',
            'costs_group_sng'  => 'Соотношение расходов по группам',
            'costs_center_mul'=> 'Динамика затрат по центрам',
            'costs_group_mul' => 'Динамика затрат по группам',
            'profit_centers_mul' => 'Прибыль по центрам',
            'profit_groups_mul' => 'Прибыль по группам',
            'profitability_income_centers_mul'  => 'Рентабельность доходов по центрам',
            'profitability_costs_centers_mul'   => 'Рентабельность расходов по центрам',
            'profitability_income_groups_mul'   => 'Рентабельность доходов по группам',
            'profitability_costs_groups_mul'    => 'Рентабельность расходов по группам',
            'skipped_all_sng'       => 'Соотношение по посещениям',
            'skipped_center_mul'    => 'Количество пропусков по уважительной причине в центрах',
            'skipped_group_mul'     => 'Количество пропусков по уважительной причине в группах',
            'conducted_all_sng'     => 'Соотношение по статусу занятий',
            'conducted_all_mul'     => 'График отношения статусов занятий',
            'conducted_center_mul'  => 'Количество проведенных занятий в центрах',
            'conducted_group_mul'   => 'Количество проведенных занятий в группах',
            'clients_source'   => 'Источники клиентов',
        );
    }
    
    public function getChartLabel($key) {
        $labels = $this->chartLabels();
        if (isset($labels[$key])) return $labels[$key];
        return $key;
    }
    
    protected function chartObjects() {
        return array(
            'income_center_sng' => 'ChDoughnut',
            'income_group_sng'  => 'ChDoughnut',
            'client_increase_mul' => 'ChBars',
            'income_centers_mul'=> 'ChLine',
            'income_groups_mul' => 'ChLine',
            'costs_center_sng' => 'ChDoughnut',
            'costs_group_sng'  => 'ChDoughnut',
            'costs_center_mul'=> 'ChLine',
            'costs_group_mul' => 'ChLine',
            'profit_centers_mul' => 'ChLine',
            'profit_groups_mul' => 'ChLine',
            'profitability_income_centers_mul' => 'ChLine',
            'profitability_costs_centers_mul' => 'ChLine',
            'profitability_income_groups_mul' => 'ChLine',
            'profitability_costs_groups_mul' => 'ChLine',
            'skipped_all_sng'       => 'ChPie',
            'skipped_center_mul'    => 'ChBars',
            'conducted_all_sng'     => 'ChPie',
            'conducted_all_mul'     => 'ChLine',
            'conducted_center_mul'  => 'ChBars',
            'clients_source'  => 'ChDoughnut',
        );
    }
    
    public function getChartObject($key) {
        $labels = $this->chartObjects();
        if (isset($labels[$key])) return $labels[$key];
        return 'ChBars';
    }
    
    protected function clearColorPull() {
        $this->_colorPull = array();
    }
    
    protected function getRandomColor() {
        do {
            switch (rand(0,2)) {
                case 0: $colorArr = array(rand(0,5)*51,rand(0,5)*51,rand(0,4)*51); break;
                case 1: $colorArr = array(rand(0,5)*51,rand(0,4)*51,rand(0,5)*51); break;
                case 2:
                default:$colorArr = array(rand(0,4)*51,rand(0,5)*51,rand(0,5)*51);
            }
        } while (in_array($colorArr,$this->_colorPull));
        array_push($this->_colorPull,$colorArr);
        return $colorArr;
    }
    
    protected function afterConstruct() {
        $this->_charts = array();
        return parent::afterConstruct();
    }
    
    protected function getMonthData() {
        if (empty($this->_monthData)) {
            $this->_monthData = array(); for ($i = 0; $i < 12; $i++) $this->_monthData[$i] = 0;
        }
        return $this->_monthData;
    }
    
    protected function getClientSkippedAllStaticStat() {
        
        $idSelector = $this->filter->getIdSelector('group');
        if ($idSelector === false) return array();
        
        $data = Yii::app()->db->createCommand()
            ->select('COUNT(*) AS count, is_skipped')
            ->from(Yii::app()->db->tablePrefix.'tr_client_log t')
            ->join(Yii::app()->db->tablePrefix.'schedule lesson','lesson.id = t.tr_lesson_id')
            ->where($this->addDatetimeCondition(array('in','lesson.tr_group_id',$idSelector),'lesson.datetime_start'))
            ->order('is_skipped ASC')
            ->group('is_skipped')
            ->queryAll();
            
        $labels = TrClientLog::model()->getIsSkippedList();
        $datasetsStatic = array();
        $this->clearColorPull();
        foreach ($data as $currentSet) {
            $color = implode(',',$this->getRandomColor());
            $datasetsStatic[$currentSet['is_skipped']] = array(
                'value' => (int)$currentSet['count'],
                'color' => "rgba($color,1)",
                'label' => $labels[$currentSet['is_skipped']],
            );
        }
        
        return array('skipped_all_sng' => array('datasets' => array_values($datasetsStatic)));
        
    }
    
    protected function getClientSkippedByReasonCenterStat() {
        
        $idSelector = $this->filter->getIdSelector('center');
        if ($idSelector === false) return array();
        
        $where = array('and',array('in','lesson.tr_center_id',$idSelector),'is_skipped = '.TrClientLog::SKIPPED_BY_REASON);
        
        $data = Yii::app()->db->createCommand()
            ->select('COUNT(*) AS count, lesson.tr_center_id AS center_id, center.name AS center_name, FROM_UNIXTIME(lesson.datetime_start,\'%m\') AS month')
            ->from(Yii::app()->db->tablePrefix.'tr_client_log t')
            ->join(Yii::app()->db->tablePrefix.'schedule lesson', 'lesson.id = t.tr_lesson_id')
            ->join(Yii::app()->db->tablePrefix.'tr_center center', 'lesson.tr_center_id = center.id')
            ->where($this->addDatetimeCondition($where,'lesson.datetime_start'))
            ->group('lesson.tr_center_id, month')
            ->queryAll();
        
        $datasetsSkippedByReason = array();
        $this->clearColorPull();
        foreach ($data as $currentSet) {
            $color = implode(',',$this->getRandomColor());
            if (!isset($datasetsSkippedByReason[$currentSet['center_id']]))
                $datasetsSkippedByReason[$currentSet['center_id']] = array(
                    'data' => $this->getMonthData(),
                    'fillColor' => "rgba($color,0.5)",
                    'strokeColor' => "rgba($color,1)",
                    'pointColor' => "rgba($color,1)",
                    'pointStrokeColor' => '#ffffff',
                    'label' => CHtml::encode($currentSet['center_name']),
                );
            $datasetsSkippedByReason[$currentSet['center_id']]['data'][$currentSet['month']-1] += $currentSet['count'];
        }
        
        return array(
            'skipped_center_mul' => array('datasets' => array_values($datasetsSkippedByReason), 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true))),  
        );
    }
    
    protected function getClientSkippedByReasonGroupStat() {
        
        $idSelector = $this->filter->getIdSelector('group');
        if ($idSelector === false) return array();
        
        $where = array('and',array('in','lesson.tr_group_id',$idSelector),'is_skipped = '.TrClientLog::SKIPPED_BY_REASON);
        
        $data = Yii::app()->db->createCommand()
            ->select('COUNT(*) AS count, lesson.tr_group_id AS group_id, group.name AS group_name, FROM_UNIXTIME(lesson.datetime_start,\'%m\') AS month')
            ->from(Yii::app()->db->tablePrefix.'tr_client_log t')
            ->join(Yii::app()->db->tablePrefix.'schedule lesson', 'lesson.id = t.tr_lesson_id')
            ->join(Yii::app()->db->tablePrefix.'tr_group group', 'lesson.tr_group_id = group.id')
            ->where($this->addDatetimeCondition($where,'lesson.datetime_start'))
            ->group('lesson.tr_group_id, month')
            ->queryAll();
        
        $datasetsSkippedByReason = array();
        $this->clearColorPull();
        foreach ($data as $currentSet) {
            $color = implode(',',$this->getRandomColor());
            if (!isset($datasetsSkippedByReason[$currentSet['group_id']]))
                $datasetsSkippedByReason[$currentSet['group_id']] = array(
                    'data' => $this->getMonthData(),
                    'fillColor' => "rgba($color,0.5)",
                    'strokeColor' => "rgba($color,1)",
                    'pointColor' => "rgba($color,1)",
                    'pointStrokeColor' => '#ffffff',
                    'label' => CHtml::encode($currentSet['group_name']),
                );
            $datasetsSkippedByReason[$currentSet['group_id']]['data'][$currentSet['month']-1] += $currentSet['count'];
        }
        
        return array(
            'skipped_group_mul' => array('datasets' => array_values($datasetsSkippedByReason), 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true))),  
        );
    }
    
    protected function getLessonConductedStat() {
        
        $idSelector = $this->filter->getIdSelector('group');
        if ($idSelector === false) return array();
    
        $data = Yii::app()->db->createCommand()
            ->select('COUNT(*) AS count, is_conducted, FROM_UNIXTIME(t.datetime_start,\'%m\') AS month')
            ->from(Yii::app()->db->tablePrefix.'schedule t')
            ->where($this->addDatetimeCondition(array('in','t.tr_group_id',$idSelector),'t.datetime_start'))
            ->order('is_conducted ASC')
            ->group('is_conducted, month')
            ->queryAll();
            
        $datasetsStatic = array();
        $datasetsDynamic = array();
        $labels = Schedule::model()->getConductedList();
        $this->clearColorPull();
        foreach ($data as $currentSet) {
            $color = implode(',',$this->getRandomColor());
            if (!isset($datasetsStatic[$currentSet['is_conducted']])) {
                $datasetsStatic[$currentSet['is_conducted']] = array(
                    'value' => (int)$currentSet['count'],
                    'color' => "rgba($color,1)",
                    'label' => $labels[$currentSet['is_conducted']],
                );
            } else {
                $datasetsStatic[$currentSet['is_conducted']]['value'] += (int)$currentSet['count'];
            }
            if (!($currentSet['month'] = (int)$currentSet['month'])) continue;
            if (!isset($datasetsDynamic[$currentSet['is_conducted']]))
                $datasetsDynamic[$currentSet['is_conducted']] = array(
                    'data' => $this->getMonthData(),
                    'fillColor' => "rgba($color,0.5)",
                    'strokeColor' => "rgba($color,1)",
                    'pointColor' => "rgba($color,1)",
                    'pointStrokeColor' => '#ffffff',
                    'label' => $labels[$currentSet['is_conducted']],
                );
            $datasetsDynamic[$currentSet['is_conducted']]['data'][$currentSet['month']-1] += $currentSet['count'];
        }
        
        return array(
            'conducted_all_sng' => array('datasets' => array_values($datasetsStatic)),
            'conducted_all_mul' => array('datasets' => array_values($datasetsDynamic), 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true))),
        );
        
    }
    
    protected function getLessonConductedOKCenterStat() {
        
        $idSelector = $this->filter->getIdSelector('center');
        if ($idSelector === false) return array();
        
        $where = array('and',array('in','t.tr_center_id',$idSelector),'is_conducted = '.Schedule::CONDUCT_OK);
        
        $data = Yii::app()->db->createCommand()
            ->select('COUNT(*) AS count, t.tr_center_id AS center_id, center.name AS center_name, FROM_UNIXTIME(t.datetime_start,\'%m\') AS month')
            ->from(Yii::app()->db->tablePrefix.'schedule t')
            ->join(Yii::app()->db->tablePrefix.'tr_center center', 't.tr_center_id = center.id')
            ->where($this->addDatetimeCondition($where,'t.datetime_start'))
            ->group('t.tr_center_id, month')
            ->queryAll();
        
        $datasetsConducted = array();
        $this->clearColorPull();
        foreach ($data as $currentSet) {
            $color = implode(',',$this->getRandomColor());
            if (!isset($datasetsConducted[$currentSet['center_id']]))
                $datasetsConducted[$currentSet['center_id']] = array(
                    'data' => $this->getMonthData(),
                    'fillColor' => "rgba($color,0.5)",
                    'strokeColor' => "rgba($color,1)",
                    'pointColor' => "rgba($color,1)",
                    'pointStrokeColor' => '#ffffff',
                    'label' => CHtml::encode($currentSet['center_name']),
                );
            $datasetsConducted[$currentSet['center_id']]['data'][$currentSet['month']-1] += $currentSet['count'];
        }
        
        return array(
            'conducted_center_mul' => array('datasets' => array_values($datasetsConducted), 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true))),
        );
        
    }
    
    
    protected function getLessonConductedOKGroupStat() {
        
        $idSelector = $this->filter->getIdSelector('group');
        if ($idSelector === false) return array();
        
        $where = array('and',array('in','t.tr_group_id',$idSelector),'is_conducted = '.Schedule::CONDUCT_OK);
        
        $data = Yii::app()->db->createCommand()
            ->select('COUNT(*) AS count, t.tr_group_id AS group_id, group.name AS group_name, FROM_UNIXTIME(t.datetime_start,\'%m\') AS month')
            ->from(Yii::app()->db->tablePrefix.'schedule t')
            ->join(Yii::app()->db->tablePrefix.'tr_group group', 't.tr_group_id = group.id')
            ->where($this->addDatetimeCondition($where,'t.datetime_start'))
            ->group('t.tr_group_id, month')
            ->queryAll();
        
        $datasetsConducted = array();
        $this->clearColorPull();
        foreach ($data as $currentSet) {
            $color = implode(',',$this->getRandomColor());
            if (!isset($datasetsConducted[$currentSet['group_id']]))
                $datasetsConducted[$currentSet['group_id']] = array(
                    'data' => $this->getMonthData(),
                    'fillColor' => "rgba($color,0.5)",
                    'strokeColor' => "rgba($color,1)",
                    'pointColor' => "rgba($color,1)",
                    'pointStrokeColor' => '#ffffff',
                    'label' => CHtml::encode($currentSet['group_name']),
                );
            $datasetsConducted[$currentSet['group_id']]['data'][$currentSet['month']-1] += $currentSet['count'];
        }
        
        return array(
            'conducted_group_mul' => array('datasets' => array_values($datasetsConducted), 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true))),
        );
        
    }
    
    
    protected function getIncomeCenterStaticStat() {
        $idSelector = $this->getFilter()->getIdSelector('center');
        if ($idSelector === false) return array();
        $baseSelector = $this->getFilter()->getIdSelector('partner', true);

        $criteria = new CDbCriteria;
        $criteria->select = 'id, name';

        $criteria->addInCondition('id',$idSelector);
        $criteria->compare('project_id',$baseSelector);

        $criteria->order = 'id ASC';
        $data = TrCenter::model()->findAll($criteria);
        $datasets = array();
        $this->clearColorPull();
        $centerIDs = array();
        foreach ($data as $currentObject) {
            $color = implode(',',$this->getRandomColor());
            $currentObject->setDatetimeLimit($this->datetimeFilter,null,true);
            array_push($centerIDs,$currentObject->id);
            array_push($datasets,array(
                'value' => $income = $currentObject->income,
                'color' => "rgba($color,1)",
                'label' => CHtml::link(CHtml::encode($currentObject->name),array('trcenter/view','id' => $currentObject->id)).' - '.$income,
            ));
        }

        $this->filter->setIdSelector('center',$centerIDs);
        return array(
            'income_center_sng' => array('datasets' => $datasets),
        );
    }
    
    
    protected function getIncomeGroupStaticStat() {
        $idSelector = $this->getFilter()->getIdSelector('group');
        if ($idSelector === false) return array();
        $baseSelector = $this->getFilter()->getIdSelector('center', true);
        
        $where = array('in','tr_center_id',$baseSelector);
        if ($idSelector) $where = array('and',$where,array('in','t.id',$idSelector));
        $groupIDs = Yii::app()->db->createCommand()
            ->select('t.id')
            ->from(Yii::app()->db->tablePrefix.'tr_group t')
            ->where($where)
            ->queryColumn();
        
        $criteria = new CDbCriteria;
        $criteria->select = 'id, tr_course_id, name';
        $criteria->addInCondition('id',$groupIDs);
        $criteria->order = 'id ASC';
        $data = TrGroup::model()->findAll($criteria);
        $datasets = array();
        $this->clearColorPull();
        foreach ($data as $currentObject) {
            $color = implode(',',$this->getRandomColor());
            $currentObject->setDatetimeLimit($this->datetimeFilter,null,true);
            array_push($datasets,array(
                'value' => $income = $currentObject->income,
                'color' => "rgba($color,1)",
                'label' => CHtml::link(CHtml::encode($currentObject->name),array('trgroup/view','id' => $currentObject->id)).' - '.$income,
            ));
        }
        
        $this->filter->setIdSelector('group',$groupIDs);
        return array(
            'income_group_sng' => array('datasets' => $datasets),
        );
    }
    
    
    protected function getIncomeClientIncreaseStat() {
        $idSelector = $this->getFilter()->getIdSelector('client');
        if ($idSelector === false) return array();
        $where = 'system_status = '.ClientComments::SYSTEM_STATUS_TOSTUDY;
        if ($idSelector) $where = array('and',$where,array('in','client_id',$idSelector));
        
        $clientCount = Yii::app()->db->createCommand()
            ->select('datetime')
            ->from(Yii::app()->db->tablePrefix.'client_history')
            ->where($this->addDatetimeCondition($where))
            ->queryColumn();

        $months = $this->getMonthData();
        foreach ($clientCount as $currentDatetime) {
            $months[(int)date('m',$currentDatetime)-1]++;
        }
        $this->clearColorPull();
        $color = implode(',',$this->getRandomColor());
        $datasets = array(
            array(
                'data' => $months,
                'fillColor' => "rgba($color,1)",
                'strokeColor' => "rgba($color,1)",
                'pointColor' => "rgba($color,1)",
                'pointStrokeColor' => '#ffffff',
            ),
        );
        return array(
            'client_increase_mul' => array('datasets' => $datasets, 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true))),
        );
    }
    
    
    protected function getIncomeCenterDynamicStat() {
        $idSelector = $this->getFilter()->getIdSelector('center');
        if ($idSelector === false) return array();
        $baseSelector = $this->getFilter()->getIdSelector('partner', true);
        
        $criteria = new CDbCriteria;
        $criteria->select = 'id, name';
        $criteria->addInCondition('id',$idSelector);
        $criteria->compare('project_id',$baseSelector);
        $centerDynamic = TrCenter::model()->findAll($criteria);
        $this->clearColorPull();
        $datasets = array();
        foreach ($centerDynamic as $currentCenter) {
            $color = implode(',',$this->getRandomColor());
            $currentCenter->setDatetimeLimit($this->datetimeFilter,null,true);
            $datasets[] = array(
                'data' => $currentCenter->getFinanceIncomeDataStat()->income_total,
                'fillColor' => "rgba($color,0.5)",
                'strokeColor' => "rgba($color,1)",
                'pointColor' => "rgba($color,1)",
                'pointStrokeColor' => '#ffffff',
                'label' => CHtml::link(CHtml::encode($currentCenter->name),array('trcenter/view','id' => $currentCenter->id)),
            );
        }
        return array(
            'income_centers_mul' => array('datasets' => $datasets, 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true))),
        );
    }
    
    
    protected function getIncomeGroupDynamicStat() {
        $idSelector = $this->getFilter()->getIdSelector('group');
        if ($idSelector === false) return array();

        $criteria = new CDbCriteria;
        $criteria->select = 'id, name, tr_course_id';
        $criteria->addInCondition('id',$idSelector);
        $groupDynamic = TrGroup::model()->findAll($criteria);
        $this->clearColorPull();
        $datasets = array();
        foreach ($groupDynamic as $currentGroup) {
            $color = implode(',',$this->getRandomColor());
            $currentGroup->setDatetimeLimit($this->datetimeFilter,null,true);
            $datasets[] = array(
                'data' => $currentGroup->getFinanceIncomeDataStat()->income_total,
                'fillColor' => "rgba($color,0.5)",
                'strokeColor' => "rgba($color,1)",
                'pointColor' => "rgba($color,1)",
                'pointStrokeColor' => '#ffffff',
                'label' => CHtml::link(CHtml::encode($currentGroup->name),array('trgroup/view','id' => $currentGroup->id)),
            );
        }
        return array(
            'income_groups_mul' => array('datasets' => $datasets, 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true))),
        );
    }
    
    
    protected function getProfitCenterStat() {
        $idSelector = $this->filter->getIdSelector('center');
        if ($idSelector === false) return array();
        $baseSelector = $this->filter->getIdSelector('partner',true);

        $criteria = new CDbCriteria;
        $criteria->select = 'id, name';
        $criteria->addInCondition('id',$idSelector);
        $criteria->compare('project_id',$baseSelector);
        $centerDynamic = TrCenter::model()->findAll($criteria);
        $this->clearColorPull();
        $datasets = array();
        $centerIDs = array();
        foreach ($centerDynamic as $currentObject) {
            $color = implode(',',$this->getRandomColor());
            $currentObject->setDatetimeLimit($this->datetimeFilter,null,true);
            array_push($centerIDs,$currentObject->id);
            $financeData = $currentObject->getFinanceDataStat();
            $datasets[] = array(
                'data' => $financeData->profit,
                'fillColor' => "rgba($color,0.5)",
                'strokeColor' => "rgba($color,1)",
                'pointColor' => "rgba($color,1)",
                'pointStrokeColor' => '#ffffff',
                'label' => CHtml::link(CHtml::encode($currentObject->name),array('trcenter/view','id' => $currentObject->id)),
            );
        }
        
        $this->filter->setIdSelector('center',$centerIDs);
        return array('profit_centers_mul' => array('datasets' => $datasets, 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true))));
    }
    
    
    protected function getProfitGroupStat() {
        $idSelector = $this->getFilter()->getIdSelector('group');
        if ($idSelector === false) return array();
        
        $criteria = new CDbCriteria;
        $criteria->select = 'id, name, tr_course_id';
        $criteria->addInCondition('id',$idSelector);
        $groupDynamic = TrGroup::model()->findAll($criteria);
        $this->clearColorPull();
        $datasets = array();
        foreach ($groupDynamic as $currentObject) {
            $color = implode(',',$this->getRandomColor());
            $currentObject->setDatetimeLimit($this->datetimeFilter,null,true);
            $financeData = $currentObject->getFinanceDataStat();
            $datasets[] = array(
                'data' => $financeData->profit,
                'fillColor' => "rgba($color,0.5)",
                'strokeColor' => "rgba($color,1)",
                'pointColor' => "rgba($color,1)",
                'pointStrokeColor' => '#ffffff',
                'label' => CHtml::link(CHtml::encode($currentObject->name),array('trgroup/view','id' => $currentObject->id)),
            );
        }
        return array('profit_groups_mul' => array('datasets' => $datasets, 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true))));
    }
    
    
    protected function getProfitabilityCenterStat() {
        $idSelector = $this->getFilter()->getIdSelector('center');
        if ($idSelector === false) return array();
        $baseSelector = $this->getFilter()->getIdSelector('partner', true);
        
        $criteria = new CDbCriteria;
        $criteria->select = 'id, name';
        $criteria->addInCondition('id',$idSelector);
        $criteria->compare('project_id',$baseSelector);
        $centerDynamic = TrCenter::model()->findAll($criteria);
        $this->clearColorPull();
        $datasetsIncome = array();
        $datasetsCosts = array();
        $centerIDs = array();
        foreach ($centerDynamic as $currentObject) {
            $color = implode(',',$this->getRandomColor());
            $currentObject->setDatetimeLimit($this->datetimeFilter,null,true);
            array_push($centerIDs,$currentObject->id);
            $financeData = $currentObject->getFinanceDataStat();
            $dataset = array(
                'fillColor' => "rgba($color,0.5)",
                'strokeColor' => "rgba($color,1)",
                'pointColor' => "rgba($color,1)",
                'pointStrokeColor' => '#ffffff',
                'label' => CHtml::link(CHtml::encode($currentObject->name),array('trcenter/view','id' => $currentObject->id)),
            );
            $datasetsIncome[] = array_merge($dataset,array('data' => $financeData->profitability_income));
            $datasetsCosts[] = array_merge($dataset,array('data' => $financeData->profitability_costs));
        }
        
        // $this->filter->setIdSelector('center',$centerIDs);
        return array(
            'profitability_income_centers_mul' => array('datasets' => $datasetsIncome, 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true)), 'options' => array('scaleLabel' => '<%=value+"%"%>')),
            'profitability_costs_centers_mul' => array('datasets' => $datasetsCosts, 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true)), 'options' => array('scaleLabel' => '<%=value+"%"%>')),
        );
    }
    
    
    protected function getProfitabilityGroupStat() {
        $idSelector = $this->getFilter()->getIdSelector('group');
        if ($idSelector === false) return array();
            
        $criteria = new CDbCriteria;
        $criteria->select = 'id, name, tr_course_id';
        $criteria->addInCondition('id',$idSelector);
        $groupDynamic = TrGroup::model()->findAll($criteria);
        $this->clearColorPull();
        $datasetsIncome = array();
        $datasetsCosts = array();
        foreach ($groupDynamic as $currentObject) {
            $color = implode(',',$this->getRandomColor());
            $currentObject->setDatetimeLimit($this->datetimeFilter,null,true);
            $financeData = $currentObject->getFinanceDataStat();
            $dataset = array(
                'fillColor' => "rgba($color,0.5)",
                'strokeColor' => "rgba($color,1)",
                'pointColor' => "rgba($color,1)",
                'pointStrokeColor' => '#ffffff',
                'label' => CHtml::link(CHtml::encode($currentObject->name),array('trgroup/view','id' => $currentObject->id)),
            );
            $datasetsIncome[] = array_merge($dataset,array('data' => $financeData->profitability_income));
            $datasetsCosts[] = array_merge($dataset,array('data' => $financeData->profitability_costs));
        }
        return array(
            'profitability_income_groups_mul' => array('datasets' => $datasetsIncome, 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true)), 'options' => array('scaleLabel' => '<%=value+"%"%>')),
            'profitability_costs_groups_mul' => array('datasets' => $datasetsCosts, 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true)), 'options' => array('scaleLabel' => '<%=value+"%"%>')),
        );
    }
    
    
    
    protected function getCostsCenterStaticStat() {
        $idSelector = $this->getFilter()->getIdSelector('center');
        if ($idSelector === false) return array();
        $baseSelector = $this->getFilter()->getIdSelector('partner', true);
        
        if (!$idSelector) {
            if ($baseSelector) {
                $where = count($baseSelector) == 1 ? 't.project_id = '.array_pop($baseSelector) : array('in','t.project_id',$baseSelector);
            } else {
                $where = null;
            }
        } else {
            $where = array('in','t.id',$idSelector);
        }
        $where = $this->addDatetimeCondition($where,'costs.datetime');
        $query = Yii::app()->db->createCommand()
            ->select('SUM(costs.value) AS sum, t.id AS id, t.name AS name')
            ->from(Yii::app()->db->tablePrefix.'tr_center t')
            ->leftJoin(Yii::app()->db->tablePrefix.'tr_costs costs','costs.center_id = t.id');
        if ($where) $query = $query->where($where);
        $query = $query->group('t.id');
        $data = $query->queryAll();

        $datasets = array();
        $this->clearColorPull();
        $centerIDs = array();
        foreach ($data as $currentSet) {
            $color = implode(',',$this->getRandomColor());
            array_push($centerIDs,$currentSet['id']);
            array_push($datasets,array(
                'value' => $currentSet['sum'] = (int)$currentSet['sum'],
                'color' => "rgba($color,1)",
                'label' => CHtml::link(CHtml::encode($currentSet['name']),array('trcenter/view','id' => $currentSet['id'])).' - '.$currentSet['sum'],
            ));
        }
        
        $this->filter->setIdSelector('center',$centerIDs);
        return array(
            'costs_center_sng' => array('datasets' => $datasets),
        );
    }
    
    
    protected function getCostsGroupStaticStat() {
        $idSelector = $this->getFilter()->getIdSelector('group');
        if ($idSelector === false) return array();
        $baseSelector = $this->getFilter()->getIdSelector('center', true);
        
        $where = array('in','t.tr_center_id',$baseSelector);
        if ($idSelector) $where = array('and',$where,array('in','t.id',$idSelector));

        $data = Yii::app()->db->createCommand()
            ->select('SUM(costs.value) AS sum, t.id AS id, t.name AS name')
            ->from(Yii::app()->db->tablePrefix.'tr_group t')
            ->leftJoin(Yii::app()->db->tablePrefix.'tr_costs costs','costs.group_id = t.id')
            ->where($this->addDatetimeCondition($where,'costs.datetime'))
            ->group('t.id')
            ->queryAll();
        
        $datasets = array();
        $this->clearColorPull();
        $groupIDs = array();
        foreach ($data as $currentSet) {
            $color = implode(',',$this->getRandomColor());
            array_push($groupIDs,$currentSet['id']);
            array_push($datasets,array(
                'value' => $currentSet['sum'] = (int)$currentSet['sum'],
                'color' => "rgba($color,1)",
                'label' => CHtml::link(CHtml::encode($currentSet['name']),array('trgroup/view','id' => $currentSet['id'])).' - '.$currentSet['sum'],
            ));
        }
        
        $this->filter->setIdSelector('group',$groupIDs);
        return array(
            'costs_group_sng' => array('datasets' => $datasets),
        );
    }

    protected function getClientsSourceStat() {

        $groupSelector = $this->getFilter()->getIdSelector('group');
        if ($groupSelector === false) return array();
        $centerSelector = $this->getFilter()->getIdSelector('center', true);
        $partnerSelector = $this->getFilter()->getIdSelector('partner', true);

        $where = array('and', 't.is_deleted = 0', array('in','group.tr_center_id',$centerSelector));
        if ($groupSelector) $where = array('and',$where,array('in','group.id',$groupSelector));
        if ($partnerSelector) $where = array('and',$where,array('in','t.project_id',$partnerSelector));

        $data = Yii::app()->db->createCommand()
            ->select('COUNT(t.id) AS count, t.source AS source')
            ->from(Yii::app()->db->tablePrefix.'client t')
            ->leftJoin(Yii::app()->db->tablePrefix.'rel_group_member rel_group','rel_group.client_id = t.id')
            ->leftJoin(Yii::app()->db->tablePrefix.'tr_group group','group.id = rel_group.tr_group_id')
            ->where($this->addDatetimeCondition($where,'t.reg_date'))
            ->group('t.source')
            ->queryAll();

        $datasets=array();
        foreach ($data as $currentSet) {
            $color = implode(',',$this->getRandomColor());
            array_push($datasets,array(
                'value' => $currentSet['count'] = (int)$currentSet['count'],
                'color' => "rgba($color,1)",
                'label' => CHtml::encode(Client::model()->getSourceText($currentSet['source'])).'-'.$currentSet['count'],
            ));
        }

        return array(
            'clients_source' => array('datasets' => array_values($datasets)),
        );
    }
    
    protected function getCostsCenterDynamicStat() {
        $idSelector = $this->getFilter()->getIdSelector('center');
        if ($idSelector === false) return array();
        $baseSelector = $this->getFilter()->getIdSelector('partner', true);
        
        if (!$idSelector) {
            if ($baseSelector) {
                $where = count($baseSelector) == 1 ? 't.project_id = '.array_pop($baseSelector) : array('in','t.project_id',$baseSelector);
            } else {
                $where = null;
            }
        } else {
            $where = array('in','t.id',$idSelector);
        }
        $data = Yii::app()->db->createCommand()
            ->select('FROM_UNIXTIME(costs.datetime,\'%m\') AS month, SUM(costs.value) AS sum, t.id AS id, t.name AS name')
            ->from(Yii::app()->db->tablePrefix.'tr_center t')
            ->join(Yii::app()->db->tablePrefix.'tr_costs costs','costs.center_id = t.id')
            ->where($this->addDatetimeCondition($where,'costs.datetime'))
            ->group('t.id, month')
            ->queryAll();
        
        $this->clearColorPull();
        $datasets = array();
        foreach ($data as $currentSet) {
            if (!($currentSet['month'] = (int)$currentSet['month'])) continue;
            $color = implode(',',$this->getRandomColor());
            if (!isset($datasets[$currentSet['id']]))
                $datasets[$currentSet['id']] = array(
                    'data' => $this->getMonthData(),
                    'fillColor' => "rgba($color,0.5)",
                    'strokeColor' => "rgba($color,1)",
                    'pointColor' => "rgba($color,1)",
                    'pointStrokeColor' => '#ffffff',
                    'label' => CHtml::link(CHtml::encode($currentSet['name']),array('trcenter/view','id' => $currentSet['id'])),
                );
            $datasets[$currentSet['id']]['data'][$currentSet['month']-1] += $currentSet['sum'];
        }
        return array(
            'costs_center_mul' => array('datasets' => array_values($datasets), 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true))),
        );
    }
    
    
    protected function getCostsGroupDynamicStat() {
        $idSelector = $this->getFilter()->getIdSelector('group');
        if ($idSelector === false) return array();
        
        $where = array('in','t.id',$idSelector);
        $data = Yii::app()->db->createCommand()
            ->select('FROM_UNIXTIME(costs.datetime,\'%m\') AS month, SUM(costs.value) AS sum, t.id AS id, t.name AS name')
            ->from(Yii::app()->db->tablePrefix.'tr_group t')
            ->join(Yii::app()->db->tablePrefix.'tr_costs costs','costs.group_id = t.id')
            ->where($this->addDatetimeCondition($where,'costs.datetime'))
            ->group('t.id, month')
            ->queryAll();
        
        $this->clearColorPull();
        $datasets = array();
        foreach ($data as $currentSet) {
            if (!($currentSet['month'] = (int)$currentSet['month'])) continue;
            $color = implode(',',$this->getRandomColor());
            if (!isset($datasets[$currentSet['id']]))
                $datasets[$currentSet['id']] = array(
                    'data' => $this->getMonthData(),
                    'fillColor' => "rgba($color,0.5)",
                    'strokeColor' => "rgba($color,1)",
                    'pointColor' => "rgba($color,1)",
                    'pointStrokeColor' => '#ffffff',
                    'label' => CHtml::link(CHtml::encode($currentSet['name']),array('trgroup/view','id' => $currentSet['id'])),
                );
            $datasets[$currentSet['id']]['data'][$currentSet['month']-1] += $currentSet['sum'];
        }
        return array(
            'costs_group_mul' => array('datasets' => array_values($datasets), 'labels' => array_values(Yii::app()->locale->getMonthNames('abbreviated',true))),  
        );
    }
    
    
    public function addIncomeStat() {
        $this->_charts = array_merge($this->_charts,$this->getIncomeCenterStaticStat(),$this->getIncomeGroupStaticStat(),$this->getIncomeClientIncreaseStat(),$this->getIncomeCenterDynamicStat(),$this->getIncomeGroupDynamicStat());
    }
    
    public function addProfitStat() {
        $this->_charts = array_merge($this->_charts,$this->getProfitCenterStat(),$this->getProfitGroupStat());
    }
    
    public function addProfitabilityStat() {
        $this->_charts = array_merge($this->_charts,$this->getProfitabilityCenterStat(),$this->getProfitabilityGroupStat());
    }
    
    public function addCostsStat() {
        $this->_charts = array_merge($this->_charts,$this->getCostsCenterStaticStat(),$this->getCostsGroupStaticStat(),$this->getCostsCenterDynamicStat(),$this->getCostsGroupDynamicStat());
    }
    
    public function addLessonsStat() {
        $this->_charts = array_merge($this->_charts, $this->getClientsSourceStat(),$this->getClientSkippedAllStaticStat(),$this->getClientSkippedByReasonCenterStat(),$this->getClientSkippedByReasonGroupStat(),$this->getLessonConductedStat(),$this->getLessonConductedOKCenterStat(),$this->getLessonConductedOKGroupStat());
    }
    
    public function getStat() {
        return $this->_charts;
    }

}

?>