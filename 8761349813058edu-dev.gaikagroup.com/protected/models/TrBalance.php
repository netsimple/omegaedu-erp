<?php

class TrBalance {
    
    public static function checkUpdateAccess() {
        return Yii::app()->user->model->is_owner || (Yii::app()->user->model->id == Yii::app()->user->model->project_id);
    }

    public static function create() {
        $id = Yii::app()->user->projectId;
        Yii::app()->db->createCommand("insert into {{tr_balance}} (`key`, `value`, `project_id`) values ('cash', 0, {$id}), ('cashless', 0, {$id})")->execute();
    }

    public static function getCash() {
        $balans = Yii::app()->db->createCommand()->select('value')->from('{{tr_balance}}')->where("`key` = 'cash' AND `project_id` = " . Yii::app()->user->projectId)->queryScalar();

        if ($balans === false) {
            self::create();
            return 0;
        }

        return $balans;
    }
    
    public static function getCashless() {
        $balans = Yii::app()->db->createCommand()->select('value')->from('{{tr_balance}}')->where("`key` = 'cashless' AND `project_id` = " . Yii::app()->user->projectId)->queryScalar();

        if ($balans === false) {
            self::create();
            return 0;
        }

        return $balans;
    }
    
    public static function update($key,$val) {
        if ($key != 'cash' && $key != 'cashless') return false;
        // create if not exist
        self::getCash();
        $val = (int)$val;
        Yii::app()->db->createCommand()->update('{{tr_balance}}',array('value' => $val),"`key` = '{$key}' AND `project_id` = " . Yii::app()->user->projectId);
        return true;
    }
    
    public static function updateRel($key,$incrVal) {
        if ($key != 'cash' && $key != 'cashless') return false;
        $incrVal = (int)$incrVal;
        // create if not exist
        self::getCash();
        $sql = "UPDATE {{tr_balance}} SET `value` = `value` + $incrVal WHERE `key` = :key AND `project_id` = :project_id LIMIT 1";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(':key',$key, PDO::PARAM_STR);
        $command->bindValue(':project_id',Yii::app()->user->projectId, PDO::PARAM_STR);
        $command->execute();
        return true;
    }
    
    public static function getTotal() {
        return self::getCash() + self::getCashless();
    }
    
}

?>