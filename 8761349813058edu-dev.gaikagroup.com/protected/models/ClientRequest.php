<?php

class ClientRequest extends CActiveRecord {
	
	protected $_extendModel;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{client_request}}';
    }
	
	public function getClassName() {
		return 'Заявка';
	}
    
    public function rules() {
        return array(
            array('email, fullname, phone_num, school, social_vk', 'length', 'max' => 255),
			array('source, status', 'length', 'max' => 2),
			array('other', 'length', 'max' => 2),
        );
    }
    
    public function behaviors() {
        return array(
			'softDelete' => array('class' => 'application.components.SoftDeleteBehavior'),
        );
    }

    public function relations() {
        return array(
            'partner' => array(self::BELONGS_TO, 'UserMain', 'project_id'),
        );
    }
    
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'email' => 'E-Mail',
            'project_id' => 'ID проекта',
            'fullname' => 'ФИО',
            'phone_num' => 'Номер телефона',
			'datetime' => 'Дата',
			'school' => 'Школа',
			'social_vk' => 'Страница в VK',
			'other' => 'Список указанных предметов',
			'source' => 'Источник',
			'status' => 'Статус',
        );
    }
	
	public function defaultScope() {
		return array(
			'condition' => Yii::app()->user->getProjectIdCondition('project_id',$this->getTableAlias(false,false)),	
		);
	}
	
	public function beforeSave() {
		if (parent::beforeSave()) {
			if ($this->isNewRecord) {
				$this->datetime = time();
			}
			return true;
		}
	}
	
	public function extend() {
		$result = new Client;
		$result->scenario = 'request';
        $result->email = $this->email;
		$result->fullname = $this->fullname;
		$result->phone_num = $this->phone_num;
		$result->school = $this->school;
		$result->social_vk = $this->social_vk;
		$result->other = $this->other;
		$result->status = $this->status;
		$result->source = $this->source;
		return $result;
	}
	
	public function getName() {
		return $this->fullname;
	}

    public static function requestIcon() {

        if (Yii::app()->user->isGuest || !Yii::app()->user->model->inPartnerGroup && !Yii::app()->user->model->inTeacherGroup) return '';

        if (!$count = self::model()->count('is_deleted = 0')) return '';

        return CHtml::tag('span', array('class'=>'claim')) . CHtml::tag('span', array('class'=>'claimCount'), $count);
    }
	
	public function isValid($attributeName) {
		if ($this->_extendModel === null) $this->_extendModel = $this->extend();
		return $this->_extendModel->hasAttribute($attributeName) && $this->_extendModel->validate($attributeName);
	}

}



?>