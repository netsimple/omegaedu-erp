<?php

class ClientComments extends CActiveRecord {
	
	const SYSTEM_STATUS_NONE 	= 0;
	const SYSTEM_STATUS_CHANGE  = 1;
	const SYSTEM_STATUS_TOSTUDY = 2;
	
	public $username;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{client_history}}';
    }
	
	public function getClassName() {
		return 'Комментарий в истории клиента';
	}
    
    public function rules() {
        return array(
            array('content', 'required'),
            array('content', 'length', 'max' => 255),
			array('content, datetime, username', 'safe', 'on' => 'search'),
        );
    }
    
    public function relations() {
        return array(
            'client' => array(self::BELONGS_TO, 'Client', array('client_id' => 'id')),
			'user' => array(self::BELONGS_TO, 'UserMain', array('user_id' => 'id')),
        );
    }
    
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'datetime' => 'Дата',
            'content' => 'Комментарий',
			'username' => 'Пользователь',
        );
    }
	
    protected function beforeSave() {
        parent::beforeSave();
        
        if ($this->isNewRecord) {
            $this->datetime = time();
        }
        
        return true;
    }
	
	public function search() {
		$criteria = new CDbCriteria;
		$criteria->with = array('user');
		$criteria->compare('user.fullname',$this->username,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('client_id',$this->client_id);
		return new CActiveDataProvider(__CLASS__,array('criteria' => $criteria, 'sort' => array('defaultOrder' => 'datetime DESC'), 'pagination' => array('pageSize' => 20)));
	}
	
	public function getName() {
		return $this->client ? $this->client->name : 'ID #'.$this->id;
	}

    
}



?>