<?php

class TrIncome extends CActiveRecord {
    
	const INCOME_CUSTOM = -1;
	const INCOME_UNDEFINED = 0;
	const INCOME_FRANCHISE = 1;
	const INCOME_ADVERT = 2;
	const INCOME_CLIENT = 3;
	
	const TYPE_UNDEFINED = 0;
	const TYPE_CASHLESS  = 1;
	const TYPE_CASH 	 = 2;
	
	public $username;
	public $city_id;

	protected $_idSelector;
	
	protected $_periodStart;
	protected $_periodEnd;
	
	protected $_searchDP;
	protected $_searchCashlessValue;
	protected $_searchCashlessValueBySection = array();
	protected $_searchCashValue;
	protected $_searchCashValueBySection = array();
	protected $_searchSummaryValue;
	protected $_searchSummaryValueBySection = array();
	
	private $_currentValue;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{tr_income}}';
    }
	
	public function getClassName() {
		return 'Доходы';
	}
    
    public function rules() {
        return array(
			array('period_start, period_end', 'required', 'on' => 'insert, clientInsert, update, ajaxUpdate'),
            array('section', 'required', 'on' => 'insert, clientInsert, update'),
			array('type, value', 'required', 'except' => 'autoCosts'),
			array('section', 'in', 'range' => array(self::INCOME_CUSTOM, self::INCOME_FRANCHISE, self::INCOME_ADVERT, self::INCOME_CLIENT), 'on' => 'insert'),
			array('section', 'in', 'range' => array(self::INCOME_CLIENT), 'on' => 'clientInsert'),
			array('section', 'in', 'range' => array(self::INCOME_CUSTOM, self::INCOME_FRANCHISE, self::INCOME_ADVERT), 'on' => 'update'),
			array('type', 'in', 'range' => array(self::TYPE_CASHLESS, self::TYPE_CASH), 'except' => 'autoCosts', 'message' => 'Необходимо выбрать тип расчёта.'),
            array('value', 'numerical', 'min' => 0, 'except' => 'autoCosts'),

			array('client_id, course_id, group_id, hours', 'safe', 'on' => 'insert'),
            array('client_id, course_id, group_id, hours', 'required', 'on' => 'clientInsert, clientUpdate, ajaxUpdate'),
            array('client_id', 'exist', 'attributeName' => 'id', 'className' => 'Client', 'on' => 'clientInsert'),
            array('course_id', 'exist', 'attributeName' => 'id', 'className' => 'TrCourse', 'on' => 'clientInsert'),
            array('group_id', 'exist', 'attributeName' => 'id', 'className' => 'TrGroup', 'on' => 'clientInsert'),
            array('hours', 'numerical', 'min' => 1, 'on' => 'clientInsert, ajaxUpdate'),
			
			array('period_start', 'date', 'format' => 'd MMM yyyy', 'timestampAttribute' => 'period_start', 'except' => 'ajaxUpdate, autoCosts'),
			array('period_end', 'date', 'format' => 'd MMM yyyy', 'timestampAttribute' => 'period_end', 'except' => 'ajaxUpdate, autoCosts'),
			array('period_start', 'date', 'format' => 'yyyy-MM-dd', 'timestampAttribute' => 'period_start', 'on' => 'ajaxUpdate'),
			array('period_end', 'date', 'format' => 'yyyy-MM-dd', 'timestampAttribute' => 'period_end', 'on' => 'ajaxUpdate'),
			array('period_start', 'compare', 'compareAttribute' => 'period_end', 'operator' => '<=', 'message' => 'Дата начала периода не должна быть больше даты конца периода.', 'on' => 'insert, clientInsert'),
			
			array('comment', 'length', 'max' => 255, 'on' => 'insert, update, ajaxUpdate'),
			array('username, type, section, comment, value, period_start, period_end, datetime, city_id', 'safe', 'on' => 'search'),
			array('client_id, course_id, hours, group_id', 'safe', 'on' => 'search'),
        );
    }
	
	public function behaviors() {
		return array(
			'datetimeLimit' => array('class' => 'application.components.DatetimeLimitBehavior'),
			'softDelete'	=> array('class' => 'application.components.SoftDeleteBehavior'),
		);
	}
	
	public function relations() {
		return array(
			'user' 	 => array(self::BELONGS_TO, 'UserMain', 'user_id'),
			'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
            'course' => array(self::BELONGS_TO,'TrCourse','course_id'),
		);
	}
	
	public function defaultScope() {
		return array(
			'condition' => Yii::app()->user->getProjectIdCondition('project_id',$this->getTableAlias(false,false)),	
		);
	}
    
    public function attributeLabels() {
        return array(
            'id' => 'Доходы',
            'user_id' => 'Пользователь',
            'username' => 'Пользователь',
			'type' => 'Тип',
            'section' => 'Вид',
			'comment' => 'Комментарий',
            'value' => 'Сумма',
            'datetime' => 'Дата добавления',
			'period_start' => 'Начало периода',
			'period_end' => 'Конец периода',
			'client_id' => 'Клиент',
			'course_id' => 'Предмет',
			'group_id' => 'Группа',
			'hours' => 'Кол-во часов',
			'city_id' => 'Город',
        );
    }
	
	protected function afterConstruct() {
		if ($this->scenario == 'clientInsert' || $this->scenario == 'clientUpdate') {
			$this->section = self::INCOME_CLIENT;
		}
		parent::afterConstruct();
	}
	
	protected function afterFind() {
		$this->_currentValue = $this->value;
		$this->_periodStart = $this->period_start;
		$this->_periodEnd = $this->period_end;
		$this->period_start = Yii::app()->dateFormatter->format('yyyy-MM-dd',$this->period_start);
		$this->period_end = Yii::app()->dateFormatter->format('yyyy-MM-dd',$this->period_end);
		if ($this->section == self::INCOME_CLIENT) {
			$this->scenario = $this->hours > 0 ? 'clientUpdate' : 'autoCosts';
		}
		return parent::afterFind();
	}
	
	protected function beforeValidate() {
		if (parent::beforeValidate()) {
			if ($this->section == self::INCOME_CLIENT && $this->isNewRecord)
                $this->scenario = 'clientInsert';
            if ($this->group_id && !$this->course_id)
                $this->course_id = TrGroup::model()->findByPk($this->group_id)->course->id;
			return true;
		}
	}
    
    protected function beforeSave() {
        if (parent::beforeSave()) {
			if ($this->isNewRecord) {
				$this->project_id = Yii::app()->user->projectId;
				$this->datetime = time();
				$this->user_id = Yii::app()->user->id;
			}
			if ($this->section == self::INCOME_CLIENT && $this->hours > 0) {
				$this->comment = Yii::t('general','{n} час|{n} часа|{n} часов',$this->hours);
				if ($this->course) $this->comment .= ', '.$this->course->name;
				if ($this->client) {
					$this->comment = $this->client->name . ' ('.$this->comment.')';
				}
			}

            if ($this->value < 0)
                return false;

            return true;
        }
    }
	
	protected function afterSave() {
		if ($this->hours > 0) $this->recountLessonsPaid();
		if ($updateValue = ($this->isNewRecord ? $this->value : $this->value - $this->_currentValue)) {
			if ($this->type) TrBalance::updateRel($this->type == self::TYPE_CASHLESS ? 'cashless' : 'cash', $updateValue);
			$this->_currentValue = $this->value;
		}
		return parent::afterSave();
	}
	
	protected function beforeDelete() {
		if ($this->type) TrBalance::updateRel($this->type == self::TYPE_CASHLESS ? 'cashless' : 'cash', -$this->value);
		return parent::beforeDelete();
	}
	
	public function afterRestore() {
		if ($this->type) TrBalance::updateRel($this->type == self::TYPE_CASHLESS ? 'cashless' : 'cash', $this->value);
	}
    
	public function getSectionList($scenario=null) {
		if ($scenario === null) $scenario = $this->scenario;
		switch ($scenario) {
			case 'update':
				return array(
					self::INCOME_CUSTOM => 'Внесение средств',
					self::INCOME_FRANCHISE => 'Франшиза',
					self::INCOME_ADVERT => 'Реклама',
				);
			case 'clientUpdate':
				return array(
					self::INCOME_CLIENT => 'Клиент',
				);
			default:
				return array(
					self::INCOME_CUSTOM => 'Внесение средств',
					self::INCOME_FRANCHISE => 'Франшиза',
					self::INCOME_ADVERT => 'Реклама',
					self::INCOME_CLIENT => 'Клиент',
				);
		}
	}
	
	public function getTypeList() {
		$data = array(
			self::TYPE_UNDEFINED => 'не опр.',
			self::TYPE_CASHLESS  => 'безнал',
			self::TYPE_CASH      => 'нал',
		);
		return $data;
	}
	
	public function getSearchCashless($section = null) {
		$this->search();
		if ($section === null) return $this->_searchCashlessValue;
		return isset($this->_searchCashlessValueBySection[$section]) ? $this->_searchCashlessValueBySection[$section] : 0;
	}
	
	public function getSearchCash($section = null) {
		$this->search();
		if ($section === null) return $this->_searchCashValue;
		return isset($this->_searchCashValueBySection[$section]) ? $this->_searchCashValueBySection[$section] : 0;
	}
	
	public function getSearchSummary($section = null) {
		$this->search();
		if ($section === null) return $this->_searchSummaryValue;
		return isset($this->_searchSummaryValueBySection[$section]) ? $this->_searchSummaryValueBySection[$section] : 0;
	}
    
    public function search($pageSize=100) {
		if (!empty($this->_searchDP)) return $this->_searchDP;
        $criteria = new CDbCriteria;
		if ($this->scenario == 'search') {


            $period_start = CDateTimeParser::parse($this->period_start,'d MMM yyyy',array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0));
            if ($period_start)
                $criteria->addCondition('period_start >= ' . $period_start);

            $period_end = CDateTimeParser::parse($this->period_end,'d MMM yyyy',array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0));
            if ($period_end)
                $criteria->addCondition('period_end <= ' . $period_end);

            $datetime = CDateTimeParser::parse($this->datetime,'d MMM yyyy',array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0));
            if ($datetime)
                $criteria->addBetweenCondition('datetime', $datetime, $datetime+3600*24);

			$criteria->compare('section',$this->section);
			$criteria->compare('comment',$this->comment,true);
			$criteria->compare('type',$this->type);
			$criteria->compare('value',$this->value);
			$criteria->compare('client_id',$this->client_id);
			if ($this->username) {
                $criteria->with = array('user');
                $criteria->compare('user.fullname',$this->username,true);
			}
			$criteria->addCondition('hours >= 0');
		}
		if ($this->scenario == 'clientUpdate') {
			$criteria->compare('client_id',$this->client_id);
			$criteria->compare('section',TrIncome::INCOME_CLIENT);
		}

        if ($this->city_id) {
            $projectArray = UserMain::projectsByCityId($this->city_id);
            $criteria->addInCondition('t.project_id', array_unique($projectArray));
        }
		
		$this->beforeFind();
		$this->applyScopes($criteria);
		$arr = $criteria->toArray();
		$data = Yii::app()->db->createCommand()
            ->select('type, section, SUM(value)')
            ->from($this->tableName().' t')
            ->join('{{user_main}} user', 'user.id = t.user_id')
            ->where($arr['condition'],$arr['params'])
            ->group('type, section')->queryAll();
		$this->_searchCashlessValue = 0;
		$this->_searchCashlessValueBySection = array();
		$this->_searchCashValue = 0;
		$this->_searchCashValueBySection = array();
		$this->_searchSummaryValue = 0;
		$this->_searchSummaryValueBySection = array();
		foreach ($data as $currSet) {
			switch ($currSet['type']) {
				case self::TYPE_CASHLESS:
					$this->_searchCashlessValueBySection[$currSet['section']] = $currSet['SUM(value)'];
					$this->_searchCashlessValue += $currSet['SUM(value)'];
					break;
				case self::TYPE_CASH:
					$this->_searchCashValueBySection[$currSet['section']] = $currSet['SUM(value)'];
					$this->_searchCashValue += $currSet['SUM(value)'];
					break;
				default:
					$this->_searchSummaryValueBySection[$currSet['section']] = $currSet['SUM(value)'];
					$this->_searchSummaryValue += $currSet['SUM(value)'];
			}
		}
		$this->_searchSummaryValue = $this->_searchSummaryValue + $this->_searchCashlessValue + $this->_searchCashValue;

        return $this->_searchDP = new CActiveDataProvider(__CLASS__, array(
            'criteria'=>$criteria,
            'sort'=>array('defaultOrder'=>'t.period_start DESC, t.period_end DESC, t.datetime DESC'),
            'pagination' => array('pageSize' => $pageSize)
        ));
    }
	
	public function setIdSelector($idArr) {
		$this->_idSelector = $idArr;
	}
	
	public function getName() {
		$name = $this->getSectionList('search');
		$name = $name[$this->section];
		if ($this->client_id && $this->client) {
			$name .= ' ('.$this->client->name.')';
		}
		return $name;
	}
	
	public function getInfo() {
		return $this->value;
	}
	
    public function getCourseList() {
        $result = array();
        if ($this->client_id) {
            $criteria = new CDbCriteria;
            $criteria->select = 'id, name';
            $criteria->order = 'name ASC';
            $data = TrCourse::model()->findAll($criteria);
            foreach ($data as $currentDS) $result[$currentDS->id] = $currentDS->name;
        }
        return $result;
    }
	
	protected function getDatetimeRange() {
		$period_start = !is_numeric($this->period_start) ? strtotime($this->period_start) : $this->period_start;
		$period_end   = !is_numeric($this->period_end)   ? strtotime($this->period_end)   : $this->period_end;
		if ($this->hours >= 0) {
			if (!$this->isNewRecord) {
				$period_start = min($period_start,$this->_periodStart);
				$period_end   = max($period_end,$this->_periodEnd);
			}
			return array('period_start' => $period_start, 'period_end' => $period_end);
		}
		
		$where = array('and',array('and','course_id = '.$this->course_id,'client_id = '.$this->client_id),array('and','section = '.self::INCOME_CLIENT,'is_deleted = 0'));
		$where = array('and',$where,'hours > 0');
		if ($period_start) $where = array('and',$where,array('and','period_start <= '.$period_start,'period_end >= '.$period_start));

		$result = Yii::app()->db->createCommand()
			->select('period_start, period_end')
			->from($this->tableName())
			->where($where)
			->queryRow();
		return $result ? $result : array('period_start' => null, 'period_end' => null);
	}
	
	public function getBalance() { // DEPRECATED
		$where = array('and',array('and','course_id = '.$this->course_id,'client_id = '.$this->client_id),array('and','section = '.self::INCOME_CLIENT,'is_deleted = 0'));
		$bounds = $this->getDatetimeRange();
		if ($bounds['period_start'] && $bounds['period_end']) {
			$where = array('and',$where,array('and','period_start >= '.$bounds['period_start'],'period_end <= '.$bounds['period_end']));
		}
		return Yii::app()->db->createCommand()
			->select('SUM(value * SIGN(hours))')
			->from($this->tableName())
			->where($where)
			->queryScalar();
	}
	
	public function getDebt() {
		if ($this->hours > 0) throw new CException();
		
		list($start,$end) = array_values($this->getDatetimeRange());
		if (!$start || !$end) return $this->value;
		
		$whereId = array('and',array('and','course_id = '.$this->course_id,'client_id = '.$this->client_id),array('and','section = '.self::INCOME_CLIENT,'is_deleted = 0'));
		$whereTime = array(
			'or',
			array(
				'and',
				'hours > 0',
				array('and','period_start >= '.$start,'period_end <= '.$end),
			),
			array(
				'and',
				'hours < 0',
				array('and','period_start <= '.(!is_numeric($this->period_start) ? strtotime($this->period_start) : $this->period_start)),
			),
		);
		$where = array('and',$whereId,$whereTime);
		$debt = Yii::app()->db->createCommand()
			->select('SUM(value * SIGN(hours))')
			->from($this->tableName())
			->where($where)
			->queryScalar();
		
		return $debt < 0 ? -$debt : 0;
	}
	
	public function recountLessonsPaid() {
		if ($this->section != self::INCOME_CLIENT || !$this->client_id || !$this->course_id) return false;
		
		list($start,$end) = array_values($this->getDatetimeRange());
		
		$criteria = new CDbCriteria;
		$criteria->addBetweenCondition('datetime_start',(string)$start,(string)$end);
		$criteria->compare('tr_course_id',$this->course_id);
		$criteria->with = array(
			'clients' => array(
				'joinType'	=> 'INNER JOIN',
				'condition' => 'client_id = '.$this->client_id,
			),
		);
		$criteria->order = 'datetime_start ASC';
		$data = Schedule::model()->findAll($criteria);
		
		foreach ($data as $currLesson) $currLesson->clients[0]->updateIncome();
		foreach ($data as $currLesson) $currLesson->clients[0]->update();
		
		return true;
	}

}

?>