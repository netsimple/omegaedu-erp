<?php

class City extends CActiveRecord {

    private static $projectToCityRel = array();

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{city}}';
    }

    public function getClassName() {
        return 'Город';
    }

    public function rules() {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 255),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Название',
        );
    }

    public static function range() {
        return Yii::app()->db->createCommand()->select('id')->from('{{city}}')->queryColumn();
    }

    public static function listArray() {
        return CHtml::listData(self::model()->findAll(), 'id', 'name');
    }

    public static function name($id) {
        $city = self::model()->findByPk($id);

        if ($city)
            return $city->name;
        else
            return 'Не определен';
    }

    public static function nameByProjectId($id) {

        if (isset(self::$projectToCityRel[$id]))
            return self::$projectToCityRel[$id];

        $user = UserMain::model()->findByPk($id);

        if ($user && $user->city)
            self::$projectToCityRel[$id] = $user->city->name;
        else
            self::$projectToCityRel[$id] = 'Не определен';

        return self::$projectToCityRel[$id];
    }

}