<?php

class TrCosts extends CActiveRecord {
    
	const COSTS_CUSTOM = -1;
	const COSTS_UNDEFINED = 0;
	const COSTS_TAX = 2;
	const COSTS_TEACHER_WAGE = 4;
	const COSTS_ADVERT_INTERNER = 5;
	const COSTS_ADVERT_OFFLINE = 6;
	const COSTS_RENT = 7;
	const COSTS_UTILITIES = 8;
	const COSTS_CONTRACTORS = 9;
	const COSTS_ACCOUNTING = 10;
	const COSTS_PROCUREMENT = 11;
	const COSTS_REPAIR = 12;
	const COSTS_OTHER = 13;
	const COSTS_ADMIN_WAGE = 14;
	const COSTS_FRANCHISE = 15;
	const COSTS_COMUNAL = 16;
	const COSTS_TELEPHONY = 17;
	const COSTS_CLEANING = 18;

	
	const TYPE_UNDEFINED = 0;
	const TYPE_CASHLESS  = 1;
	const TYPE_CASH 	 = 2;
	
	public $username;
	public $city_id;

	protected $_idSelector;
	
	protected $_searchDP;
	protected $_searchCashlessValue;
	protected $_searchCashlessValueBySection = array();
	protected $_searchCashValue;
	protected $_searchCashValueBySection = array();
	protected $_searchSummaryValue;
	protected $_searchSummaryValueBySection = array();
	
	private $object_param;
	private $_currentValue;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{tr_costs}}';
    }
	
	public function getClassName() {
		return 'Расходы';
	}
    
    public function rules() {
        return array(
			array('period_start, period_end, object_param', 'required', 'on' => 'insert, update, updateNoSection, ajaxUpdate'),
            array('type, value', 'required'),
            array('section', 'required', 'except' => 'updateNoSection'),
			array('section', 'in', 'range' => $this->getAllowedSections(), 'except' => 'updateNoSection'),
			array('type', 'in', 'range' => array(self::TYPE_CASHLESS, self::TYPE_CASH), 'message' => 'Необходимо выбрать тип расчёта.'),
            array('value', 'numerical', 'min' => 0),
			array('period_start', 'date', 'format' => 'd MMM yyyy', 'timestampAttribute' => 'period_start', 'except' => 'ajaxUpdate'),
			array('period_end', 'date', 'format' => 'd MMM yyyy', 'timestampAttribute' => 'period_end', 'except' => 'ajaxUpdate'),
			array('period_start', 'date', 'format' => 'yyyy-MM-dd', 'timestampAttribute' => 'period_start', 'on' => 'ajaxUpdate'),
			array('period_end', 'date', 'format' => 'yyyy-MM-dd', 'timestampAttribute' => 'period_end', 'on' => 'ajaxUpdate'),
			array('period_start', 'compare', 'compareAttribute' => 'period_end', 'operator' => '<=', 'message' => 'Дата начала периода не должна быть больше даты конца периода.', 'on' => 'insert, update, updateNoSection'),
			array('comment', 'length', 'max' => 255),
			array('username, type, section, comment, value, object_param, period_start, period_end, datetime, city_id', 'safe', 'on' => 'search'),
        );
    }
	
	public function behaviors() {
		return array(
			'datetimeLimit' => array('class' => 'application.components.DatetimeLimitBehavior'),
			'softDelete'	=> array('class' => 'application.components.SoftDeleteBehavior', 'parents' => array('center', 'group')),
		);
	}
	
	public function relations() {
		return array(
			'user'  => array(self::BELONGS_TO, 'UserMain', 'user_id'),
			'group' => array(self::BELONGS_TO, 'TrGroup', 'group_id'),
			'center'=> array(self::BELONGS_TO, 'TrCenter', 'center_id'),
		);
	}
	
	public function defaultScope() {
		return array(
			'condition' => Yii::app()->user->getProjectIdCondition('project_id',$this->getTableAlias(false,false)),	
		);
	}
	
	public function getAllowedSections() {
		$customArr = array(self::COSTS_CUSTOM);
		$otherArr = array(self::COSTS_FRANCHISE);
		$accountingArr = array(self::COSTS_COMUNAL, self::COSTS_TELEPHONY, self::COSTS_CLEANING);
		$groupArr = array(self::COSTS_ADMIN_WAGE,self::COSTS_TAX,self::COSTS_PROCUREMENT,self::COSTS_TEACHER_WAGE);
		$centerArr = array(self::COSTS_ADMIN_WAGE,self::COSTS_ADVERT_INTERNER,self::COSTS_ADVERT_OFFLINE,self::COSTS_RENT,self::COSTS_UTILITIES,self::COSTS_CONTRACTORS,self::COSTS_PROCUREMENT,self::COSTS_REPAIR,self::COSTS_OTHER);
		switch ($this->scenario) {
			case 'group':
				return $groupArr;
			case 'center':
				return $centerArr;
			case 'search':
				return array_merge($groupArr,$centerArr);
			case 'updateNoSection':
				return $customArr;
			case 'insert':
			case 'update':
			case 'ajaxUpdate':
				if ($this->group_id) return $groupArr;
				if ($this->center_id) return $centerArr;
				if ($this->section == self::COSTS_CUSTOM) return $customArr;
				if ($this->section == self::COSTS_ACCOUNTING) return $accountingArr;
				if ($this->center_id !== null) return array_merge($groupArr,$centerArr,$otherArr);
			default:
				return array();
		}
	}
    
    public function attributeLabels() {
        return array(
            'id' => 'Расходы',
			'center_id' => 'Центр',
			'group_id' => 'Группа',
            'user_id' => 'Пользователь',
            'username' => 'Пользователь',
			'type' => 'Тип',
            'section' => 'Вид',
			'comment' => 'Комментарий',
            'value' => 'Сумма',
            'datetime' => 'Дата добавления',
			'period_start' => 'Начало периода',
			'period_end' => 'Конец периода',
			'object_param' => 'Элемент',
            'city_id' => 'Город',
        );
    }

	protected function afterConstruct() {
		if (!$this->center_id) $this->center_id = 0;
		if (!$this->group_id) $this->group_id = 0;
		return parent::afterConstruct();
	}
	
	protected function afterFind() {
		$this->_currentValue = $this->value;
		$this->period_start = Yii::app()->dateFormatter->format('yyyy-MM-dd',$this->period_start);
		$this->period_end = Yii::app()->dateFormatter->format('yyyy-MM-dd',$this->period_end);
		if ($this->section == self::COSTS_CUSTOM) {
			$this->object_param = 'custom';
			if ($this->scenario == 'update') $this->scenario = 'updateNoSection';
		} elseif ($this->section == self::COSTS_ACCOUNTING) {
			$this->object_param = 'accounting';
		} elseif ($this->group_id) {
			$this->object_param = 'g_'.$this->group_id;
		} else {
			$this->object_param = 'c_'.(int)$this->center_id;
		}
		return parent::afterFind();
	}
    
    protected function beforeSave() {
        if (parent::beforeSave()) {
			if ($this->group_id && !$this->center_id && $this->group) $this->center_id = $this->group->tr_center_id;
			if ($this->isNewRecord) {
				$this->datetime = time();
				if (!$this->period_start) $this->period_start = $this->datetime;
				if (!$this->period_end) $this->period_end = $this->datetime;
				$this->user_id = Yii::app()->user->id;
				$this->project_id = Yii::app()->user->projectId;
			}
            if ($this->scenario == 'ajaxUpdate') {
                $this->validate(array('period_start', 'period_end'));
            }
            return true;
        }
    }
	
	protected function afterSave() {
		if ($updateValue = -($this->isNewRecord ? $this->value : $this->value - $this->_currentValue)) {
			if ($this->type) TrBalance::updateRel($this->type == self::TYPE_CASHLESS ? 'cashless' : 'cash', $updateValue);
			$this->_currentValue = $this->value;
		}
		return parent::afterSave();
	}
	
	protected function beforeDelete() {
		if ($this->type) TrBalance::updateRel($this->type == self::TYPE_CASHLESS ? 'cashless' : 'cash', $this->value);
		return parent::beforeDelete();
	}
	
	public function afterRestore() {
		if ($this->type) TrBalance::updateRel($this->type == self::TYPE_CASHLESS ? 'cashless' : 'cash', -$this->value);
	}
    
	public function getSectionList($check = false) {
		if ($this->section == self::COSTS_CUSTOM) {
			return array(
				self::COSTS_CUSTOM => 'Вывод средств',
			);
		}
		$labels = array(
			self::COSTS_TAX => 'Налоги',
			self::COSTS_TEACHER_WAGE => 'Зарплата преподавателю',
			self::COSTS_ADMIN_WAGE => 'Персонал',
			self::COSTS_ADVERT_INTERNER => 'Реклама в Интернет',
			self::COSTS_ADVERT_OFFLINE => 'Реклама оффлайн',
			self::COSTS_RENT => 'Аренда',
			self::COSTS_UTILITIES => 'Коммунальные платежи',
			self::COSTS_CONTRACTORS => 'Подряд',
			self::COSTS_ACCOUNTING => 'Бух.учет',
			self::COSTS_PROCUREMENT => 'Закупки',
			self::COSTS_REPAIR => 'Ремонт',
			self::COSTS_FRANCHISE => 'Франчайзинг',
			self::COSTS_OTHER => 'Прочее',
			self::COSTS_COMUNAL => 'Комунальные',
			self::COSTS_TELEPHONY => 'Телефония',
			self::COSTS_CLEANING => 'Уборка',
		);
		if ($check) {
			$allowedKeys = $this->getAllowedSections();
			foreach ($labels as $currKey => $currLabel) {
				if (!in_array($currKey,$allowedKeys)) unset($labels[$currKey]);
			}
		}
		return $labels;
	}
	
	public function getTypeList() {
		$data = array(
			self::TYPE_UNDEFINED => 'не опр.',
			self::TYPE_CASHLESS  => 'безнал',
			self::TYPE_CASH      => 'нал',
		);
		return $data;
	}
	
	public function getSearchCashless($section = null) {
		$this->search();
		if ($section === null) return $this->_searchCashlessValue;
		return isset($this->_searchCashlessValueBySection[$section]) ? $this->_searchCashlessValueBySection[$section] : 0;
	}
	
	public function getSearchCash($section = null) {
		$this->search();
		if ($section === null) return $this->_searchCashValue;
		return isset($this->_searchCashValueBySection[$section]) ? $this->_searchCashValueBySection[$section] : 0;
	}
	
	public function getSearchSummary($section = null) {
		$this->search();
		if ($section === null) return $this->_searchSummaryValue;
		return isset($this->_searchSummaryValueBySection[$section]) ? $this->_searchSummaryValueBySection[$section] : 0;
	}
    
    public function search($pageSize=100) {
		if (!empty($this->_searchDP)) return $this->_searchDP;
        $criteria = new CDbCriteria;
		if ($this->scenario == 'search') {
			if ($this->group_id) {
				$criteria->compare('group_id',$this->group_id);
			} elseif ($this->center_id !== null) {
				$criteria->compare('center_id',$this->center_id);
				$criteria->compare('group_id',0);
			}
		} else {
			$criteria->compare('center_id',$this->center_id);
			$criteria->compare('group_id',$this->group_id);
		}
		if ($this->scenario == 'search') {

            $period_start = CDateTimeParser::parse($this->period_start,'d MMM yyyy',array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0));
            if ($period_start)
                $criteria->addCondition('period_start >= ' . $period_start);

            $period_end = CDateTimeParser::parse($this->period_end,'d MMM yyyy',array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0));
            if ($period_end)
                $criteria->addCondition('period_end <= ' . $period_end);

            $datetime = CDateTimeParser::parse($this->datetime,'d MMM yyyy',array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0));
            if ($datetime)
                $criteria->addBetweenCondition('datetime', $datetime, $datetime+3600*24);

			$criteria->compare('type',$this->type);
			$criteria->compare('section',$this->section);
			$criteria->compare('comment',$this->comment,true);
			$criteria->compare('value',$this->value);
			if ($this->username) {
                $criteria->with = array('user');
                $criteria->compare('user.fullname',$this->username,true);
			}
		}

        if ($this->city_id) {
            $projectArray = UserMain::projectsByCityId($this->city_id);
            $criteria->addInCondition('t.project_id', array_unique($projectArray));
        }

		$this->beforeFind();
		$this->applyScopes($criteria);
		$arr = $criteria->toArray();
		$data = Yii::app()->db->createCommand()
            ->select('type, section, SUM(value)')
            ->from($this->tableName().' t')
            ->join('{{user_main}} user', 'user.id = t.user_id')
            ->where($arr['condition'],$arr['params'])
            ->group('type, section')->queryAll();
		$this->_searchCashlessValue = 0;
		$this->_searchCashlessValueBySection = array();
		$this->_searchCashValue = 0;
		$this->_searchCashValueBySection = array();
		$this->_searchSummaryValue = 0;
		$this->_searchSummaryValueBySection = array();
		foreach ($data as $currSet) {
			switch ($currSet['type']) {
				case self::TYPE_CASHLESS:
					$this->_searchCashlessValueBySection[$currSet['section']] = $currSet['SUM(value)'];
					$this->_searchCashlessValue += $currSet['SUM(value)'];
					break;
				case self::TYPE_CASH:
					$this->_searchCashValueBySection[$currSet['section']] = $currSet['SUM(value)'];
					$this->_searchCashValue += $currSet['SUM(value)'];
					break;
				default:
					$this->_searchSummaryValueBySection[$currSet['section']] = $currSet['SUM(value)'];
					$this->_searchSummaryValue += $currSet['SUM(value)'];
			}
		}
		$this->_searchSummaryValue = $this->_searchSummaryValue + $this->_searchCashlessValue + $this->_searchCashValue;
		
        return $this->_searchDP = new CActiveDataProvider(__CLASS__, array(
            'criteria'=>$criteria,
            'sort'=>array('defaultOrder'=>'period_start DESC, period_end DESC, datetime DESC'),
            'pagination' => array('pageSize' => $pageSize)
        ));
    }
	
	public function setIdSelector($idArr) {
		$this->_idSelector = $idArr;
	}
    
    public function getSummaryData() {
        $criteria = new CDbCriteria;
        $criteria->select = 'section, value';
		if ($this->_idSelector) {
			$criteria = new CDbCriteria;
			if ($this->scenario == 'center') {
				$criteria->compare('center_id',$this->_idSelector);
			} elseif ($this->scenario == 'group') {
				$criteria->compare('center_id',$this->center_id);
				$criteria->compare('group_id',$this->_idSelector);
			} else {
				$criteria->compare('center_id',0);
			}
		} else {
			if ($this->scenario == 'center' || $this->scenario == 'group') {
				$criteria->compare('center_id',$this->center_id);
			}
			if ($this->scenario == 'group') {
				$criteria->compare('group_id',$this->group_id);
			}
		}
        $criteria->order = 'section ASC';
        $criteria->mergeWith($this->dbCriteria);
        $data = $this->findAll($criteria);
        $keyStore = array();
        foreach ($data as $currKey => $currentDataset) {
            if (!isset($keyStore[$currentDataset->section])) {
                $keyStore[$currentDataset->section] = $currKey;
            } else {
                $data[$keyStore[$currentDataset->section]]->value += $currentDataset->value;
                unset($data[$currKey]);
            }
        }
        return new CArrayDataProvider(array_values($data),array('pagination' => false));
    }
    
    public function getSummaryCostsValue() {
        if ($this->center_id === null && $this->section != self::COSTS_CUSTOM && $this->_idSelector === null) return 0;
		if ($this->scenario == 'group' && $this->group_id === null) return 0;
		if ($this->_idSelector) {
			$where = $this->scenario == 'group' ? array('and','center_id = '.$this->center_id,array('in','group_id',$this->_idSelector)) : array('in','center_id',$this->_idSelector);
		} else {
			$where = $this->scenario == 'group' ? array('and','center_id = '.$this->center_id,'group_id = '.$this->group_id) : ($this->center_id ? 'center_id = '.$this->center_id : '0=1');
		}
		$where = $this->addDatetimeCondition($where);
		$result = Yii::app()->db->createCommand()
			->select('SUM(value)')
			->from(Yii::app()->db->tablePrefix.'tr_costs')
			->where($where)
			->queryScalar();
        return (int)$result;
    }
	
	public function getName() {
		$name = $this->sectionList[$this->section];
		if ($this->group_id && $this->group) {
			return $name.' ('.$this->group->name.')';
		}
		if ($this->center_id && $this->center) {
			return $name.' ('.$this->center->name.')';
		}
		if ($this->section != self::COSTS_CUSTOM) $name .= ' (общие)';
		return $name;
	}
	
	public function getInfo() {
		return $this->value;
	}
	
	public function getObjectName($extended=false) {
		if ($this->group_id) {
			$name = 'Группа';
			return $this->group ? $name.=' ('.$this->group->name.')' : $name;
		}
		if ($this->center_id) {
			$name = 'Центр';
			return $this->center ? $name.=' ('.$this->center->name.')' : $name;
		}
        if (!$extended)
		    return '';

        $sections = array('custom' => 'Вывод средств', 'c_0' => 'Общие', 'accounting' => 'Бух.учет');;
        if (array_key_exists($this->object_param, $sections))
            return $sections[$this->object_param];

        return '';
	}
	
	public function getObject_param() {
		return $this->object_param;
	}
	
	public function getObjectParamList($implodeList = false) {
		$result = array('custom' => 'Вывод средств', 'c_0' => 'Общие', 'accounting' => 'Бух.учет');
		if (!$implodeList) {
			$result['Центры'] = array();
			$result['Группы'] = array();
		}
		$criteria = new CDbCriteria;
		$criteria->select = 'id, name';
		$criteria->order = 'name ASC';
		$data = TrCenter::model()->findAll($criteria);
		foreach ($data as $currElem) {
			if (!$implodeList) {
				$result['Центры']['c_'.$currElem->id] = $currElem->name;
			} else {
				$result['c_'.$currElem->id] = $currElem->className.' ('.$currElem->name.')';
			}
		}
		$data = TrGroup::model()->findAll($criteria);
		foreach ($data as $currElem) {
			if (!$implodeList) {
				$result['Группы']['g_'.$currElem->id] = $currElem->name;
			} else {
				$result['g_'.$currElem->id] = $currElem->className.' ('.$currElem->name.')';
			}
		}
		return $result;
	}
	
	public function setObject_param($objectId) {
		if ($objectId == 'custom') {
			$this->section = self::COSTS_CUSTOM;
			$this->object_param = $objectId;
			return;
		}

        if ($objectId == 'accounting') {
			$this->section = self::COSTS_ACCOUNTING;
			$this->object_param = $objectId;
			return;
		}

        if (count($arr = explode('_',$objectId)) != 2) return;
        switch ($arr[0]) {
            case 'c':
                $this->center_id = (int)$arr[1];
				$this->group_id = 0;
                break;
            case 'g':
                $this->group_id = (int)$arr[1];
				$this->center_id = $this->group ? $this->group->tr_center_id : 0;
                break;
        }
		$this->object_param = $objectId;
		if ($this->scenario == 'ajaxUpdate' && !in_array($this->section,$arr=$this->getAllowedSections()))
            $this->section = array_shift($arr);
        return;
	}

}

?>