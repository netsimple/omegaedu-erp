<?php

class Extauth extends CActiveRecord {
	
	protected $_service;

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
    
	public function tableName() {
	    return '{{eauth}}';
	}
	 
	public function relations() {
		return array(
			'user' => array(self::BELONGS_TO, 'UserMain', array('user_id' => 'id')),
		);
	 }
	 
	public function setService($obj) { $this->_service = $obj; }
	
	protected function beforeSave() {
		if (parent::beforeSave()) {
			$this->id = $this->_service->id;
			$this->user_id = Yii::app()->user->id;
			$this->service_name = $this->_service->serviceName;
			AccessLog::add('openid',array('serviceName' => $this->_service->serviceName));
			return true;
		}
	}

}

?>