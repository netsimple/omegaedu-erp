<?php

class StatFilterForm extends DateFilterForm {

    const SOURCE_PARTNER = 1;
    const SOURCE_GROUP = 2;
    const SOURCE_CLIENT = 3;
    const SOURCE_CENTER = 4;
    const SOURCE_TEACHER = 5;

    const TYPE_INCOME = 1;
    const TYPE_COSTS = 2;
    const TYPE_PROFIT = 3;
    const TYPE_GAIN = 4;
    const TYPE_EDUCATION = 5;

    public $type;
    public $source;
    public $source_id;
    public $client_id;
    public $group_id;
    public $center_id;
    public $teacher_id;
    public $partner_id;
    public $city_id;
    public $projects;

    protected $_idSelectors;

    protected function afterConstruct() {
        $this->_idSelectors = array(
            'partner'   => null,
            'center'    => null,
            'group'     => null,
            'client'    => null,
            'teacher'   => null,
        );
        parent::afterConstruct();
    }
    
    public function excludeType($type) {
        if ($this->_idSelectors[$type] === null) $this->_idSelectors[$type] = false;
    }

    public function getIdSelector($type, $systemCall = false) {
        if ($this->_idSelectors[$type] === null || $systemCall && !$this->_idSelectors[$type]) {

            $this->projects = UserMain::projectsList();
            if ($this->city_id)
                $this->projects = UserMain::projectsByCityId($this->city_id);

            switch ($type) {
                case 'partner':
                    $data = Yii::app()->user->model->project_id ? array(Yii::app()->user->model->project_id) : $this->projects;
                    break;
                case 'center':
                    $data = Yii::app()->db->createCommand()
                        ->select('id')
                        ->from(Yii::app()->db->tablePrefix.'tr_center')
                        ->where(
                            Yii::app()->user->model->project_id ?
                            'is_deleted = 0 AND project_id='.Yii::app()->user->model->project_id :
                            array('and', 'is_deleted = 0', array('in','project_id', $this->getIdSelector('partner', true)))
                        )->queryColumn();
                    break;
                case 'group':
                    $data = Yii::app()->db->createCommand()
                        ->select('t.id')
                        ->from(Yii::app()->db->tablePrefix.'tr_group t')
                        ->where(array('and', 't.is_deleted = 0', array('in','t.tr_center_id',$this->getIdSelector('center', true))))
                        ->queryColumn();
                    break;
                case 'client':
                    $data = Yii::app()->db->createCommand()
                        ->select('cl.id')
                        ->from(Yii::app()->db->tablePrefix.'client cl')
                        ->where(Yii::app()->user->model->project_id ?
                                'cl.is_deleted = 0 AND cl.project_id='.Yii::app()->user->model->project_id :
                                array('and', 'cl.is_deleted = 0', array('in','cl.project_id', $this->getIdSelector('partner', true))))
                        ->queryColumn();
                    break;
                case 'teacher':
                    $where = 'is_deleted = 0 AND is_teacher = 1';
                    $data = Yii::app()->db->createCommand()
                        ->select('id')
                        ->from(Yii::app()->db->tablePrefix.'user_main')
                        ->where(Yii::app()->user->model->project_id ?
                            array('and',$where,'project_id='.Yii::app()->user->model->project_id) :
                            array('and', $where, array('in','project_id', $this->getIdSelector('partner', true))))
                        ->queryColumn();
                    break;
            }

            if ($this->_idSelectors[$type] !== false) $this->_idSelectors[$type] = $data;
            if ($systemCall) return $data;
        }
        return $this->_idSelectors[$type] !== false ? (array)$this->_idSelectors[$type] : false;
    }
    
    public function setIdSelector($type,$idSelector) {
        if ($this->_idSelectors[$type] === false) return;
        $this->_idSelectors[$type] = is_array($idSelector) ? $idSelector : (array)$idSelector;
        switch ($type) {
            case 'group':
                $this->excludeType('partner');
                $this->excludeType('center');
                break;
            case 'teacher':
                if (!$idSelector) $idSelector = $this->getIdSelector('teacher',true);
                if ($idSelector) {
                    $data = Yii::app()->db->createCommand()
                        ->select('id')
                        ->from(Yii::app()->db->tablePrefix.'tr_group')
                        ->where(array('in','teacher_id',$idSelector))
                        ->queryColumn();
                } else {
                    $data = array();
                }
                $this->setIdSelector('group',$data);
                break;
            case 'client':
                $this->excludeType('client');
                if (!$idSelector) $idSelector = $this->getIdSelector('client',true);
                $data = $idSelector ? Yii::app()->db->createCommand()
                    ->select('rel.tr_group_id')
                    ->from(Yii::app()->db->tablePrefix.'rel_group_member rel')
                    ->where(array('in','rel.client_id',$idSelector))
                    ->queryColumn() : array();
                $this->setIdSelector('group',$data);
                break;
            case 'center':
                $this->excludeType('partner');
                break;
        }
    }

    public function getSources($noPartner=false) {
        $sources = array(
            self::SOURCE_PARTNER => 'Партнер',
            self::SOURCE_CENTER => 'Центр',
            self::SOURCE_GROUP => 'Группа',
        );

        if ($noPartner)
            unset($sources[self::SOURCE_PARTNER]);

        return $sources;
    }

    public function getTypes() {
        if ($this->scenario == 'finance')
            return array(
                self::TYPE_INCOME => 'Доход',
                self::TYPE_COSTS => 'Расход',
                self::TYPE_PROFIT => 'Рентабельность',
                self::TYPE_GAIN => 'Прибыль',
            );
        if ($this->scenario == 'lessons')
            return array(
                self::TYPE_EDUCATION => 'Занятия',  
            );
        return array();
    }

    public function getGroups() {
        return CHtml::listData(TrGroup::model()->findAll(),'id','name');
    }

    public function rules() {
        return array_merge(
            parent::rules(),
            array(
                array('type, source, source_id, client_id, group_id, center_id, teacher_id, partner_id, city_id', 'safe'),
            )
        );
    }
    
    protected function beforeValidate() {
        if (parent::beforeValidate()) {
            foreach (array('client_id','teacher_id','group_id','center_id','partner_id') as $currKey) {
                if (is_array($this->$currKey) && count($arr = $this->$currKey) == 1 && $arr[0] == 0) $this->$currKey = 0;
            }
            return true;
        }
    }
    
    protected function afterValidate() {
        //if ($this->source == self::SOURCE_GROUP && !$this->group_id) $this->source = self::SOURCE_CENTER;
        //if ($this->source == self::SOURCE_CENTER && !$this->center_id) $this->source = self::SOURCE_PARTNER;
        switch ($this->source) {
            case self::SOURCE_CENTER: $this->excludeType('partner'); break;
            case self::SOURCE_GROUP:
            case self::SOURCE_CLIENT:
            case self::SOURCE_TEACHER: $this->excludeType('partner'); $this->excludeType('center'); break;
        }
        foreach (array('client','teacher','group','center','partner') as $currKey) {
            $formKey = $currKey.'_id';
            if (!empty($this->$formKey)) $this->setIdSelector($currKey,$this->$formKey ? $this->$formKey : array());
        }
        return parent::afterValidate();
    }

}

?>