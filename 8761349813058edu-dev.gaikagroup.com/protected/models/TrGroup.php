<?php

class TrGroup extends CActiveRecord {
	
	private $_clientIDs;
	
	protected $center_name;
	protected $course_name;
	protected $teacher_name;
	
	protected $_debtByClientId = array();
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{tr_group}}';
    }
	
	public function getClassName() {
		return 'Группа';
	}
    
    public function rules() {
        return array(
            array('tr_center_id, tr_course_id', 'required'),
			array('tr_center_id, tr_course_id', 'numerical', 'integerOnly' => true, 'min' => 1),
			array('teacher_id', 'numerical', 'integerOnly' => true, 'min' => 1, 'allowEmpty' => true),
			array('tr_center_id', 'exist', 'attributeName' => 'id', 'className' => 'TrCenter'),
			array('tr_course_id', 'exist', 'attributeName' => 'id', 'className' => 'TrCourse'),
			array('teacher_id', 'exist', 'attributeName' => 'id', 'className' => 'UserMain', 'criteria' => array('condition' => 'is_teacher = 1'), 'allowEmpty' => true),
			array('name', 'length', 'max' => 255),
			array('center_name', 'safe'),
        );
    }
	
	public function behaviors() {
		return array(
			'datetimeLimit' => array('class' => 'application.components.DatetimeLimitBehavior', 'datetimeAttribute' => 't.datetime'),
			'financeData' => array('class' => 'application.components.FinanceDataBehavior'),
			'softDelete' => array('class' => 'application.components.SoftDeleteBehavior', 'children' => array('lessons', 'costs'), 'parents' => array('center','course')),
		);
	}
    
    public function relations() {
		$lessonsCondition = 'datetime_start <= '.time(); 
        return array(
			// schedule
            'user' 			=> array(self::BELONGS_TO, 'UserMain', 'user_id'),
            'teacher' 		=> array(self::BELONGS_TO, 'UserMain', 'teacher_id'),
			'center' 		=> array(self::BELONGS_TO, 'TrCenter', 'tr_center_id'),
            'course' 		=> array(self::BELONGS_TO, 'TrCourse', 'tr_course_id'),
			'members' 		=> array(self::MANY_MANY, 'Client', Yii::app()->db->tablePrefix.'rel_group_member(tr_group_id,client_id)'),
			'lessons' 		=> array(self::HAS_MANY, 'Schedule', 'tr_group_id'),
			'first_lesson' 	=> array(self::HAS_ONE, 'Schedule', 'tr_group_id', 'order' => 'datetime_start ASC'),
			'lessons_success' => array(self::STAT, 'Schedule', 'tr_group_id', 'condition' => 'is_conducted = '.Schedule::CONDUCT_OK.' AND '.$lessonsCondition),
			'lessons_fail' 	=> array(self::STAT, 'Schedule', 'tr_group_id', 'condition' => '(is_conducted = '.Schedule::CONDUCT_MOVED.' OR is_conducted = '.Schedule::CONDUCT_CANCELED.') AND '.$lessonsCondition),
			'member_count' 	=> array(self::STAT, 'Client', Yii::app()->db->tablePrefix.'rel_group_member(tr_group_id,client_id)'),
			'costs'		  	=> array(self::HAS_MANY, 'TrCosts', 'group_id'),
			'countDebt'   	=> array(self::STAT, 'TrClientLog', 'tr_group_id', 'select' => 'COUNT(DISTINCT client_id)', 'condition' => 'debt != 0'),
			'summaryDebt' 	=> array(self::STAT, 'TrClientLog', 'tr_group_id', 'select' => 'SUM(debt)'),
        );
    }
    
    public function attributeLabels() {
        return array(
            'id' => 'Группа',
			'name' => 'Название',
			'tr_center_id' => 'Центр',
            'tr_course_id' => 'Предмет',
			'user_id' => 'Пользователь',
			'teacher_id' => 'Преподаватель',
			'create_date' => 'Дата создания',
			'update_date' => 'Дата обновления',
			/* ************** */
			'course_name' => 'Предмет',
			'center_name' => 'Центр',
			'teacher_name' => 'Преподаватель',
			/* ************** */
			'conducted_success' => 'Кол-во проведенных занятий',
			'conducted_fail' => 'Форс-мажор',
			'lessons_success' => 'Кол-во проведённых уроков',
			'member_count' => 'Кол-во учащихся',
			'debt' => 'Списано',
			'course_contract_sum' => 'Договор',
			'first_lesson_date' => 'Начало занятий',
        );
    }
	
	protected function beforeValidate() {
		if (parent::beforeValidate()) {
			if (!$this->teacher_id) $this->teacher_id = null;
			return true;
		}
	}
	
	public function defaultScope() {
		return array(
			'condition' => Yii::app()->user->getProjectIdCondition('project_id',$this->getTableAlias(false,false)),	
		);
	}
    
    protected function beforeSave() {
        if (!parent::beforeSave()) return false;
        if ($this->isNewRecord) {
            $this->create_date = time();
            $this->update_date = $this->create_date;
			$this->user_id = Yii::app()->user->model->id;
			$isTeacherChanged = true;
			$oldTeacherId = 0;
        } else {
            $this->update_date = time();
			$oldModel = $this->findByPk($this->id);
			$isTeacherChanged = $oldModel->teacher_id != $this->teacher_id;
			$oldTeacherId = $oldModel->teacher_id;
        }
		
		if (!$this->name) $this->name = 'Группа №'.$this->id;
		
		if ($isTeacherChanged && $this->teacher_id) {
			if ($oldTeacherId) {
				if (TrGroup::model()->count('teacher_id = '.$oldTeacherId.' AND tr_course_id = '.$this->tr_course_id) == 1) {
					CourseRates::model()->deleteAllByAttributes(array('teacher_id' => $oldTeacherId, 'group_id' => $this->id), 'rate = 0');
					CourseRates::model()->updateAll(array('in_archive' => 1), 'teacher_id = '.$oldTeacherId.' AND group_id = '.$this->id);
				}
			}
			if ($courseRate = CourseRates::model()->resetScope()->findByAttributes(array('teacher_id' => $this->teacher_id, 'group_id' => $this->id))) {
				if ($courseRate->in_archive) {
					$courseRate->in_archive = 0;
					$courseRate->save();
				}
			} else {
				$courseRate = new CourseRates;
				$courseRate->teacher_id = $this->teacher_id;
				$courseRate->course_id = $this->tr_course_id;
				$courseRate->group_id = $this->id;
				$courseRate->save(false);
			}
		}
        
        return true;
    }
	
	protected function beforeDelete() {
		if (parent::beforeDelete()) {
			if ($this->lessons) foreach ($this->lessons as $currentObject) $currentObject->delete();
			$this->deleteRelations('member');
			$this->deleteRelations('schedule');
			return true;
		}
	}
	
	public function search() {
			
		$criteria = new CDbCriteria;
		$criteria->select = 'id, name, tr_center_id, tr_course_id, teacher_id, create_date';
		$criteria->compare('t.name', $this->name, true);
		
		if ($this->tr_center_id) {
			$criteria->compare('tr_center_id', $this->tr_center_id);
		}
		if ($this->tr_course_id) {
			$criteria->compare('tr_course_id', $this->tr_course_id);
		}
		
		$criteria->with = array('center', 'course', 'teacher', 'member_count', 'user');
		$criteria->compare('center.project_id',Yii::app()->user->projectIdCompare);
		
		$criteria->mergeWith($this->dbCriteria);

		return new CActiveDataProvider(__CLASS__, array('criteria' => $criteria, 'pagination' => array('pageSize' => 40)));
	
	}
	
	public function storeRelations($relName,$data) {
		
		switch ($relName) {
			case 'member':
				$idField = 'client_id';
				break;
			case 'schedule':
				$idField = 'schedule_id';
				break;
			default:
				return false;
		}
		
		$this->deleteRelations($relName);
		
		$connection = Yii::app()->db;
		
		$tableName = $connection->tablePrefix.'rel_group_'.$relName;
		$groupId = $this->id;
		
		$sql = "INSERT INTO $tableName (tr_group_id, $idField) VALUES (:groupId, :elemId)";

		foreach ($data as $currentElem) {
			
			$currentElem = (int)$currentElem;
			
			$command = $connection->createCommand($sql);
			$command = $command->bindParam(":groupId", $groupId, PDO::PARAM_STR);
			$command = $command->bindParam(":elemId", $currentElem, PDO::PARAM_STR);
			$command->execute();
		
		}
		
		return true;
		
	}
	
	public function deleteRelations($relName) {
		
		if ($relName != 'member' && $relName != 'schedule') return false;
		
		$connection = Yii::app()->db;
		$tableName = $connection->tablePrefix.'rel_group_'.$relName;
		$groupId = $this->id;
		
		$sql = "DELETE FROM $tableName WHERE tr_group_id = :groupId";
		$command = $connection->createCommand($sql);
		$command = $command->bindParam(":groupId", $groupId, PDO::PARAM_STR);
		$command->execute();
		
		return true;
		
	}
	
	public function getClientList($clientObj, $exceptions = array()) {
		$criteria = new CDbCriteria;
		$criteria->select = 'id, fullname';
		$criteria->compare('status', array(Client::STATUS_CONCLUDE,Client::STATUS_STUDY));
		$criteria->compare('fullname', $clientObj->fullname, true);
		return new CActiveDataProvider('Client', array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => 20),
		));
	}
	
	public function getTrCourseList() {
		$criteria = new CDbCriteria;
		$criteria->select = 'id, name, description';
		$criteria->order = 'name ASC';
		$criteria->compare('project_id',Yii::app()->user->projectIdCompare);

		$result = array();
		if ($arr = TrCourse::model()->findAll($criteria)) {
			foreach ($arr as $currObj) {
				$result[$currObj->id] = Yii::app()->user->model->is_owner ? $currObj->name . " [{$currObj->description}]" : $currObj->name;
			}
		}

		return $result;
	}
	
	public function getTeacherList() {
		$criteria = new CDbCriteria;
		$criteria->select = 'id, fullname';
		$criteria->order = 'fullname ASC';
		$criteria->compare('is_teacher', 1);
		$criteria->compare('project_id', Yii::app()->user->projectIdCompare);

		$result = array();
		if ($arr = UserMain::model()->findAll($criteria)) {
			foreach ($arr as $currObj) {
				$result[$currObj->id] = $currObj->fullname;
			}
		}
		
		return $result;
	}
	
	public function getTeacher_name() {
		if ($this->teacher_name === null)
			$this->teacher_name = $this->teacher ? $this->teacher->fullname : '';
		return $this->teacher_name;
	}
	
	public function getCenter_name() {
		if ($this->center_name === null)
			$this->center_name = $this->center ? $this->center->name : '';
		return $this->center_name;
	}
	
	public function getCourse_name() {
		if ($this->course_name === null)
			$this->course_name = $this->course ? $this->course->name : '';
		return $this->course_name;
	}
	
	private function getClientIDs() {
		if ($this->_clientIDs === null) {
			$this->_clientIDs = Yii::app()->db->createCommand()
				->select('client_id')
				->from(Yii::app()->db->tablePrefix.'rel_group_member')
				->where('tr_group_id = '.$this->id)
				->queryColumn();
		}
		return $this->_clientIDs;
	}
	
	public function getClientFinance() {
        $idSelector = $this->getClientIDs();
		$criteria = new CDbCriteria;
		$criteria->addInCondition('id',$idSelector);
		$model = CActiveRecord::model('Client');
		$model->datetimeLimit = $this->datetimeFilter;
		return new CActiveDataProvider($model,array('criteria' => $criteria, 'pagination' => false));
	}
	
	public function getPaidSum($clients = array()) { // DEBET, clients
		if (!$clients) {
			$clients = $this->getClientIDs();
		}
		$where = array('and', array('and', 'course_id = '.$this->tr_course_id, 'hours > 0'), array('in', 'client_id', $clients));
		$where = $this->addDatetimeCondition($where);
		$paidCount = Yii::app()->db->createCommand()
            ->select('SUM(value)')
            ->from(Yii::app()->db->tablePrefix.'tr_income t')
            ->where($where)
            ->queryScalar();
		return (int)$paidCount;
	}
	
	public function getPaidSumStat() {
		$clients = $this->getClientIDs();
		$where = array('and', array('and', 'course_id = '.$this->tr_course_id, 'hours > 0'), array('in', 'client_id', $clients));
		$where = $this->addDatetimeCondition($where);
		$data = Yii::app()->db->createCommand()
            ->select('value, datetime')
            ->from(Yii::app()->db->tablePrefix.'tr_income t')
            ->where($where)
            ->queryAll();
		$result = array();
		for ($i = 0; $i <= 11; $i++) $result[$i] = 0;
		$currYear = (int)date('Y');
		foreach ($data as $currentRow) {
			if ($currYear != (int)date('Y',$currentRow['datetime'])) continue;
			$result[(int)date('m',$currentRow['datetime'])-1] += $currentRow['value'];
		}
		return $result;
	}
	
	public function getLessonsSum($clients = array()) { // CREDIT, client contracts
		
		if (!isset($this->tr_course_id) || $this->tr_course_id === null || !isset($this->id) || $this->id === null) throw new CException();
		if (!$clients) $clients = $this->getClientIDs();
		
		$where = array('and',
						array('and',
							array('and', 'income.course_id = '.$this->tr_course_id, 't.tr_group_id = '.$this->id),
							array('in', 'clog.client_id', $clients)
						),
						array('and', 'income.hours < 0', 'income.datetime = t.datetime_start')
				   );
		$where = $this->addDatetimeCondition($where,'t.datetime_start');
		$data = Yii::app()->db->createCommand()
			->select('t.datetime_start AS datetime, income.value AS price')
			->from(Yii::app()->db->tablePrefix.'schedule t')
			->join(Yii::app()->db->tablePrefix.'tr_client_log clog', 'clog.tr_lesson_id = t.id')
			->join(Yii::app()->db->tablePrefix.'tr_income income', 'income.client_id = clog.client_id')
			->where($where)
			->queryAll();
			
		$result = 0;
		$currYear = (int)date('Y');
		foreach ($data as $currentRow) {
			$result += abs($currentRow['price']);
		}
		return $result;
		
	}
	
	public function getLessonsSumStat() {
		if (!isset($this->tr_course_id) || $this->tr_course_id === null || !isset($this->id) || $this->id === null) throw new CException();
		$clients = $this->getClientIDs();

		$where = array('and',
						array('and',
							array('and', 'income.course_id = '.$this->tr_course_id, 't.tr_group_id = '.$this->id),
							array('in', 'clog.client_id', $clients)
						),
						array('and', 'income.hours < 0', 'income.datetime = t.datetime_start')
				   );
		$where = $this->addDatetimeCondition($where,'t.datetime_start');
		$data = Yii::app()->db->createCommand()
			->select('t.datetime_start AS datetime, income.value AS price')
			->from(Yii::app()->db->tablePrefix.'schedule t')
			->join(Yii::app()->db->tablePrefix.'tr_client_log clog', 'clog.tr_lesson_id = t.id')
			->join(Yii::app()->db->tablePrefix.'tr_income income', 'income.client_id = clog.client_id')
			->where($where)
			->queryAll();
			
		$result = array();
		for ($i = 0; $i <= 11; $i++) $result[$i] = 0;
		$currYear = (int)date('Y');
		foreach ($data as $currentRow) {
			if ($currYear != (int)date('Y',$currentRow['datetime'])) continue;
			$result[(int)date('m',$currentRow['datetime'])-1] += $currentRow['price'];
		}
		return $result;
	}
	
	public function getFinanceIncomeData($idSelector = array()) {
		$result = new CAttributeCollection(array('income_debet' => 0, 'income_credit' => 0, 'income_total' => 0));
		if ($idSelector !== null) {
			$result->income_debet = $this->getPaidSum($idSelector); // клиенты заплатили
			$result->income_credit= $this->getLessonsSum($idSelector); //
			$result->income_total = $result->income_debet - $result->income_credit;
		}
		return $result;
	}
	
	public function getFinanceIncomeDataStat() {
		$result = new CAttributeCollection(array(
			'income_debet' => array(),
			'income_credit' => array(),
			'income_total' => array(),
		));
		$result->income_debet = $this->getPaidSumStat();
		$result->income_credit= $this->getLessonsSumStat();
		$incomeTotal = array();
		for ($i = 0; $i <= 11; $i++) {
			$incomeTotal[$i] = $result->income_debet[$i] - $result->income_credit[$i];
		}
		$result->income_total = $incomeTotal;
		return $result;
	}
	
	public function getFinanceCostsData($idSelector = array()) {
        $data = (int)Yii::app()->db->createCommand()
            ->select('SUM(costs.value) AS sum')
            ->from(Yii::app()->db->tablePrefix.'tr_group t')
            ->join(Yii::app()->db->tablePrefix.'tr_costs costs','costs.group_id = t.id')
            ->where($this->addDatetimeCondition('t.id = '.$this->id,'costs.datetime'))
            ->queryScalar();
		return new CAttributeCollection(array('costs' => $data));
	}
	
	public function getFinanceCostsDataStat() {
        $data = Yii::app()->db->createCommand()
            ->select('FROM_UNIXTIME(costs.datetime,\'%m\') AS month, SUM(costs.value) AS sum')
            ->from(Yii::app()->db->tablePrefix.'tr_group t')
            ->join(Yii::app()->db->tablePrefix.'tr_costs costs','costs.group_id = t.id')
            ->where($this->addDatetimeCondition('t.id = '.$this->id,'costs.datetime'))
            ->group('month')
            ->queryAll();
			
		$result = array(); for ($i = 0; $i <= 11; $i++) $result[$i] = 0;
		foreach ($data as $currentSet) {
			if (!($currentSet['month'] = (int)$currentSet['month'])) continue;
			$result[$currentSet['month']-1] = $currentSet['sum'];
		}
		return new CAttributeCollection(array('costs' => $result));
	}
	
	public function getTeacherRate() {
		$teacherRate = Yii::app()->db->createCommand()
            ->select('rate')
            ->from('{{course_rates}}')
            ->where(array('and',
					'group_id = '.$this->id,
					'teacher_id = '.$this->teacher_id
			))
            ->queryScalar();
		return (int)$teacherRate;
	}

	public function getRateID() {
		$teacherRate = Yii::app()->db->createCommand()
            ->select('id')
            ->from('{{course_rates}}')
            ->where(array('and',
					'group_id = '.$this->id,
					'teacher_id = '.$this->teacher_id
			))
            ->queryScalar();
		return (int)$teacherRate;
	}
	
	public function getIncome() {
		$financeData = $this->getFinanceIncomeData();
		return $financeData->income_debet - $financeData->income_credit;
	}

	public static function getByCenterId($center_id) {
        if ($center_id) {
            /* @var $c CDbCommand */
            $c = Yii::app()->db->createCommand();
            $c->select('group.id, group.name');
            $c->from(Yii::app()->db->tablePrefix.'tr_group group');
            $c->leftJoin(Yii::app()->db->tablePrefix.'tr_center center', 'center.id = group.tr_center_id');

            if (is_array($center_id))
                $c->where('center.id IN ('.implode(', ', $center_id).') AND group.is_deleted = 0 AND center.is_deleted = 0');
            else
                $c->where('center.id = '.$center_id.' AND group.is_deleted = 0 AND center.is_deleted = 0');


            $data = json_decode(json_encode($c->queryAll()));
        } else {
            $data = TrGroup::model()->findAll();
        }

        return $data;
	}
	
	public function getTrCenterList() {
		$criteria = new CDbCriteria;
		$criteria->select = 'id, name';
		$criteria->compare('project_id',Yii::app()->user->projectIdCompare);

		$result = array();
		if ($arr = TrCenter::model()->findAll($criteria)) {
			foreach ($arr as $currObj) $result[$currObj->id] = $currObj->name;
		}
		
		return $result;
	}
	
	public function getDebtByClientId($clientId) {
		if (!$clientId) return null;
		if (isset($this->_debtByClientId[$clientId])) return $this->_debtByClientId[$clientId];
		return $this->_debtByClientId[$clientId] = $this->getRelated('summaryDebt',true,array('condition' => 'client_id = '.$clientId));
	}
	
	public function getDebtForGridView() {
		$label = Yii::t("general","{n} должник|{n} должника|{n} должников",$this->countDebt);
		if ($this->countDebt) {
			return CHtml::link($label,array('trgroup/clients','id' => $this->id));
		} else {
			return $label;
		}
	}
	
	public function getContractSumByClientId($clientId) {
		if (!$clientId) return null;
		if (!$model = Client::model()->findByPk($clientId)) return null;
		$criteria = new CDbCriteria;
		$criteria->compare('client_id',$clientId);
		/*
		$currDate = Yii::app()->dateFormatter->format('yyyy-MM-dd',time());
		$criteria->addCondition('start_date < '.$currDate);
		$criteria->addCondition('end_date > '.$currDate);
		*/
		$criteria->compare('status',array(Contract::STATUS_CONCLUDED,Contract::STATUS_PAID));
		$criteria->with = array('courses' => array('select' => 'period_price', 'joinType' => 'INNER JOIN', 'condition' => 'courses.course_id = '.$this->tr_course_id));
		$data = Contract::model()->findAll($criteria);
		if (!$data) return null;
		$sum = 0;
		foreach ($data as $currContract) {
			$sum += $currContract->courses[0]->period_price;
		}
		return $sum;
	}

    public function getFirstLessonDate() {
        if (!$this->first_lesson)
            return 'Нет';

        return Yii::app()->dateFormatter->format("dd MMM yyyy", $this->first_lesson->datetime_start);
    }

    public function getNameWithCourse() {
        return "$this->name [{$this->course->name}]";
    }
    
}



?>