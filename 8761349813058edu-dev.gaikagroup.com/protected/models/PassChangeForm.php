<?php

class PassChangeForm extends CFormModel {
    
    private $_identity;
    
    public $password_old;
    public $password_new;
    public $password_retype;
    
    public function rules() {
        return array(
            array('password_old, password_new, password_retype', 'required'),
            array('password_old', 'length', 'max'=>255),
            array('password_new, password_retype', 'length', 'min'=>6, 'max'=>30),
            array('password_retype', 'compare', 'compareAttribute' => 'password_new'),
        );
    }
    
    public function attributeLabels() {
        return array(
            'password_old' => 'Текущий пароль',
            'password_new' => 'Новый пароль',
            'password_retype' => 'Новый пароль (еще раз)',
        );
    }
    
    public function passchange() {
	if(!$this->hasErrors()) {
            $username = Yii::app()->user->model->email;
	    $this->_identity = new UserIdentity($username,$this->password_old);
	    if(!$this->_identity->authenticate()) {
		$this->addError('password_old','Неправильно введен текущий пароль.');
	    } else {
                Yii::app()->user->model->setPassword('restore', $this->password_new);
                return true;
            }
	}
        return false;
    }
    
}

?>