<?php

class AdvertDocument extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{advert_documents}}';
    }
    
    public function relations() {
        return array(
            'advert' => array(self::BELONGS_TO,'Advert','advert_id'),
        );
    }
    
    public function upload($document,$advertId) {
        $this->filename = $this->generateRandomSequence(8).'_'.date('YmdHis').substr($document->getName(),strrpos($document->getName(),'.'));
        if ($document->saveAs($this->filepath)) {
            $this->filesize = filesize($this->filepath);
            $this->advert_id = $advertId;
            return $this->save();
        }
        return false;
    }
    
    public function getFilepath() {
        return Yii::getPathOfAlias('webroot').'/'.Yii::app()->params['uploadDirectory'].'/advert/'.$this->filename;
    }
    
    public function getViewpath() {
        return '../'.Yii::app()->params['uploadDirectory'].'/advert/'.$this->filename;
    }
    
    public function unlink() {
        if (file_exists($this->filepath)) unlink($this->filepath);
        return $this->delete();
    }
    
    private function generateRandomSequence($length=10) {
        $seq = '';
        $rand_table = array('a','b','c','d','e','f','g','h','i','g','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9');
        for ($i = 0; $i < $length; $i++) {
            $seq .= $rand_table[rand(0,35)];
        }
        return $seq;
    }


}

?>