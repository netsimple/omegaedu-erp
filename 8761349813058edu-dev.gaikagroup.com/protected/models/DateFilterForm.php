<?php

class DateFilterForm extends CFormModel {

    public $datefrom;
    public $dateto;
    
    public function rules() {
        return array(
            array('datefrom', 'date', 'format' => 'd MMM yyyy', 'timestampAttribute' => 'datefrom', 'allowEmpty' => true),
            array('dateto', 'date', 'format' => 'd MMM yyyy', 'timestampAttribute' => 'dateto', 'allowEmpty' => true),
        );
    }
    
    public function behaviors() {
        return array(
            'formatDates' => array('class' => 'application.components.FormatDatesBehavior', 'attributes' => array('datefrom', 'dateto')),
        );
    }
    
    protected function afterValidate() {
        if (!empty($this->dateto) && !empty($this->datefrom) && $this->dateto < $this->datefrom) {
            list($this->dateto, $this->datefrom) = array($this->datefrom, $this->dateto);
        }
        return parent::afterValidate();
    }

}

?>