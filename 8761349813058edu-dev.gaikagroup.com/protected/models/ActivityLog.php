<?php

class ActivityLog extends CActiveRecord {
    
    public $username;
    public $timestamp;
    
    protected $_object;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{activity_log}}';
    }
    
    public function rules() {
        return array(
            array('username, datetime, class_name, object_id, action, comment', 'safe', 'on' => 'search'),  
        );
    }
    
    public function relations() {
        return array(
            'user' => array(self::BELONGS_TO,'UserMain','user_id'),
        );
    }
    
	public function defaultScope() {
		return array(
            'with' => array('user' => array('joinType' => 'INNER JOIN', 'condition' => Yii::app()->user->getProjectIdCondition('project_id','user'))),
		);
	}
    
    public function getObject() {
        if (!empty($this->_object)) return $this->_object;
        if ($this->class_name && $this->object_id) {
            $modelInstance = CActiveRecord::model($this->class_name);
            if (!($this->_object = $modelInstance->findByPk($this->object_id))) {
                $behaviors = array_keys($modelInstance->behaviors());
                if (in_array('softDelete',$behaviors)) $this->_object = $modelInstance->getArchiveObject($this->object_id);
            }
        } else {
            $this->_object = null;
        }
        return $this->_object;
    }
    
    public function attributeLabels() {
        return array(
            'user_id' => 'Пользователь',
            'username' => 'Пользователь',
            'datetime'=> 'Дата',
            'class_name' =>'Тип',
            'object_id'=>'Элемент',
            'action'  => 'Действие',
            'comment' => 'Комментарий',
        );
    }
    
    public function getActionNames() {
        return array(
            'update' => 'Обновление данных',
            'create' => 'Добавление данных',
            'delete' => 'Перемещение в архив',
            'restore'=> 'Восстановление из архива',
            'uploadDocuments' => 'Загрузка документов',
            'avatar' => 'Загрузка аватара',
            'generate'=>'Генерация купонов',
            'crop'   => 'Модификация аватара',
            'comment'=> 'Добавление комментария',
            'balance'=> 'Пополнение баланса',
            'complete'=>'Обработка заявки',
            'addCosts'  => 'Добавление позиции',
        );
    }
    
    public function getClassNames() {
        $classes = array();
        $c = new CDbCriteria;
        $c->select = 'DISTINCT class_name AS class_name';
        $data = CActiveRecord::model(__CLASS__)->findAll($c);
        foreach ($data as $currObj) {
            $model = CActiveRecord::model($currObj->class_name);
            if ($className = $this->getClassName($model)) $classes[$currObj->class_name] = $className;
        }
        return $classes;
    }
    
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->user_id = Yii::app()->user->id;
            $this->datetime= time();
            return true;
        }
    }
    
    public static function add($object,$actionId,$actionParams=array()) {
        if ($object === null || !isset($object->id) || !$object->id) return null;
        $model = new ActivityLog;
        $model->class_name = get_class($object);
        $model->object_id  = $object->id;
        if (!in_array($actionId,array('create','update','generate','balance','comment','delete','restore','uploadDocuments','avatar','crop','complete','addCosts'))) return null;
        switch ($actionId) {
            case 'update':
                if (isset($actionParams['attributeName'])) {
                    $model->comment = 'Поле "'.$object->getAttributeLabel($actionParams['attributeName']).'"';
                }
                break;
            case 'generate':
                if (isset($actionParams['count'])) {
                    $model->comment = $actionParams['count'].' купонов';
                }
                break;
            case 'balance':
                $model->comment = $object->value;
                break;
        }
        if (!$model->comment && method_exists($object,'getInfo')) {
            $model->comment = $object->info;
        }
        $model->action = $actionId;
        $model->save();
        return $model;
    }
    
    public function search() {
        $criteria = new CDbCriteria;
        if ($this->username) {
            $criteria->with = array('user');
            $criteria->compare('user.fullname',$this->username,true);
        }
		if ($this->datetime && $this->timestamp = CDateTimeParser::parse($this->datetime,'d MMM yyyy',array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0))) {
            $criteria->addBetweenCondition('datetime',$this->timestamp,$this->timestamp+86400);
        }
        $criteria->compare('class_name',$this->class_name);
        $criteria->compare('action',$this->action);
        if ($this->object_id) {
            $ids = array();
            $data = CActiveRecord::model(__CLASS__)->findAll($criteria);
            foreach ($data as $currRecord) {
                if ($currRecord->object_id && ($model = CActiveRecord::model($currRecord->class_name)->findByPk($currRecord->object_id)) && ($objectName = $this->getObjectName($model))) {
                    if (stripos($objectName,$this->object_id) !== false) array_push($ids,$currRecord->id);
                }
            }
            $criteria->compare('id',$ids);
        }
        return new CActiveDataProvider(__CLASS__,array('criteria' => $criteria, 'sort' => array('defaultOrder' => 'datetime DESC'), 'pagination' => array('pageSize' => 40)));
    }
    
    public function getClassName($object = null) {
        if ($object === null) $object = $this->getObject();
        if (!$object) $object = CActiveRecord::model($this->class_name);
        if ($object && method_exists($object,'getClassName')) return $object->getClassName();
        return $this->class_name;
    }
    
    public function getObjectName($object = null) {
        $name = null;
        if ($object === null) $object = $this->getObject();
        if ($object) {
            if (method_exists($object,'getName')) {
                $name = $object->getName();
            } elseif (isset($object->name)) {
                $name = $object->name;
            }
        }
        if ($name === null) {
            $name = 'ID #'.$this->object_id;
        }
        return $name;
    }
    
    public function getActionName(){
        $actionNames = $this->actionNames;
        return isset($actionNames[$this->action]) ? $actionNames[$this->action] : $this->action;
    }
    
}

?>