<?php

class Advert extends CActiveRecord {
	
	const TYPE_NONE = 0;
	const TYPE_LEAFLETING = 1;
	const TYPE_PRESENTATION=2;
	const TYPE_BOTH = 3;
    
    public $username;
	public $activities = array();

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{advert}}';
    }
    
	public function getClassName() {
		return 'Рекламный отчёт';
	}
    
    public function relations() {
        return array(
            'documents' => array(self::HAS_MANY,'AdvertDocument','advert_id'),
            'user' => array(self::BELONGS_TO,'UserMain','user_id'),
        );
    }
    
    public function rules() {
        return array(
            array('title', 'required'),
            array('title', 'length', 'max' => 255),
            array('description', 'length', 'max' => 2000),
            array('member_count', 'numerical', 'integerOnly' => true, 'min' => 0),
            array('activity_type', 'in', 'range' => array(self::TYPE_NONE, self::TYPE_LEAFLETING, self::TYPE_PRESENTATION, self::TYPE_BOTH)),
            array('date', 'date', 'format' => 'yyyy-MM-dd', 'on' => 'ajaxUpdate'),
            array('date', 'DateValidator', 'format' => 'd MMM yyyy', 'outputFormat' => 'yyyy-MM-dd', 'except' => 'ajaxUpdate'),
            array('documents', 'file', 'types' => 'jpg,jpeg,gif,png', 'maxFiles' => 100, 'maxSize' => 10 * 1024 * 1024, 'allowEmpty' => true),
			array('activities','safe'),
            array('title,member_count,activity_type,date,username', 'safe', 'on'=>'search'),
        );
    }
    
    public function behaviors() {
        return array(
            'formatDates' => array('class' => 'application.components.FormatDatesBehavior', 'attributes' => 'date'),
			'softDelete' => array('class' => 'application.components.SoftDeleteBehavior'),
        );
    }
    
    public function attributeLabels() {
        return array(
            'id' => 'Рекламный отчёт',
            'title' => 'Название',
            'description' => 'Описание',
            'member_count' => 'Кол-во участников',
            'activity_type' => 'Тип мероприятия',
            'activities' => 'Тип мероприятия',
            'date' => 'Дата проведения',
            'documents' => 'Фотографии',
            'user_id' => 'Рекламный агент',
            'username' => 'Рекламный агент',
            'create_date' => 'Дата создания',
            'update_date' => 'Дата обновления',
        );
    }
    
	public function defaultScope() {
		return array(
			'condition' => Yii::app()->user->getProjectIdCondition('project_id',$this->getTableAlias(false,false)),	
		);
	}

    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('title',$this->title,true);
		if ($this->activities) $this->activity_type = array($this->activities,self::TYPE_BOTH);
		$criteria->compare('activity_type',$this->activity_type);
        $criteria->compare('member_count',$this->member_count);
        $criteria->compare('user_id',$this->user_id);
        if ($this->date) {
            $criteria->compare('date',Yii::app()->dateFormatter->format('yyyy-MM-dd',$this->date));
        }
        if ($this->username) {
            $criteria->compare('user.fullname',$this->username,true);
            $criteria->with = array('user');
        }
        return new CActiveDataProvider(__CLASS__, array('criteria' => $criteria, 'pagination' => array('pageSize' => 40)));
    }
    
    protected function afterConstruct() {
        $this->user_id = Yii::app()->user->id;
        parent::afterConstruct();
    }
	
	protected function afterFind() {
		switch ($this->activity_type) {
			case self::TYPE_LEAFLETING:
			case self::TYPE_PRESENTATION:
				$this->activities = array($this->activity_type);
				break;
			case self::TYPE_BOTH:
				$this->activities = array(self::TYPE_LEAFLETING,self::TYPE_PRESENTATION);
				break;
		}
		return parent::afterFind();
	}
	
	protected function beforeValidate() {
		if (parent::beforeValidate()) {
			if (!is_array($this->activities)) return false;
			$this->activity_type = count($this->activities) == 1 ? $this->activities[0] : (count($this->activities) == 2 ? self::TYPE_BOTH : self::TYPE_NONE);
			return true;
		}
	}
    
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->update_date = time();
            if ($this->isNewRecord) {
                $this->create_date = $this->update_date;
                $this->project_id = Yii::app()->user->projectId;
                $this->user_id = Yii::app()->user->id;
            }
        }
        return true;
    }
    
    public function getActivityType($htmlStyle = false) {
		$labels = array(
			self::TYPE_LEAFLETING => 'Раздача листовок',
			self::TYPE_PRESENTATION=>'Презентация',
		);
		if (!$htmlStyle) return $labels;
		foreach ($labels as $currKey => $currLabel) if (!in_array($currKey,$this->activities)) unset($labels[$currKey]);
		return implode('<br>',$labels);
    }
	
	public function getName() { return $this->title; }
}

?>