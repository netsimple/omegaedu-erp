<?php

class TrCourse extends CActiveRecord {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{tr_course}}';
    }
	
	public function getClassName() {
		return 'Курс';
	}
    
    public function rules() {
        return array(
            array('name, client_lvl', 'required'),
			array('client_lvl', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 1),
			array('hours', 'numerical', 'min' => 0),
            array('start_date', 'date', 'format' => 'yyyy-MM-dd', 'on' => 'ajaxUpdate'),
            array('start_date', 'DateValidator', 'format' => 'd MMM yyyy', 'outputFormat' => 'yyyy-MM-dd', 'except' => 'ajaxUpdate'),
            array('name, description', 'length', 'max' => 255),
			array('name, description, hours, start_date, center_name', 'safe', 'on' => 'search'),
        );
    }
    
    public function relations() {
        return array(
            'user' => array(self::BELONGS_TO, 'UserMain', 'user_id'),
			'groups' => array(self::HAS_MANY, 'TrGroup', 'tr_course_id'),
			'teacherRates' => array(self::HAS_MANY, 'CourseRates', 'course_id'),
			'contracts' => array(self::HAS_MANY, 'ContractCourse', 'course_id'),
			'partner' => array(self::BELONGS_TO, 'UserPartner', 'project_id'),
        );
    }
	
    public function behaviors() {
        return array(
            'formatDates' => array('class' => 'application.components.FormatDatesBehavior', 'attributes' => 'start_date'),
			'softDelete' => array('class' => 'application.components.SoftDeleteBehavior', 'children' => array('groups')),
        );
    }
    
    public function attributeLabels() {
        return array(
            'id' => 'Предмет',
			'user_id' => 'Пользователь',
			'name' => 'Название',
			'description' => 'Описание',
			'client_lvl' => 'Уровень',
			'hours' => 'Кол-во часов',
			'start_date' => 'Дата начала преподавания',
			'create_date' => 'Дата создания',
			'update_date' => 'Дата обновления',
        );
    }
	
	public function defaultScope() {
		return array(
			'condition' => Yii::app()->user->getProjectIdCondition('project_id',$this->getTableAlias(false,false)),	
		);
	}
	
	public function centerFilter($centerId) {
		$this->dbCriteria->mergeWith(array(
			'with' => array('groups' => array('joinType' => 'INNER JOIN', 'select' => false, 'condition' => 'groups.tr_center_id = '.$centerId))
		));
		return $this;
	}
    
    protected function beforeSave() {
        if (!parent::beforeSave()) return false;
        if ($this->isNewRecord) {
            $this->create_date = time();
            $this->update_date = $this->create_date;
			$this->user_id = Yii::app()->user->id;
			$this->project_id = Yii::app()->user->projectId;
        } else {
            $this->update_date = time();
        }
        
        return true;
    }
	
	public function setTeacherId($userId) {
		// in database scheme: UNIQUE (tr_course_id, teacher_id)
		$criteria = new CDbCriteria;
		$criteria->select = 'tr_course_id';
		$criteria->compare('teacher_id',$userId);
		$ids = TrGroup::model()->findAll($criteria);
		foreach ($ids as $currKey => $currentScore) {
			$ids[$currKey] = $currentScore->tr_course_id;
		}
		$this->dbCriteria->addInCondition('t.id',$ids);
	}
	
	public function search() {
			
		$criteria = new CDbCriteria;
		$criteria->select = 'id, name, description, hours, start_date, client_lvl';
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('hours', $this->hours);
		
		$criteria->compare('client_lvl',$this->client_lvl);
		
		if ($this->start_date) {
			$criteria->compare('start_date',date('Y-m-d',strtotime($this->start_date)));
		}
		
		$criteria->compare('project_id',Yii::app()->user->projectIdCompare);
		
		$this->dbCriteria->mergeWith($criteria);

		return new CActiveDataProvider(__CLASS__, array('criteria' => $this->dbCriteria, 'pagination' => array('pageSize' => 40)));
	
	}
    
    public function getLevelList() {
        return array(
            0 => 'Базовый уровень',
            1 => 'Продвинутый уровень',
        );
    }
	
	public function getTrCenterList($allCenters = false) {
		$criteria = new CDbCriteria;
		$criteria->select = 'id, name';
		$criteria->compare('project_id',$this->project_id);

		$result = array();
		if ($arr = TrCenter::model()->findAll($criteria)) {
			foreach ($arr as $currObj) {
				$result[$currObj->id] = $currObj->name;
			}
		}
		
		return $result;
	}
	
	public function getTrCourseList() {
		$criteria = new CDbCriteria;
		$criteria->select = 'id, name, description';
		$criteria->order = 'name ASC';
		$criteria->compare('project_id',Yii::app()->user->projectIdCompare);

		$result = array();
		if ($arr = TrCourse::model()->findAll($criteria)) {
			foreach ($arr as $currObj) {
				$result[$currObj->id] = Yii::app()->user->model->is_owner ? $currObj->name . " [{$currObj->description}]" : $currObj->name;
			}
		}

		return $result;
	}

    public static function findAllByCenter($center_id) {
        $center_id = intval($center_id);
        if (!($center = TrCenter::model()->findByPk($center_id)))
            return array();

        $c = new CDbCriteria();
        $c->compare('project_id',$center->project_id);
        return TrCourse::model()->findAll($c);
	}
	
	public function getCanCreate() {
		return true;
	}
	
	private $_groupFinanceDP;
	
	public function getGroupFinanceDP($idSelector = array()) {
		if ($this->_groupFinanceDP === null) {
			$criteria = new CDbCriteria;
			$criteria->with = array('lessons_success');
			$criteria->select = 'id, name, tr_course_id';
			$criteria->compare('tr_course_id',$this->id);
			if ($idSelector) {
				$criteria->addInCondition('t.id',$idSelector);
			}
			$this->_groupFinanceDP = new CActiveDataProvider('TrGroup',array('criteria' => $criteria, 'pagination' => false));
		}
		return $this->_groupFinanceDP;
	}
	
	protected function beforeDelete() {
		if (parent::beforeDelete()) {
			if ($this->groups) foreach ($this->groups as $currentObject) $currentObject->delete();
			if ($this->teacherRates) foreach ($this->teacherRates as $currentObject) $currentObject->delete();
			return true;
		}
	}

    public static function listArray() {

        if (Yii::app()->user->model->is_owner) {
            return CHtml::listData(TrCourse::model()->findAll(), 'id', 'nameWithCenter');
        }

        return CHtml::listData(TrCourse::model()->findAll(), 'id', 'name');
    }

    public function getNameWithCenter() {
        return "{$this->name} [{$this->partner->organization}]";
    }
}



?>