<?php

$db_name = 'edu';
$db_username = 'root';
$db_password = 'root';

if (strpos(__FILE__, 'edu.org')) {
    $db_name = 'edu';
    $db_username = 'root';
    $db_password = 'root';
}

if (strpos(__FILE__, 'www/gaikagroup.com/edu')) {
    $db_name = 'edu';
    $db_username = '';
    $db_password = '';
}

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

    'import'=>array(
        'application.models.*',
        'application.components.*',
    ),

	// application components
	'components'=>array(

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname='.$db_name,
			'emulatePrepare' => true,
			'username' => $db_username,
			'password' => $db_password,
			'charset' => 'utf8',
            'tablePrefix' => 'edu_',
		),

        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
            // 'loginUrl' => array('site/login'), // #pdovlatov #09-04-2013
            'class' => 'WebUser', // #pdovlatov #09-04-2013
        ),
		
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
    
    'commandMap'=>array(
        'migrate'=>array(
            'class'=>'system.cli.commands.MigrateCommand',
            'migrationTable'=>'edu_migration',
        ),
    ),
    
);