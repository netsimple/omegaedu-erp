<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

// Yii::setPathOfAlias('bootstrap','../extensions/bootstrap');

return CMap::mergeArray(

require(dirname(__FILE__).'/main.php'),
array(

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'0306323922',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
                        /*
                        'generatorPaths'=>array(
                            'bootstrap.gii',
                        ),
                        */

		),
		
	),

	// application components
	'components'=>array(
		
		'eauth' => array(
                    'class' => 'ext.eauth.EAuth',
                    'popup' => true, // Use the popup window instead of redirecting.
                    'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache'.
                    'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
                    'services' => array( // You can change the providers and their classes.
                        'google' => array(
                            'class' => 'GoogleOpenIDService',
                        ),
                        'yandex' => array(
                            'class' => 'YandexOpenIDService',
                        ),
                        'twitter' => array(
                            // register your app here: https://dev.twitter.com/apps/new
                            'class' => 'TwitterOAuthService',
                            'key' => 'baeXMIeoQcuHrzrHccKo3g',
                            'secret' => 'ydGvGAFqg2D0iSkAwVYoyWYp6eNpTJZOCqylGHek',
                        ),
                        /*
                        'google_oauth' => array(
                            // register your app here: https://code.google.com/apis/console/
                            'class' => 'GoogleOAuthService',
                            'client_id' => '...',
                            'client_secret' => '...',
                            'title' => 'Google (OAuth)',
                        ),
                        'yandex_oauth' => array(
                            // register your app here: https://oauth.yandex.ru/client/my
                            'class' => 'YandexOAuthService',
                            'client_id' => '...',
                            'client_secret' => '...',
                            'title' => 'Yandex (OAuth)',
                        ),*/
                        'facebook' => array(
                            // register your app here: https://developers.facebook.com/apps/
                            'class' => 'FacebookOAuthService',
                            'client_id' => '132014703651891',
                            'client_secret' => '28e9f11521a4833077290719a538c4ca',
                        ), /*
                        'linkedin' => array(
                            // register your app here: https://www.linkedin.com/secure/developer
                            'class' => 'LinkedinOAuthService',
                            'key' => '...',
                            'secret' => '...',
                        ),
                        'github' => array(
                            // register your app here: https://github.com/settings/applications
                            'class' => 'GitHubOAuthService',
                            'client_id' => '...',
                            'client_secret' => '...',
                        ),
                        'live' => array(
                            // register your app here: https://manage.dev.live.com/Applications/Index
                            'class' => 'LiveOAuthService',
                            'client_id' => '...',
                            'client_secret' => '...',
                        ),*/
                        'vkontakte' => array(
                            // register your app here: https://vk.com/editapp?act=create&site=1
                            'class' => 'VKontakteOAuthService',
                            'client_id' => '3584806',
                            'client_secret' => 'tOU4TA8Ns6MRgxewF6Yh',
                        ),
                        'mailru' => array(
                            // register your app here: http://api.mail.ru/sites/my/add
                            'class' => 'MailruOAuthService',
                            'client_id' => '703430',
                            'client_secret' => 'b2ffc009d87b3b8c4c83835a276baf37',
                        ), /*
                        'moikrug' => array(
                            // register your app here: https://oauth.yandex.ru/client/my
                            'class' => 'MoikrugOAuthService',
                            'client_id' => '...',
                            'client_secret' => '...',
                        ),
                        'odnoklassniki' => array(
                            // register your app here: http://dev.odnoklassniki.ru/wiki/pages/viewpage.action?pageId=13992188
                            // ... or here: http://www.odnoklassniki.ru/dk?st.cmd=appsInfoMyDevList&st._aid=Apps_Info_MyDev
                            'class' => 'OdnoklassnikiOAuthService',
                            'client_id' => '...',
                            'client_public' => '...',
                            'client_secret' => '...',
                            'title' => 'Odnokl.',
                        ),
                        */
                    ),
		),
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=edu',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
            'tablePrefix' => 'edu_',
            'enableProfiling' => true,
            //'enableParamLogging' => true, 
		),
        
        /*
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'trace, info, error, warning',
                ),
                array( 
                    'class'=>'CProfileLogRoute', 
                    'report'=>'summary',
                ), 
            ),
        ),
        */
                
                // #pdovlatov --> enable Authorization Manager
                /*
                'authManager'=>array(
                    'class'=>'CDbAuthManager',
                    'connectionID'=>'db',
                    'defaultRoles' => array('owner', 'partner', 'manager', 'teacher'), // #pdovlatov
                ),
                */

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'p.dovlatov@gaikagroup.com',
        'uploadDirectory' => 'uploads',
	),
));