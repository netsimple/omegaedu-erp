<?php

class m140701_080822_new extends CDbMigration
{
	public function up()
	{
        echo "m140701_080822_new\n";
        // strict mysql mode
//        $this->execute("ALTER TABLE `edu_access_log` CHANGE `user_id` `user_id` INT(10)  UNSIGNED  NULL");
//        $this->execute("ALTER TABLE `edu_tr_costs` CHANGE `deleted_by` `deleted_by` INT(10)  UNSIGNED  NULL  DEFAULT '0'");
//        $this->execute("ALTER TABLE `edu_tr_costs` CHANGE `is_deleted` `is_deleted` INT(10)  UNSIGNED  NULL  DEFAULT '0'");
        return true;
	}

	public function down()
	{
		echo "m140701_080822_new does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}