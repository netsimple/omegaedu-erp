<style>
	.warning {background-color: rgba(255,153,153,0.5);}
</style>

<?php

$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=>array(
		'Планируемые занятия' => array('ajax' => array('list', 'mode' => 'future')),
		'Прошедшие занятия' => array('ajax' => array('list', 'mode' => 'past')),
	),
    'options'=>array(
        'collapsible'=>false,
        'selected'=>0,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));

?>