<?php

$pxLeft = floor((($schedule->hours_start - $dayStart) * 60 + $schedule->minutes_start) * 100/60); // 100 px in hour
$pxWidth = floor(( (($schedule->hours_end - $dayStart) * 60 + $schedule->minutes_end) - (($schedule->hours_start - $dayStart) * 60 + $schedule->minutes_start) ) * 100 / 60) - 3 ;
?>
<div
    data-show-description="true"
    class="tl_event <?= $schedule->seenNoted ? '' : 'tl_event_alert' ;?>" data-eventid="<?php echo $schedule->id ?>"
    data-event="<?php echo $schedule->id ?>"
    style="left: <?php echo $pxLeft ?>px; width: <?php echo $pxWidth ?>px;  "
    data-location="<?php echo $locationKey; ?>"
    >


    <span class="tl_event_time">
        <?php echo $schedule->hours_start ?>:<?php echo $schedule->minutes_start ?> - <?php echo $schedule->hours_end ?>:<?php echo $schedule->minutes_end ?>
    </span>

    <h2><?php echo $schedule->course->name; ?></h2>

    <p class="tl_subtitle">
        <?php echo $schedule->group ? $schedule->group->name : '-'; ?>
        <?php echo $schedule->center ? $schedule->center->name : '-'; ?>
    </p>

</div>