<?php
	$this->menu = array(
	    array('label'=>'Вернуться к календарю', 'url'=>array('schedule/index')),
	    // array('label'=>'Удалить текущую дату', 'url'=>array('schedule/delete', 'id' => $model->id), 'linkOptions' => array('confirm' => 'Вы действительно желаете удалить редактируемую дату в календаре?')),
	);
?>

<h1>Редактирование даты в системном календаре</h1>

<?php
    $this->renderPartial('_form',array('model'=>$model));
?>