<?php
    $this->widget('application.components.widgets.TabLinksWidget', array(
		'links' => array(
			array('label' => 'Редактировать', 'url' => array('update','id' => $model->id), 'visible' => Yii::app()->user->checkAccess('trcenter/update')),
		),
	));
?>

<?php

$attributes = array(
    'name' => array(
        'name' => 'name',
        'editable' => array(
            'type' => 'text',
            'validate' => 'function(value) {
                if (!value) return "Название обязательно для заполнения."
            }'
        ),
    ),
	'address',
    'contacts' => array(
        'name' => 'contacts',
        'editable' => array(
            'type' => 'textarea',
        ),
	),
	'manager' => array(
		'name' => 'managerIDs',
		'type' => 'raw',
		// 'value' => $model->managerLinks,
		'editable' => array(
			'type'      => 'checklist',
			'model'     => $model,
			'attribute' => 'managerIDs',
			'placement' => 'right',
			// 'url'       => $this->createUrl('ajaxUpdateManagers'),
			'source'    => UserMain::model()->getManagerList(),
		),
	),
);

?>

<?php
    $this->widget('editable.EditableDetailView', array(
		'data'       => $model,
		'url'        => $this->createUrl('ajaxUpdate'),
		'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
		'emptytext'  => 'Редактировать',
		'attributes' => $attributes,
    ));
?>