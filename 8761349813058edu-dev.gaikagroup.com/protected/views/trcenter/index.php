<div>
	<?php $this->widget('application.components.widgets.AddLinkWidget'); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'client-grid',
	'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	'columns'=>array(
		array(
			'name' => 'name',
			'type' => 'html',
			'value'=> 'CHtml::link($data->name,array(\'trcenter/view\', \'id\' => $data->id))',
		),
        'address' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'address',
		   'editable' => array(
				'type' => 'text',
				'url' => $this->createUrl('ajaxUpdate'),
		   ),
        ),
        'contacts' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'contacts',
		   'editable' => array(
				'type' => 'textarea',
				'url' => $this->createUrl('ajaxUpdate'),
		   ),
        ),
		array(
			'class'=>'ButtonColumn',
			'template' => '{delete}',
		),
	),
)); ?>