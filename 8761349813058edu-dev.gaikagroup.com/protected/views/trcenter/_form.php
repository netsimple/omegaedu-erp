<?php

	$jsManager = CJSON::encode($model->manager);

	$js = <<<JSCRIPT
		$(document).ready(function(){
			var selectedManagerIds = [];
			var selectedManagerNames = [];
			var jsManager = $jsManager;
			for (var i in jsManager) {
				var appendText = '<div id="manager-"' + i + '" style="border: 1px solid black; background: #E5F1F4; padding: 5px; margin: 10px;">' +
				'<a href="#" class="manager-delete-btn" data-id="' + i + '" style="float: right; color: black; text-decoration: none;">[x]</a>' + 
				'<input name="TrCenter[manager][]" type="hidden" value="' + i + '" />' +
				jsManager[i] + 
				'</div>';
				$('#manager-add-container').append(appendText);
				selectedManagerIds.push(i);
				selectedManagerNames.push(jsManager[i]);
			}
			$('#manager-add-btn').click(function(){
				var appendText = '<div id="manager-' + $('#manager-add-src').val() + '" style="border: 1px solid black; background: #E5F1F4; padding: 5px; margin: 10px;">' +
				'<a href="#" class="manager-delete-btn" data-id="' + i + '" style="float: right; color: black; text-decoration: none;">[x]</a>' + 
				'<input name="TrCenter[manager][]" type="hidden" value="' + $('#manager-add-src').val() + '" />' +
				$('#manager-add-src :selected').text() + 
				'</div>';
				$('#manager-add-container').append(appendText);
				selectedManagerIds.push($('#manager-add-src').val());
				selectedManagerNames.push($('#manager-add-src :selected').text());
				$('#manager-add-src :selected').remove();
				if ($('select[id=manager-add-src] option').size()) {
					$('#manager-add-src :first').select();
				} else {
					$('#manager-add-src').parent().parent().hide();
				}
			});
		});
JSCRIPT;

Yii::app()->clientScript->registerScript('addManagerJS',$js,CClientScript::POS_READY);

?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'trcenter-form',
	'enableAjaxValidation'=>false,
)); ?>
 
    <?php echo $form->errorSummary($model); ?>
	
	<fieldset style="width: 40%; float: left; margin-right: 10px">
		<legend>Основная информация</legend>
		<div class="row">
			<?php echo $form->label($model,'name'); ?>
			<?php echo $form->textField($model, 'name', array('style'=>'width: 95%')); ?>
		</div>
		
		<div class="row">
			<?php echo $form->label($model,'address'); ?>
			<?php echo $form->textField($model, 'address', array('style'=>'width: 95%')); ?>
		</div>
		
		<div class="row">
			<?php echo $form->label($model,'contacts'); ?>
			<?php echo $form->textArea($model, 'contacts', array('rows' => 3, 'style'=>'width: 95%')) ?>
		</div>
	</fieldset>
	
	<fieldset style="width: 40%; float: left">
		<legend>Менеджеры</legend>
		<div class="row">
			<?php if ($managers): ?>
			<div style="float: left; margin-right: 15px;">
				<?php echo CHtml::dropDownList('manager_list', current($managers), $managers, array('id' => 'manager-add-src')) ?>
			</div>
			<div style="float: left;">
				<?php echo CHtml::button('Добавить', array('id' => 'manager-add-btn')); ?>
			</div>
			<div style="clear: left;"></div>
			<?php elseif (!$model->manager): ?>
			Нет доступных для выбора менеджеров.
			<?php endif; ?>
		</div>
		<div class="row" id="manager-add-container">
			
		</div>
	</fieldset>
	
	<div style="clear: left;"></div>
 
    <div class="row submit">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array('class' => 'btn btn-primary')); ?>
    </div>
 
<?php $this->endWidget(); ?>
</div><!-- form -->