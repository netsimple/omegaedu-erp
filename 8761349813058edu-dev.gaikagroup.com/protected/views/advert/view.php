<script type="text/javascript">
    jQuery(function($){
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    });
</script>
<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->getClientScript()->getCoreScriptUrl() . '/jui/js/jquery-ui-i18n.min.js');

?>

<h1><?php echo CHtml::encode($model->title); ?></h1>

<?php

$attributes = array(
    'name' => array(
        'name' => 'username',
		'value' => $model->user->fullname,
    ),
    'title' => array(
        'name' => 'title',
        'editable' => array(
            'type' => 'text',
            'validate'   => 'function(value) {
                if (!value) return "Название обязательно для заполнения."
            }'
        ),
    ),
    'description' => array(
        'name' => 'description',
        'editable' => array(
            'type'       => 'textarea',
        ),
    ),
	'date' => array(
		'name' => 'date',
		'editable' => array(
			'type'       => 'date',
			'viewformat' => 'dd M yyyy',
			'params' => array(
				'weekStart' => 1,
			)
		),
	),
	'member_count',
	'activities' => array(
		'name' => 'activities',
		'type' => 'raw',
		'value'=> $model->getActivityType(true),
		'editable' => array(
			'type' => 'checklist',
			'source' => $model->activityType,
		),
	),
);

?>

<?php
    $this->widget('editable.EditableDetailView', array(
    'data'       => $model,
    'url'        => $this->createUrl('ajaxUpdate'),
    'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
    'emptytext'  => 'Редактировать',
    'attributes' => $attributes
    ));
?>

<?php if ($model->documents): ?>
	<?php $this->renderPartial('_documents',array('documents' => $model->documents)); ?>
<?php endif; ?>