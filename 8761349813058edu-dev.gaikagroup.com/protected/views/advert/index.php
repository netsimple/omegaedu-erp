<script type="text/javascript">
    jQuery(function($){
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    });
</script>

<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->getClientScript()->getCoreScriptUrl() . '/jui/js/jquery-ui-i18n.min.js');
?>

<div>
	<?php $this->widget('application.components.widgets.AddLinkWidget'); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'advert-grid',
    'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'function(){
		jQuery("#date").datepicker({
			dateFormat: "dd.mm.yy",
			changeYear:true
		});
	}',
	'columns'=>array(
		array(
			'name' => 'username',
			'type' => 'raw',
			'value'=> 'CHtml::link($data->user->fullname,array(\'user/view\',\'id\' => $data->user_id))',
		),
		array(
			'name' => 'title',
			'type' => 'raw',
			'value' => 'CHtml::link($data->title,array("advert/view", "id" => $data->id))',
		),
		array(
            'class' => 'editable.EditableColumn',
			'name' => 'member_count',
			'type' => 'raw',
			'value' => '$data->member_count ? $data->member_count : ""',
			'editable' => array(
				'type' => 'text',
				'url' => $this->createUrl('ajaxUpdate'),
			),
		),
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'activities',
			'type' => 'raw',
			'value' => '$data->getActivityType(true)',
			'filter' => $model->activityType,
			'editable' => array(
				'type' => 'checklist',
				'source' => $model->activityType,
				'url' => $this->createUrl('ajaxUpdate'),
			),
		),
		array(
            'class' => 'editable.EditableColumn',
			'name' => 'date',
			'type' => 'text',
			'value' => '$data->date ? Yii::app()->dateFormatter->format(\'d MMM yyyy\',$data->date) : ""',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'date',
                'attribute' => 'date',
                'options' => array(
                    'dateFormat' => 'dd.mm.yy',
                    'changeYear' => true
                ),
            ), true),
			'editable' => array(
				'type'       => 'date',
				'url' => $this->createUrl('ajaxUpdate'),
				'viewformat' => 'dd M yyyy',
				'params' => array(
					'weekStart' => 1,
				),
			),
		),
		array(
			'class'=>'ButtonColumn',
			'template' => '{update} {delete}',
		),
	),
)); ?>