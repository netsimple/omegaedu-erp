<?php
    $this->widget('editable.EditableDetailView', array(
    'data'       => $model,
	'id'		 => 'groupDetailView',
    'url'        => $this->createUrl('ajaxUpdate'),
    'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
    'emptytext'  => 'Редактировать',
      
    'attributes' => array(
		array(
			'name' => 'name',
			'value' => $model->name,
			'editable' => array(
				'type'       => 'text',
			),
		),
		array(
			'name' => 'tr_center_id',
			'type' => 'raw',
			'value' => $model->center_name,
			'editable' => array(
				'type'       => 'select',
				'source'	 => $model->getTrCenterList(),
			),
		),
		array(
			'name' => 'tr_course_id',
			'value' => $model->course_name,
			'editable' => array(
				'type'       => 'select',
				'source'	 => $model->getTrCourseList(),
			),
		),
		array(
			'name' => 'teacher_id',
			'value' => $model->teacher_name,
			'editable' => array(
				'type'       => 'select',
				'source'	 => $model->getTeacherList(),
			),
		),
	)
    ));
?>