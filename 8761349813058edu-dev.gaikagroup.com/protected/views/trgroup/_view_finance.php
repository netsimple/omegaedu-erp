<?php

$js = <<<JSCRIPT
    $(document).ready(function(){
       var topCheckBoxId = 'clients-grid_c0_all';
       var idSelector = getAllIDs();
       
       function updateValues(idSelector) {
            if (idSelector) {
                $.ajax({
                    url: '{$this->createUrl('ajaxFinances', array('id' => $model->id))}',
                    type: 'post',
                    data: {ids: idSelector, datefrom: $('#datefrom').val(), dateto: $('#dateto').val()},
                    success: function(result){
                        result = JSON.parse(result);
                        $('#credited-sum').html(result.income_credit);
                        $('#paid-sum').html(result.income_debet);
                        $('#total-sum').html(result.income_debet - result.income_credit);
                        if (result.costs_table) $('#summary-costs-table').html(result.costs_table);
                        // if (result.selector) $('#section-selector').html(result.selector);
                        restoreSelected();
                    }
                });
            } else {
                $('#credited-sum').html(0);
                $('#paid-sum').html(0);
                $('#total-sum').html(0);
                $('#summary-costs-table').html('');
                $('#section-selector').html('');
            }
       }
       
       function getAllIDs() {
            var arr = [];
            $('.checkbox-column input[type=checkbox]').each(function(){
                if ($(this).attr('id') != topCheckBoxId && $(this).prop('checked')) arr.push($(this).val());
            });
            return arr;
       }
       
       function restoreSelected() {
            $('.checkbox-column input[type=checkbox]').each(function(){
                $(this).prop('checked',$(this).attr('id') != topCheckBoxId && $.inArray($(this).val(),idSelector) != -1);
            });
            $('#'+topCheckBoxId).prop('checked',$('.checkbox-column input[type=checkbox]').length == idSelector.length + 1);
       }

       $('.checkbox-column input[type=checkbox]').click(function(){
            idSelector = [];
            if ($(this).attr('id') == topCheckBoxId) {
                if ($('#'+topCheckBoxId).prop('checked')) {
                    $('.checkbox-column input[type=checkbox]').each(function(){
                        if ($(this).attr('id') != topCheckBoxId) idSelector.push($(this).val());
                    });
                }
            } else {
                idSelector = getAllIDs();
            }
            updateValues(idSelector);
       });
       
       $('#datefrom, #dateto').change(function(){
            updateValues(idSelector);
       });
       
       $('#link-income').click(function(){
            $('#link-income').addClass('active');
            $('#link-costs').removeClass('active');
            $('#section-costs').hide();
            $('#section-income').show();
            return false;
       });
       
       $('#link-costs').click(function(){
            $('#link-costs').addClass('active');
            $('#link-income').removeClass('active');
            $('#section-income').hide();
            $('#section-costs').show();
            return false;
       });

    });

JSCRIPT;

Yii::app()->clientScript->registerScript('clients-selector',$js,CClientScript::POS_READY);
Yii::app()->clientScript->registerCoreScript('jquery.ui');

$css = <<<CSS
    a.active { font-weight: bold; text-decoration: none; }
CSS;

Yii::app()->clientScript->registerCss('links-custom-style',$css);

$financeData = $model->getFinanceIncomeData();

?>
<div id="msg" class="alert hide"></div>
<div style="margin-top: 5px; margin-bottom: 15px;"><?php echo CHtml::link('Доход','#',array('id' => 'link-income', 'class' => 'active')); ?> | <?php echo CHtml::link('Расход','#',array('id' => 'link-costs')); ?></div>
<?php $this->widget('application.components.widgets.DateFilterWidget'); ?>

<div id="section-income">
<div id="section-selector"><?php $this->renderPartial('_view_finance_selector',array('model' => $model)); ?></div>

<div style="margin-top: 5px; margin-bottom: 5px;">Начислено:&nbsp;<span id="credited-sum"><?php echo $financeData->income_credit; ?></span></div>
<div style="margin-top: 5px; margin-bottom: 5px;">Оплачено:&nbsp;<span id="paid-sum"><?php echo $financeData->income_debet; ?></span></div>

<div style="margin-top: 15px;">Итого:&nbsp;<span id="total-sum"><?php echo $financeData->income_debet - $financeData->income_credit; ?></span></div>

</div>

<div id="section-costs" style="display: none;">

    <div style="margin-top: 20px; margin-bottom: 15px;">
        <p>Суммарные показатели:</p>
        <div id="summary-costs-table"><?php $this->renderPartial('_view_finance_summarycosts',array('modelCosts' => $modelCosts)); ?></div>
    </div>

</div>

<div style="margin-top: 15px;">Стоимость ведения курса у преподавателя:&nbsp;<span>
<?php
$this->widget('editable.Editable', array(
    'type'      => 'text',
    'name'      => 'rate',
    'text'      => $model->teacherRate,
    'url'       => $this->createUrl('ajaxUpdateRate'),
    'title'     => 'Стоимость курса',
    'placement' => 'right',
    'onSave' => 'js: function(e, params) {
					$.ajax({
						type: "POST",
						data: {name: "rate", pk: '.$model->rateId.', scenario: "update", value: params.newValue},
						url: "'.$this->createUrl('ajaxUpdateRate').'"
					});
				}'
));
?>(ак/ч)</span></div>