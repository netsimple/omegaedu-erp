<?php

$clientList = array();
if ($model->members) {
	foreach ($model->members as $currentClient) {
		$clientList[] = array('id' => $currentClient->id, 'fullname' => $currentClient->fullname);
	}
}

$clientList = CJSON::encode($clientList);

$urlClientProfile = Yii::app()->createAbsoluteUrl('client/view');
$urlAddclient = Yii::app()->createAbsoluteUrl('trgroup/addclient');
$urlRemoveclient = Yii::app()->createAbsoluteUrl('trgroup/removeclient');

$css = <<<CSS
	.client-in-list { padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; background: #E5F1F4; border: 1px solid black; width: 40%; display: block; margin: 5px; }
	.client-in-list a { text-decoration: none; color: black; font-weight: bold; }
	.client-in-list a:hover { color: black; }
	#client-new-grid table { margin-bottom: 0.7em }
CSS;

$js = <<<JSCRIPT
	
	function addClient(clientId, clientName) {
		if (!$('#client'+clientId).length) {
			var newElem = '<span class="client-in-list" id="client' + clientId + '" data-clientid="' + clientId + '">';
			newElem    += '<input type="hidden" name="Client[id][]" value="' + clientId + '">';
			newElem    += '<a href="#" class="remove-client" data-parentid="client' + clientId + '" style="float: right;">[x]</a><a href="$urlClientProfile/'+ clientId +'" target="_blank">';
			newElem    += clientName + '</a></span>';
			$('#selected-clients').html($('#selected-clients').html() + newElem);
			return true;
		} else {
			return false;
		}
	}
		
JSCRIPT;

if (!$model->isNewRecord)
	$js .= <<<JSCRIPT
	
	$(document).ready(function(){
		
		var clientList = $clientList;
		for (var currentClient in clientList) {
			addClient(clientList[currentClient]['id'],clientList[currentClient]['fullname']);
		}
		
	});
		
	
JSCRIPT;

$js .= <<<JSCRIPT
		
	$(document).ready(function(){
	
		$('body').on('click', 'a.new-client', function(e) {
			if (addClient($(this).data('clientid'),this.innerHTML)) {
				$.ajax({
					url: '$urlAddclient',
					type: 'get',
					data: {'clientId': $(this).data('clientid')},
					success: function(result){
						console.log('yes!');
					}
				});
			}
		});
		
		$('body').on('click', 'a.remove-client', function(e) {
			var spanId = '#'+$(this).data('parentid');
			var clientId = $(spanId).data('clientid');
			$(spanId).remove();
			$.ajax({
				url: '$urlRemoveclient',
				type: 'get',
				data: {'clientId': clientId},
				success: function(result){
					console.log('yes!');
				}
			});
		});
		
		$('body').on('click', 'a.new-time', function(e) {
			var spanId = '#'+$(this).data('parentid');
			var timeId = $(spanId).data('timeid');
			if ($(spanId).hasClass('selected-cell')) {
				$(spanId).removeClass('selected-cell');
				$(spanId+'_input').remove();
			} else {
				addTime(timeId);
			}
	
		});
	
	});

JSCRIPT;

Yii::app()->clientScript->registerScript('selectUpdateJS', $js, CClientScript::POS_READY);
Yii::app()->clientScript->registerCss('selectUpdateCSS', $css);

?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'trgroup-form',
	'enableAjaxValidation'=>false,
)); ?>
 
    <?php echo $form->errorSummary($model); ?>
	
	<div class="row">
        <?php echo $form->label($model,'name'); ?>
        <?php echo $form->textField($model, 'name', array('size' => 35)); ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'tr_center_id'); ?>
        <?php echo $form->dropDownList($model, 'tr_center_id', $model->getTrCenterList(), array('empty' => '-- Выберите элемент --')); ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'tr_course_id'); ?>
        <?php echo $form->dropDownList($model, 'tr_course_id', $model->getTrCourseList(), array('empty' => '-- Выберите элемент --')); ?>
    </div>
	
    <div class="row">
        <?php echo $form->label($model,'teacher_id'); ?>
        <?php echo $form->dropDownList($model, 'teacher_id', $model->getTeacherList(), array('empty' => '-- Выберите элемент --')); ?>
    </div>
	
	<div class="row">
		<?php
			$this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'client-new-grid',
				'dataProvider'=>$model->getClientList($client, $this->getClients()),
				'filter' => $client,
				'htmlOptions' => array('style' => 'width: 50%'),
				'template' => '{items}{pager}',
				'columns'=>array(
					'fullname' => array(
						'name' => 'fullname',
						'type' => 'raw',
						'value' => 'CHtml::link($data->fullname, "#", array("class" => "new-client", "data-clientid" => $data->id, "data-groupid" => '.'1'.'))',
						'header' => 'Список клиентов',
					),
				),
			));
		?>
		<div id="selected-clients" style="line-height: 1.0em; margin-bottom: 10px;"></div>
	</div>
	
	<div class="row" style="margin-top: 40px;">
		<div id="table"><?php if (!$model->isNewRecord && count($schedules)) $this->renderPartial('/schedule/table', array('schedules' => $schedules)); ?></div>
	</div>
 
    <div class="row submit">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array('class' => 'btn btn-primary')); ?>
    </div>
 
<?php $this->endWidget(); ?>
</div><!-- form -->