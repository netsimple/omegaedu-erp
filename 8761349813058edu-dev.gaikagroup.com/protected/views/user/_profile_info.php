<div style="margin-bottom: 10px; text-align: right;">
	<?php if ($model->isCurrentUser) echo CHtml::link('Сменить пароль',array('passchange')); ?>
</div>

<?php if (Yii::app()->user->checkAccess('user/update', 'self')): ?>
<?php else: ?>
<?php endif; ?>

<script type="text/javascript">
    jQuery(function($){
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    });
</script>
<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->getClientScript()->getCoreScriptUrl() . '/jui/js/jquery-ui-i18n.min.js');

$editableDate = array(
    'type'       => 'date',
    'viewformat' => 'dd M yyyy',
    'params' => array(
        'weekStart' => 1,
    )
);

$attributes = array(
    array(
        'name' => 'email',
        'editable' => array(
            'type'       => 'text',
            'options'     => array(
                'placeholder'=> 'mail@mail.ru',
            ),
            'validate' => 'js: function(value) {
                if (!validateEmail(value)) return "Email должен быть в формате mail@mail.ru";
            }',
        ),
    ),
    array(
        'name' => 'fullname',
        'editable' => array(
            'type'       => 'text',            
            'validate'   => 'function(value) {
                if (!value) return "ФИО обязательны для заполнения."
            }'
        ),
    ),
    array(
        'name' => 'phone_num',
        'editable' => array(
            'type'       => 'text',
            'options'     => array(
                'placeholder'=> '+7-922-333-4444',
            ),
            'validate'   => 'function(value) {
                if (!validatePhone(value)) return "Телефона должен быть в формате +7-922-333-4444";
            }'
        ),
    ),
	array(
		'name' => 'hire_date',
		// 'value' => $model->hire_date ? Yii::app()->dateFormatter->format('d MMM yyyy',$model->hire_date) : '',
        'editable' => $editableDate,
	),
	array(
		'name' => 'city_id',
		'value' => $model->city ? $model->city->name : 'Не выбрано',
        'editable' => array(
            'type' 	=> 'select',
            'source'=> CHtml::listData(City::model()->findAll(), 'id', 'name'),
        ),
	),
	'comments',
    array(
        'name' => 'social_google',
        'editable' => array(
            'success'  =>
            'function(response, newValue) { console.log(response);
				if(!response) {
					return "Неизвестная ошибка.";
				}          
									
				if(response.errors) {
					return response.errors;
				}
			}'
        )
    ),
    'social_yandex',
    'social_twitter',
    'social_vk',
    'social_facebook',
    'social_mailru',
	'skype',
);
if ($model->inPartnerGroup) {
    $attributes = array_merge($attributes,array('partner.organization', 'partner.position', 'partner.legal_addr', 'partner.real_addr', 'partner.contacts', 'partner.details', 'partner.web_id'));
}
if ($model->inTeacherGroup) {
    $attributes = array_merge($attributes,array('teacher.education','teacher.birthdate' => array('name' => 'teacher.birthdate', 'editable' => $editableDate)));
}
?>

<?php
    $this->widget('editable.EditableDetailView', array(
    'data'       => $model,
    //you can define any default params for child EditableFields 
    'url'        => $this->createUrl('ajaxUpdate'), //common submit url for all fields
    'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken), //params for all fields
    'emptytext'  => 'Редактировать',

    'attributes' => $attributes
    ));
?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'docs-form',
    'action'=>'uploadDocuments?user_id='.$model->id,
    'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

<div class="row" style="margin-top: 12px;">
    <?php echo $form->label($model,'documents'); ?>
</div>
<div class="clearfix"></div>
<?php $this->renderPartial('_profile_documents', array('documents' => $model->documents, 'allowDelete' => true)); ?>

<div class="row" style="margin-top: 12px;">
    <?php echo $form->fileField($model, 'documents[]', array('multiple'=>1, 'style'=>'width: 540px')) ?>
</div>
<div class="clearfix"></div>
<div class="row" style="margin-top: 12px;">
    <?php echo CHtml::submitButton('Загрузить'); ?>
</div>
<div class="clearfix"></div>
<?php $this->endWidget(); ?>

<?php
	if ($model->id == Yii::app()->user->id)
		$this->renderPartial('_extattach', array('activeServices' => $activeServices));
?>