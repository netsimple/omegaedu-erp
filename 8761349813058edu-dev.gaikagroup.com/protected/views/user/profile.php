<?php if ($flashMsg = Yii::app()->user->getFlash('coupons')): ?>
<div style="border: 1px solid green; background: lightgreen; padding-left: 15px; padding-top: 5px; padding-bottom: 5px; margin-bottom: 15px; margin-top: 10px;">
	<?php echo CHtml::encode($flashMsg); ?>
</div>
<?php endif; ?>

<h1><?php echo $model->fullname; ?></h1>

<div class="role-block">
<b><?php echo CHtml::activeLabel($model,'role'); ?>:</b>&nbsp;
<?php $_isFirst = true;
	  foreach ($model->roles as $curRole) {
		if (!$_isFirst) echo ', ';
		echo '<b>';
		echo CHtml::activeLabel($model,$curRole);
		echo '</b>';
		$_isFirst = false;
	  }
?>
</div>

<?php

    if ($model->avatar_name) {

        ?>
            <img src="<?php echo $model->avatarPath; ?>" alt="" id="thumb_imageToCrop" class="userAvatar"/>
            <div id="mirror_imageToCrop">
                <?php echo CHtml::image($model->avatarOrigPath, '', array('id'=>'thumbEvent_imageToCrop')); ?>
            </div>
        <?php

    } else {
        echo CHtml::image(Yii::app()->getBaseUrl().'/images/noavatar.jpg', '', array('class'=>'userAvatar noAvatar'));
    }

    if (Yii::app()->user->checkAccess('user/update', $model->roles))
        $this->renderPartial('_avatar_edit', array('model'=>$model));

?>

<?php

$tabs = array();

$tabs['Профиль'] = $this->renderPartial('_profile_info',array('model' => $model, 'activeServices' => $activeServices),true);

if ($model->inPartnerGroup || $model->inManagerGroup) {
	$tabs['Купоны']  = $this->renderPartial('_profile_coupons',array('model' => $model),true);
}

if ($model->inTeacherGroup) {
	$tabs['Предметы']  = $this->renderPartial('_profile_courses',array('model' => $model, 'courses' => $courseRates),true);
}

if ($model->checkRole('adman')) {
	$tabs['Отчёты'] = $this->renderPartial('_profile_advert',array('model' => $advert),true);
}

if ($model->inPartnerGroup) {
	$tabs['Финансы'] = $this->renderPartial('_profile_finance_partner',array('model' => $model, 'modelCosts' => $modelCosts),true);
} elseif ($model->inTeacherGroup) {
	$tabs['Финансы'] = $this->renderPartial('_profile_finance_teacher',array('model' => $model),true);
}

$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=>$tabs,
    'options'=>array(
        'collapsible'=>false,
        'selected'=>0,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));

?>