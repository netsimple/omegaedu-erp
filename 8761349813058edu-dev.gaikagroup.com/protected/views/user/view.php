<div>
	<?php
		$this->widget('application.components.widgets.AddLinkWidget', array('accessKey' => array('user/create',$type), 'actionUrl' => array('create','target'=>$type), 'htmlOptions' => array('onclick' => '$(".user-partnes-add-form").toggle(); return false;')));
	?>
</div>






<div class="hidden user-partnes-add-form">
    <?php
        if (Yii::app()->user->checkAccess('user/create', $type)) {
            $miniModel = new UserMain('create');
            $miniModel->type = $type;

            if ($miniModel->inPartnerGroup && !$miniModel->partner) $miniModel->partner = new UserPartner;
            if ($miniModel->inManagerGroup && !$miniModel->manager) $miniModel->manager = new UserManager;
            if ($miniModel->inTeacherGroup && !$miniModel->teacher) $miniModel->teacher = new UserTeacher;

            $miniModel->formatDates();
            if ($miniModel->inTeacherGroup && $miniModel->teacher) {
                $miniModel->teacher->formatDates();
            }

            $this->renderPartial('application.views.user._form_mini', array('model'=>$miniModel, 'target'=>$type));
        }

    ?>
</div>




<?php

$columns = array(
	array(
		'name' => 'avatar_name',
		'type' => 'raw',
		'value'=> '$data->avatar_name ? CHtml::link(CHtml::image(Yii::app()->getBaseUrl()."/'.Yii::app()->params['uploadDirectory'].'/avatars/".$data->avatar_name),array("view", "id" => $data->id)) : ""',
		'filter' => false,
        'htmlOptions'=>array('width'=>'100'),
	),
	array(
		'name' => 'fullname',
		'type' => 'html',
		'value'=> 'CHtml::link($data->fullname,array("view", "id" => $data->id))',
	),
    array(
		'name' => 'email',
		'type' => 'html',
		'value'=> 'CHtml::link($data->email, "mailto:".$data->email)',
	),
    array(
		'name' => 'phone_num',
		'type' => 'raw',
		'value'=> 'CHtml::link($data->phone_num, "tel:".$data->phone_num)',
	),
);

if ($model->inTeacherGroup) {
	$columns[] = array(
		'name' => 'group_name',
		'type' => 'raw',
		'value' => '$data->groupLinks',
	);
	$columns[] = array(
		'name' => 'center_id',
		'type' => 'raw',
		'value' => '$data->centerLinks',
		'filter' => CHtml::activeDropDownList($model,'center_id',$model->centerList),
	);
}

$columns[] = array(
	'class'=>'ButtonColumn',
	'template' => '{delete}',
);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
    'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search($type),
	'filter'=>$model,
	'columns'=>$columns,
));

?>