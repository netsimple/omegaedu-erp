

<h1>Создание пользователя</h1>
<div style="margin-bottom: 15px;">
<h3>Тип:&nbsp;
<?php $_isFirst = true;
	  foreach ($model->roles as $curRole) {
		if (!$_isFirst) echo ', ';
		echo CHtml::activeLabel($model,$curRole);
		$_isFirst = false;
	  }
?></h3>
</div>

<?php
    $this->renderPartial('_form',array('model'=>$model, 'target' => $target));
?>