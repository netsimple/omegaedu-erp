<div style="margin-bottom: 15px;">
	<?php $this->widget('application.components.widgets.AddLinkWidget', array('actionUrl' => array('advert/create'))); ?>
</div>

<?php
	$dataProvider = $model->search();
	if ($dataProvider->pagination->pageCount == 1) $dataProvider->pagination = false;
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'advert-grid',
    'template'=>'<div class="pagerControl">{pager}</div>{items}',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array(
			'name' => 'title',
			'type' => 'raw',
			'value' => 'CHtml::link($data->title,array("advert/view", "id" => $data->id))',
		),
		array(
            'class' => 'editable.EditableColumn',
			'name' => 'member_count',
			'type' => 'raw',
			'value' => '$data->member_count ? $data->member_count : ""',
			'editable' => array(
				'type' => 'text',
				'url' => $this->createUrl('advert/ajaxUpdate'),
			),
		),
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'activities',
			'type' => 'raw',
			'value' => '$data->getActivityType(true)',
			//'filter' => $model->activityType,
			'editable' => array(
				'type' => 'checklist',
				'source' => $model->activityType,
				'url' => $this->createUrl('advert/ajaxUpdate'),
			),
		),
		array(
            'class' => 'editable.EditableColumn',
			'name' => 'date',
			'type' => 'text',
			'value' => '$data->date ? Yii::app()->dateFormatter->format(\'d MMM yyyy\',$data->date) : ""',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'date',
                'attribute' => 'date',
                'options' => array(
                    'dateFormat' => 'dd.mm.yy',
                    'changeYear' => true
                ),
            ), true),
			'editable' => array(
				'type'       => 'date',
				'url' => $this->createUrl('advert/ajaxUpdate'),
				'viewformat' => 'dd M yyyy',
				'params' => array(
					'weekStart' => 1,
				),
			),
		),
		array(
			'class'=>'ButtonColumn',
			'template' => '{delete}',
		),
	),
)); ?>