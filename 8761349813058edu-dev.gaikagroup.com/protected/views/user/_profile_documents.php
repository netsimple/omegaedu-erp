<?php if ($documents): ?>
<table>
        <?php foreach ($documents as $curr_doc):
        ?>
            <?php
                if ($curr_doc->filesize > 1024 * 1024) {
                    $size = (int)($curr_doc->filesize / (1024 * 1024));
                    $size .= ' Мб';
                }
                elseif ($curr_doc->filesize > 1024) {
                    $size = (int)($curr_doc->filesize / 1024);
                    $size .= ' Кб';
                }
                else {
                    $size = $curr_doc->filesize . ' байт';
                }
                echo '<tr>';
                echo '<td width="30%">'.CHtml::link($curr_doc->filetitle,Yii::app()->getBaseUrl().'/'.Yii::app()->params['uploadDirectory'].'/documents/'.$curr_doc->filename).'</td><td>['.$size.']</td>';
		if ($allowDelete)
			echo '<td>['.CHtml::link('удалить',array('document/delete', 'id' => $curr_doc->id)).']</td>';
                echo '</tr>';
            ?>
        <?php
              endforeach;
        ?>
</table>
<?php endif; ?>