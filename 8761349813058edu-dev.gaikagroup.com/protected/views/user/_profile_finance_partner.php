<?php

$js = <<<JSCRIPT
    $(document).ready(function(){
       var topCheckBoxId = 'centers-grid_c0_all';
       var idSelector = getAllIDs();
       
       function updateValues(idSelector) {
            if (idSelector) {
                $.ajax({
                    url: '{$this->createUrl('ajaxFinances', array('id' => $model->id))}',
                    type: 'post',
                    data: {ids: idSelector, datefrom: $('#datefrom').val(), dateto: $('#dateto').val()},
                    success: function(result){
                        result = JSON.parse(result);
                        $('#credited-sum').html(result.income_credit);
                        $('#paid-sum').html(result.income_debet);
                        $('#total-sum').html(result.income_debet - result.income_credit);
                        $('#total-costs').html(result.costs);
                    }
                });
            } else {
                $('#credited-sum').html(0);
                $('#paid-sum').html(0);
                $('#total-sum').html(0);
                $('#total-costs').html(0);
            }
       }
       
       function getAllIDs() {
            var arr = [];
            $('.checkbox-column input[type=checkbox]').each(function(){
                if ($(this).attr('id') != topCheckBoxId && $(this).prop('checked')) arr.push($(this).val());
            });
            return arr;
       }
       
       $('.checkbox-column input[type=checkbox]').click(function(){
            idSelector = [];
            if ($(this).attr('id') == topCheckBoxId) {
                if ($('#'+topCheckBoxId).prop('checked')) {
                    $('.checkbox-column input[type=checkbox]').each(function(){
                        if ($(this).attr('id') != topCheckBoxId) idSelector.push($(this).val());
                    });
                }
            } else {
                idSelector = getAllIDs();
            }
            updateValues(idSelector);
       });
       
       $('#datefrom, #dateto').change(function(){
            updateValues(idSelector);
       });
       
       $('#link-income').click(function(){
            $('#link-income').addClass('active');
            $('#link-costs').removeClass('active');
            $('#section-costs').hide();
            $('#section-income').show();
            return false;
       });
       
       $('#link-costs').click(function(){
            $('#link-costs').addClass('active');
            $('#link-income').removeClass('active');
            $('#section-income').hide();
            $('#section-costs').show();
            return false;
       });
       
    });
JSCRIPT;

Yii::app()->clientScript->registerScript('centers-selector',$js,CClientScript::POS_READY);

$css = <<<CSS
    a.active { font-weight: bold; text-decoration: none; }
CSS;

Yii::app()->clientScript->registerCss('links-custom-style',$css);

$financeData = $model->getFinanceIncomeData(); 

?>

<div style="margin-top: 5px; margin-bottom: 15px;"><?php echo CHtml::link('Доход','#',array('id' => 'link-income', 'class' => 'active')); ?> | <?php echo CHtml::link('Расход','#',array('id' => 'link-costs')); ?></div>

<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'centers-grid',
    'dataProvider'=>$model->centerFinanceDP,
    'template' => '{items}{pager}',
    'columns'=>array(
        array(
            'class' => 'CCheckBoxColumn',
            'selectableRows' => 2,
            'checked' => 'true',
        ),
        'name' => array(
            'name' => 'name',
            'type' => 'raw',
            'value' => 'CHtml::link(CHtml::encode($data->name),array("trcenter/view", "id" => $data->id))',
        ),
    ),
));

?>

<div id="section-income">

    <div style="margin-top: 5px; margin-bottom: 5px;">Начислено:&nbsp;<span id="credited-sum"><?php echo $financeData->income_credit; ?></span></div>
    <div style="margin-top: 5px; margin-bottom: 5px;">Оплачено:&nbsp;<span id="paid-sum"><?php echo $financeData->income_debet; ?></span></div>
    
    <div style="margin-top: 15px;">Итого:&nbsp;<span id="total-sum"><?php echo $financeData->income_debet - $financeData->income_credit; ?></span></div>

</div>

<div id="section-costs" style="display: none;">

    <div style="margin-top: 15px;">Итого:&nbsp;<span id="total-costs"><?php echo $modelCosts->summaryCostsValue; ?></span></div>
    
</div>