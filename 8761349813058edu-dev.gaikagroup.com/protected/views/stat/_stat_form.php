<?php

$js = <<<JSCRIPT
    $(document).ready(function(){
        $('#StatFilterForm_type input').attr('checked', 'checked');
    });
JSCRIPT;

Yii::app()->clientScript->registerScript('auto-check-all',$js,CClientScript::POS_READY);

?>
<form method="post" id = "filterForm">
<div class="form-inline">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'stat-filter-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <div class="row">
        <div class="span-5">
            Дата, от
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'id' => 'datefrom',
                'attribute' => 'datefrom',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
                'htmlOptions' => array('size' => 15),
            ));
            ?>
        </div>
        <div class="span-4">
            до
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'id' => 'dateto',
                'attribute' => 'dateto',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
                'htmlOptions' => array('size' => 15),
            ));?>
        </div>
        <div class="span-10">
            <?php echo $form->checkBoxList($model, 'type', $model->types, array('separator'=>' ')); ?>
            <?php if (count($model->types) > 1): ?>
            <a href="#" onclick="$('#StatFilterForm_type input').attr('checked', 'checked'); return false;">Все</a>
            <?php endif; ?>
        </div>
        </div>
    </div>

    <div class="clear"></div>


<script type="text/javascript">

    function showInputs(source) {
        $('#filterInputs > span').hide();

        switch(parseInt(source)){
            case <?php echo StatFilterForm::SOURCE_PARTNER; ?>:

                $('#StatFilterForm_partner_id').parent().show();
                break;

            case <?php echo StatFilterForm::SOURCE_CENTER; ?>:

                $('#StatFilterForm_center_id').load('<?php echo Yii::app()->request->baseUrl; ?>/stat/getCenters', function(){
                    $('#StatFilterForm_center_id').change();
                }).parent().show();
                break;

            case <?php echo StatFilterForm::SOURCE_GROUP; ?>:

                $('#StatFilterForm_group_id').load('<?php echo Yii::app()->request->baseUrl;?>/stat/getGroups').parent().show();
                break;
        }
    }

    $(function(){
        $("#StatFilterForm_source").change(function(){
            $('#filterInputs input').val('');
            $('#filterInputs span select').val('');
            showInputs($(this).val());
        });

        $( "#filterForm" ).on( "change", "#StatFilterForm_group_id_0", function() {
            $('#StatFilterForm_group_id input').prop('checked', $(this).prop('checked'));
        });

        $('#StatFilterForm_center_id').change(function(){
            $('#StatFilterForm_group_id').load('<?php echo Yii::app()->request->baseUrl;?>/stat/getGroups?center_id='+$(this).val()).parent().show();
        });
        
        $('#export-button').click(function(){
            $('#filterForm').attr('action','<?php echo $this->createUrl('csv', array('action' => $action)); ?>');
            return true;
        });
        
        $('#show-button').click(function(){
            $('#filterForm').attr('action','<?php echo $this->createUrl($action); ?>');
            return true;
        });

        showInputs($("#StatFilterForm_source").val());
    })
</script>


    <div class="row" style="margin-top: 12px;">
        <div class="row" id="filterInputs">
            <div class="span-4">

                <?php echo $form->dropDownList($model, 'source', $model->getSources(!Yii::app()->user->checkAccess('owner'))); ?>

                <?php if (Yii::app()->user->checkAccess('owner')) { ?>
                    <div class="cityList">
                        <?php echo $form->dropDownList($model, 'city_id', City::listArray(),array('empty' => 'Все города'));?>
                    </div>
                <?php } ?>

            </div>
            <span class="span-6 hidden">
                <script type="text/javascript">
                    $(function(){
                        $('#StatFilterForm_partner_id').click(function(){
                            var ids = [];
                            $(this).find('input:checked').each(function(){
                                ids.push($(this).val());
                            })

                            $('#StatFilterForm_center_id').load('<?php echo Yii::app()->request->baseUrl; ?>/stat/getCenters', {partner_id:ids}, function(){$(this).change();}).parent().show();
                        });
                    })
                </script>
                <?php echo $form->checkBoxList($model, 'partner_id',
                    CHtml::listData(
                        Yii::app()->user->checkAccess('owner') ?
                            UserMain::model()->findAll('(is_partner = 1 OR is_owner = 1) AND is_deleted = 0') :
                            array()
                        , 'id', 'fullname'
                    ));
                ?>
            </span>
            <span class="span-3 hidden">
                <?php echo $form->dropDownList($model, 'center_id', array(), array('style'=>'width: 100%;')); ?>
            </span>
            <span class="span-18 hidden">
                <?php echo $form->checkboxList($model, 'group_id', array(), array('style'=>'width: 100%;')); ?>
            </span>

        </div>

        <div class="clear"></div>

        <div class="row submit text-align-right" style="margin-top: 12px;">
            <?php echo CHtml::submitButton('Отобразить', array('id' => 'show-button', 'class' => 'btn')); ?>
            <?php echo CHtml::submitButton('Экспортировать',array('id' => 'export-button', 'class' => 'btn')); ?>
        </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->
</form>

