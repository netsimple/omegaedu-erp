<style>h2 {margin-top: 25px; margin-bottom: 15px;} .labels { margin-top: 10px; } .chart-label a {text-decoration: none; color: #FFFFFF;}</style>

<?php $this->renderPartial('_stat_form', array('model'=>$statFilter, 'action' => $action)); ?>

<?php foreach ($model->getStat() as $currKey => $currentChart): ?>
    <h2><?php echo $model->getChartLabel($currKey); ?></h2>
    <div style="padding-bottom: 20px;">
        <?php

            $data = array(
                'width' => 600,
                'height' => 300,
                'htmlOptions' => array(),
                'options' => array()
            );
            if (($object = $model->getChartObject($currKey)) == 'ChDoughnut' || $object == 'ChPie') $data['drawLabels'] = true;
            if ($object == 'ChBars' || $object == 'ChLine') $data['options']['scaleLabel'] = '<%=Math.ceil(value) == value ? Math.ceil(value) : ""%>';
            $this->widget('chartjs.widgets.'.$object, array_merge($data,$currentChart));
            if ($object == 'ChBars' || $object == 'ChLine') $this->renderPartial('_label',$currentChart);

        ?>
    </div>
<?php endforeach; ?>