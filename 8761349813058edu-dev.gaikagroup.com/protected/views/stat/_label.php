<?php

if (isset($datasets) && is_array($datasets)) {
    echo '<div class="labels">';
    foreach ($datasets as $currentElem) {
        if (!isset($currentElem['strokeColor']) || !isset($currentElem['fillColor']) || !isset($currentElem['label'])) continue;
        echo "<span class=\"chart-label\" style=\"background-color: {$currentElem['strokeColor']};\">
                {$currentElem['label']}
              </span>";
    }
    echo '</div>';
}

?>