<?php
/**
 * Global defination for CGridView
 */
return array(
	'default'=>array(
		'cssFile'=>Yii::app()->baseUrl.'/css/skins/gridview/styles.css',
        'summaryText'=>'{start}-{end} из {count}',
        'columns' => array(
                array(
                    'header'=>'№',
                    'value'=>'($this->grid && $this->grid->dataProvider && $this->grid->dataProvider->pagination && isset($this->grid->dataProvider->pagination->currentPage)) ?
                        ++$row + ($this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize) : ++$row',
            )
        )
	)
);