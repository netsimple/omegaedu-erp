<?php if ($clientId = Yii::app()->user->getFlash('notice')): ?>
<div style="border: 1px solid yellow; background: lightyellow; padding-left: 15px; padding-top: 5px; padding-bottom: 5px; margin-bottom: 15px; margin-top: 10px;">
	Проверьте <?php echo CHtml::link(CHtml::encode('статус клиента'),array('client/view','id' => $clientId),array('target' => '_blank')); ?>. 
    При необходимости вы можете изменить его, проследовав по <?php echo CHtml::link(CHtml::encode('этой ссылке'),array('client/update','id' => $model->id),array('target' => '_blank')); ?>.
</div>
<?php echo CHtml::link('Перейти к просмотру договора',array('pdf', 'id' => $model->id)); ?> | <?php echo CHtml::link('Перейти к списку договоров',array('index')); ?>
<?php endif; ?>