<?php

$redirectUrl = Yii::app()->createAbsoluteUrl('contract/create');

$js = <<<JSCRIPT
	$(document).ready(function(){
		$('#continue-btn').click(function(){
			if ($('#client-id').val()) {
				location.href = '$redirectUrl/id/' + $('#client-id').val();
			}
		});
	});
JSCRIPT;

Yii::app()->clientScript->registerScript('clientAutoComplete',$js,CClientScript::POS_READY);

?>

<div class="form">
	
	<fieldset style="border: 1px solid black;">
 
    <div class="row">
        <?php echo CHtml::label('Имя заявки/клиента в системе:',''); ?>
        <?php $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
			'name'=>'client_name',
			'sourceUrl' => array('getClientName'),
			'options'=>array(
				'minLength'=>'2',
				'select' =>'js: function(event, ui) {
					this.value = ui.item.fullname;
					$("#client-id").val(ui.item.id);
					return false;
				}',
			),
			'htmlOptions'=>array(
				'size'=>'75',
			),
		)); ?>
		<?php echo CHtml::hiddenField('client_id','0',array('id' => 'client-id')); ?>
    </div>
	
	</fieldset>
 
    <div class="row submit">
        <?php echo CHtml::button('Продолжить',array('id' => 'continue-btn')); ?>
    </div>

</div><!-- form -->