<?php
    function printDate($date) {
        $date = explode('-',$date);
        $date[1] = (int)$date[1];
        switch ($date[1]) {
            case 1:  $date[1] = 'января'; break;
            case 2:  $date[1] = 'февраля'; break;
            case 3:  $date[1] = 'марта'; break;
            case 4:  $date[1] = 'апреля'; break;
            case 5:  $date[1] = 'мая'; break;
            case 6:  $date[1] = 'июня'; break;
            case 7:  $date[1] = 'июля'; break;
            case 8:  $date[1] = 'августа'; break;
            case 9:  $date[1] = 'сентября'; break;
            case 10: $date[1] = 'октября'; break;
            case 11: $date[1] = 'ноября'; break;
            case 12: $date[1] = 'декабря'; break;
        }
        echo "«{$date[2]}» {$date[1]} {$date[0]}  г.";
        return;
    }
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE>ДОГОВОР № <?php echo $model->id; ?> </TITLE>
    <META NAME="GENERATOR" CONTENT="LibreOffice 3.6  (Linux)">
    <META NAME="AUTHOR" CONTENT="Евгений">
    <META NAME="CREATED" CONTENT="20110910;1470000">
    <META NAME="CHANGEDBY" CONTENT="Admin">
    <META NAME="CHANGED" CONTENT="20110910;1470000">
    <META NAME="Info 1" CONTENT="">
    <META NAME="Info 2" CONTENT="">
    <META NAME="Info 3" CONTENT="">
    <META NAME="Info 4" CONTENT="">
    <STYLE TYPE="text/css">
        <!--
        @page {
            margin-right: 0.59in;
            margin-top: 0.24in;
            margin-bottom: 0.49in
        }

        P {
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            widows: 2;
            orphans: 2
        }

        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ru-RU
        }

        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt
        }

        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 11pt;
            so-language: ar-SA
        }

        A:link {
            color: #0000ff
        }

        -->
    </STYLE>
</HEAD>
<BODY LANG="en-US" TEXT="#000000" LINK="#0000ff" DIR="LTR">

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                        STYLE="font-size: 11pt"><B>ДОГОВОР
                № <?php echo $model->id; ?> </B></FONT></FONT>
</P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                        STYLE="font-size: 11pt"><I><B>ОБ
                    ОКАЗАНИИ КОНСУЛЬТАТИВНЫХ УСЛУГ</B></I></FONT></FONT></P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><BR>
</P>

<P STYLE="widows: 2; orphans: 2">
    <FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                               STYLE="font-size: 11pt">г.
                    Санкт-Петербург
                    <?php printDate($model->date); ?></FONT></FONT></FONT></FONT></P>

<P STYLE="widows: 2; orphans: 2"><BR>
</P>

<P CLASS="western"><FONT SIZE=2 STYLE="font-size: 11pt">Индивидуальный
        Предприниматель Горелый Николай
        Евгеньевич, именуемый в дальнейшем
    </FONT><FONT SIZE=2 STYLE="font-size: 11pt"><B>«Исполнитель»</B></FONT><FONT SIZE=2 STYLE="font-size: 11pt">,
        в лице Горелого Н.Е., действующий на
        основании Свидетельства, с одной стороны,
    </FONT>
</P>

<P CLASS="western"><FONT SIZE=2 STYLE="font-size: 11pt">и
        гражданин
        (-ка) <b><?php echo $model->parent_name; ?></b>,</FONT></P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                        STYLE="font-size: 11pt"><I>(Ф
                И О родителя/законного представителя
                несовершеннолетнего)</I></FONT></FONT></P>

<P STYLE="widows: 2; orphans: 2"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">именуемый
                    (-ая) в дальнейшем – </FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                                 STYLE="font-size: 11pt"><B>«Заказчик»,</B></FONT></FONT></FONT></FONT>
</P>

<P STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">и
            гражданин (-ка) <b><?php echo $model->client_name; ?></b>,</FONT></FONT></P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">(</FONT></FONT><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><I>Ф
                        И О несовершеннолетнего)</I></FONT></FONT></FONT></FONT></P>

<P STYLE="widows: 2; orphans: 2"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">именуемый
                    (-ая) в дальнейшем </FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                               STYLE="font-size: 11pt"><B>–
                        «Потребитель»</B></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                                 STYLE="font-size: 11pt">,
                    с другой стороны, </FONT></FONT></FONT></FONT>
</P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">при
                    совместном упоминании именуемые
                </FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B>«Стороны»,</B></FONT></FONT><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">
                    заключили настоящий Договор о
                    нижеследующем:</FONT></FONT></FONT></FONT></P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><BR>
</P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                        STYLE="font-size: 11pt"><B>1.&nbsp;ПРЕДМЕТ
                ДОГОВОРА</B></FONT></FONT></P>

<P CLASS="western" ALIGN=JUSTIFY>
    <FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt">1.1.&nbsp;Исполнитель
            оказывает Потребителю, а Заказчик
            оплачивает консультационные услуги по
            общеобразовательным предметам. </FONT></FONT>
</P>

<P CLASS="western" ALIGN=JUSTIFY>
    <FONT SIZE=2 STYLE="font-size: 11pt">1.2.&nbsp;Характеристика
        консультационных услуг: знакомство с
        требованиями, предъявляемыми к изучению
        предмета, особенностями подготовки.</FONT></P>

<P CLASS="western" ALIGN=JUSTIFY><FONT SIZE=2 STYLE="font-size: 11pt">1.2.1.&nbsp;Цель
        оказания консультационных услуг:
        консультации Потребителя по
        общеобразовательным предметам, по
        подготовке к сдаче единого государственного
        экзамена (ЕГЭ), по поступлению в высшие
        учебные заведения. </FONT>
</P>

<P CLASS="western">
    <FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt">1.2.2.&nbsp;Для
            достижения цели, указанной в п.1.2.1.
            Договора, Исполнитель проводит
            консультации по следующим предмету
            (предметам)</FONT></FONT></P>

<?php for ($i = 0; $i < 5; $i++): ?>
    <?php if (isset($model->courses[$i])): ?>
        <P CLASS="western">
            <FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt"><?php echo $model->courses[$i]->course->name; ?>
                    (всего <?php echo $model->courses[$i]->total_time; ?> часов)</FONT></FONT></P>
    <?php else: ?>
        <P CLASS="western">
            <FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt">___________________
                    (всего ________ часов)</FONT></FONT></P>
    <?php endif; ?>
<?php endfor; ?>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">1.2.3.
                    Форма проведения консультаций – очная.
                </FONT></FONT></FONT></FONT>
</P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">1.2.4.
            Время проведения консультаций - по
            согласованию «Сторон».</FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">1.2.5.
                    Срок оказания услуги с <?php printDate($model->start_date); ?>
                    по <?php printDate($model->end_date); ?></FONT></FONT>
</P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">1.2.6.
            Количество человек в группе:</FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">-
                    мини-группы - до 10 человек.</FONT></FONT></FONT></FONT></P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><BR>
</P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                        STYLE="font-size: 11pt"><B>2.
                ОБЯЗАННОСТИ СТОРОН</B></FONT></FONT></P>

<P STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B>2.1.&nbsp;
                Исполнитель обязан:</B></FONT></FONT></P>

<P CLASS="western" ALIGN=JUSTIFY>
    <FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt">2.1.1.
            Зачислить в группу на проведение
            консультаций Потребителя, выполнившего
            установленные Исполнителем условия
            приема.</FONT></FONT></P>

<P CLASS="western" ALIGN=JUSTIFY>
    <FONT SIZE=2 STYLE="font-size: 11pt">2.1.2. Организовать
        и обеспечить надлежащее оказание
        консультативных услуг. </FONT>
</P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">Для
            выполнения обязательства, предусмотренного
            настоящим подпунктом, Исполнитель
            формирует высококвалифицированный
            корпус преподавателей-консультантов.</FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.1.3.
                    Предоставить для проведения консультаций
                    помещения, соответствующие санитарным
                    и гигиеническим требованиям.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.1.4.
            Своевременно информировать Заказчика
            о режиме проведения консультаций (место
            проведения, периодичность, расписание,
            время начала и окончания консультаций).</FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.1.5.
            Проявлять уважение к личности Потребителя,
            оберегать его от всех форм физического
            и психологического насилия. </FONT></FONT>
</P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.1.6.
                    Сохранить место за Потребителем в случае
                    не посещения консультаций по причинам,
                    возникшим не по вине Потребителя
                    (болезнь, карантин и т.п.), или по
                    уважительным причинам (отпуск родителей,
                    каникулы и т.п.), если данные причины
                    будут документально подтверждены
                    (медицинской справкой, письменным
                    заявлением родителей или законных
                    представителей) при условии оплаты
                    Заказчиком данных консультаций. </FONT></FONT></FONT></FONT>
</P>

<P STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B>2.2.
                Заказчик обязан:</B></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.2.1.
                    Вносить плату за оказание Потребителю
                    консультативной услуги.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.2.2.
                    Обеспечить посещение Потребителем
                    консультаций в соответствии с расписанием,
                    утвержденным Исполнителем.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.2.3.
                    Обеспечить Потребителя за свой счет
                    учебными пособиями, рекомендуемыми
                    Исполнителем, а также иными предметами,
                    необходимыми для надлежащего оказания
                    Исполнителем консультативных услуг, в
                    количестве, соответствующем возрасту
                    и потребностям Потребителя.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.2.4.
                    Сообщать Исполнителю об изменении
                    контактного телефона. </FONT></FONT></FONT></FONT>
</P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.2.5.
            Извещать Исполнителя об уважительных
            причинах отсутствия Потребителя на
            консультациях.</FONT></FONT></P>

<P CLASS="western" ALIGN=JUSTIFY>
    <FONT SIZE=2 STYLE="font-size: 11pt">2.2.6.&nbsp;Нести
        ответственность за жизнь и здоровье
        Потребителя во время проезда на
        консультации.</FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.2.7.
                    Проявлять уважение к преподавателям-консультантам,
                    администрации и техническому персоналу
                    Исполнителя.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.2.8.
            Возмещать ущерб, причиненный Потребителем
            имуществу Исполнителя, в соответствии
            с законодательством Российской Федерации.</FONT></FONT></P>

<P STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B>2.3.
                Потребитель обязан:</B></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.3.1.
                    Посещать консультации в соответствии
                    с планом, утвержденным Исполнителем.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.3.2.
            Выполнять рекомендации
            преподавателей-консультантов.</FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.3.3.
            Соблюдать учебную дисциплину и
            общепринятые нормы поведения, в частности,
            проявлять уважение к преподавателям-консультантам,
            администрации и техническому персоналу
            Исполнителя и другим слушателям, не
            посягать на их честь и достоинство.</FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.3.4.
            Бережно относиться к имуществу
            Исполнителя.</FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">2.3.5.
            Иметь на консультациях сменную обувь.</FONT></FONT></P>

<P STYLE="widows: 2; orphans: 2"><BR>
</P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                        STYLE="font-size: 11pt"><B>3.
                ПРАВА СТОРОН</B></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B>3.1.
                Исполнитель вправе:</B></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">3.1.1.
            Проводить консультации и определять
            уровень знаний Потребителя по конкретным
            предметам в рамках программы, по которой
            идет консультативный процесс, в течение
            всего периода консультаций.</FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">3.1.2.
            Вносить изменения в утвержденное
            расписание</FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">3.1.3.
                    Заменять преподавателя в связи с
                    производственной необходимостью.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B>3.2.
                Заказчик вправе:</B></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">3.2.1.
                    Требовать от Исполнителя предоставления
                    информации по вопросам, касающимся
                    организации и обеспечения надлежащего
                    оказания консультативных услуг. </FONT></FONT></FONT></FONT>
</P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">3.2.2.
            Требовать от Исполнителя предоставления
            информации о поведении, отношении
            Потребителя к консультациям в целом и
            по отдельным дисциплинам.</FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B>3.3.
                Потребитель вправе:</B></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">3.3.1.
                    Обращаться к работникам Исполнителя
                    по всем вопросам, возникающим в процессе
                    оказания консультативных услуг. </FONT></FONT></FONT></FONT>
</P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">3.3.2.
            Получать полную и достоверную информацию
            об оценке своих знаний и умений.</FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">3.3.3.
                    Пользоваться имуществом Исполнителя,
                    необходимым для оказания консультативных
                    услуг.</FONT></FONT></FONT></FONT></P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><BR>
</P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                        STYLE="font-size: 11pt"><B>4.
                ОПЛАТА УСЛУГ</B></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 0; orphans: 0"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">4.1.&nbsp;Цена
                    консультативной
                    услуги <?php if ($model->has_discount == 0) echo '<span style="text-decoration: underline;">без учета</span>/с учетом скидки'; else echo 'без учета/<span style="text-decoration: underline;">с учетом скидки</span>'; ?>
                    (в соответствии с утвержденным у Исполнителя Положением о скидке по следующему основанию
                    - <?php if ($model->discount_reason) echo $model->discount_reason; else echo '____________________'; ?>
                    ):</FONT></FONT></FONT></FONT></P>

<?php for ($i = 0; $i < 5; $i++): ?>
    <?php if (isset($model->courses[$i])): ?>
        <?php $currentCourse = $model->courses[$i]; ?>
        <P ALIGN=JUSTIFY STYLE="widows: 0; orphans: 0"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                        COLOR="#000000"><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                  STYLE="font-size: 11pt"><?php echo $i + 1; ?>
                                )
                            </FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                           STYLE="font-size: 11pt">по
                            предмету <b><?php echo $currentCourse->course->name; ?></b> за 1 период (
                        </FONT></FONT><FONT COLOR="#000000"><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                                      STYLE="font-size: 11pt"><?php echo $currentCourse->period_time; ?>
                                часов) составляет <b><?php echo $currentCourse->period_price; ?></b>
                                рублей;</FONT></FONT></FONT></FONT></FONT></P>
    <?php else: ?>
        <P ALIGN=JUSTIFY STYLE="widows: 0; orphans: 0"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                        COLOR="#000000"><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                  STYLE="font-size: 11pt"><?php echo $i + 1; ?>
                                )
                            </FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                           STYLE="font-size: 11pt">по
                            предмету _________________ за 1 период ( </FONT></FONT><FONT COLOR="#000000"><FONT
                            FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">________
                                часов) составляет __________ рублей;</FONT></FONT></FONT></FONT></FONT></P>
    <?php endif; ?>
<?php endfor; ?>


<P ALIGN=JUSTIFY STYLE="widows: 0; orphans: 0"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">4.2.
                    Оплата консультативной услуги происходит
                    перед началом течения очередного,
                    указанного в пункте 4.1, периода ее
                    оказания Потребителю. </FONT></FONT></FONT></FONT>
</P>

<P ALIGN=JUSTIFY STYLE="widows: 0; orphans: 0"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">4.3.
                    Зачисление Потребителя на консультации
                    производится после поступления
                    Исполнителю платежа от Заказчика за
                    первый период оказания услуги в
                    соответствии с порядком оплаты,
                    предусмотренным пунктом 4.1.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 0; orphans: 0"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">4.4.
                    По желанию Заказчик вправе осуществить
                    оплату консультативной услуги за весь
                    или оставшийся срок единовременно.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">4.5.
                    В случае пропуска Потребителем оплаченных
                    Заказчиком консультаций в соответствии
                    с утвержденным расписанием, денежные
                    средства Исполнителем не возвращаются
                    независимо от причин пропуска.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">4.6.
                    В том случае, если консультации не
                    состоялись по не зависящим от воли
                    Сторон причинам, таким как: военные
                    действия, стихийные бедствия, катастрофы,
                    решения органов государственной власти,
                    террористические акты или их угроза и
                    тому подобным, денежные средства
                    Исполнителем за несостоявшиеся занятия
                    не возвращаются.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">4.7.
                    В случае нарушения Заказчиком по его
                    вине порядка оплаты консультационных
                    услуг (пункт 4.2) Исполнитель имеет право
                    не допустить Потребителя до посещения
                    консультаций в неоплаченном периоде.
                    Денежные средства за не посещенные в
                    таком случае консультации Исполнителем
                    не возвращаются. </FONT></FONT></FONT></FONT>
</P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">4.8.
                    В случае нарушения Заказчиком порядка
                    оплаты консультационных услуг (пункт
                    4.2) Заказчик обязан уплатить Исполнителю
                    проценты на неуплаченную сумму с момента
                    начала течения очередного неоплаченного
                    периода оказания консультационной
                    услуги до момента фактической оплаты.
                    Размер процентов определяется существующей
                    в месте нахождения Исполнителя ставкой
                    рефинансирования Центрального Банка
                    Российской Федерации.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><BR>
</P>

<P ALIGN=CENTER STYLE="widows: 0; orphans: 0"><BR>
</P>

<P ALIGN=CENTER STYLE="widows: 0; orphans: 0"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B>5.
                        ОСОБЫЕ УСЛОВИЯ</B></FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 0; orphans: 0"><FONT FACE="Courier New, monospace"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">5.1.
                    Дата начала занятий определяется
                    степенью формирования группы, дата
                    окончания - завершением прохождения
                    образовательного курса.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 0; orphans: 0"><BR>
</P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B>6.
                        ОСНОВАНИЯ ДЛЯ ИЗМЕНЕНИЯ ИЛИ РАСТОРЖЕНИЯ
                        ДОГОВОРА</B></FONT></FONT></FONT></FONT></P>

<P CLASS="western" ALIGN=JUSTIFY>
    <FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt">6.1.
            Условия, на которых заключен настоящий
            Договор, могут быть изменены либо по
            соглашению Сторон, либо в соответствии
            с действующим законодательством
            Российской Федерации.</FONT></FONT></P>

<P CLASS="western" ALIGN=JUSTIFY>
    <FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt">6.2.
            Настоящий Договор может быть расторгнут
            по соглашению Сторон.</FONT></FONT></P>

<P CLASS="western" ALIGN=JUSTIFY><FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt">6.3.
        </FONT></FONT><FONT SIZE=2 STYLE="font-size: 11pt">Заказчик
        вправе отказаться от оказания
        консультационной услуги при условии
        оплаты Исполнителю фактически понесенных
        им расходов.</FONT></P>

<P CLASS="western" ALIGN=JUSTIFY>
    <FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt">6.4.
            Исполнитель не возвращает денежные
            средства за консультационные услуги,
            оказанные к моменту расторжения договора.
        </FONT></FONT>
</P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><BR>
</P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><SPAN
                        LANG="en-US"><B>7</B></SPAN></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                                            STYLE="font-size: 11pt"><B>.
                        ОТВЕТСТВЕННОСТЬ ЗА НЕИСПОЛНЕНИЕ ИЛИ
                        НЕНАДЛЕЖАЩЕЕ</B></FONT></FONT></FONT></FONT></P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Times New Roman, serif"><FONT SIZE=2
                                                                                        STYLE="font-size: 11pt"><B>ИСПОЛНЕНИЕ
                ОБЯЗАТЕЛЬСТВ ПО НАСТОЯЩЕМУ ДОГОВОРУ</B></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">7.1.
                    В случае неисполнения или ненадлежащего
                    исполнения Сторонами обязательств по
                    настоящему Договору они несут
                    ответственность, предусмотренную
                    российским гражданским законодательством.
                </FONT></FONT></FONT></FONT>
</P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">7.2.
                    Все споры и разногласия, вытекающие из
                    настоящего Договора или возникающие в
                    связи с ним, разрешаются путем переговоров.
                    В случае, если между Сторонами не
                    достигнуто соглашение, спор рассматривается
                    в порядке, предусмотренном российским
                    гражданско-процессуальным законодательством.</FONT></FONT></FONT></FONT></P>

<P STYLE="widows: 2; orphans: 2"><BR>
</P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B>8.
                        СРОК ДЕЙСТВИЯ ДОГОВОРА</B></FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">8.1.
                    Настоящий Договор вступает в силу со
                    дня его подписания Сторонами и действует
                    до момента выполнения Исполнителем
                    обязательств по оказанию консультативных
                    услуг.</FONT></FONT></FONT></FONT></P>

<P ALIGN=JUSTIFY STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt">8.2.
                    Договор составлен в двух экземплярах:
                    один – для Исполнителя и один для
                    Заказчика. Оба экземпляра имеют равную
                    юридическую силу.</FONT></FONT></FONT></FONT></P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2; page-break-before: always">
    <BR>
</P>

<P ALIGN=CENTER STYLE="widows: 2; orphans: 2"><FONT FACE="Arial, sans-serif"><FONT SIZE=2><FONT
                FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B>9.
                        ПОДПИСИ СТОРОН</B></FONT></FONT></FONT></FONT></P>

<P CLASS="western" ALIGN=JUSTIFY>
    <FONT SIZE=2 STYLE="font-size: 11pt"><B>Исполнитель:</B></FONT></P>

    <style type="text/css">.ispolnitel p {font-size:12px; line-height: 2px;}</style>
    <div class="ispolnitel">
    <?php if ($model->partner && $model->partner->user && (!$model->partner->user->is_owner || $model->partner->legal_addr && $model->partner->real_addr && $model->partner->contacts && $model->partner->details)): ?>
        <p>ИП <?php echo $model->partner->name; ?></p>
        <p>Фактический адрес: <?php echo $model->partner->real_addr; ?></p>
        <p></p>
        <p><?php echo $model->partner->contacts; ?></p>
        <p>Юридический адрес: <?php echo $model->partner->legal_addr; ?></p>
        <p>
            <?php echo nl2br($model->partner->details); ?>
        </p>
    <?php else: ?>
        <p>ИП Горелый Николай Евгеньевич</p>
        <p>Фактический адрес: Санкт-Петербург, ул. Рылеева., д.4 кв.7, ст.м. Чернышевская.</p>
        <p></p>
        <p>тел.: (812)-952 94 73, мобильная линия 906-256-67-90</p>
        <p>Юридический адрес: 196191, Санкт-Петербург, ул. Варшавская д. 61/1 кв. 153</p>
        <p>ИНН 780723236417</p>
        <p>Филиал «Петровский» ОАО Банк «ОТКРЫТИЕ» г.Санкт-Петербург.</p>
        <p>Расчетный счет №40802810601000119130</p>
        <p>№30101810400000000766 в ГРКЦ ГУ Банка России по Санкт-Петербургу;</p>
        <p>БИК 044030766</p>
    <?php endif; ?>
    <p><br/></p>
</div>

<style type="text/css">.users p {margin: 2px;}</style>
<div class="users">

    <P CLASS="western">
        <FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt"><B>Заказчик:</B></FONT></FONT><br/><br/>
        <FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt">

                (ФИО) <?php echo $model->parent_name; ?><BR>
                Паспорт:
                серия, номер
                <?php echo $model->parent_pass_num; ?><br/>
                Кем выдан: <?php echo $model->parent_pass_where; ?></FONT></FONT></P>

    <P CLASS="western">
        <FONT SIZE=2 STYLE="font-size: 11pt">Дата выдачи  <?php printDate($model->parent_pass_when); ?><br>
            Зарегистрирован по
            адресу: индекс <?php echo $model->parent_addr_index; ?></FONT></P>

    <P CLASS="western">
        <FONT SIZE=2 STYLE="font-size: 11pt"><?php echo $model->parent_addr; ?></FONT></P>

    <P CLASS="western">
        <FONT SIZE=2 STYLE="font-size: 11pt">Телефон (дом.)  <?php echo $model->parent_phone_home; ?>,
            (раб/сот.)   <?php echo $model->parent_phone_mobile; ?></FONT></P>

    <P CLASS="western">
        <BR>
    </P>

    <P CLASS="western">
        <FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt"><B>Потребитель:</B><br/></FONT></FONT><FONT COLOR="#000000"><FONT SIZE=2 STYLE="font-size: 11pt">
            </FONT></FONT>
    </P>

    <P CLASS="western">
        <FONT SIZE=2 STYLE="font-size: 11pt">(ФИО) <?php echo $model->client_name; ?></FONT></P>

    <P CLASS="western">
        <FONT SIZE=2 STYLE="font-size: 11pt">Паспорт: серия,
            номер
            <?php echo $model->client_pass_num; ?></FONT></P>

    <P CLASS="western">
        <FONT SIZE=2 STYLE="font-size: 11pt">Кем выдан: <?php echo $model->client_pass_where; ?></FONT></P>

    <P CLASS="western">
        <FONT SIZE=2 STYLE="font-size: 11pt">Дата выдачи: <?php printDate($model->client_pass_when); ?> <br>
            Зарегистрирован по адресу:
            индекс <?php echo $model->client_addr_index; ?></FONT></P>

    <P CLASS="western">
        <FONT SIZE=2 STYLE="font-size: 11pt"><?php echo $model->client_addr; ?><BR>Телефон
            (дом.)  <?php echo $model->client_phone_home; ?>, (раб/сот.)   <?php echo $model->client_phone_mobile; ?></FONT>
    </P>

</div>


<P CLASS="western" ALIGN=JUSTIFY>
    <BR>
</P>






<P CLASS="western" ALIGN=JUSTIFY style="width: 33%; float: left; font-size: 11px;">
    <FONT SIZE=2 STYLE="font-size: 11pt"><B>ИСПОЛНИТЕЛЬ:</B></FONT>

    <br>Гражданин (-ка)<br><br><br><br>
    __________________________
</P>
<P CLASS="western" ALIGN=JUSTIFY style="width: 33%; float: left; font-size: 11px;">
    <FONT SIZE=2 STYLE="font-size: 11pt"><B>ЗАКАЗЧИК:</B></FONT>
    <br>Гражданин (-ка) <br><br><br><br>
    __________________________
</P>
<P CLASS="western" ALIGN=JUSTIFY style="width: 33%; float: left; font-size: 11px;">
    <FONT SIZE=2 STYLE="font-size: 11pt"><B>ПОТРЕБИТЕЛЬ:</B></FONT>
    <br> <br><br><br><br>
    __________________________
</P>

<P CLASS="western" ALIGN=JUSTIFY><BR>
</P>


<P CLASS="western" ALIGN=RIGHT><SUP> </SUP><FONT SIZE=2 STYLE="font-size: 11pt"><?php printDate($model->date); ?></FONT>
</P>

</BODY>
</HTML>