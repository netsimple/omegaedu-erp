<div>
	<?php $this->widget('application.components.widgets.AddLinkWidget'); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contract-grid',
    'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search(),
	'filter' => $model,
    'afterAjaxUpdate' => 'function(){
		jQuery("#date_from_filter, #date_to_filter").datepicker({
			dateFormat: "d M yy",
			monthNames: '.CJSON::encode(array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь')).',
			monthNamesShort: '.CJSON::encode(array('янв','февр','марта','апр','мая','июня','июля','авг','сент','окт','нояб','дек')).',
			dayNames: '.CJSON::encode(array('Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота')).',
            dayNamesShort: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            dayNamesMin: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            firstDay: 1,
			changeYear: true
		});
	}',
	'columns'=>array(
        'city_id' => array(
            'name' => 'city_id',
            'type' => 'raw',
            'value'=> 'City::nameByProjectId($data->project_id)',
            'filter'=> CHtml::activeDropDownList($model,'city_id',City::listArray(),array('empty' => 'Все города')),
            'visible'=> Yii::app()->user->model->checkRole('owner'),
        ),
        array(
			'class' => 'editable.EditableColumn',
            'name' => 'status',
            'type' => 'html',
            'value' => '$data->statusList[$data->status]',
            'filter' => CHtml::activeDropDownList($model,'status',$model->statusList,array('empty' => 'Все')),
			'editable' => array(
				'type' => 'select',
				'source' => $model->statusList,
				'url' => $this->createUrl('ajaxUpdate'),
			),
        ),
		array(
			'name' => 'id',
			'type' => 'raw',
			'value'=> 'CHtml::link(CHtml::encode("Договор №").$data->id,array("contract/pdf", "id" => $data->id))',
			// 'filter'=> CHtml::encode('Договор № ').CHtml::activeTextField($model,'id',array('style' => 'width: 50px;')),
			'htmlOptions' => array(
				'width' => '15%',	
			),
		),
		array(
			'name' => 'client_name',
			'type' => 'raw',
			'value' => 'CHtml::link($data->client_name,array("client/view","id"=>$data->client_id))',
		),
		array(
			'name' => 'date',
            'type' => 'raw',
            'value' => 'Yii::app()->dateFormatter->format("dd MMM yyyy",$data->date)',
            'filter' => 'с '.
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'id' => 'date_from_filter',
                    'attribute' => 'date_from',
                    'options' => array(
                        'dateFormat' => 'd M yy',
                        'changeYear' => true
                    ),
                ), true) . ' по ' .
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'id' => 'date_to_filter',
                    'attribute' => 'date_to',
                    'options' => array(
                        'dateFormat' => 'd M yy',
                        'changeYear' => true
                    ),
                ), true),
		),
		'client_phone_mobile',
		array(
			'class'=>'ButtonColumn',
			'template' => '{update} {delete}',
		),
	),
)); ?>

<style>
    #date_from_filter, #date_to_filter {
        width: 40%;
    }
</style>