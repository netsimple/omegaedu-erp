<script type="text/javascript">
    <?php
        if (($errors = Yii::app()->user->getFlash('clientImportError'))) {
            $mess = '';
            foreach ($errors as $error) $mess .= $error[0].'\n';
            echo "$(window).load(function() { alert('$mess'); })";
        }
    ?>
</script>


<?php

$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=> array(
		'Список' => $this->renderPartial('_index_clients',array('model' => $model),true),
		'Необработанные заявки' => array('ajax'=>array('requestList')),
		'Должники' => array('ajax'=>array('debtList')),
	),
    'options'=>array(
        'collapsible'=>false,
        'selected'=>$item,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));

?>