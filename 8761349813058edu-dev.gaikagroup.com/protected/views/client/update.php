<?php
	$this->menu = array(
	    array('label'=>'Необработанные заявки', 'url'=>array('client/requests')),
	    array('label'=>'Список заявок', 'url'=>array('client/index')),
	    array('label'=>'Профиль', 'url'=>array('client/view', 'id' => $model->id)),
	);
?>

<h1>Редактирование профиля</h1>

<?php
    $this->renderPartial('_form',array('model'=>$model, 'managers' => $managers));
?>