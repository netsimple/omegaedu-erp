<h1><?php echo $model->fullname; ?></h1>

<?php

	$tabs = array();
	
	$tabs['Профиль'] = $this->renderPartial('_view_profile',array('model' => $model, 'comments' => $comments),true);
	$tabs['Обучение'] = $this->renderPartial('_view_education',array('model' => $model),true);
	$tabs['Финансы'] = $this->renderPartial('_view_finance',array('client' => $model, 'model' => $clientBalance),true);
	
	$this->widget('zii.widgets.jui.CJuiTabs', array(
		'tabs'=>$tabs,
		'options'=>array(
			'collapsible'=>false,
			'selected'=>0,
		),
		'htmlOptions'=>array(
			'style'=>'width:100%;'
		),
	));
	
?>

