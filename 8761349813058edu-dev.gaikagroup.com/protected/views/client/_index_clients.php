<style type="text/css">
    td a {
        word-wrap: break-word;
    }
</style>

<div>
	<?php $this->widget('application.components.widgets.AddLinkWidget', array('htmlOptions' => array('onclick' => '$(".client-add-form").toggle(); return false;'))); ?>
</div>


<div class="hidden client-add-form">
    <?php
    if (Yii::app()->user->checkAccess('client/create')) {
        $userModel = Yii::app()->user->model;
        $managerList = UserMain::getManagerList();
        $clientModel = new Client(($managerList && ($userModel->is_partner || $userModel->is_owner)) ? 'createWithManagers' : 'createDefault');

        $this->renderPartial('_form', array('model'=>$clientModel, 'managers' => $managerList));
    }

    ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'client-grid',
	'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	/* не удалять
	'afterAjaxUpdate' => 'function(){
		jQuery("#reg_date").datepicker({
			dateFormat: "d M yy",
			monthNames: '.CJSON::encode(array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь')).',
			monthNamesShort: '.CJSON::encode(array('янв','февр','марта','апр','мая','июня','июля','авг','сент','окт','нояб','дек')).',
			dayNames: '.CJSON::encode(array('Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота')).',
            dayNamesShort: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            dayNamesMin: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            firstDay: 1,  
			changeYear: true
		});
		jQuery("#phone_num").mask(
			"+7-999-999-9999",
			{ placeholder:"*" }
		);
	}',
	*/
	'columns'=>array(
        'city_id' => array(
            'name' => 'city_id',
            'type' => 'raw',
            'value'=> 'City::nameByProjectId($data->project_id)',
            'filter'=> CHtml::activeDropDownList($model,'city_id',City::listArray(),array('empty' => 'Все города')),
            'visible'=> Yii::app()->user->model->checkRole('owner'),
        ),
        array( 
            'class' => 'editable.EditableColumn',
            'name' => 'status',
            'type' => 'raw',
            'filter'=> $model->getStatusList(false),
            'headerHtmlOptions' => array('style' => 'width: 150px'),
            'editable' => array(
                'type'     => 'select',
                'url'      => $this->createUrl('ajaxUpdate'),
                'source'   => $model->getStatusList(),
				'success'  => 'function(response, newValue) {
									if(!response) {
										return "Неизвестная ошибка.";
									}          
									
									if(response.errors) {
										 return response.errors;
									}
								}',
                'options'  => array(    //custom display
                    'display' => 'js: function(value, sourceData) {
                          var selected = $.grep(sourceData, function(o){ return value == o.value; }),
                              icons = ' . json_encode(Client::getStatusIcons()) . ';
                          $(this).html("<img style=\'height: 16px;\' src=\""+icons[value]+"\"> " + selected[0].text);
                      }'
                ),
            ),
			'value' => 'CHtml::image($data->statusIcons[$data->status], "", array("style"=>"height: 16px;")) . " " . $data->getStatusText()',
        ),
        array(
            'class' => 'editable.EditableColumn',
            'name' => 'source',
            'type' => 'raw',
            'filter' => $model->sourceList,
            'headerHtmlOptions' => array('style' => 'width: 100px'),
            'value'=> '$data->sourceText',
            'editable' => array(
                'type'     => 'select',
                'url'      => $this->createUrl('ajaxUpdate'),
                'source'   => $model->sourceList,
                'success'  => 'function(response, newValue) {
									if(!response) {
										return "Неизвестная ошибка.";
									}

									if(response.errors) {
										 return response.errors;
									}
								}'
            ),
        ),
		array(
			'name' => 'fullname',
			'type' => 'html',
			'value'=> 'CHtml::link($data->fullname,array(\'client/view\', \'id\' => $data->id))',
		),
		/* не удалять
		'reg_date' => array(
			'name' => 'reg_date',
			'type' => 'text',
			'value' => 'Yii::app()->dateFormatter->format(\'dd MMM yyyy\', $data->reg_date)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'reg_date',
                'attribute' => 'reg_date',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
		),
		*/
        'phone_num' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'phone_num',
           'headerHtmlOptions' => array('style' => 'width: 110px'),
           'editable' => array(    //editable section
               'url'      => $this->createUrl('ajaxUpdate'),
               'placement'  => 'right',
               'options'     => array(
                   'placeholder'=> '+7-922-333-4444',
               ),
               'validate'   => 'function(value) {
                   if (!validatePhone(value)) return "Телефона должен быть в формате +7-922-333-4444";
               }'
            ),
        ),
        'school' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'school',
           'headerHtmlOptions' => array('style' => 'width: 110px'),
           'editable' => array(    //editable section
                'url'      => $this->createUrl('ajaxUpdate'),
                'placement'  => 'right',
            ),       
        ),
		'course_list' => array(
			'name' => 'course_list',
			'type' => 'raw',
			'value'=> '$data->contractCourseLinks',
			'filter' => CHtml::activeDropDownList($model,'course_list',CHtml::listData(TrCourse::model()->findAll(),'id','name'),array('empty' => 'Все')),
		),
		'group_name' => array(
			'name' => 'group_name',
			'type' => 'raw',
			'value' => '$data->groupLinks',
		),
		'center_id' => array(
			'name' => 'center_id',
			'type' => 'raw',
			'value' => '$data->centerLinks',
			'filter' => CHtml::activeDropDownList($model,'center_id',TrCenter::model()->getCenterNames(),array('empty' => 'Все')),
		),
		'comments_text' => array(
			'name' => 'comments_text',
			'type' => 'raw',
			'value' => '$data->commentsText',
			'htmlOptions' => array('style'=>'font-size: 11px;'),
            'filter' =>''
		),
		array(
			'class'=>'ButtonColumn',
			'template' => '{create} {delete}',
			'buttons'=>array(
				'create' => array(
					'url' => 'array("contract/create", "id" => $data->id)',
					'label' => 'Добавить договор',
					'imageUrl' => Yii::app()->baseUrl.'/images/icon-add-contract.png',
					'visible' => '!$data->contracts',
				),
			    'update' => array(
					'url' => 'array("client/update", "id" => $data->id)',
			    ),
			    'delete' => array(
					'url' => 'array("client/delete", "id" => $data->id)',
			    ),
			),
		),
	),
)); ?>