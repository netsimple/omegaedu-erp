<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'debt-grid',
	'dataProvider'=>$dataProvider,
	'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'columns'=>array(
		array(
			'name' => 'debt',
			'header' => 'Долг',
			'type' => 'raw',
			'value' => '$data->debt',
			'htmlOptions' => array('class' => 'is-debt'),
		),
		array(
			'name' => 'client_name',
			'header' => 'Клиент',
			'type' => 'raw',
			'value' => 'CHtml::link($data->name,array("client/view","id" => $data->id))',
		),
		array(
			'name' => 'partner_name',
			'header' => 'Партнер',
			'type' => 'raw',
			'value' => 'CHtml::link($data->partner->name,array("user/view","id" => $data->partner->id))',
		),
		array(
			'name' => 'center_name',
			'header' => 'Центр',
			'type' => 'raw',
			'value' => 'CHtml::link($data->attachedGroup->center->name,array("trcenter/view","id" => $data->attachedGroup->tr_center_id))',
		),
		array(
			'name' => 'group_name',
			'header' => 'Группа',
			'type' => 'raw',
			'value' => 'CHtml::link($data->attachedGroup->name,array("trgroup/view","id" => $data->attachedGroup->id))',
		),
	),
)); ?>