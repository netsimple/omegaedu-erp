<script type="text/javascript">
    jQuery(function($){
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    });
</script>

<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->getClientScript()->getCoreScriptUrl() . '/jui/js/jquery-ui-i18n.min.js');
?>

<?php

    $js = <<<JSCRIPT
    
        $(document).ready(function(){
            var errorMsg = 'Неизвестная ошибка';
            var successMsg = 'Указанная сумма была успешно внесена на счет клиента.';
           $('#btn-add').click(function(){
                var formObj = $(this).parent().parent();
                $.ajax({
                   url: '{$this->createUrl('ajaxFinance', array('id' => $client->id))}',
                   type: 'POST',
                   data: formObj.serialize(),
                   dataType: 'json',
                   success: function(response, config){
                        if (response && typeof response.errors == 'undefined') {
                            $.fn.yiiGridView.update('balance-grid');
                            formObj.trigger('reset');
                            $("#msg").removeClass("alert-error").addClass("alert-success").html(successMsg).show().fadeToggle(1000, "swing");
                        } else {
                            this.error.call(this, response && response.errors ? response.errors : errorMsg);
                        }
                   },
                   error: function(errors) {
                        var msg = "";
                        if(errors && errors.responseText) {
                            msg = errors.responseText;
                        } else {
                            $.each(errors, function(k, v) { msg += v+"<br>"; });
                        }
                        $("#msg").removeClass("alert-success").addClass("alert-error").html(msg).show();
                   },
                });
                return false;
           });
        });
    
JSCRIPT;

    Yii::app()->clientScript->registerScript('add-balance',$js,CClientScript::POS_READY);

?>

<div style="margin-top: 10px; margin-bottom: 15px;">
    <b>Текущий баланс: <span id="current-balance"><?php echo $client->balanceRel; ?></span> руб.</b>
</div>

<div style="margin-top: 10px; margin-bottom: 25px;">
<?php $groupObj = TrGroup::model(); $dataProvider = new CArrayDataProvider($client->groups,array('pagination' => false)); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'education-grid',
    'template'=>'{items}',
	'dataProvider'=> $dataProvider,
	'columns'=>array(
		
		'name' => array(
			'name' => CHtml::activeLabel($groupObj,'name'),
			'header' => 'Группа',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->name),array("trgroup/view", "id" => $data->id))',
		),
		
		'center.name' => array(
			'name' => CHtml::activeLabel($groupObj,'tr_center_id'),
			'type' => 'raw',
			'value' => 'CHtml::link($data->center->name,array("trcenter/view", "id" => $data->tr_center_id))',
		),
		
		'course.name' => array(
			'name' => CHtml::activeLabel($groupObj,'tr_course_id'),
			'type' => 'raw',
			'value' => 'CHtml::link($data->course->name,array("trcourse/view", "id" => $data->tr_course_id))',
		),
		
		'course_contract_sum' => array(
			'name' => CHtml::activeLabel($groupObj,'course_contract_sum'),
			'type' => 'raw',
			'value'=> '$data->getContractSumByClientId('.$client->id.')',
		),
		
		'debt' => array(
			'name' => CHtml::activeLabel($groupObj,'debt'),
			'type' => 'raw',
			'value' => '($debt = $data->getDebtByClientId('.$client->id.')) ? $debt : ""',
		),

        
	),
)); ?>
</div>



<div id="msg" class="alert hide"></div>

<?php if ($courseList = $model->getCourseList()): ?>
<div id="add-detail-view">
	<?php
		$newModel = new TrIncome('clientInsert');
	?>
	
	<div class="form">
		
	<?php echo CHtml::beginForm(); ?>
	
	<div class="row" style="float: left; margin-right: 10px">
	<?php
		echo CHtml::activeLabel($newModel,'period_start');
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $newModel,
			'id' => 'new-income-period-start',
			'attribute' => 'period_start',
			'options' => array(
				'dateFormat' => 'd M yy',
				'changeYear' => true,
			),
			'htmlOptions' => array('size' => 13),
		));
	?>
	</div>
	
	<div class="row" style="float: left; margin-right: 10px">
	<?php
		echo CHtml::activeLabel($newModel,'period_end');
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $newModel,
			'id' => 'new-income-period-end',
			'attribute' => 'period_end',
			'options' => array(
				'dateFormat' => 'd M yy',
				'changeYear' => true,
			),
			'htmlOptions' => array('size' => 13),
		));
	?>
	</div>
	
	<div class="row" style="float: left; margin-right: 10px;">
		<?php echo CHtml::activeLabel($newModel,'type'); ?>
		<?php echo CHtml::activeDropDownList($newModel,'type',$newModel->typeList); ?>
	</div>
	
	<div class="row" style="float: left; margin-right: 10px;">
		<?php echo CHtml::activeLabel($newModel,'course_id'); ?>
		<?php echo CHtml::activeDropDownList($newModel,'group_id',$client->groupList,array('empty' => 'Выберите предмет')); ?>
	</div>
	
	<div class="row" style="float: left; margin-right: 10px;">
		<?php echo CHtml::activeLabel($newModel,'hours'); ?>
		<?php echo CHtml::activeTextField($newModel,'hours',array('size' => 10)); ?>
	</div>
	
	<div class="row" style="float: left; margin-right: 10px">
	<?php
		echo CHtml::activeLabel($newModel,'value');
		echo CHtml::activeTextField($newModel,'value', array('size' => 10));
	?>
	</div>
	
	<div class="row" style="float: left; margin-top: 10px">
	<?php
		echo CHtml::submitButton('+',array('class' => 'btn btn-primary', 'id' => 'btn-add'));
	?>
	</div>
	
	<div style="clear: left;"></div>
	
	<?php echo CHtml::endForm(); ?>
	
	</div>
</div>

<?php endif; ?>

<div style="margin-top: 10px; margin-bottom: 15px;">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'balance-grid',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	'afterAjaxUpdate' => 'function(){
		jQuery("#period-start, #period-end").datepicker({
			dateFormat: "d M yy",
			monthNames: '.CJSON::encode(array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь')).',
			monthNamesShort: '.CJSON::encode(array('янв','февр','марта','апр','мая','июня','июля','авг','сент','окт','нояб','дек')).',
			dayNames: '.CJSON::encode(array('Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота')).',
            dayNamesShort: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            dayNamesMin: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            firstDay: 1,  
			changeYear: true
		});
	}',
    'template' => '{items}{pager}',
	'columns'=>array(
        'course_id' => array(
            'name' => 'course_id',
            'type' => 'raw',
            'value' => '$data->course ? CHtml::link($data->course->name,array("trcourse/view", "id" => $data->course_id)) : ""'
        ),
        'value' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'value',
            'type' => 'raw',
            'value' => '$data->hours < 0 ? -$data->value : $data->value',
			'editable' => array(
				'url'      => $this->createUrl('ajaxUpdateBalance'),
				'validate'   => 'function(value) {
					if (value <= 0) return "Сумма должна являться целым неотрицательным числом.";
				}'
            ),  
        ),
        'hours' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'hours',
            'type' => 'raw',
            'value' => 'abs($data->hours)',
			'editable' => array(
				'url'      => $this->createUrl('ajaxUpdateBalance'),
				'validate'   => 'function(value) {
					if (value <= 0) return "Количество часов должно являться целым неотрицательным числом.";
				}'
            ),  
        ),
        'period_start' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'period_start',
            'type' => 'raw',
            'value'=> '$data->period_start ? Yii::app()->dateFormatter->format("d MMM yyyy",$data->period_start) : ""',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'period-start',
                'attribute' => 'period_start',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
			'editable' => array(
				'type'       => 'date',
				'url' => $this->createUrl('ajaxUpdateBalance'),
				'viewformat' => 'dd M yyyy',
				'params' => array(
					'weekStart' => 1,
				),
			),
            'htmlOptions' => array('width' => 120),
        ),
        'period_end' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'period_end',
            'type' => 'raw',
            'value'=> '$data->period_end ? Yii::app()->dateFormatter->format("d MMM yyyy",$data->period_end) : ""',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'period-end',
                'attribute' => 'period_end',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
			'editable' => array(
				'type'       => 'date',
				'url' => $this->createUrl('ajaxUpdateBalance'),
				'viewformat' => 'dd M yyyy',
				'params' => array(
					'weekStart' => 1,
				),
			),
            'htmlOptions' => array('width' => 120),
        ),
		array(
			'class' => 'ButtonColumn',
			'template' => '{delete}',
			'buttons' => array(
				'delete' => array(
					'visible' => '$data->hours > 0',
					'url' => 'array("operations/deleteIncome","id" => $data->id)',
				),
			),
		),
	),
)); ?>
</div>