<?php Yii::app()->clientScript->scriptMap['jquery.js'] = false; ?>

<div style="float:left; width: 250px; padding-left: 5px; border-left: 1px solid black;">
	<h4>Группы</h4>
	<?php foreach ($selector->baseGroups as $currKey => $currentSelector): ?>
	<div style="padding-bottom: 5px; padding-top: 5px;">
	<?php
		$this->widget('ext.EchMultiSelect.EchMultiSelect', array(
			'model' => $selector,
			'dropDownAttribute' => 'groups['.$currKey.']',
			'data' => $currentSelector,
			'dropDownHtmlOptions'=> array(
				'class' => 'select-groups',
				'style'=>'width:250px;',
			),
			'options' => array(
				'noneSelectedText' => $selector->getCenterNameById($currKey),
			),
		));
	?>
	</div>
	<?php
		/**
		 * Следующий блок находит скрипт, инициализирующий multiselect, и добавляет его в HTML-код
		 * Имеет смысл только в случае отсутствия автоматической подгрузки скриптов
		 */
	?>
	<?php $name = 'groups['.$currKey.']'; $elementId = 'EchMultiselect#'.CHtml::getIdByName(CHtml::resolveName($selector,$name)); ?>
	<script>
	<?php
		foreach (Yii::app()->clientScript->scripts as $currentScript) {
			if (isset($currentScript[$elementId])) {
				echo $currentScript[$elementId];
				break;
			}
		}
	?>
	</script>
	<?php endforeach; ?>
</div>

<script>
    jQuery('body').on('change','.select-groups',function(){ refreshTable('group'); });
</script>