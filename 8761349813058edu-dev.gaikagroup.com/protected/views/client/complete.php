<?php
	$this->menu = array(
	    array('label'=>'Необработанные заявки', 'url'=>array('client/requests')),
	    array('label'=>'Список заявок', 'url'=>array('client/index')),
	);
?>

<h1>Обработка заявки</h1>

<?php
    $this->renderPartial('_form',array('model'=>$model, 'managers'=>null));
?>