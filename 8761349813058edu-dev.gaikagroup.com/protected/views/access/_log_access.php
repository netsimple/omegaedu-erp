<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'access-grid',
    'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'function(){
		jQuery("#datetime").datepicker({
			dateFormat: "d M yy",
			monthNames: '.CJSON::encode(array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь')).',
			monthNamesShort: '.CJSON::encode(array('янв','февр','марта','апр','мая','июня','июля','авг','сент','окт','нояб','дек')).',
			dayNames: '.CJSON::encode(array('Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота')).',
            dayNamesShort: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            dayNamesMin: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            firstDay: 1,
			changeYear: true
		});
	}',
	'columns'=>array(
		'user_ip_addr',
		'username' => array(
			'name' => 'username',
			'type' => 'raw',
			'value'=> '$data->user ? $data->user->name : "ID #".$data->user_id',
		),
		'datetime' => array(
			'name' => 'datetime',
			'type' => 'raw',
			'value'=> 'Yii::app()->dateFormatter->format("dd MMM yyyy, HH:mm",$data->datetime)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'datetime',
                'attribute' => 'datetime',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
		),
		'action' => array(
			'name' => 'action',
			'type' => 'raw',
			'value'=> '$data->actionName',
			'filter'=>$model->actionNames,
		),
		'is_success' => array(
			'name' => 'is_success',
			'type' => 'raw',
			'filter'=>$model->successList,
			'value'=> '$data->successList[$data->is_success]'
		),
	),
)); ?>