<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'activity-grid',
    'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	'afterAjaxUpdate' => 'function(){
		jQuery("#datetime").datepicker({
			dateFormat: "d M yy",
			monthNames: '.CJSON::encode(array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь')).',
			monthNamesShort: '.CJSON::encode(array('янв','февр','марта','апр','мая','июня','июля','авг','сент','окт','нояб','дек')).',
			dayNames: '.CJSON::encode(array('Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота')).',
            dayNamesShort: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            dayNamesMin: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            firstDay: 1,
			changeYear: true
		});
	}',
	'columns'=>array(
		'username' => array(
			'name' => 'username',
			'type' => 'raw',
			'value'=> '$data->user ? $data->user->name : "ID #".$data->user_id',
		),
		'datetime' => array(
			'name' => 'datetime',
			'type' => 'raw',
			'value'=> 'Yii::app()->dateFormatter->format("dd MMM yyyy, HH:mm",$data->datetime)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'datetime',
                'attribute' => 'datetime',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
		),
		'action' => array(
			'name' => 'action',
			'type' => 'raw',
			'value'=> '$data->actionName',
			'filter'=>$model->actionNames,
		),
		'class_name' => array(
			'name' => 'class_name',
			'type' => 'raw',
			'value'=> '$data->className',
			'filter'=>$model->classNames,
		),
		'object_id' => array(
			'name' => 'object_id',
			'type' => 'raw',
			'value'=> '$data->objectName',
		),
		'comment' => array(
			'name' => 'comment',
			'type' => 'raw',
			'filter'=>false,
			'value'=> '$data->comment',
		),
	),
)); ?>