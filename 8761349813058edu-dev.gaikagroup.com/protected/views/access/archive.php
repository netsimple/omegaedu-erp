<?php

$js = <<<JSCRIPT
    $(document).ready(function(){
    
       var topCheckBoxId = 'archive-grid_c0_all';
       $('#restore-btn').click(function(){
            var arr = [];
            $('.checkbox-column input[type=checkbox]').each(function(){
                if ($(this).attr('id') != topCheckBoxId && $(this).prop('checked')) {
					var tmp = $(this).val().split('_');
					var dataset = {"class": tmp[0], "id": tmp[1]};
					arr.push(dataset);
				}
            });
            if (arr) {
                $.ajax({
                    url: '{$this->createUrl('restore')}',
                    type: 'post',
                    data: {ids: arr},
					success: function(result){
						$.fn.yiiGridView.update('archive-grid');
					}
                });
            }
       });
       
    });
JSCRIPT;

Yii::app()->clientScript->registerScript('archive-selector',$js,CClientScript::POS_READY);

?>

<?php echo CHtml::button('Восстановить',array('id' => 'restore-btn', 'class' => 'btn', 'style' => 'margin-top: 10px')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'archive-grid',
    'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
        array(
			'class'=> 'CCheckBoxColumn',
            'selectableRows' => 2,
			'value' => 'get_class($data)."_".$data->id',
        ),
		array(
			'name' => 'Элемент',
			'type' => 'raw',
			'value'=> '$data->className',
		),
		array(
			'name' => 'Название',
			'type' => 'raw',
			'value'=> '$data->name',
		),
		array(
			'name' => 'Изменения',
			'type' => 'raw',
			'value'=> 'Yii::app()->dateFormatter->format("d MMM yyyy, HH:mm",$data->is_deleted)',
		),
		array(
			'name' => 'Автор',
			'type' => 'raw',
			'value'=> '$data->getDeletedBy() ? CHtml::link($data->getDeletedBy()->fullname,array("user/view","id" => $data->getDeletedBy()->id)) : ""',
		),
	),
)); ?>