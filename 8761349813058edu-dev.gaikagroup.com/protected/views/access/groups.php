<?php if ($flashMsg = Yii::app()->user->getFlash('success')): ?>
<div class="alert alert-success">
	<?php echo CHtml::encode($flashMsg); ?>
</div>
<?php endif; ?>


<?php echo CHtml::beginForm(); ?>
    <table width="100%">
      <tr>
        <td>ФИО</td>
        <td>Владелец</td>
        <td>Партнер</td>
        <td>Менеджер</td>
        <td>Рекламный агент</td>
        <td>Учитель</td>
      </tr>
    <?php
        foreach ($dataProvider as $currentUser):
    ?>
      <tr>
        <td><?php echo CHtml::hiddenField('id'.$currentUser->id.'[id]',$currentUser->id); echo $currentUser->fullname; ?></td>
        <td><?php echo $currentUser->is_owner ? 'да' : 'нет'; ?></td>
        <td><?php echo CHtml::checkBox('id'.$currentUser->id.'[is_partner]', $currentUser->is_partner); ?></td>
        <td><?php echo CHtml::checkBox('id'.$currentUser->id.'[is_manager]', $currentUser->is_manager); ?></td>
        <td><?php echo CHtml::checkBox('id'.$currentUser->id.'[is_adman]', $currentUser->is_adman); ?></td>
        <td><?php echo CHtml::checkBox('id'.$currentUser->id.'[is_teacher]', $currentUser->is_teacher); ?></td>
      </tr>
    <?php
        endforeach;
    ?>
    </table>
    <?php echo CHtml::submitButton('Сохранить',array('class' => 'btn')); ?>
<?php echo CHtml::endForm(); ?>