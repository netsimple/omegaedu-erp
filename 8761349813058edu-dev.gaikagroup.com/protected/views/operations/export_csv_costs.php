<?php

$output = array('Город;Пользователь;Дата добавления;Элемент;Тип;Вид;Комментарий;Сумма');
// предположим что на 999999 памяти хватит, время покажет
foreach ($costsModel->search(999999)->getData() as $data)
    $output[] = implode(';', array(
        City::nameByProjectId($data->project_id),
        $data->user->fullname,
        Yii::app()->dateFormatter->format("d MMM yyyy",$data->datetime),
        $data->getObjectName(true),
        $data->typeList[$data->type],
        $data->sectionList[$data->section],
        $data->comment,
        $data->value,
    ));

if (!$output) Yii::app()->end();
$filename = 'edu-costs-'.time().'.csv';

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.$filename);
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
echo "\xEF\xBB\xBF";
echo $result = implode("\r\n",$output);
die();