<?php

$js = <<<JSCRIPT
    $(document).ready(function(){

       $('#link-income').click(function(){
            $('#link-income').addClass('active');
            $('#link-costs').removeClass('active');
            $('#link-balance').removeClass('active');
            $('#section-costs').hide();
            $('#section-balance').hide();
            $('#section-income').show();
            return false;
       });

       $('#link-costs').click(function(){
            $('#link-costs').addClass('active');
            $('#link-income').removeClass('active');
            $('#link-balance').removeClass('active');
            $('#section-income').hide();
            $('#section-balance').hide();
            $('#section-costs').show();
            return false;
       });
       
       $('#link-balance').click(function(){
            $.ajax({
               type: 'GET',
               url: '{$this->createUrl('ajaxBalance')}',
               success: function(response){
                    $('#section-balance').html(response);
                    $('#link-balance').addClass('active');
                    $('#link-income').removeClass('active');
                    $('#link-costs').removeClass('active');
                    $('#section-income').hide();
                    $('#section-costs').hide();
                    $('#section-balance').show();
               }
            });
            return false;
       });

    });

JSCRIPT;

Yii::app()->clientScript->registerScript('link-selector',$js,CClientScript::POS_READY);

$css = <<<CSS
    a.link-selector { color: #000000; }
    a.active { font-weight: bold; text-decoration: none; }
CSS;

Yii::app()->clientScript->registerCss('links-custom-style',$css);

?>

<div style="margin-top: 5px; margin-bottom: 15px;">
    <?php echo CHtml::link('Доход','#',array('id' => 'link-income', 'class' => 'link-selector active')); ?> |
    <?php echo CHtml::link('Расход','#',array('id' => 'link-costs', 'class' => 'link-selector')); ?> |
    <?php echo CHtml::link('Баланс','#',array('id' => 'link-balance', 'class' => 'link-selector')); ?>
</div>

<div id="section-income"><?php $this->renderPartial('income', array('model' => $incomeModel)); ?></div>
<div id="section-costs" style="display: none;"><?php $this->renderPartial('costs', array('model' => $costsModel)); ?></div>
<div id="section-balance" style="display: none;"><?php //$this->renderPartial('balance', array('income' => $incomeModel, 'costs' => $costsModel)); ?></div>