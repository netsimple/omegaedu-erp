<script type="text/javascript">
    jQuery(function($){
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    });
</script>

<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->getClientScript()->getCoreScriptUrl() . '/jui/js/jquery-ui-i18n.min.js');
?>

<?php

    $constSectionClient = $model::INCOME_CLIENT;
    $js = <<<JSCRIPT
    
        $(document).ready(function(){
            var errorMsg = 'Неизвестная ошибка';
            var successMsg = 'Позиция была успешно добавлена.';
           $('#btn-add-income').click(function(){
                var formObj = $(this).parent().parent();
                $.ajax({
                   url: '{$this->createUrl('ajaxAddIncome')}',
                   type: 'POST',
                   data: formObj.serialize(),
                   dataType: 'json',
                   success: function(response, config){
                        if (response && typeof response.errors == 'undefined') {
                            $.fn.yiiGridView.update('income-grid');
                            formObj.trigger('reset');
                            $('#new-income-client').val('');
                            $("#msg-income").removeClass("alert-error").addClass("alert-success").html(successMsg).show().fadeToggle(1000, "swing");
                        } else {
                            this.error.call(this, response && response.errors ? response.errors : errorMsg);
                        }
                   },
                   error: function(errors) {
                        var msg = "";
                        if(errors && errors.responseText) {
                            msg = errors.responseText;
                        } else {
                            $.each(errors, function(k, v) { msg += v+"<br>"; });
                        }
                        $("#msg-income").removeClass("alert-success").addClass("alert-error").html(msg).show();
                   },
                });
                return false;
           });
           $('#new-income-section-list').change(function(){
                $('#new-income-value').prop('disabled',!$(this).val());
                if ($(this).val()) {
                    if ($(this).val() == $constSectionClient) {
                        var showSection = "#section-income-client";
                        var hideSection = "#section-income-comment";
                    } else {
                        var hideSection = "#section-income-client";
                        var showSection = "#section-income-comment";
                    }
                    $(hideSection).fadeOut(300,'swing' && function(){ $(showSection).fadeIn(300,'swing'); });
                } else {
                    $('#section-income-client').fadeOut(300,'swing');
                    $('#section-income-comment').fadeOut(300,'swing');
                }
           });
           $('#new-income-client').change(function(){
                if ($(this).val()) {
                    $.ajax({
                       url: '{$this->createUrl('client/getCourses')}',
                       type: 'GET',
                       dataType: 'json',
                       data: {id: $(this).val()},
                       success: function(response, config){
                            $('#new-income-course option[value]').each(function() { if (this.value) this.remove(); } );
                            if (response) {
                                var result = '';
                                $.each(response, function(k, v) { result += '<option value='+k+'>'+v+'</option>'; });
                                $('#new-income-course').append(result);
                            }
                            $('#new-income-course').prop('disabled',!response);
                            $('#new-income-hours').prop('disabled',!response);
                            $('#new-income-value').prop('disabled',!response);
                       },
                    });
                } else {
                    $('#new-income-course').prop('disabled',true);
                    $('#new-income-hours').prop('disabled',true);
                    $('#new-income-value').prop('disabled',true);
                }
           });
        });
    
JSCRIPT;

    Yii::app()->clientScript->registerScript('add-income',$js,CClientScript::POS_READY);

?>

<?php $newModel = new TrIncome; ?>

<div id="msg-income" class="alert hide"></div>

<div class="form">
    
<?php echo CHtml::beginForm(); ?>

<div class="row" style="float: left; margin-right: 15px">
<?php
    echo CHtml::activeLabel($newModel,'period_start');
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model' => $newModel,
        'id' => 'new-income-period-start',
        'attribute' => 'period_start',
        'options' => array(
            'dateFormat' => 'd M yy',
            'changeYear' => true,
        ),
        'htmlOptions' => array('size' => 15),
    ));
?>
</div>

<div class="row" style="float: left; margin-right: 15px">
<?php
    echo CHtml::activeLabel($newModel,'period_end');
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model' => $newModel,
        'id' => 'new-income-period-end',
        'attribute' => 'period_end',
        'options' => array(
            'dateFormat' => 'd M yy',
            'changeYear' => true,
        ),
        'htmlOptions' => array('size' => 15),
    ));
?>
</div>

<div class="row" style="float: left; margin-right: 15px">
<?php
    echo CHtml::activeLabel($newModel,'type');
    echo CHtml::activeDropDownList($newModel,'type',$newModel->typeList);
?>
</div>

<div style="clear: left;"></div>

<div class="row" style="float: left; margin-right: 15px">
<?php
    echo CHtml::activeLabel($newModel,'section');
    echo CHtml::activeDropDownList($newModel,'section',$newModel->getSectionList('search'),array('empty' => 'Выберите категорию', 'id' => 'new-income-section-list'));
?>
</div>

<div class="row" id="section-income-comment" style="float: left; margin-right: 15px; display: none;">
<?php
    echo CHtml::activeLabel($newModel,'comment');
    echo CHtml::activeTextField($newModel,'comment', array('id' => 'new-income-comment'));
?>
</div>

<div id="section-income-client" style="display: none;">
    <div class="row" style="float: left; margin-right: 15px;">
        <?php echo CHtml::activeLabel($newModel,'client_id'); ?>
        <?php echo CHtml::activeHiddenField($newModel,'client_id',array('id' => 'new-income-client')); ?>
        <?php 
            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name' => 'clientname',
                'source'=>$this->createUrl('client/suggestName'),
                'options'=>array(
                    'minLength'=>'2',
                    'select' =>'js: function(event, ui) {
                        this.value = ui.item.fullname;
                        $("#new-income-client").val(ui.item.id);
                        $("#new-income-client").trigger("change");
                        return false;
                    }',
                ),
            ));
        ?>
    </div>
    <div class="row" style="float: left; margin-right: 15px;">
        <?php echo CHtml::label('Центр', ''); ?>
        <?php
            echo CHtml::dropDownList('center_id','', TrCenter::model()->centerNames, array('empty'=>'Выберите центр', 'ajax' => array(
                    'url'=>$this->createUrl('trgroup/groupsList'),
                    'data'=>array('center_id'=>'js:this.value'),
                    'update'=>'#TrIncome_group_id'
            )));
        ?>
    </div>
    <div class="row" style="float: left; margin-right: 15px;">
        <?php echo CHtml::activeLabel($newModel,'group_id'); ?>
        <?php echo CHtml::activeDropDownList($newModel,'group_id',array()); ?>
    </div>
    <div class="row" style="float: left; margin-right: 15px;">
        <?php echo CHtml::activeLabel($newModel,'hours'); ?>
        <?php echo CHtml::activeTextField($newModel,'hours',array('size' => 13, 'id' => 'new-income-hours')); ?>
    </div>
</div>

<div class="row" style="float: left; margin-right: 15px">
<?php
    echo CHtml::activeLabel($newModel,'value');
    echo CHtml::activeTextField($newModel,'value', array('id' => 'new-income-value', 'disabled' => true, 'size' => 10));
?>
</div>

<div class="row" style="float: left; margin-top: 10px">
<?php
    echo CHtml::submitButton('+',array('class' => 'btn btn-primary', 'id' => 'btn-add-income'));
?>
</div>

<div style="clear: left;"></div>

<?php echo CHtml::endForm(); ?>

</div>

<button class="btn " onclick="location.href='/operations?export=income&' + $('#income-grid :input').serialize(); return false;">Экспорт доходов</button>

<?php

$summary = "<div style=\"text-align: right; margin-bottom: 5px;\">";
if ($model->searchCash !== null) 	 $summary .= "<b>Наличный расчёт: {$model->searchCash}</b><br/>";
if ($model->searchCashless !== null) $summary .= "<b>Безналичный расчёт: {$model->searchCashless}</b><br/>";
$summary .= "<b>Итого: {$model->searchSummary}</b>";
$summary .= "</div>";

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'income-grid',
	'template'=>$summary.'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	'afterAjaxUpdate' => 'function(){
		jQuery("#income-period-start, #income-period-end, #income-datetime").datepicker({
			dateFormat: "d M yy",
			monthNames: '.CJSON::encode(array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь')).',
			monthNamesShort: '.CJSON::encode(array('янв','февр','марта','апр','мая','июня','июля','авг','сент','окт','нояб','дек')).',
			dayNames: '.CJSON::encode(array('Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота')).',
            dayNamesShort: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            dayNamesMin: '.CJSON::encode(array('Вс','Пн','Вт','Ср','Чт','Пт','Сб')).',
            firstDay: 1,  
			changeYear: true
		});
	}',
    'columns' => array(
        'city' => array(
            'name' => 'city_id',
            'type' => 'raw',
            'value'=> 'City::nameByProjectId($data->project_id)',
            'filter'=> CHtml::activeDropDownList($model,'city_id',City::listArray(),array('empty' => 'Все города')),
            'visible'=> Yii::app()->user->model->checkRole('owner'),
        ),
        'username' => array(
            'name' => 'username',
            'type' => 'raw',
            'value'=> 'CHtml::link($data->user->fullname,array("user/view","id" => $data->user_id))',
        ),
        'datetime' => array(
            'name' => 'datetime',
            'type' => 'raw',
            'value'=> 'Yii::app()->dateFormatter->format("d MMM yyyy",$data->datetime)',
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'id' => 'income-datetime',
                'attribute' => 'datetime',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
            'htmlOptions' => array('width' => 100),
        ),
        'period_start' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'period_start',
            'type' => 'raw',
            'value'=> '$data->period_start ? Yii::app()->dateFormatter->format("d MMM yyyy",$data->period_start) : ""',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'income-period-start',
                'attribute' => 'period_start',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
			'editable' => array(
				'type'       => 'date',
				'url' => $this->createUrl('ajaxUpdateIncome'),
				'viewformat' => 'dd M yyyy',
				'params' => array(
					'weekStart' => 1,
				),
			),
            'htmlOptions' => array('width' => 100),
        ),
        'period_end' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'period_end',
            'type' => 'raw',
            'value'=> '$data->period_end ? Yii::app()->dateFormatter->format("d MMM yyyy",$data->period_end) : ""',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'income-period-end',
                'attribute' => 'period_end',
                'options' => array(
                    'dateFormat' => 'd M yy',
                    'changeYear' => true
                ),
            ), true),
			'editable' => array(
				'type'       => 'date',
				'url' => $this->createUrl('ajaxUpdateIncome'),
				'viewformat' => 'dd M yyyy',
				'params' => array(
					'weekStart' => 1,
				),
			),
            'htmlOptions' => array('width' => 100),
        ),
        'type' => array(
            'class'  => 'editable.EditableColumn',
            'name' 	 => 'type',
            'type' 	 => 'raw',
            'value'	 => '$data->typeList[$data->type]',
            'filter' =>$model->typeList,
            'editable' => array(
                'url' 	=> $this->createUrl('ajaxUpdateIncome'),
                'type' 	=> 'select',
                'source'=> $model->typeList,
            ),
        ),
        'section' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'section',
            'type' => 'raw',
            'value'=> '$data->sectionList[$data->section]',
            'filter'=>$model->sectionList,
            'editable' => array(
                'url' => $this->createUrl('ajaxUpdateIncome'),
                'type' => 'select',
                'source' => $model->getSectionList('update'),
            ),
        ),
        'comment' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'comment',
            'type' => 'raw',
            'editable' => array(
                'url' => $this->createUrl('ajaxUpdateIncome'),  
            ),
        ),
        'value' => array(
            'class' => 'editable.EditableColumn',
            'name' => 'value',
            'editable' => array(
                'url' => $this->createUrl('ajaxUpdateIncome'),
            ),
            'htmlOptions' => array('width' => 50),
        ),
        array(
            'class' => 'ButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array('url' => 'array("deleteIncome", "id" => $data->id)'),  
            ),
        ),
    ),
)); ?>