<?php Yii::app()->clientScript->scriptMap['jquery.js'] = false;?>
<div style="margin-top: 20px;">

    <?php if (Yii::app()->user->checkAccess('owner')) { ?>
        <div class="cityList">
            <?php echo CHtml::dropDownList('city_id', Yii::app()->request->getParam('city_id'), City::listArray(), array('empty' => 'Все города'));?>
        </div>

        <script>
            $(function(){
                $('.cityList select').change(function(){
                    $.ajax({
                        url: 'operations/ajaxBalance',
                        data: {city_id: $('.cityList select').val()},
                        success: function(data) {
                            $('#section-balance').html(data);
                        }
                    });
                });
            })
        </script>

    <?php } ?>

	<div style="margin-top: 10px; margin-bottom: 20px;">
		<u><b>Заключено договоров на сумму
                <?php
                    echo Contract::model()->sum;
                ?> рублей
            </b></u><br/><br/>

		<u><b>Внесения</b></u><br/><br/>
		а) Доходы<br/>
		<ul>
			<?php $sumCashless = 0; $sumCash = 0; ?>
			<?php foreach ($income->getSectionList() as $currSectKey => $currSectLabel): ?>
			<?php if ($currSectKey == $income::INCOME_CUSTOM) continue; ?>
			<?php
				$sumCashless += $income->getSearchCashless($currSectKey);
				$sumCash += $income->getSearchCash($currSectKey);
			?>
			<li><b><?php echo $currSectLabel; ?></b>, нал: <?php echo $income->getSearchCash($currSectKey); ?>, безнал: <?php echo $income->getSearchCashless($currSectKey); ?></li>
			<?php endforeach; ?>
			<li><b>Итого доходы</b>, нал: <?php $sumCash; $sumCashless; echo $sumCash; ?>, безнал: <?php echo $sumCashless; ?></li>
		</ul>
		б) Внесения собственных средств<br/>
		<b>Итого внесено</b>, нал: <?php echo $income->getSearchCash($income::INCOME_CUSTOM); ?>, безнал: <?php echo $income->getSearchCashless($income::INCOME_CUSTOM); ?><br/>
		<br/>
		<b>Итого получено всего</b>, нал: <?php echo $income->getSearchCash(); ?>, безнал: <?php echo $income->getSearchCashless(); ?><br/>
	</div>
	
	<div style="margin-top: 10px; margin-bottom: 20px;">
		<u><b>Выводы</b></u><br/><br/>
		а) Расходы<br/>
		<ul>
			<?php $sumCashless = 0; $sumCash = 0; ?>
			<?php foreach ($costs->getSectionList() as $currSectKey => $currSectLabel): ?>
			<?php if ($currSectKey == $costs::COSTS_CUSTOM) continue; ?>
			<?php
				$sumCashless += $costs->getSearchCashless($currSectKey);
				$sumCash += $costs->getSearchCash($currSectKey);
			?>
			<li><b><?php echo $currSectLabel; ?></b>, нал: <?php echo $costs->getSearchCash($currSectKey); ?>, безнал: <?php echo $costs->getSearchCashless($currSectKey); ?></li>
			<?php endforeach; ?>
			<li><b>Итого расходы</b>, нал: <?php echo $sumCash; ?>, безнал: <?php echo $sumCashless; ?></li>
		</ul>
		б) Вывод средств<br/>
		<b>Итого выведено</b>, нал: <?php echo $costs->getSearchCash($costs::COSTS_CUSTOM); ?>, безнал: <?php echo $costs->getSearchCashless($costs::COSTS_CUSTOM); ?><br/>
		<br/>
		<b>Итого потрачено всего</b>, нал: <?php echo $costs->getSearchCash(); ?>, безнал: <?php echo $costs->getSearchCashless(); ?><br/>
	</div>
	
	<script type="text/javascript">
		var cash = <?php echo TrBalance::getCash(); ?>, cashless = <?php echo TrBalance::getCashless(); ?>;
	</script>
	
	<div style="margin-top: 10px; margin-bottom: 20px;">
		<u><b>Баланс</b></u><br/><br/>
        <b>Итого</b>,
            нал: <?php echo $income->getSearchCash() - $costs->getSearchCash(); ?>,
            безнал: <?php echo $income->getSearchCashless() - $costs->getSearchCashless(); ?><br/><br/>

        <?php if (TrBalance::checkUpdateAccess()) { ?>
            <u><b>Касса</b></u><br/><br/>
            а) Итого, наличный расчет:&nbsp;

            <?php $this->widget('editable.Editable', array(
                    'type'      => 'text',
                    'name'      => 'cash',
                    'text'      => TrBalance::getCash(),
                    'url'       => $this->createUrl('ajaxUpdateBalance'),
                    'title'     => 'Введите остаток в кассе',
                    'placement' => 'right',
                    'onSave' => 'js: function(e, params) {
                        cash = Number(params.newValue);
                        $("#custom-balance-total").html(cash + cashless);
                        $.ajax({
                            type: "POST",
                            data: {type: "cash", value: params.newValue},
                            url: "'.$this->createUrl('ajaxUpdateBalance').'"
                        });
                    }'
                ));
            ?>

            <br/>
            б) Итого, безналичный расчет:&nbsp;

            <?php $this->widget('editable.Editable', array(
                    'type'      => 'text',
                    'name'      => 'cashless',
                    'text'      => TrBalance::getCashless(),
                    'url'       => $this->createUrl('ajaxUpdateBalance'),
                    'title'     => 'Введите остаток на счёте',
                    'placement' => 'right',
                    'onSave' => 'js: function(e, params) {
                        cashless = Number(params.newValue);
                        $("#custom-balance-total").html(cash + cashless);
                        $.ajax({
                            type: "POST",
                            data: {type: "cashless", value: params.newValue},
                            url: "'.$this->createUrl('ajaxUpdateBalance').'",
                        });
                    }'
                ));
            ?>

            <br/>
            в) Итого, всего доступно:&nbsp;
            <span id="custom-balance-total"><?php echo TrBalance::getTotal(); ?></span>
        <?php } ?>
	</div>
	
</div>