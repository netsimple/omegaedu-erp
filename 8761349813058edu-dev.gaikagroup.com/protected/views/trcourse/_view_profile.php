<script type="text/javascript">
    jQuery(function($){
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    });
</script>
<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->getClientScript()->getCoreScriptUrl() . '/jui/js/jquery-ui-i18n.min.js');

$attributes = array(
    'name' => array(
        'name' => 'name',
        'editable' => array(
            'type'       => 'text',
            'validate'   => 'function(value) {
                if (!value) return "Название обязательно для заполнения."
            }'
        ),
    ),
    'description' => array(
        'name' => 'description',
        'editable' => array(
            'type'       => 'textarea',
        ),
    ),
	'hours',
	'start_date' => array(
		'name' => 'start_date',
		'editable' => array(
			'type'       => 'date',
			'viewformat' => 'dd M yyyy',
			'params' => array(
				'weekStart' => 1,
			)
		),
	),
	'client_lvl' => array(
		'name' => 'client_lvl',
		'editable' => array(
			'type' => 'select',
			'source' => $model->levelList,
		),
	),
);

?>

<?php
    $this->widget('editable.EditableDetailView', array(
    'data'       => $model,
    'url'        => $this->createUrl('ajaxUpdate'),
    'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
    'emptytext'  => 'Редактировать',
    'attributes' => $attributes
    ));
?>