<script type="text/javascript">
    jQuery(function($){
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    });
</script>

<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->getClientScript()->getCoreScriptUrl() . '/jui/js/jquery-ui-i18n.min.js');
?>

<div>
	<?php $this->widget('application.components.widgets.AddLinkWidget'); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'trcourse-grid',
    'template'=>'<div class="pagerControl">{summary}{pager}</div>{items}',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	'afterAjaxUpdate' => 'function(){
		jQuery("#start_date").datepicker({
			dateFormat: "dd.mm.yy",
			changeYear:true
		});
	}',
	'columns'=>array(
		array(
			'name' => 'name',
			'type' => 'html',
			'value'=> 'CHtml::link($data->name,array(\'trcourse/view\', \'id\' => $data->id))',
		),
        'description' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'description',
		   'editable' => array(
				'type' => 'textarea',
				'url' => $this->createUrl('ajaxUpdate'),
		   ),
        ),
        'hours' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'hours',
		   'editable' => array(
				'type' => 'text',
				'url' => $this->createUrl('ajaxUpdate'),
		   ),
		   'value' => '$data->hours ? $data->hours : ""',
        ),
		'start_date' => array(
            'class' => 'editable.EditableColumn',
			'name' => 'start_date',
			'type' => 'text',
			'value' => '$data->start_date ? Yii::app()->dateFormatter->format(\'d MMM yyyy\',$data->start_date) : ""',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'start_date',
                'attribute' => 'start_date',
                'options' => array(
                    'dateFormat' => 'dd.mm.yy',
                    'changeYear' => true
                ),
            ), true),
			'editable' => array(
				'type'       => 'date',
				'url' => $this->createUrl('ajaxUpdate'),
				'viewformat' => 'dd M yyyy',
				'params' => array(
					'weekStart' => 1,
				),
			),
		),
		'client_lvl' => array(
           'class' => 'editable.EditableColumn',
           'name' => 'client_lvl',
		   'editable' => array(
				'type' => 'select',
				'source' => $model->levelList,
				'url' => $this->createUrl('ajaxUpdate'),
		   ),
		   'value' => '$data->levelList[$data->client_lvl]',
		   'filter' => $model->levelList,
		),
		array(
			'class'=>'ButtonColumn',
			'template' => '{delete}',
		),
	),
)); ?>