<?php

Yii::import('ext.widgets.ddmenu.XDropDownMenu');

class YDropDownMenu extends XDropDownMenu {
    protected function registerClientScript() {
        $assets = Yii::getPathOfAlias('ext.widgets.ddmenu') . '/vendors';
        $baseUrl = Yii::app()->getAssetManager()->publish($assets);

        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerCssFile($baseUrl . '/' . $this->cssFile);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/' . 'superfish.js',CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/' . 'hoverIntent.js',CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/' . 'CDropDownMenu.js',CClientScript::POS_END);
    }
}