<?php

class FormatDatesBehavior extends CBehavior {

    public $attributes;
    
    public function formatDates() {
        if (is_string($this->attributes)) $this->attributes = array($this->attributes);
        if (is_array($this->attributes)) {
            foreach ($this->attributes as $currentAttribute) {
                $this->owner->$currentAttribute = $this->owner->$currentAttribute ? str_replace('.','',Yii::app()->dateFormatter->format('d MMM yyyy',$this->owner->$currentAttribute)) : $this->owner->$currentAttribute;
            }
        }
    }

    public function datesFormatFromTo($format='', $fromField = '', $toField = '') {

        if(!$format || !$fromField || !$toField) return false;

        $this->owner->$toField = str_replace('.','',Yii::app()->dateFormatter->format($format,$this->owner->$fromField));

    }

}

?>