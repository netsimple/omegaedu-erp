<?php

class ProjectHelper {
    public static function formatList($data,$urlString,$nameAttribute='name') {
        $links = array();
        foreach ($data as $currentObject) {
            array_push($links,CHtml::link(CHtml::encode($currentObject->$nameAttribute),array($urlString,'id' => $currentObject->id)));
        }
        return implode('<br/>',$links);
    }
}

?>