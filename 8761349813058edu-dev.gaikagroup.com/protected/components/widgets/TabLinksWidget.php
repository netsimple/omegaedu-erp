<?php

class TabLinksWidget extends CWidget {

    public $links;
    
    public function run(){
        if (!is_array($this->links) || !$this->links) return;
        foreach ($this->links as $currentKey => $currentLink) {
            if (isset($currentLink['visible']) && !$currentLink['visible'] || !isset($currentLink['label']) || !$currentLink['label']) {
                unset($this->links[$currentKey]);
            } else {
                if (!isset($currentLink['url']) || !$currentLink['url'] || !is_string($currentLink['url']) && !is_array($currentLink['url']))
                    $currentLink['url'] = '#';
                if (!isset($currentLink['htmlOptions']) || !is_array($currentLink['htmlOptions']))
                    $currentLink['htmlOptions'] = array();
                $this->links[$currentKey] = CHtml::link(CHtml::encode($currentLink['label']),$currentLink['url'],$currentLink['htmlOptions']);
            }
        }
        if ($this->links) {
            echo '<div style="margin-bottom: 10px; text-align: right;">';
            echo implode(' | ',$this->links);
            echo '</div>';
        }
    }

}

?>