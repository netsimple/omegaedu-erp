<?php

class DateValidator extends CDateValidator {

    public $outputFormat;
    
    protected function validateAttribute($object,$attribute) {
        
        parent::validateAttribute($object,$attribute);
        if ($object->hasErrors($attribute) || $this->outputFormat === null || !$object->$attribute) return;
    
        $formats=is_string($this->format) ? array($this->format) : $this->format;
        foreach ($formats as $format) {
            $timestamp=CDateTimeParser::parse($object->$attribute,$format,array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0));
            if($timestamp!==false) {
                if ($this->timestampAttribute !== null) $object->{$this->timestampAttribute} = $timestamp;
                break;
            }
        }
        
        $object->$attribute = Yii::app()->dateFormatter->format($this->outputFormat,$timestamp);
        
        return;
    
    }

}

?>