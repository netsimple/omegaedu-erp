<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	
	private $_id; // #pdovlatov
	public $username; // #pdovlatov
	
	
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate() {
		$model = UserMain::model();
		$identity = $model->findByAttributes(array('email' => $this->username));
		if ($identity) {
			if ($identity->passwd == $model->generateOldHash($this->username,$this->password)) {
				$identity->passwd = $model->generateHash($identity->reg_date,$this->password);
				$identity->save(false,array('passwd'));
			}
			if ($identity->passwd == $model->generateHash($identity->reg_date,$this->password)) {
				$this->_id = $identity->id;
				$this->username = $identity->fullname; //$model->formatName($identity->firstname,$identity->lastname);
				
				$this->setState('name',$this->username);
				
				$this->errorCode = self::ERROR_NONE;
			 } else {
				$this->errorCode = self::ERROR_USERNAME_INVALID;
			 }
		 } else {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		 }

		return !$this->errorCode;
	}
	
	public function getId() { return $this->_id; }
	
	public function getName() { return $this->username; }
	
}