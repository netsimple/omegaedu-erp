<?php

class SoftDeleteBehavior extends CActiveRecordBehavior {
    
    public $children;
    public $parents;
    
    protected $deleteTime;
    protected $deletedBy;
    protected $_userId;
    
    public function events() {
        return array(
            'onBeforeDelete' => 'beforeDelete',
            'onBeforeFind'   => 'beforeFind',
            'onBeforeCount'  => 'beforeCount',
        );
    }
    
    public function setDeleteTime($datetime) { $this->deleteTime = $datetime; return $this->owner; }
    
    public function beforeFind($event) {
        $this->owner->dbCriteria->addCondition('`'.$this->owner->getTableAlias(false,false).'`.`is_deleted` = 0');
    }
    
    public function beforeCount($event) {
        $this->owner->dbCriteria->addCondition('`'.$this->owner->getTableAlias(false,false).'`.`is_deleted` = 0');
    }

    public function beforeDelete($event) {
        if ($this->deleteTime === null) $this->deleteTime = time();
        if ($this->_userId === null) $this->_userId = Yii::app()->user->id;
        $this->owner->is_deleted = $this->deleteTime;
        $this->owner->deleted_by = $this->_userId;
        $this->owner->save(false,array('is_deleted', 'deleted_by'));
        $event->isValid = false;
        $this->children = (array)$this->children;
        foreach ($this->children as $currRelation) {
            foreach ($this->owner->getRelated($currRelation) as $currentObject) $currentObject->setDeleteTime($this->deleteTime)->delete();
        }
    }
    
    public function getArchiveObject($objectId) {
        $this->owner->disableBehavior('softDelete');
        $result = $this->owner->findByPk($objectId,'is_deleted != 0 AND deleted_by != 0');
        $this->owner->enableBehavior('softDelete');
        return $result;
    }
    
    public function getAllArchiveObjects() {
        $criteria = new CDbCriteria;
        $criteria->addCondition('t.is_deleted != 0');
        $criteria->addCondition('t.deleted_by != 0');
        $this->parents = (array)$this->parents;
        $with = array();
        foreach ($this->parents as $currRelation) {
            $with[$currRelation] = array('type' => 'INNER JOIN', 'condition' => $currRelation.'.is_deleted = 0');
        }
        $criteria->with = $with;
        $criteria->order = 't.is_deleted DESC';
        $this->owner->disableBehavior('softDelete');
        $result = $this->owner->findAll($criteria);
        $this->owner->enableBehavior('softDelete');
        return $result;
    }
    
    public function restore() {
        if (empty($this->deleteTime)) $this->deleteTime = Yii::app()->db->createCommand()->select('is_deleted')->from($this->owner->tableName())->where('id = '.$this->owner->id)->queryScalar();
        Yii::app()->db->createCommand()->update($this->owner->tableName(),array('is_deleted' => 0, 'deleted_by' => 0),'id = '.$this->owner->id);
        $this->children = (array)$this->children;
        foreach ($this->children as $currentRelation) {
            if ($rel = $this->owner->getActiveRelation($currentRelation)) {
                $model = CActiveRecord::model($rel->className);
                $model->detachBehavior('softDelete');
                $criteria = new CDbCriteria;
                $criteria->compare($rel->foreignKey,$this->owner->id);
                $criteria->compare('is_deleted',$this->deleteTime);
                $data = $model->findAll($criteria);
                foreach ($data as $currObject) $currObject->setDeleteTime($this->deleteTime)->restore();
            }
        }
        if (method_exists($this->owner,'afterRestore')) $this->owner->afterRestore();
    }
    
    public function getDeletedBy() {
        if (empty($this->deletedBy)) $this->deletedBy = $this->owner->deleted_by ? UserMain::model()->findByPk($this->owner->deleted_by) : null;
        return $this->deletedBy;
    }
    
    public function hiddenDelete() {
        $this->_userId = 0;
        $this->owner->delete();
    }
    
    public function exterminate() {
        $this->owner->detachBehavior('softDelete');
        $this->owner->delete();
    }

}

?>