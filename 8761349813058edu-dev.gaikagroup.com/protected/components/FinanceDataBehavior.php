<?php

class FinanceDataBehavior extends CBehavior {
    
	public function getFinanceData() {
        if (!method_exists($this->owner,'getFinanceIncomeData') || !method_exists($this->owner,'getFinanceCostsData')) throw new CException(__CLASS__ . ': Требуемые для работы методы не определены в классе');
		$collectionIncome = $this->owner->getFinanceIncomeData();
		$collectionCosts = $this->owner->getFinanceCostsData();

		$profit = $collectionIncome->income_total - $collectionCosts->costs;
		$profitabilityIncome = $collectionIncome->income_total != 0 ? $profit / $collectionIncome->income_total * 100 : 0;
		$profitabilityCosts = $collectionCosts->costs != 0 ? $profit / $collectionCosts->costs * 100 : 0;
            
		$collection = $collectionIncome;
        $collection->mergeWith($collectionCosts);
		$collection->add('profit',$profit);
		$collection->add('profitability_income',$profitabilityIncome);
		$collection->add('profitability_costs',$profitabilityCosts);
		return $collection;
	}
    
	public function getFinanceDataStat() {
        if (!method_exists($this->owner,'getFinanceIncomeDataStat') || !method_exists($this->owner,'getFinanceCostsDataStat')) throw new CException(__CLASS__ . ': Требуемые для работы методы не определены в классе');
		$collectionIncome = $this->owner->getFinanceIncomeDataStat();
		$collectionCosts = $this->owner->getFinanceCostsDataStat();
		$profit = array(); $profitabilityIncome = array(); $profitabilityCosts = array();
		for ($i = 0; $i <= 11; $i++) {
			$profit[$i] = $collectionIncome->income_total[$i] - $collectionCosts->costs[$i];
			$profitabilityIncome[$i] = $collectionIncome->income_total[$i] != 0 ? $profit[$i] / $collectionIncome->income_total[$i] * 100 : 0;
			$profitabilityCosts[$i] = $collectionCosts->costs[$i] != 0 ? $profit[$i] / $collectionCosts->costs[$i] * 100 : 0;
		}
		$collection = $collectionIncome;
        $collection->mergeWith($collectionCosts);
		$collection->add('profit',$profit);
		$collection->add('profitability_income',$profitabilityIncome);
		$collection->add('profitability_costs',$profitabilityCosts);
		return $collection;
	}
    
}

?>