<?php

class TrcenterController extends Controller {

	public $layout='//layouts/column2';
	public $modelName = 'TrCenter';
	
	public function actions() {
		return array(
			'ajaxUpdate' => array(
				'class' => 'application.controllers.actions.AjaxUpdateAction',
				'accessKey' => 'trcenter/update',
			),
		);
	}
	
	public function actionIndex() {
		if (!Yii::app()->user->checkAccess('trcenter/index')) throw new CHttpException(403);
		$this->layout = '//layouts/column1';
		$model = new TrCenter('search');
		$model->unsetAttributes();
		if (isset($_GET['TrCenter'])) $model->attributes = $_GET['TrCenter'];
		$this->render('index', array('model' => $model));
	}
	
	public function actionView($id) {
		if (!Yii::app()->user->checkAccess('trcenter/view')) throw new CHttpException(403);
		$this->layout = '//layouts/column1';
		$model = $this->loadModel($id);
		$groupModel = new TrGroup;
		$groupModel->tr_center_id = $model->id;
		$modelCosts = new TrCosts('center');
		$modelCosts->center_id = $model->id;
		$this->render('view', array('model' => $model, 'groupModel' => $groupModel, 'modelCosts' => $modelCosts));
	}
	
	public function actionUpdate($id) {
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('trcenter/update')) throw new CHttpException(403);
		$this->layout = '//layouts/column1';
		$managerList = UserMain::getManagerList();
		$model = $this->loadModel($id);
		
		if (isset($_POST['TrCenter'])) {
			$model->attributes = $_POST['TrCenter'];
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id));
			}
		} elseif ($model->managers) {
			foreach ($model->managers as $currentObj) {
				$model->manager[] = $currentObj->manager_id;
			}
		}
		
		$model->attachManagerNames();
		if ($model->manager) {
			$managerList = array_diff($managerList,$model->manager);
		}
		
		$this->render('update', array('model' => $model, 'managers' => $managerList));
	}
	
	public function actionCreate() {
		if (!Yii::app()->user->checkAccess('trcenter/create')) throw new CHttpException(403);
		
		$this->layout = '//layouts/column1';
		
		// access for managers?..
		$managerList = UserMain::getManagerList();
		
		$model = new TrCenter;
		
		if (isset($_POST['TrCenter'])) {
			$model->attributes = $_POST['TrCenter'];
			$model->project_id = Yii::app()->user->projectId;
			if ($model->save()) {
				$this->storeModel($model);
				$this->redirect(array('view','id'=>$model->id));
			}
			
			$model->attachManagerNames();
			$managerList = array_diff($managerList,$model->manager);

		}
		
		$this->render('create', array('model' => $model, 'managers' => $managerList));
	}
	
	public function actionDelete($id) {
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('trcenter/delete')) throw new CHttpException(403);
		$model = $this->loadModel($id);
		$model->delete();
		if (Yii::app()->request->isAjaxRequest) {
			$this->logAction();
		} else {
			$this->redirect(array('index'));
		}
		Yii::app()->end();
	}
	
	public function actionAjaxFinances($id) {
		$id = (int)$id;
		if (!Yii::app()->request->isAjaxRequest) throw new CHttpException(400);
		if (!isset($_POST['datefrom']) || !isset($_POST['dateto'])) throw new CHttpException(400);
		if (!Yii::app()->user->checkAccess('trcenter/update')) throw new CHttpException(403);
		$model = $this->loadModel($id);
		
		$dateFilter = new DateFilterForm;
		$dateFilter->datefrom = $_POST['datefrom'];
		$dateFilter->dateto = $_POST['dateto'];
		if (!$dateFilter->validate()) Yii::app()->end(); // здесь - вернуть ошибку с её текстом
		
		if (isset($_POST['ids'])) {
			$idSelector = array();
			foreach ($_POST['ids'] as $currentId) array_push($idSelector,(int)$currentId);
		} else {
			$idSelector = null;
		}
		
		$model->setDatetimeLimit($dateFilter,null,true);
		$income = $model->getFinanceIncomeData($idSelector)->toArray();
		$costs = array('costs' => 0, 'costs_summary' => 0, 'costs_table' => '');
		if ($modelCosts = $model->getCostsObject($idSelector)) {
			$costs['costs'] = $modelCosts->summaryCostsValue;
			$modelCosts->scenario = 'center';
			$modelCosts->idSelector = null;

		} else {
			$modelCosts = new TrCosts('center');
			$modelCosts->center_id = $model->id;
			$modelCosts->setDatetimeLimit($dateFilter,null,true);
		}
		$costs['costs_summary'] = $modelCosts->summaryCostsValue;
		$costs['costs_table'] = $this->renderPartial('_view_finance_summarycosts',array('modelCosts' => $modelCosts),true);
		
		echo CJSON::encode(array_merge($income,$costs,array('selector' => $this->renderPartial('_view_finance_selector',array('model' => $model),true))));
		Yii::app()->end();
	}
	
	public function actionAjaxCostsAdd($id) {
		$id = (int)$id;
		if (!Yii::app()->request->isAjaxRequest || !Yii::app()->request->isPostRequest) throw new CHttpException(400);
		if (!Yii::app()->user->checkAccess('trcenter/finance')) throw new CHttpException(403);
		if (!($center = TrCenter::model()->findByPk($id))) throw new CHttpException(404);

		$model = new TrCosts('center');
		$model->attributes = $_POST;
		$model->center_id = $id;
		if($model->save()) {
			$model->scenario = 'center';
			$this->logAction($model,'addCosts');
			echo CJSON::encode(array('costs' => $model->getSummaryCostsValue(), 'id'=>$model->id));
		} else {
			$errors = array_map(function($v){ return join(', ', $v); }, $model->getErrors());
			echo CJSON::encode(array('errors' => $errors));
		}
		Yii::app()->end();
	}

    public function actionAjaxCostsDel($id) {
		$id = (int)$id;
		if (!Yii::app()->request->isAjaxRequest || !Yii::app()->request->isPostRequest) throw new CHttpException(400);
		if (!Yii::app()->user->checkAccess('trcenter/finance')) throw new CHttpException(403);
		if (!($model = TrCosts::model()->findByPk($id))) throw new CHttpException(404);
		if ($model->user_id != Yii::app()->user->id) throw new CHttpException(403);

		if($model) {
            if ($model->delete())
			    echo CJSON::encode(array('success' => '1'));
            else
                echo CJSON::encode(array('error' => 'Ошибка удаления'));
		} else {
			echo CJSON::encode(array('error' => 'Данной позиции нет'));
		}
		Yii::app()->end();
	}
	
}

?>