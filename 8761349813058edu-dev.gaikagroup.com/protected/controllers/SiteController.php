<?php

class SiteController extends Controller {
	
	public function actionRecomment() {
		foreach ($data = TrIncome::model()->findAll() as $currObj) $currObj->update(array('comment'));
		echo 'Done.';
	}
	
	public function actionFillGroupId() {
		foreach ($data = TrClientLog::model()->findAll() as $currObj) {
			$currObj->tr_group_id = (int)Yii::app()->db->createCommand()->select('tr_group_id')->from('{{schedule}}')->where('id = '.$currObj->tr_lesson_id)->queryScalar();
			Yii::app()->db->createCommand()->update($currObj->tableName(),array('tr_group_id' => $currObj->tr_group_id),'id = '.$currObj->id);
		}
		echo 'Done.';
	}
	
}

?>