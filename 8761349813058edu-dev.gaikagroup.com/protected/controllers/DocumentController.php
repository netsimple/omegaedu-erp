<?php

class DocumentController extends Controller {
	
	public $modelName = 'UserDocuments';
        
    public function actionDelete($id) {
        $id = (int)$id;
        if (!Yii::app()->user->checkAccess('user/update')) throw new CHttpException(403);
        $model = $this->loadModel($id);
        $model->delete();
		$this->redirect(Yii::app()->request->urlReferrer ? Yii::app()->request->urlReferrer : array('user/view'));
    }
        
        

}

?>