<?php

class TrcourseController extends Controller {

	public $layout='//layouts/column2';
	public $modelName = 'TrCourse';
	
	public function actions() {
		return array(
			'ajaxUpdate' => array(
				'class' => 'application.controllers.actions.AjaxUpdateAction',
				'accessKey' => 'trcourse/update',
			),
		);
	}
	
	public function actionIndex() {
		if (!Yii::app()->user->checkAccess('trcourse/index')) throw new CHttpException(403);
		$this->layout = '//layouts/column1';
		$model = new TrCourse('search');
		$model->unsetAttributes();
		if (isset($_GET['TrCourse'])) $model->attributes = $_GET['TrCourse'];
		$this->render('index', array('model' => $model));
	}
	
	public function actionView($id) {
		if (!Yii::app()->user->checkAccess('trcourse/view')) throw new CHttpException(403);
		$model = $this->loadModel($id);
		$this->layout = '//layouts/column1';
		$groupModel = new TrGroup;
		$groupModel->tr_course_id = $model->id;
		$this->render('view', array('model' => $model, 'groupModel' => $groupModel));
	}
	
	public function actionUpdate($id) {
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('trcourse/update')) throw new CHttpException(403);
		$model = $this->loadModel($id);
		
		if (isset($_POST['TrCourse'])) {
			$model->attributes = $_POST['TrCourse'];
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id));
			}
		}
		
		$model->formatDates();
		
		$this->render('update', array('model' => $model));
	}
	
	public function actionCreate() {
		if (!Yii::app()->user->checkAccess('trcourse/create')) throw new CHttpException(403);
	
		$model = new TrCourse;
		
		if (isset($_POST['TrCourse'])) {
			$model->attributes = $_POST['TrCourse'];
			if ($model->save()) {
				$this->storeModel($model);
				$this->redirect(array('view','id'=>$model->id));
			}
		}
		
		$this->render('create', array('model' => $model));
		
	}
	
	public function actionDelete($id) {
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('trcourse/delete')) throw new CHttpException(403);
		$model = $this->loadModel($id);
		$model->delete();
		$this->redirect(array('index'));
	}
	
}

?>