<?php

class ClientController extends Controller {

	public $modelName = 'Client';
	public $layout='//layouts/column1';
	
	public function actions() {
		return array(
			'ajaxUpdate' => array(
				'class' => 'application.controllers.actions.AjaxUpdateAction',
				'accessKey' => 'client/update',
			),
			'ajaxUpdateRequest' => array(
				'class' => 'application.controllers.actions.AjaxUpdateAction',
				'modelName' => 'ClientRequest',
				'accessKey' => 'client/update',
			),
			'ajaxUpdateBalance' => array(
				'class' => 'application.controllers.actions.AjaxUpdateAction',
				'modelName' => 'TrIncome',
				'accessKey' => 'client/finance',
			),
		);
	}
	
	public function actionRequestList() {
		if (!Yii::app()->user->checkAccess('client/index')) throw new CHttpException(403);
		$criteria = new CDbCriteria;
		$criteria->select = 'id, fullname, email, phone_num, datetime, school, social_vk, other, status, source, project_id';
		$dataProvider = new CActiveDataProvider('ClientRequest', array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => 40),
			'sort' => array('defaultOrder' => 'datetime ASC'),
		));
		// if (Yii::app()->user->model->project_id) $criteria->compare('project_id',Yii::app()->user->model->project_id);
		if (Yii::app()->request->isAjaxRequest) {
			$this->renderPartial('_index_requests', array('dataProvider' => $dataProvider),false,true);
		} else {
			$this->render('_index_requests', array('dataProvider' => $dataProvider));
		}
	}
	
	public function actionDebtList() {
		if (!Yii::app()->user->checkAccess('client/debt')) throw new CHttpException(403);
		
		$filter = new DebtFilterForm;
		$filterData = Yii::app()->request->getPost(get_class($filter));
		if ($filterData) {
			$filter->attributes = $filterData;
			if (!$filter->validate()) throw new CHttpException(400);
			$update = $filter->update;
		}
		
		$result = array();
		$criteria = new CDbCriteria;
		$criteria->order = 'groups.tr_center_id ASC';
		
		$criteria->compare('t.project_id',$filter->partners);
		$withArr = array(
			'joinType' => 'INNER JOIN',
		);
		if ($filter->centers || $filter->groups) {
			$conditions = array();
			if ($filter->centers) array_push($conditions,'groups.tr_center_id IN ('.implode(', ',$filter->centers).')');
			if ($filter->groups) array_push($conditions,'groups.id IN ('.implode(', ',$filter->groups).')');
			$withArr['condition'] = implode(' AND ',$conditions);
		}
		$criteria->with = array('groups' => $withArr);
		
		foreach (Client::model()->findAll($criteria) as $currClient) {
			foreach ($currClient->groups as $currGroup) {
                if ($currClient->balanceRel > 0)
                    continue;

				if ($currGroup->getDebtByClientId($currClient->id)) {
					array_push($result, clone $currClient->attachGroup($currGroup));
					$filter->addBasePartner($currClient->partner);
					$filter->addBaseCenter($currClient->attachedGroup->center,$currClient->partner->id);
					$filter->addBaseGroup($currClient->attachedGroup,$currClient->attachedGroup->tr_center_id);
				}
			}
		}
		
		if (Yii::app()->request->isAjaxRequest) {
			if (!$filterData) {
				$this->renderPartial('_index_debts', array(
					'dataProvider' => new CArrayDataProvider($result, array('pagination' => array('pageSize' => 40))),
					'selector' => $filter,
				),false,true);
			} else {
				echo CJSON::encode(array(
					'grid' => $this->renderPartial('_index_debts_grid', array(
						'dataProvider' => new CArrayDataProvider($result, array('pagination' => array('pageSize' => 40))),
					),true,true),
                    'groups' => $filter->centers ? $this->renderPartial('_index_debts_groupfilter', array(
                        'selector' => $filter,
                    ),true,true) : '',
					'centers' => $filter->partners ? $this->renderPartial('_index_debts_centerfilter', array(
						'selector' => $filter,	
					),true,true) : '',
					'refreshcenters' => $update == 'partner',
					'refreshgroups' => $update == 'center',
				));
			}
			Yii::app()->end();
		} else {
			$this->render('_index_debts', array(
				'dataProvider' => new CArrayDataProvider($result, array('pagination' => array('pageSize' => 40))),
				'selector' => $filter,
			));
		}
	}
	
	public function actionIndex() {
		if (!Yii::app()->user->checkAccess('client/index')) throw new CHttpException(403);
		$model = new Client('search');
		$model->unsetAttributes();
		if (isset($_GET['Client'])) $model->attributes = $_GET['Client'];
		if ($model->reg_date) $model->validate(array('reg_date'));
		$this->render('index', array('model' => $model, 'item' => Yii::app()->request->getParam('new') !== null ? 1 : 0));
	}
	
	public function actionView($id) {
		if (!Yii::app()->user->checkAccess('client/view')) throw new CHttpException(403);
		if (!($model = Client::model()->findByPk($id))) $this->redirect(array('client/index'));
		$comments = Yii::app()->user->checkAccess('client/update','addcomment') || Yii::app()->user->checkAccess('client/view','comment') ? new ClientComments('search') : null;
		if ($comments) {
			$comments->unsetAttributes();
			if (isset($_GET['ClientComments'])) {
				$comments->attributes = $_GET['ClientComments'];
			}
			$comments->client_id = $id;
		}
		$clientBalance = new TrIncome('clientUpdate');
		$clientBalance->client_id = $id;
		$this->render('view', array('model' => $model, 'comments' => $comments, 'clientBalance' => $clientBalance));
	}
	
	/*
	 * Request -> Client
	 */
	
	public function actionComplete($id) {
		if (!Yii::app()->user->checkAccess('client/update')) throw new CHttpException(403);
		
		if (!($request = ClientRequest::model()->findByPk($id))) throw new CHttpException(404, 'Указанная Вами заявка в системе не зарегистрирована!');
		
		// ClientRequest -> Client
		$model = $request->extend();
		$model->reg_date = time();
		$model->project_id = Yii::app()->user->projectId;
		
		if ($model->save()) {
			$request->hiddenDelete();
			$this->redirect(array('view','id'=>$model->id));
		}

        Yii::app()->user->setFlash('clientImportError', $model->getErrors());
		$this->redirect(array('client/new'));
	}
	
	public function actionReject($id) {
		if (!Yii::app()->user->checkAccess('client/delete')) throw new CHttpException(403);
		if (!($model = ClientRequest::model()->findByPk($id))) throw new CHttpException(404, 'Указанная Вами заявка в системе не зарегистрирована!');
		$model->delete();
		$this->logAction($model,'delete');
	}
	
	public function actionComment($id) {
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('client/update','addcomment')) throw new CHttpException(403);
		if (isset($_POST['ClientComments'])) {
			$commentsObj = new ClientComments;
			$commentsObj->attributes = $_POST['ClientComments'];
			$commentsObj->user_id = Yii::app()->user->id;
			$commentsObj->client_id = $id;
			$commentsObj->save();
			$this->storeModel($commentsObj);
		}
		$this->redirect(array('view', 'id' => $id));
	}
	
	public function actionCreate() {
		
		if (!Yii::app()->user->checkAccess('client/create')) throw new CHttpException(403);
		
		// установить права доступа #pdovlatov
		
		$managerList = UserMain::getManagerList();
	
		$model = new Client(($managerList && (Yii::app()->user->model->is_partner || Yii::app()->user->model->is_owner)) ? 'createWithManagers' : 'createDefault');
		
		if (isset($_POST['Client'])) {
			$model->attributes = $_POST['Client'];
			$model->reg_date = time();
			$model->project_id = Yii::app()->user->projectId;
			
			if ($model->save()) {
				$this->storeModel($model);
				$this->beforeRedirect();
                echo "<script>location.href='".$this->createUrl('client/view', array('id' => $model->id))."'</script>";
                Yii::app()->end();
			}
		}
        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('_form', array('model' => $model, 'managers' => $managerList), false, true);
        } else {
            $this->render('create', array('model' => $model, 'managers' => $managerList));
        }
		
	}
	
	public function actionDelete($id) {
		$id = (int)$id;
		if (!Yii::app()->user->checkAccess('client/delete')) throw new CHttpException(403);
		$model = $this->loadModel($id);
		$model->delete();
		$this->redirect(array('client/index'));
	}
	
	public function actionAjaxFinance($id) {
		$id = (int)$id;
		if (!Yii::app()->request->isAjaxRequest) $this->redirect(array('view', 'id' => $id));
		if (!Yii::app()->user->checkAccess('client/finance')) throw new CHttpException(403);
		$client = $this->loadModel($id);

		if(Yii::app()->request->isPostRequest) {
			$model = new TrIncome('clientInsert');
			$model->attributes = Yii::app()->request->getPost('TrIncome');
			$model->client_id = $id;
			if($model->save()) {
				$this->logAction($model,'balance');
				$newBalance = new TrIncome('clientInsert');
				$newBalance->client_id = $id;
				echo CJSON::encode(array('balance' => $client->getRelated('balanceRel',true)));
			} else {
				$errors = array_map(function($v){ return join(', ', $v); }, $model->getErrors());
				echo CJSON::encode(array('errors' => $errors));
			}
		} else {
			throw new CHttpException(400);  
		}
	}
	
	public function actionSuggestName($term) {
		if (!Yii::app()->request->isAjaxRequest) throw new CHttpException(400);
		if (!Yii::app()->user->checkAccess('client/index')) throw new CHttpException(403);
		echo CActiveRecord::model($this->modelName)->getJSONNames($term);
		Yii::app()->end();
	}
	
	public function actionGetCourses($id) {
		$id = (int)$id;
		if (!Yii::app()->request->isAjaxRequest) throw new CHttpException(400);
		if (!Yii::app()->user->checkAccess('trcourse/index')) throw new CHttpException(403);
		$model = new TrIncome('clientInsert');
		$model->client_id = $id;
		echo CJSON::encode($model->courseList);
		Yii::app()->end();
	}

    public static function actionAddToGroup($clientId, $groupId) {

        if (!Yii::app()->request->isAjaxRequest) throw new CHttpException(400);
        if (!Yii::app()->user->checkAccess('client/update')) throw new CHttpException(403);

        Client::addToGroup($clientId, $groupId);

    }

    public static function actionDeleteFromGroup($clientId, $groupId) {

        if (!Yii::app()->request->isAjaxRequest) throw new CHttpException(400);
        if (!Yii::app()->user->checkAccess('client/update')) throw new CHttpException(403);

        Client::deleteFromGroup($clientId, $groupId);

    }
	
}