<?php

class ContractController extends Controller {
    
	public $modelName = 'Contract';
    public $layout = '//layouts/column1';
	
	public function actions() {
		return array(
			'ajaxUpdate' => array(
				'class' => 'application.controllers.actions.AjaxUpdateAction',
				'accessKey' => 'contract/update',
			),
		);
	}
	
	public function actionGetClientName($term) {
		if (!Yii::app()->request->isAjaxRequest || (!Yii::app()->user->checkAccess('contract/create') && !Yii::app()->user->checkAccess('contract/update'))) Yii::app()->end();
		echo Client::model()->getJSONNames($term);
		Yii::app()->end();
	}
    
    public function actionPdf($id, $lib = 'mpdf') {
		if (!Yii::app()->user->checkAccess('contract/pdf')) throw new CHttpException(403);

		$id = (int)$id;
		$model = $this->loadModel($id);
		if (Yii::app()->user->hasFlash('notice')) {
			$this->render('notice', array('model' => $model));
		} else {
			switch ($lib) {
				case 'html2pdf':
					$pdfConverter = Yii::app()->ePdf->HTML2PDF();
					break;
				default:
					$pdfConverter = Yii::app()->ePdf->mPDF();
			}
			$pdfConverter->WriteHTML($this->renderPartial('pdf', array('model' => $model), true));
			$pdfConverter->Output();
		}
		Yii::app()->end();
    }

    public function actionIndex() {
		if (!Yii::app()->user->checkAccess('contract/index')) throw new CHttpException(403);
        $model = new Contract('search');
		$model->unsetAttributes();
		if (isset($_GET['Contract']))
            $model->attributes = $_GET['Contract'];
        $this->render('index', array('model' => $model));
    }

    public function actionCreate($id = 0) {
		if (!Yii::app()->user->checkAccess('contract/create')) throw new CHttpException(403);

        $id = (int)$id;
		
		if (!$id) {
			
			$this->render('create', array('model' => null, 'course' => null));
			
		} else {

			$model = new Contract('create');
			if (!($client = Client::model()->findByPk($id))) throw new CHttpException(400);
			$model->client_id = $id;
			$model->client_name = $client->fullname;
		   
			if (isset($_POST['Contract'])) {
				$model->attributes = $_POST['Contract'];
				if (isset($_POST['ContractCourse'])) {
					foreach ($_POST['ContractCourse'] as $contractCourseData) {
						$courseObj = new ContractCourse;
                        if (isset($contractCourseData['id']))
                            unset ($contractCourseData['id']);

						$courseObj->attributes = $contractCourseData;
						$model->addCourse($courseObj);
					}
				}
				if ($model->save()) {
					$errors = 0;
					foreach ($model->getAllCourses() as $courseObj) {
						$courseObj->contract_id = $model->id;
						if (!$courseObj->save()) {
							// some error, I guess
							$errors++;
						};
					}
					if (!$errors) {
						$this->storeModel($model);
						Yii::app()->user->setFlash('notice',$model->client_id);
						$this->redirect(array('pdf', 'id' => $model->id));
					}
				}
			} else {
				$model->getClientData($client);
			}
			
			$model->formatDates();
			
			$course = new TrCourse;
	
			$this->render('create', array('model' => $model, 'course' => $course));
		
		}

    }

    public function actionUpdate($id) {
		if (!Yii::app()->user->checkAccess('contract/update')) throw new CHttpException(403);
        $id = (int)$id;
        
        $model = $this->loadModel($id);
		$model->dublicateCheck();
        
        if (isset($_POST['Contract'])) {
            $model->attributes = $_POST['Contract'];
            if ($model->save()) {
                $errors = 0;
				$oldCourses = array();
				foreach ($model->courses as $currCourse) $oldCourses[$currCourse->id] = $currCourse;
                if (isset($_POST['ContractCourse'])) {
                    foreach ($_POST['ContractCourse'] as $contractCourseData) {
                        if (!$contractCourseData['id'] || !($courseObj = ContractCourse::model()->findByPk((int)$contractCourseData['id'])) || $courseObj->contract_id != $model->id) {
                            $courseObj = new ContractCourse;
                        }
                        $courseObj->attributes = $contractCourseData;
                        $courseObj->contract_id = $model->id;
                        if (!$courseObj->save()){
                            // some error, I guess
							$model->addErrors($courseObj->errors);
                            $errors++;
                        }
						$model->addCourse($courseObj);
						if (!$courseObj->isNewRecord && isset($oldCourses[$courseObj->id])) unset($oldCourses[$courseObj->id]);
                    }
                }
				foreach ($oldCourses as $currCourse) $currCourse->delete();
                if (!$errors) {
					Yii::app()->user->setFlash('notice',$model->client_id);
                    $this->redirect(array('pdf', 'id' => $model->id));
                }
            }
        }
        
        $course = new TrCourse;
        $course->project_id = Yii::app()->user->projectId;
		
		$model->formatDates();
        
        $this->render('update', array('model' => $model, 'course' => $course));
    }
    
    public function actionDelete($id) {
		if (!Yii::app()->user->checkAccess('contract/delete')) throw new CHttpException(403);
        $id = (int)$id;
        $model = $this->loadModel($id);
        $model->delete();
		$this->logAction();
    }
    
    public function actionCourseForm($groupId, $index) {
		if (!Yii::app()->user->checkAccess('contract/create') && !Yii::app()->user->checkAccess('contract/update'))
            throw new CHttpException(403);

        $groupId = (int)$groupId;
        $index = (int)$index;
        $group = TrGroup::model()->findByPk($groupId);

        if (!Yii::app()->request->isAjaxRequest || !$group || !$group->course || $index < 0) throw new CHttpException(400);
        $model = new ContractCourse;
        $model->course_id = $group->course->id;
        echo $this->renderPartial('_form_courses', array('model' => $model, 'index' => $index), true);
    }
    
    protected function loadModel($primaryKey,$className=null) {
		if (parent::loadModel($primaryKey)) $this->_model->scenario = 'update';
		return $this->_model;
    }

}

?>