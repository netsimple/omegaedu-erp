<?php

class UserController extends Controller {
	
	const DOCUMENTS_FILES_MAX = 30;
	
	private $_allowedAvatars = array('jpg','jpeg','png','gif');
	private $_allowedDocuments = array('jpg','jpeg','png','gif','pdf','doc','docx');

	public $layout='//layouts/column2';
	
	public $defaultAction = 'view';
	public $modelName = 'UserMain';
	
	public function filterAccessControl($filterChain) {

	}
	
	public function actions() {
		return array(
			'ajaxUpdateRate' => array(
				'class' => 'application.controllers.actions.AjaxUpdateAction',
				'modelName' => 'CourseRates',
				'accessKey' => 'user/rate',
			),
		);
	}
	    
	public function actionError() {
		if($error=Yii::app()->errorHandler->error) {
			switch ($error['code']) {
				case 401:
					if (!$error['message']) $error['message'] = 'Вы не авторизованы в системе.';
					break;
				case 403:
					if (!$error['message']) $error['message'] = 'Доступ запрещен.';
					break;
				case 404:
					if (!$error['message']) $error['message'] = 'Объект не найден.';
					break;
			}
			if(Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}
	
	
	public function actionAttach() {
	
		/*
		 * Авторизация через OpenID
		 */
	
		$service = Yii::app()->request->getQuery('service');
		if (isset($service)) {
			$authIdentity = Yii::app()->eauth->getIdentity($service);
			$authIdentity->redirectUrl = Yii::app()->user->returnUrl;
			$authIdentity->cancelUrl = $this->createAbsoluteUrl('view');

			if ($authIdentity->authenticate()) {
				$identity = new ExtAuthUserIdentity($authIdentity);
				if ($identity->attach()) {
					$authIdentity->redirect();
				} else {
					$authIdentity->cancel();
				}
			}
		}
		
		$this->redirect(array('view'));
		
	}
	
	
	public function actionLogin() {

        $this->layout = '//layouts/login';

		/*
		 * Авторизация через OpenID
		 */
	
		$service = Yii::app()->request->getQuery('service');
		if (isset($service)) {
			$authIdentity = Yii::app()->eauth->getIdentity($service);
			$authIdentity->redirectUrl = Yii::app()->user->returnUrl;
			$authIdentity->cancelUrl = $this->createAbsoluteUrl('login');

			if ($authIdentity->authenticate()) {
				$identity = new ExtAuthUserIdentity($authIdentity);

				// успешная авторизация
				if ($identity->authenticate()) {
					Yii::app()->user->login($identity);

					// специальное перенаправления для корректного закрытия всплывающего окна
					$authIdentity->redirect();
				}
				else {
					// закрытие всплывающего окна и перенаправление на cancelUrl
					$authIdentity->cancel();
				}
			}

			// авторизация не удалась, перенаправляем на страницу входа
			$this->redirect(array('login'));
		}
		
		/*
		 * Здесь мы оказываемся в случае, если авторизация через OpenID не удалась
		 */
	
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(/*Yii::app()->user->returnUrl*/array('view'));
		}
		// display the login form
		/*
		 * В представлении "login" должна также содержаться форма OpenID-авторизации
		 */
		$this->render('login',array('model'=>$model));
	}
	
	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionView($target='profile',$id=0) {
		$target = strtolower((string)$target);
		$id = (int)$id;
		if (Yii::app()->user->isGuest) $this->redirect(array('login'));

		$currentUser = Yii::app()->user->model;
		if ($target == 'profile') {

			$this->layout = '//layouts/column1';
				
			/*
			 * Пространство профиля
			 */
				
			if (!$id) $id = $currentUser->id;
			if ($id != $currentUser->id) {
					
				/*
				 * Пространство чужого профиля
				 */
					
				$model = UserMain::model()->findByPk($id);
				if (!$model) throw new CHttpException(404);

				if (!Yii::app()->user->checkAccess('user/profile', $model->roles)) throw new CHttpException(403);
					
				$openid = array();
			} else {
				/*
				 * Пространство собственного профиля
				 * Обработка данных OpenID
				 */
				$model = &$currentUser;
				$criteria = new CDbCriteria();
				$criteria->select = 'service_name';
				$criteria->condition = 'user_id=:uid';
				$criteria->params = array(':uid' => $id);
				$openid = Extauth::model()->findAll($criteria);
			}
				
			$advert = new Advert('search');
			$advert->unsetAttributes();
			if (isset($_GET['Advert'])) $advert->attributes = $_GET['Advert'];
			$advert->user_id = $id;
				
			if ($model->inTeacherGroup) {
				$courseRates = new CourseRates;
				$courseRates->teacher_id = $id;
			} else {
				$courseRates = null;
			}
			
			$this->render('profile', array(
				'model' => $model,
				'advert' => $advert,
				'courseRates' => $courseRates,
				'activeServices' => $openid,
				'modelCosts' => $model->getCostsObject(),
			));
		} else {

			/*
			 * Пространства списка пользователей
			 */
				
			if (!Yii::app()->user->checkAccess('user/view', $target)) throw new CHttpException(403);
				
			$this->layout = '//layouts/column1';

			$model = new UserMain('search');
			$model->unsetAttributes();
			if (isset($_GET['UserMain'])) $model->attributes = $_GET['UserMain'];
			$model->setType($target);
			$this->render('view', array(
				'type' => $target,
				'model'=> $model,
			));
				
		}
	}
	
	public function actionAjaxUpdate() {
		if (!Yii::app()->request->isAjaxRequest || !isset($_POST['name'])) throw new CHttpException(400);
		$field = $_POST['name'];
		if (isset($_POST['pk'])) {
			$primaryKey = (int)$_POST['pk'];
			$model = null;
			foreach(array('UserMain','UserPartner','UserTeacher') as $currentModelName) {
				if (in_array($_POST['name'],CActiveRecord::model($currentModelName)->ajaxUpdateFields)) {
					$model = CActiveRecord::model($currentModelName)->findByPk($primaryKey);
					break;
				}
			}
			if (!$model) throw new CHttpException(400);
			if (Yii::app()->user->id == $model->primaryKey && !Yii::app()->user->checkAccess('user/update', 'self')) throw new CHttpException(403);
			if (Yii::app()->user->id != $model->primaryKey && get_class($model) != 'UserMain') {
				$mainObj = UserMain::model()->findByPk($primaryKey);
				if (!Yii::app()->user->checkAccess('user/update', $mainObj->roles)) throw new CHttpException(403);
			}
			$model->scenario = 'ajaxUpdate';
			$model->$field = $_POST['value'];
			if ($model->save(true,array($field))) {
				ActivityLog::add($model,'update',array('attributeName' => $field));
				echo CJSON::encode(array('errors' => null));
			} else {
				echo CJSON::encode(array('errors' => array_map(function($v){return implode(',',$v);},$model->errors)));
			}
		}
		Yii::app()->end(200);
	}
	
	public function actionAjaxCourseFinances($id) {
		if (!Yii::app()->user->checkAccess('user/finances')) throw new CHttpException(403);
		$id = (int)$id;
		if (!Yii::app()->request->isAjaxRequest) throw new CHttpException(400);
		$result = array('income' => 0, 'costs' => 0);
		if (isset($_POST['ids'])) {
			if (!($model = UserMain::model()->findByPk($id))) throw new CHttpException(404);
			$idSelector = array();
			foreach ($_POST['ids'] as $currentId) {
				array_push($idSelector,(int)$currentId);
			}
			$result['income'] = $model->getLessonsPrice($idSelector);
			$result['costs'] = $model->getLessonsCost($idSelector);
		}
		echo CJSON::encode($result);
		Yii::app()->end();
	}

    public function actionAvatarCrop() {

        $id = (int)Yii::app()->request->getPost('uid');
        if (!$id || $id == Yii::app()->user->id) {
            if (!Yii::app()->user->checkAccess('user/update', 'self')) throw new CHttpException(403);
            $model = Yii::app()->user->model;
        } else {
            if (!($model = UserMain::model()->findByPk($id)))
                throw new CHttpException(404, 'Указанный Вами пользователь в системе не зарегистрирован.');
            if (!Yii::app()->user->checkAccess('user/update', $model->roles)) throw new CHttpException(403);
        }

        Yii::import('ext.jcrop.EJCropper');
        $jcropper = new EJCropper();
        $jcropper->thumbPath = Yii::getPathOfAlias('webroot').'/'.Yii::app()->params['uploadDirectory'].'/avatars';

        $coords = $jcropper->getCoordsFromPost('imageToCrop');
        foreach ($coords as $k => $v)
            $coords[$k] = $v * 2;
			
		$thumbName = 'u'.$model->id.'_d'.date('YmdHis').'.'.pathinfo($model->avatar_name,PATHINFO_EXTENSION);

        $jcropper->jpeg_quality = 95;
        $jcropper->png_compression = 8;
        $jcropper->targ_h = 100;
        $jcropper->targ_w = $coords['w'] / $coords['h'] * $jcropper->targ_h;
		$jcropper->thumbName = $thumbName;


        if ($newName = $jcropper->crop(Yii::getPathOfAlias('webroot').'/'.Yii::app()->params['uploadDirectory'].'/avatars/orig/'.$model->avatarMain, $coords)) {
			$model->scenario = 'crop';
			$model->avatar_name = $thumbName;
			$model->save(true,array('avatar_name'));
		}
		
		ActivityLog::add($model,'crop');
		echo Yii::app()->baseUrl . '/'.Yii::app()->params['uploadDirectory'].'/avatars/' . $thumbName;
		Yii::app()->end();
    }

    public function actionAvatarUpload() {
        $id = (int)Yii::app()->request->getPost('touid');
        if (!$id || $id == Yii::app()->user->id) {
            if (!Yii::app()->user->checkAccess('user/update', 'self')) throw new CHttpException(403);
            $model = Yii::app()->user->model;
        } else {
            if (!($model = UserMain::model()->findByPk($id)))
                throw new CHttpException(404, 'Указанный Вами пользователь в системе не зарегистрирован.');
            if (!Yii::app()->user->checkAccess('user/update', $model->roles)) throw new CHttpException(403);
        }
		
		$model->scenario = 'crop';

        $webFolder = '/'.Yii::app()->params['uploadDirectory'].'/avatars/';
        $mainFolder = Yii::getPathOfAlias('webroot').$webFolder;
        $origName = isset($_REQUEST['qqfilename']) ? $_REQUEST['qqfilename'] : 'png-file-default.png';
        $fileName = "u" . $model->id . "_d" . date("YmdHis") . "." . pathinfo($origName, PATHINFO_EXTENSION);
        $fullTmpFilePath = $mainFolder . 'tmp_' .$fileName;
		$oldAvatar = $model->avatar_name; 
		$model->avatar_name = $fileName;
		$bigFileName = $model->avatarMain;
		
        Yii::import("ext.EFineUploader.qqFileUploader");
        $uploader = new qqFileUploader();
        $uploader->allowedExtensions = array('jpg','jpeg','png','gif');
        $uploader->sizeLimit = 10 * 1024 * 1024;//maximum file size in bytes
        $uploader->chunksFolder = $mainFolder.'chunks';

        $result = $uploader->handleUpload($mainFolder, $fullTmpFilePath);
		if (isset($result['success']) && $result['success']) {
			$result['file'] = $webFolder . $fileName;
			$result['bigFile'] = $webFolder.'orig/'.$bigFileName;
			
			$dst = $mainFolder.'orig/'.$bigFileName;
			if (file_exists($dst)) @unlink($dst);
			$image = new EasyImage($fullTmpFilePath);
			$image->resize(NULL, 1000);
			$image->save($dst);			
			@unlink($fullTmpFilePath);
			if (!file_exists($dst)) Yii::app()->end();
			
			$dst = $mainFolder.$fileName;
			if (file_exists($dst)) @unlink($dst);
			$image = new EasyImage($mainFolder.'orig/'.$bigFileName);
			$min = min($image->image()->width, $image->image()->height);
			$image->crop($min,$min);
			$image->resize(100, 100);
			$image->save($dst);
			if (!file_exists($dst)) Yii::app()->end();
			
			if (!$model->save(true,array('avatar_name'))) {
				$result = array('success'=>false, 'error'=>$model->getErrors());
			} else {
				if ($oldAvatar) @unlink($mainFolder.$oldAvatar);
			}
		}

		ActivityLog::add($model,'avatar');
        header("Content-Type: text/plain");
        $result=htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $result;
        Yii::app()->end();

    }
	
	protected function loadModel($primaryKey,$className = null) {
		if (!empty($this->_model)) return $this->_model;
		if (!$primaryKey || $primaryKey == Yii::app()->user->id) {
			$this->_model = Yii::app()->user->model;
		} else {
			if (!($this->_model = UserMain::model()->findByPk($primaryKey))) {
				throw new CHttpException(404, 'Указанный Вами пользователь в системе не зарегистрирован.');
			}
		}
		return $this->_model;
	}

	public function actionUpdate($id=0) {
		$id = (int)$id;
		$model = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('user/update',$model->id == Yii::app()->user->id ? 'self' : $model->roles)) throw new CHttpException(403);
		
		$target = $model->role;
		
		if (isset($_POST['UserMain'])) {
			
			$error = false;
			
			$boolRolePartner = $model->is_owner || $model->is_partner;
			if ($boolRolePartner) {
				$error = $error || !isset($_POST['UserPartner']);
				if (!$error) $model->partner->attributes = $_POST['UserPartner'];
			}
			
			$boolRoleManager = $model->is_manager || $model->is_adman;
			if ($boolRoleManager) {
				if (!$error) {
					if (isset($_POST['is_adman']) && $_POST['is_adman']) {
						$model->role = 'adman';
					} else {
						$model->eraseRole('adman');
					}
				}
			}
			
			$boolRoleTeacher = $model->is_teacher;
			if ($boolRoleTeacher) {
				$error = $error || !isset($_POST['UserTeacher']);
				if (!$error) $model->teacher->attributes = $_POST['UserTeacher'];
			}
			
			$model->attributes = $_POST['UserMain'];
			
			if ($this->hasAvatarToUpload()) {
				$this->uploadAvatar($model);
			}
			
			if ($model->save()) {
				$error = $error || ($boolRolePartner && !$model->partner->save()) || /*($model->manager && !$model->manager->save()) ||*/ ($boolRoleTeacher && !$model->teacher->save());
				if (!$error) {
					if ($this->hasDocumentsToUpload()) $this->uploadDocuments($model->id);
					$this->redirect(array('view', 'id' => $model->id));
				}
			}
		}
		
		$model->formatDates();
		if ($model->inTeacherGroup && $model->teacher) {
			$model->teacher->formatDates();
		}
		
		$this->render('update', array(
			'model' => $model,
			'target' => $target,
		));
	}
	
	public function actionCreate($target) {
		
		if (!Yii::app()->user->checkAccess('user/create', $target)) throw new CHttpException(403);

		$model = new UserMain('create');
		$model->role = $target;
		if (!$model->roles)
			throw new CHttpException(400, 'Выполняемый Вами запрос некорректен.');
		
		if (isset($_POST['UserMain'])) {

			if (isset($_POST['is_adman']) && $_POST['is_adman']) $model->role = 'adman';
			
			$model->attributes = $_POST['UserMain'];
			
			if ($this->hasAvatarToUpload()) {
				$this->uploadAvatar($model);
			}
			
			$error = false;
				
			if ($model->inPartnerGroup) {
				$error = $error || !isset($_POST['UserPartner']);
				if (!$error) {
					if (!$model->partner) $model->partner = new UserPartner;
					$model->partner->attributes = $_POST['UserPartner'];
				}
			}
			if (!$error && $model->inManagerGroup) {
				if (!$error) {
					if (!$model->manager) $model->manager = new UserManager;
				}
			}
			if (!$error && $model->inTeacherGroup) {
				$error = $error || !isset($_POST['UserTeacher']);
				if (!$error) {
					if (!$model->teacher) $model->teacher = new UserTeacher;
					$model->teacher->attributes = $_POST['UserTeacher'];
				}
			}

			if ($model->save() && !$error) {
				
				if ($model->inPartnerGroup) {
					$model->partner->user_id = $model->id;
					$error = !$model->partner->save();
				}
				if (!$error && $model->inManagerGroup) {
					$model->manager->user_id = $model->id;
					$error = !$model->manager->save();
				}
				if (!$error && $model->inTeacherGroup) {
					$model->teacher->user_id = $model->id;
					$error = !$model->teacher->save();
				}
	
				if (!$error) {
					if ($this->hasDocumentsToUpload()) {
						$this->uploadDocuments($model->id);
					}
					
					$this->storeModel($model);

                    if (Yii::app()->request->isAjaxRequest) {
						$this->beforeRedirect();
                        echo "<script>location.href='".$this->createUrl('user/view', array('id' => $model->id))."'</script>";
                        Yii::app()->end();
                    }

					$this->redirect(array('view', 'id' => $model->id));
				}
			} else if (Yii::app()->request->isAjaxRequest) {

                if ($model->inPartnerGroup && !$model->partner) $model->partner = new UserPartner;
                if ($model->inManagerGroup && !$model->manager) $model->manager = new UserManager;
                if ($model->inTeacherGroup && !$model->teacher) $model->teacher = new UserTeacher;

                $model->formatDates();
                if ($model->inTeacherGroup && $model->teacher) {
                    $model->teacher->formatDates();
                }

                $this->renderPartial('_form_mini', array('model'=>$model, 'target'=>$target));
                Yii::app()->end();

            }
		}

		if ($model->inPartnerGroup && !$model->partner) $model->partner = new UserPartner;
		if ($model->inManagerGroup && !$model->manager) $model->manager = new UserManager;
		if ($model->inTeacherGroup && !$model->teacher) $model->teacher = new UserTeacher;
		
		$model->formatDates();
		if ($model->inTeacherGroup && $model->teacher) {
			$model->teacher->formatDates();
		}
		
		$this->render('create', array(
			'model' => $model,
			'target'=> $target, // TODO: проверку можно сделать по уже присвоенным модели ролям
		));
		
	}
	
	public function actionDelete($id) {
		
		$id = (int)$id;
		
		$model = $this->loadModel($id);
		
		if (!Yii::app()->user->checkAccess('user/delete', $model->roles)) throw new CHttpException(403);
		
		if ($model->id == Yii::app()->user->id) {
			throw new CHttpException(400, 'Данный запрос в системе не разрешен: свой профиль удалить нельзя.');
		}

		$model->delete();
		$this->redirect(array('view'));
		
	}
	
	public function actionPasschange() {
		$model = new PassChangeForm;
		
		if(isset($_POST['ajax']) && $_POST['ajax']==='restore-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if (isset($_POST['PassChangeForm'])) {
			$model->attributes = $_POST['PassChangeForm'];
			if ($model->validate() && $model->passchange()) {
				$this->redirect(array('view'));
			}
		}
		
		$this->render('passchange', array (
			'model' => $model,
		));
	}
	
	public function actionPassremind() {
		$model = new PassRemindForm;
		
		if(isset($_POST['ajax']) && $_POST['ajax']==='remind-form')	{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		if (isset($_POST['PassRemindForm'])) {
			$model->attributes = $_POST['PassRemindForm'];
			if ($model->validate() && $model->passremind()) {
				Yii::app()->user->logout();
				$this->redirect(array('login'));
			}
		}
		
		$this->render('passremind', array(
			'model' => $model,
		));
		
	}
	
	
	/*
	 * internal functional
	 */
	
	
	private function hasAvatarToUpload($modelName = 'UserMain') {
		return isset($_FILES[$modelName]) && isset($_FILES[$modelName]['name']['avatar_name']);
	}
	
	private function hasDocumentsToUpload($modelName = 'UserMain') {
		return isset($_FILES[$modelName]) && isset($_FILES[$modelName]['name']['documents']);
	}
	
	public function uploadAvatar($model) {
	    $avatar = array();
	    foreach ($_FILES['UserMain'] as $key => $value) {
		$avatar[$key] = $value['avatar_name'];
	    }
	    $upload = new Upload($avatar);
	    // эта опция почему-то не работает
	    // doc API: http://www.verot.net/php_class_upload_docs.htm
	    $upload->allowed	   = array('image/*');
	    /*
	     * Ручная проверка формата
	     */
	    // #pdovlatov #notice расширения вынести в настройки
	    if (!in_array($upload->file_src_name_ext,$this->_allowedAvatars))
		return false;

	    $upload->no_script     = false;
	    $upload->image_resize  = true;
	    $upload->image_x       = 150;
	    $upload->image_y       = 150;
	    $upload->image_ratio   = true;
		    
	    $newName  = UserMain::generateRandomSequence(8).'_'.date('YmdHis');
	    // #pdovlatov #notice вынести в настройки
	    $destPath = Yii::app()->getBasePath().'/../'.Yii::app()->params['uploadDirectory'].'/avatars/';
	    $destName = '';
		    
	    if ($upload->uploaded) {
		$upload->file_new_name_body = $newName;                     
		$upload->process($destPath);
			    
		// if was processed
		if ($upload->processed) {
		    $destName = $upload->file_dst_name;
			    
		    if (!$model->isNewRecord) {
			if (file_exists($destPath.$model->avatar_name)) {
				@unlink($destPath.$model->avatar_name);
			}
			if (file_exists($destPath.'th_'.$model->avatar_name)) {
				@unlink($destPath.'th_'.$model->avatar_name);
			}
			
		    }
		    $model->avatar_name = $newName.'.'.$upload->file_dst_name_ext;
		    // имеем в виду принцип именования
		    // $model->avatar_thumb= 'th_'.$newName;
			    
		    unset($upload);                       
			    
		    $upload = new Upload($destPath.$destName);
		    $upload->file_new_name_body   = 'th_' . $newName;
		    $upload->no_script            = false;
		    $upload->image_resize         = true;
		    $upload->image_x              = 65;
		    $upload->image_y              = 65;
		    $upload->image_ratio          = true;
		    $upload->process($destPath);
				    
		    return true;
				      
		} else {
		    return false;
		    // echo($upload->error);
		}
	    } else {
		return false;
	    }
	}
	
	public function actionUploadDocuments($user_id) {
        $id = (int)$user_id;
		$model = $this->loadModel($id);
		if (!Yii::app()->user->checkAccess('user/update',$model->id == Yii::app()->user->id ? 'self' : $model->roles)) throw new CHttpException(403);

        if ($this->hasDocumentsToUpload()) $this->uploadDocuments($model->id);

        $this->redirect(array('view', 'id' => $model->id));
    }

	public function uploadDocuments($user_id, $modelName = null) {
		if ($modelName === null) $modelName = $this->modelName;
		
	    // name, type, tmp_name, error, size
	    
	    $files = array();
	    foreach ($_FILES[$modelName] as $key => $curr_doc) {
		if (isset($curr_doc['documents']))
			foreach ($curr_doc['documents'] as $order_key => $value) {
				$files[$order_key][$key] = $value;
			}
	    }
	    if (!$files) return false;

	    $count = UserDocuments::model()->count('user_id='.$user_id);

	    if ($count >= self::DOCUMENTS_FILES_MAX) return false;
	    
	    foreach ($files as $current_document) {
			$upload = new Upload($current_document);
			
			// #pdovlatov #notice расширения вынести в настройки
			
			if (!in_array($upload->file_src_name_ext,$this->_allowedDocuments)) return false;
		
			$upload->no_script     = false;
				
			$newName  = UserMain::generateRandomSequence(8).'_'.date('YmdHis');
			// #pdovlatov #notice вынести в настройки
			$destPath = Yii::app()->getBasePath().'/../'.Yii::app()->params['uploadDirectory'].'/documents/';
			$destName = '';
				
			if (!$upload->uploaded) return false;
			$upload->file_new_name_body = $newName;                     
			$upload->process($destPath);
					
			if (!$upload->processed) return false;
			$destName = $upload->file_dst_name;
					
			$model = new UserDocuments;
			$model->user_id = $user_id;
			$model->filename = $newName.'.'.$upload->file_dst_name_ext;
			$model->filesize = @filesize($destPath.$model->filename);
			$model->filetitle= $newName;
			$model->uploaded = time();
			$model->save();
			
			$this->logAction($model);
					
			continue;
			
			if (++$count > self::DOCUMENTS_FILES_MAX) break;
	    }
	    
	    return true;

	}
	
	public function actionAjaxFinances($id) {
		$id = (int)$id;
		if (!Yii::app()->request->isAjaxRequest) throw new CHttpException(400);
		if (!Yii::app()->user->checkAccess('user/update', 'self')) throw new CHttpException(403); // TODO: изменить права
		if (!($model = UserMain::model()->findByPk($id))) throw new CHttpException(404);
		
		if (isset($_POST['ids'])) {
			$idSelector = array();
			foreach ($_POST['ids'] as $currentId) array_push($idSelector,(int)$currentId);
		} else {
			$idSelector = null;
		}
		$income = $model->getFinanceIncomeData($idSelector)->toArray();
		$costs = array('costs' => 0);
		if ($modelCosts = $model->getCostsObject($idSelector)) $costs['costs'] = $modelCosts->summaryCostsValue;
		echo CJSON::encode(array_merge($income,$costs));
		Yii::app()->end();
	}
	
}

?>