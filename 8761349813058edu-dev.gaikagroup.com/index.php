<?php

date_default_timezone_set('Europe/Moscow');

// change the following paths if necessary
$yii=dirname(__FILE__).'/yii-1.1.15.022a51/framework/yii.php';

if ($_SERVER['HTTP_HOST'] == 'edu.org') {
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	$config=dirname(__FILE__).'/protected/config/localhost.php';
} elseif ($_SERVER['HTTP_HOST'] == 'edu-dev.gaikagroup.com') {
	define('YII_DEBUG',true);
	$config=dirname(__FILE__).'/protected/config/dev-server.php';
}  elseif ($_SERVER['HTTP_HOST'] == 'edu-blank.gaikagroup.com') {
	define('YII_DEBUG',true);
	$config=dirname(__FILE__).'/protected/config/dev-blank-server.php';
} else {
	define('YII_DEBUG',true);
	$config=dirname(__FILE__).'/protected/config/production-server.php';
}

// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();
